# CHANGELOG

## UNRELEASED
### Added
### Changed
### Fixed
### Removed
## [1.0.0] - 2022-11-15 Create an opensource version called LightOM2M
### Changed
- add licence
## [0.10.2] - 2022-04-07 Tuned docker-compose files and fixed missing macro
### Changed
- [docker] Changed local docker-compose files for local tests
### Fixed
- [MQTTS] Fixed missing macro in two files for MQTTS_BINDING

## [0.10.1] - 2021-02-26 Fixed year in license
### Changed
- [LICENSE] Updated year in license

## [0.10.0] - 2021-02-26 Node-RED nodes & fixes
### Added
- [CONFIG] Added possibility to dynamically enable registration for all originators or not (LOM2M_ENABLE_REGISTRATION_ALL)
- [CI/CD] Added job in CI to build node-RED docker image
- [DEMO] Added use case for Access Rights check
- [Node-RED] Node-red Nodes first patch
- [Node-RED] ACP CREATE/UPDATE and RETRIEVE ACP by name
- [Node-RED] AE CREATE/UPDATE and RETRIEVE AE by name
- [Node-RED] Discovery node
- [Node-RED] Added Subscription Node
### Fixed
- [LOGS] Fixed typo in mqtt binding logs
- [CSE] Fixed issue with descendent cses update (registrar CSE was not discriminated properly)

## [0.9.1] - 2021-02-19 Bug fix
### Fixed
- [HTTP] Fix bug #177 (issue in REST HTTP CLIENT with response header RSC empty)

## [0.9.0] - 2021-02-16 Improvements and fixes
### ADDED
- [DOC] Added activity diagrams to describe MQTT binding behavior
- [DOC] Added Sequence Diagrams in doc / UML / Sequence (HTTP binding, MQTT binding)
- [DOC] Added out folder (plant uml) to gitignore
- [DOC] Added Archimate documentation (updated gitignore to avoid commit of bakcup file)
- [CSE] Added management of Descendant CSEs attribute (maintained updated in registrar CSE)
- [ACP] Added global ORIGINATOR variable to propagate the information through recursive mapping
### Changed
- [JSON] Changed gateway file name to MainController
- [JSON] Changed JsonMapper filename to JsonDatamapper
- [JSON] Changed datamapping default policy (to include UPDATE body request generation)
- [HTTP] Changed default content type header value if empty
- [HTTP] Changed default request identifier header value if empty
- [JSON] Changed mapping process to take into account Access Rights
### Fixed
- [HTTP] Fixed bug #171 HTTP REST Client in default values with query Strings
- [HTTP] Fixed bug #175 issue with LVL parameter not taken into account anymore
- [ACP] Fixed #74 ACP check recursively (linked to RCN feature)

## [0.8.1] - 2021-01-28 Minor fixes
### Added
- oneM2M requests collection (postman) for test purpose (in tests repository)
### Changed
- Improved dockerfiles and docker-compose files with default versions as ARG
- Updated Alpine and gcc docker images to latest release
- Updated JMeter version to latest release
### Fixed
- Fixed bug #165 (issue with /la /ol and empty CNT)

## [0.8.0] — 2021-01-18 Stability and MQTTS
- NEW FEATURE: MQTTS protocol binding support
- Plugtest fixes
### Added
- Topic parsing and validation (MqttTopic class)
- ProcessRequest: generic request primitive process (rework gateway)
- Added mqttTopic (string) in response and request primitive
- Request buffer to stack incoming requests while waiting for an answer (MQTTS only)
- Added filter Criteria Struct to gather filter criterias (e.g. used in discovery)
- Added support for limit criteria
- Added possibility to dynamically disable MQTTS (#104)
- Added new docker compose file for MQTTS usage
- Added ENV VAR configuration for MQTTS ENDPOINT
- Added ENV VAR configuration for MQTTS PORT
- Added ENV VAR configuration for MQTTS RETRY PERIOD
- Added ENV VAR configuration for MQTTS TIMEOUT
- Added ENV VAR configuration for MQTTS CLIENT ID PREFIX
- Added ENV VAR configuration for global resource number threshold
- Added ENV VAR configuration to enable or disable backup feature
- Added ENV VAR configuration to enable or disable HTTP BINDING feature
- Implemented first version of UPDATE operation (no attribute check yet) for ACP, AE, CNT, CSR, SUB
- Added ENV VAR configuration for default protocol to use (http/mqtts) LOM2M_DEFAULT_PROTOCOL
- Added ENV VAR to configure the dynamically CSE NAME: LOM2M_CSE_NAME
- Added ENV VAR to configure the dynamically CSE ID: LOM2M_CSE_ID
- Added Support for CSE-RELATIVE ADDRESSING
- Added discrimination between SP REL, CSE REL, and ABSOLUTE URIs
- Added other response status codes (HTTP) 4008/408 and 5207/406
- Added support for MAX BYTE SIZE attribute in CNT
- Added support for STATE TAG attribute in CNT and CIN
- Added support for the "all" originator value in Access Control Rule
- Added support of OPTIONS request: responds with the allowed headers and methods
- Added rejection (NOT IMPLEMENTED) of not supported filter criterias
- Added bad request rejection if value is not compliant in discovery and any requests (HTTP ARGS) (#142)
- Added management for RCN 2 and 3 (URI & URI + Attributes)
- Added request rejection for unauthorized values regarding OPERATION
- Added Support for CREATOR attribute in CNT resource (#146)
- Added support for Result Content 9 (modified attributes) (#148) with ACP, AE, CNT, SUB, CSR

### Changed
- Improved genericity of LOM2M core in gateway
- Major changes in HTTP binding and request process management
- Updated CSE Base resource to include MQTT point of access if activated
- WIP update of discovery procedure to take into account filter criteria structure
- WIP update of request sending and handling (HTTP binding) to include Filter Criteria update
- Updated RestHandler filename to HttBinding
- Improved Labels management in discovery with filter criteria when several labels are specified in HTTP URL
- Improved discovery with several labels: now returns the AND result with all the labels
- Improved method names (primitives)
- Updated Gitlab yml jobs name for clarity sake
- Updated JsonMapper to enable update of lists
- Changed: now accept application/json and other formats containing json
- Implemented management of RCN 1 or 0 in POST and DELETE
- Updated Discovery to support multiple ty filter criteria
- Improved notification process to handle notifications in case of child resource deletion and resource deletion

### Removed
- Old dockerfiles in docker folder
- Removed useless configuration for persistance filename
- Removed useless lines of code in init report (request primitive)

### Fixed
- Missing response status code when TCP is closed (rest client)
- Missing dependency install for NTPClient (#109)
- Missing content type header in HTTP with discovery (#110)
- Fixed #131 issue with URL and QS generation in REST CLIENT HTTP
- Fixed issue in CI/CD with docker run LOM2M
- Fixed issue in generic attribute parser (labels)
- Fixed #137 issue with CSE registration in HTTP
- Fixed #136 issue with Request redireciton (payload was lost)
- Fixed error with headers (HTTP CLIENT ESP8266 is case sensitive, implemented a workaround.) (#138)
- Fixed issue with no HTTP response when target does not start with "/~/"
- Fixed issues with CSE Registration (wrong originator, missing attributes)
- Fixed bug in response status code when DELETE
- Fixed bugs in Json Mapper for CSR and ACPI attribute (empty lists should not be serialized)
- Fixed bug: check of already existing resource with same name was not implemented properly
- Fixed BUG with /la /ol using unstructured URI and CSE rel, workaround though, should be improved
- Fixed issue with DISCOVERY when the target entity is not CSE Base, the result should mention only children of the target
- Fixed issue with DISCOVERY access rights check
- Fixed small typos in CSE BASE resource
- Fixed issue where la / ol was accepted as resource name
- Fixed issue in CNT creation: if mni is not positive integer request shall be rejected
- Fixed several minor issues in CNT and CIN serialization
- Fixed SUBSCRIPTION CREATION procedure to take into account the different URI formats in NU
- Fixed missing check of validity of MBS attribute in CNT CREATION
- Fixed issue with Query String headers: fu header was lost in retargetting
- Fixed URI format in resource attributes and discovery (CSE-RELATIVE)
- Fixed BUG #145 ACPI list was not purged before update
- Fixed issue with RCN 0 (NOTHING) #147
- Fixed issue #134 (CSE registration MQTTS)
- Fixed issue #149 (issue in resource serialization)
- Fixed issue #141 (SUB creation with AE ID in "nu")
- Fixed issue #150 (issue with notification sending in MQTT)
- Fixed issue #151 (attribute "net" not serialized properly)
- Fixed issue #152 (issue with notification management and MQTT)
- Fixed issue #154 issue with time serialization
- Fixed issue #158 (serialization of SUB after UPDATE)

## [0.7.6] - 2020-10-26 Minor improvements
### Changed
- Minor update in Dockerfile (removed useless package)
- Update in build for mangOH to include env var (TOOLCHAIN)
- Updated behavior with default max number of instances in container

## [0.7.5] - 2020-09-28 Improve CI and bug fix
### Added
- Test to check the data backup feature in CI/CD
### Fixed
- Issue with data backup (#122)

## [0.7.4] - 2020-09-16 Improve CI
### Added
- Needs in jobs for optimization of CI/CD
### Fixed
- Fixed issues with code quality intergration in CI (#54)
- Fixed names for latest builds in CI

## [0.7.3] - 2020-09-15 BUG FIX
### Fixed
- Fixed build issue when PERSISTENCE FEATURE is disabled (#115)
- Fixed build issue with PERSISTENCE backup period constant (#119)

### Removed
- Deprecated code and not used anymore methods (Filter criteria update, etc.)

## [0.7.2] - 2020-09-07
### Added
- Added version control for librairies and dependencies

## [0.7.1] - 2020-09-01 Bug Fix
- Included version specification for ESP8266 Arduino in LOM2M Dockerfile and ESP builder Dockerfile.

## [0.7.0] - 2020-07-10 DATA BACKUP FEAT.
### Added
- Persistence feature: enable to persist and load all data periodically
- Added higher nesting limit just for persistence loading in order to parse everything stored in the json file
- Added resource CREATION or DELETION detection to trigger persistence
- Feature configuration: possibility to enable or disable persistence from configuration (enabled by default)
- Added thresholds for max number of created resources due to memory limitations on ESP (this will not affect UNIX base targets)
- Added ENV var LOM2M_BACKUP_PERIOD to dynamically configure the backup period
- Added ssl build in Dockerfile (necessary for unix target)

### Changed
- Updated resource id parsing: avoid duplication in memory
- Changed IPE initialization: retrieve existing resource before creating it (nedded if data is loaded from persistence at launch)
- Updated size of global Json document for persistence use
- Updated labels for Pilot Things
- Updated Global Json document size to fit ESP
- Updated registration procedure to avoid duplication of local remote CSE resource with persistance
- Updated configuration of docker-compose for petitparis deployment

### Removed
- JsonGlobal input global variable: useless, now merged with jsonGlobal output into jsonGlobal
- Patched gitlab yml for deployment on petitparis ()

## [0.6.3] - 2020-07-09
### Changed
- Optimised docker image for LOM2M (size reduced using Alpine)

## [0.6.2] - 2020-07-02
### Added
- Test build for ESP target in CI

## [0.6.1] - 2020-06-29
### Fixed
- Missing dependency to Arduino causing issues for ESP build

## [0.6.0] - 2020-05-18
### Added
- Added Subscription / notification feature dynamic feature
### Changed
- Updated notification handler to include notification from AE RI
- Update subscription controller to handle SUB creation with AE RI
- Move methods from Gateway to Notify for code clarity

## [0.5.2] - 2020-04-22
### Changed
- Updated dockerfiles names and updated build and compose accordingly

### Removed
- Configuration file for SSID (WiFi) -> integrated in global configuration

## [0.5.1] - 2020-04-20
### Removed
- Removed now useless build petitparis specific (included with ENV variables in 0.5.0)

## [0.5.0] - 2020-04-20
### Added
- Default docker-compose file and specific petitparis file

### Changed
- Optimized LOM2M dockerfile
- Updated configuration policy using environment variables on UNIX target system
- Updated LOM2M docker compose files to run LOM2M from docker registry image

## [0.4.1] - 2020-04-17
### Changed
- Patched gitlab yml for deployment on petitparis (deactivated)
- Improved LOM2M dockerfile for smaller image and updated default configuration
- Updated gitlab yml due to gitlab latest update, docker login was not working

## [0.4.0] - 2020-04-09
### Added
- First version of feature enablement: GROUP RESOURCE
- First version of feature enablement: LVL & RCN (4) parameters
- Default target configuration set to ESP
- Automatic update of the deployment on petitparis after a patch on master
### Changed
- Updated global configuration management and features enablement
- Updated dockerfiles to include target modification
- Increased size of json global for unix based target

## [0.3.4] - 2020-04-02
### Changed
- Updated registration procedure, added resource deletion if previously registered.

### Fixed
- Fixed a bug in AE mapper: APN parameter was missing

### Removed
- Removed input json document to use less memory (merged with global json doc)

## [0.3.3] - 2020-03-19
### Fixed
- Fixed #90 target entity was not checked in case of /la or /ol

## [0.3.2] - 2020-03-09
### Changed
- Updated AE and CNT labels for Pilot Things

## [0.3.1] - 2020-02-25
### Fixed
- Fixed year in license headers
- Fixed bug in discovery label management (#83)

## [0.3.0] - 2020-02-24
### Added
- added global parameter VERBOSE: verbose answers (HTTP) for error management
- Controller classes: aim is to have each specific resource management separated
- Added abstract controller to map to other controllers
- Added documentation on mappers
- Added check of NP parameters at resource creation
### Changed
- Updated Group mapper and parser with correct management of array
- Refractor of Gateway.cpp: Mappers now parse resources from JSON
- Improved code clarity of Gateway.cpp by moving code to controller
- Improved memory management in resource creation if resource is incorrect
- Moved validity check of resource in controller, mapper only maps all attributes
- Changed operation verification in Gateway
- Changed discovery parameters management in Gateway (generic way)
- Updated source code documentation
- Updated doxygen documentation
- Updated resourceID management to enable persistence and data loading
### Fixed
- Fixed a bug on Group parsing that created issues on other resources (linked to #43)
- Fixed bug (#78) 'from' parameter issue on response

## [0.2.2] - 2020-02-12
### Added
- change log file
- skip tests (skip-tests) and skip ci (skip-ci) in commit message
- json mapper used to serialize and deserialize resources
- support of ?rcn=4
- support of ?lvl paremeter (query string in HTTP)
### Changed
- Refractor of Create Report (request primitive)
- Minor update in CI/CD for better management of docker images
- Doubled size of JSON output buffer (needed for child resources serialization)
### Fixed
- Fixed typo in test of RCN value (#72)
- Fixed bug on serialization of child resources (#77)

## [0.2.1] - 2020-02-04
### Changed
- Improved global CI / CD: release job now tags version of build if present

## [0.2.0] - 2020-02-04
### Added
- Added configuration of CI/CD and integration of functional test cases (#53)
- Added feature Request Redirection (#22)
- Added feature Verification Request for Subscription Creation (#45)
- Added Software documentation (doxygen doc) support (#46)
### Changed
- Global improvements (memory, logs, enum missing values)
- Improved discovery (issue #56)
- Improved serialization (sub resource, #60)
### Fixed
- Fixed issus on headers (#50)
- Fixed issues on ACP (#55)
- Fixed other bugs (#51, #52, #57, #59)

## [0.1.3-M2] - 2019-12-02
### Added
- Added Configuration for code building and ESP flash on Windows & WSL
### Changed
- Improved subscription / notification process (#44)
- Improved serialization (#35, #37)
### Fixed
- Fixed bugs (#38, #42) on ACP
- Fixed bug on discovery (#40)

## [0.1.3-M1] - 2019-11-27
### Added
- added support of Access Control Policy resource
- added access control check in API
### Changed
- updated resources to include link to ACP_ADMIN by default

## [0.1.2-ARCHIVE] - 2019-11-18
- archived version (APP with TTT)
### Fixed
- fixed minor issues in licences

## [0.1.2] - 2019-11-10
### Fixed
- Fixed #34, Parent ID is generated, reference not working properly.

## [0.1.1-ETSI-2019] - 2019-10-20
- First stable version for Demo at ETSI
### Changed
- updated configuration for admin originator

## [0.1.0-ETSI-2019] - 2019-10-18
### Fixed
- fixed minor issue for MangOH red.
