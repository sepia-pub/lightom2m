# Light OM2M : an efficient and oneM2M-compliant middleware for interoperable IoT applications

## oneM2M

oneM2 is an IoT standard which helps build interoperable IoT solutions.  This requires that oneM2M-compatible software runs on various layers of the system. However today’s implementations are not optimized to run efficiently on resource-constrained devices, such as a low-cost gateway.

## Light-OM2M

Light-OM2M is an implementation which lets you benefit from oneM2M standard services on resource-constrained devices. 
It builds upon years of experience and fedbacks acquired by the same project manager and developers team of Eclipse-OM2M, one of the oneM2M reference open-source implementation.
Light-OM2M is supporting oneM2M services : 

- Resources: ACP(1), AE(2), CNT(3), CIN(4), CSB(5), CSR(16), SUB(24)
- Operations: CREATE, RETRIEVE, DELETE, SUBSCRIBE, DISCOVER
- Features:
1. Communication protocols (HTTP & MQTT - restricted version)
2. Security: access rights management - restricted version
3. Subscriptions management
4. Discovery (filter criteria: level, resource type, labels, limit)

## Some characteristics
- language: C++
- Operating system: Arduino (ESP8266), Unix based system with docker
- RAM footprint: around 60KB on ESP8266 and 3MB on UNIX system
- Binaries footprint: around 500KB ROM on ESP8266 and 5MB on UNIX system





