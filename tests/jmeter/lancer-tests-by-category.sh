#! /bin/sh                                                                                                                                                                              

cd $JMETER_HOME/bin

# if [ $# = 0 ]
# then
#     echo "This script needs 1 parameter to be executed, the id of the test\n"
#     echo "Command : lancer-test-unique.sh <idTest>\n"
#     echo "For example : lancer-test-unique.sh RET-13\n"
# else

#     #Configuration du test
#     idTest=$1
#     testFile=$JMETER_HOME/Test_plans/${idTest}.jmx

#     #Création du répertoire pour mettre le rapport et le fichier de log (.csv) du test
#     reportsDirectory=$JMETER_HOME/Reports
#     testsuiteDirectory=${idTest}"_"$(date +"%Y-%m-%d|%H:%M")
#     echo "Name of the directory for the whole testsuite reports : \n"$testsuiteDirectory

#     mkdir ${reportsDirectory}/${testsuiteDirectory}


#     ######################################################################################################################################################
#     # Test avec un nombre de clients fixe : 10 clients
#     ######################################################################################################################################################

#     nb_clients=10; test_duration=60; rampup_period=0; 

#     for throughput in 6.0 12.0 30.0 60.0 120.0 240.0
#     do
#         #Création du fichier de log (.csv) et du dossier pour le report
#         logFileName="logs_test_"${nb_clients}"-clients_"${throughput}"-req-per-min.csv"
#         reportName="report_"${nb_clients}"-clients_"${throughput}"-req-per-min"
#         reportPath=${reportsDirectory}/${testsuiteDirectory}/${reportName}

#         #Lancement du test
#         ./jmeter -n -t ${testFile} -l ${logFileName} \
#         -Jnb_clients=${nb_clients} -Jtest_duration=${test_duration} -Jrampup_period=${rampup_period} -Jthroughput=${throughput} \
#         -e -o ${reportPath} 

#         if [ ! -e ${reportPath}/${logFileName} ]
#         then
#             mv ${logFileName} ${reportPath}
#         else
#             echo "The log file already exists in this directory."
#         fi
#     done


#     ######################################################################################################################################################
#     # Test avec un débit total fixe : 5 req/sec
#     ######################################################################################################################################################

#     ################################################## 
#     test_duration=120; rampup_period=0;
#     throughput=30.0; #valeur par défaut

#     for nb_clients in 1 2 5 10 20 30 40 50
#     do
#         throughput=$((300/${nb_clients}))

#         #Création du fichier de log (.csv) et du dossier pour le report
#         logFileName="logs_test_"${nb_clients}"-clients_"${throughput}"-req-per-min.csv"
#         reportName="report_"${nb_clients}"-clients_"${throughput}"-req-per-min"
#         reportPath=${reportsDirectory}/${testsuiteDirectory}/${reportName}

#         #Lancement du test
#         ./jmeter -n -t ${testFile} -l ${logFileName} \
#         -Jnb_clients=${nb_clients} -Jtest_duration=${test_duration} -Jrampup_period=${rampup_period} -Jthroughput=${throughput} \
#         -e -o ${reportPath} 

#         if [ ! -e ${reportPath}/${logFileName} ]
#         then
#             mv ${logFileName} ${reportPath}
#         else
#             echo "The log file already exists in this directory."
#         fi
#     done
   

# fi

for idTest in "CRE-AE-1" "CRE-AE-3" "CRE-AE-4" "CRE-AE-5" "CRE-AE-6"
do
    testFile=$JMETER_HOME/Test_plans/${idTest}.jmx
    #Lancement du test
    ./jmeter -n -t ${testFile} -l logfile-tests-CRE-AE.csv

done

for idTest in "CRE-CNT-1" "CRE-CNT-3" "CRE-CNT-4" "CRE-CNT-5" "CRE-CNT-6" "CRE-CNT-7" "CRE-CNT-8"
do
    testFile=$JMETER_HOME/Test_plans/${idTest}.jmx
    #Lancement du test
    ./jmeter -n -t ${testFile} -l logfile-tests-CRE-CNT.csv

done

for idTest in "CRE-CIN-1" "CRE-CIN-3" "CRE-CIN-4" "CRE-CIN-5" "CRE-CIN-6" "CRE-CIN-10"
do
    testFile=$JMETER_HOME/Test_plans/${idTest}.jmx
    #Lancement du test
    ./jmeter -n -t ${testFile} -l logfile-tests-CRE-CIN.csv

done

for idTest in "RET-13" "RET-13bis" "RET-15" "RET-16"
do
    testFile=$JMETER_HOME/Test_plans/${idTest}.jmx
    #Lancement du test
    ./jmeter -n -t ${testFile} -l logfile-tests-RET.csv

done
