var indexSectionsWithContent =
{
  0: "_abcdefghijklmnopqrstuv~",
  1: "abcdefhimnoprs",
  2: "abceghijklmnoprst",
  3: "_abcdefghijlmnoprstuv~",
  4: "abcdefghijlmnopqrstuv",
  5: "cmpu",
  6: "acefhnoru",
  7: "acefhnoru",
  8: "acdfghijmprstv"
};

var indexSectionNames =
{
  0: "all",
  1: "classes",
  2: "files",
  3: "functions",
  4: "variables",
  5: "typedefs",
  6: "enums",
  7: "enumvalues",
  8: "defines"
};

var indexSectionLabels =
{
  0: "All",
  1: "Classes",
  2: "Files",
  3: "Functions",
  4: "Variables",
  5: "Typedefs",
  6: "Enumerations",
  7: "Enumerator",
  8: "Macros"
};

