var searchData=
[
  ['r000_5fvoid_833',['R000_VOID',['../_response_primitive_8h.html#a84b382c64aaa1f5f8d0cbfa85ce34fbcae4eac3daabc802eead03fe324f2b56fd',1,'ResponsePrimitive.h']]],
  ['r2000_5fok_834',['R2000_OK',['../_response_primitive_8h.html#a84b382c64aaa1f5f8d0cbfa85ce34fbcade208a01dd84f6767edf765e9fca66f4',1,'ResponsePrimitive.h']]],
  ['r2001_5fcreated_835',['R2001_CREATED',['../_response_primitive_8h.html#a84b382c64aaa1f5f8d0cbfa85ce34fbca805e4eab1e3eeb18a6decefed12116b0',1,'ResponsePrimitive.h']]],
  ['r2002_5fdeleted_836',['R2002_DELETED',['../_response_primitive_8h.html#a84b382c64aaa1f5f8d0cbfa85ce34fbca040c830bc5ae79ac8fa2903826449637',1,'ResponsePrimitive.h']]],
  ['r2004_5fupdated_837',['R2004_UPDATED',['../_response_primitive_8h.html#a84b382c64aaa1f5f8d0cbfa85ce34fbcaea097fe46ef6987dd7a6bb72f704dadd',1,'ResponsePrimitive.h']]],
  ['r204_5fno_5fcontent_838',['R204_NO_CONTENT',['../_response_primitive_8h.html#a84b382c64aaa1f5f8d0cbfa85ce34fbca55c20365df027a73ca90dc8448b0c934',1,'ResponsePrimitive.h']]],
  ['r4000_5fbad_5frequest_839',['R4000_BAD_REQUEST',['../_response_primitive_8h.html#a84b382c64aaa1f5f8d0cbfa85ce34fbca9f742bcf818aac27a7ef4cda3cec08d8',1,'ResponsePrimitive.h']]],
  ['r4004_5fnot_5ffound_840',['R4004_NOT_FOUND',['../_response_primitive_8h.html#a84b382c64aaa1f5f8d0cbfa85ce34fbca0bede87563c028b13450f1538b16e637',1,'ResponsePrimitive.h']]],
  ['r4005_5foperation_5fnot_5fallowed_841',['R4005_OPERATION_NOT_ALLOWED',['../_response_primitive_8h.html#a84b382c64aaa1f5f8d0cbfa85ce34fbca398226089159fafe8922c1421057df74',1,'ResponsePrimitive.h']]],
  ['r4008_5frequest_5ftimeout_842',['R4008_REQUEST_TIMEOUT',['../_response_primitive_8h.html#a84b382c64aaa1f5f8d0cbfa85ce34fbca271356f1e3366510605ef99aecf95b2c',1,'ResponsePrimitive.h']]],
  ['r4015_5funsupported_5fmedia_5ftype_843',['R4015_UNSUPPORTED_MEDIA_TYPE',['../_response_primitive_8h.html#a84b382c64aaa1f5f8d0cbfa85ce34fbcab3e3e9ef241134cffd48303e330001dd',1,'ResponsePrimitive.h']]],
  ['r4103_5foriginator_5fhas_5fno_5fprivilege_844',['R4103_ORIGINATOR_HAS_NO_PRIVILEGE',['../_response_primitive_8h.html#a84b382c64aaa1f5f8d0cbfa85ce34fbcafe31d59df81c137aad0ae73e7488ab9e',1,'ResponsePrimitive.h']]],
  ['r4105_5fconflict_845',['R4105_CONFLICT',['../_response_primitive_8h.html#a84b382c64aaa1f5f8d0cbfa85ce34fbca6b1b3119d26d69a12d6a1e49a04af024',1,'ResponsePrimitive.h']]],
  ['r5000_5finternal_5fserver_5ferror_846',['R5000_INTERNAL_SERVER_ERROR',['../_response_primitive_8h.html#a84b382c64aaa1f5f8d0cbfa85ce34fbca7bfc7a426d5e9cbdb37cb7f2673a9473',1,'ResponsePrimitive.h']]],
  ['r5001_5fnot_5fimplemented_847',['R5001_NOT_IMPLEMENTED',['../_response_primitive_8h.html#a84b382c64aaa1f5f8d0cbfa85ce34fbcae12236e27d0e1cc7c978f6e5c022c7a6',1,'ResponsePrimitive.h']]],
  ['r5103_5ftarget_5fnot_5freachable_848',['R5103_TARGET_NOT_REACHABLE',['../_response_primitive_8h.html#a84b382c64aaa1f5f8d0cbfa85ce34fbca042a68fc5dc9be7384fb4a664081f486',1,'ResponsePrimitive.h']]],
  ['r5106_5falready_5fexists_849',['R5106_ALREADY_EXISTS',['../_response_primitive_8h.html#a84b382c64aaa1f5f8d0cbfa85ce34fbca09cf075b2698582ae225180628f182e9',1,'ResponsePrimitive.h']]],
  ['r5206_5fnon_5fblocking_5fsynch_5frequest_5fnot_5fsupported_850',['R5206_NON_BLOCKING_SYNCH_REQUEST_NOT_SUPPORTED',['../_response_primitive_8h.html#a84b382c64aaa1f5f8d0cbfa85ce34fbca4168636356b9cf6f16df7dcd75d705e1',1,'ResponsePrimitive.h']]],
  ['r5207_5fnot_5facceptable_851',['R5207_NOT_ACCEPTABLE',['../_response_primitive_8h.html#a84b382c64aaa1f5f8d0cbfa85ce34fbca0e9ce8c01c7e53614685025349aeafe8',1,'ResponsePrimitive.h']]],
  ['rate_5flimit_852',['RATE_LIMIT',['../_short_names_8cpp.html#a9d97814edc18f823a21a2f9001e9b635',1,'RATE_LIMIT():&#160;ShortNames.cpp'],['../_short_names_8h.html#a9d97814edc18f823a21a2f9001e9b635',1,'RATE_LIMIT():&#160;ShortNames.cpp']]],
  ['rcn_5fattr_853',['RCN_ATTR',['../_enum_8h.html#a5d2a50fd0d73e6b3ef0d722454bd0f24a85db8a518210be0bcb5448cee13c625e',1,'Enum.h']]],
  ['rcn_5fattr_5fchref_854',['RCN_ATTR_CHREF',['../_enum_8h.html#a5d2a50fd0d73e6b3ef0d722454bd0f24a124d5f25aab8229ddd5aa3fdfdf46fd0',1,'Enum.h']]],
  ['rcn_5fattr_5fchres_855',['RCN_ATTR_CHRES',['../_enum_8h.html#a5d2a50fd0d73e6b3ef0d722454bd0f24a3cd924b47a3ae05ddff247c099125268',1,'Enum.h']]],
  ['rcn_5fchref_856',['RCN_CHREF',['../_enum_8h.html#a5d2a50fd0d73e6b3ef0d722454bd0f24aeb6daf4dc0e81fc72b29c4b443b2bf67',1,'Enum.h']]],
  ['rcn_5fchres_857',['RCN_CHRES',['../_enum_8h.html#a5d2a50fd0d73e6b3ef0d722454bd0f24ab8ce5ab36c131c8ff3ee735e1b850ded',1,'Enum.h']]],
  ['rcn_5fhier_5faddr_858',['RCN_HIER_ADDR',['../_enum_8h.html#a5d2a50fd0d73e6b3ef0d722454bd0f24afcf01f9b738d7f0ad11aa8b37671546e',1,'Enum.h']]],
  ['rcn_5fhier_5faddr_5fattr_859',['RCN_HIER_ADDR_ATTR',['../_enum_8h.html#a5d2a50fd0d73e6b3ef0d722454bd0f24afd8ae9fdabed748ac91ffd4c446f8054',1,'Enum.h']]],
  ['rcn_5fmodif_5fattr_860',['RCN_MODIF_ATTR',['../_enum_8h.html#a5d2a50fd0d73e6b3ef0d722454bd0f24a6c16b23c4a2a48d3db95f24d49ad61cf',1,'Enum.h']]],
  ['rcn_5fnothing_861',['RCN_NOTHING',['../_enum_8h.html#a5d2a50fd0d73e6b3ef0d722454bd0f24a923d6d0f2206c938db60b3c2e9aacdb9',1,'Enum.h']]],
  ['rcn_5fnull_862',['RCN_NULL',['../_enum_8h.html#a5d2a50fd0d73e6b3ef0d722454bd0f24a03a162ace5850c9fef6303dd9cae09fa',1,'Enum.h']]],
  ['rcn_5foriginal_5fres_863',['RCN_ORIGINAL_RES',['../_enum_8h.html#a5d2a50fd0d73e6b3ef0d722454bd0f24af92c858f9d83a7903e62057f469726d1',1,'Enum.h']]],
  ['rcn_5fsem_5fcontent_864',['RCN_SEM_CONTENT',['../_enum_8h.html#a5d2a50fd0d73e6b3ef0d722454bd0f24af6051f19b6d9372aa9cfd30540427870',1,'Enum.h']]],
  ['reboot_865',['REBOOT',['../_short_names_8cpp.html#a2269054f3596ccfc98c906184269e28d',1,'REBOOT():&#160;ShortNames.cpp'],['../_short_names_8h.html#a2269054f3596ccfc98c906184269e28d',1,'REBOOT():&#160;ShortNames.cpp']]],
  ['reboot_5fannc_866',['REBOOT_ANNC',['../_short_names_8cpp.html#a6ee8e33314d2a539e5caaf8d474a80d5',1,'REBOOT_ANNC():&#160;ShortNames.cpp'],['../_short_names_8h.html#a6ee8e33314d2a539e5caaf8d474a80d5',1,'REBOOT_ANNC():&#160;ShortNames.cpp']]],
  ['redirector_867',['Redirector',['../class_redirector.html',1,'']]],
  ['redirector_2ecpp_868',['Redirector.cpp',['../_redirector_8cpp.html',1,'']]],
  ['redirector_2eh_869',['Redirector.h',['../_redirector_8h.html',1,'']]],
  ['register_5fobserver_870',['register_observer',['../struct_entity.html#a6e5c6864c5e65e0ffdfa82ca6b20892b',1,'Entity']]],
  ['registercse_871',['registerCSE',['../gateway_8cpp.html#a44423641fe52a2bddb1c9b811b1bcad6',1,'registerCSE():&#160;gateway.cpp'],['../gateway_8h.html#a44423641fe52a2bddb1c9b811b1bcad6',1,'registerCSE():&#160;gateway.cpp']]],
  ['registered_872',['registered',['../lom2m-server-base_8h.html#a32f2643fcbe23cdff8e2da2214c6404a',1,'lom2m-server-base.h']]],
  ['rel_5f1_873',['REL_1',['../configuration_8h.html#a01ee714da422793f00c963701ff407b4',1,'REL_1():&#160;configuration.h'],['../_request_primitive_8cpp.html#a01ee714da422793f00c963701ff407b4',1,'REL_1():&#160;RequestPrimitive.cpp']]],
  ['release_5fversion_5findicator_874',['RELEASE_VERSION_INDICATOR',['../_short_names_8h.html#a709d94a22584d58e4cd7f9757a12876f',1,'RELEASE_VERSION_INDICATOR():&#160;ShortNames.cpp'],['../_short_names_8cpp.html#a709d94a22584d58e4cd7f9757a12876f',1,'RELEASE_VERSION_INDICATOR():&#160;ShortNames.cpp']]],
  ['remote_5fcse_875',['REMOTE_CSE',['../_short_names_8h.html#aec15c2e3f1f9712fb94d5b1c8c63c623',1,'REMOTE_CSE():&#160;ShortNames.cpp'],['../_short_names_8cpp.html#aec15c2e3f1f9712fb94d5b1c8c63c623',1,'REMOTE_CSE():&#160;ShortNames.cpp']]],
  ['remote_5fcse_5fcsebase_876',['REMOTE_CSE_CSEBASE',['../_short_names_8h.html#a57b60186bafb3cf7143519c66f96c90c',1,'REMOTE_CSE_CSEBASE():&#160;ShortNames.cpp'],['../_short_names_8cpp.html#a57b60186bafb3cf7143519c66f96c90c',1,'REMOTE_CSE_CSEBASE():&#160;ShortNames.cpp']]],
  ['remote_5fcse_5fid_877',['REMOTE_CSE_ID',['../lom2m_8h.html#afe7ff166bc92526694eef2cb04350830',1,'REMOTE_CSE_ID():&#160;lom2m.h'],['../lom2m_8cpp.html#afe7ff166bc92526694eef2cb04350830',1,'REMOTE_CSE_ID():&#160;lom2m.cpp']]],
  ['remote_5fcse_5fname_878',['REMOTE_CSE_NAME',['../lom2m_8h.html#a0e80cad603338bffe6815856595ed6f5',1,'REMOTE_CSE_NAME():&#160;lom2m.h'],['../lom2m_8cpp.html#a0e80cad603338bffe6815856595ed6f5',1,'REMOTE_CSE_NAME():&#160;lom2m.cpp']]],
  ['remote_5fcse_5fpoa_879',['REMOTE_CSE_POA',['../lom2m_8h.html#ad2dfe01f22bd8c063065cea0f8c68c3e',1,'REMOTE_CSE_POA():&#160;lom2m.h'],['../lom2m_8cpp.html#ad2dfe01f22bd8c063065cea0f8c68c3e',1,'REMOTE_CSE_POA():&#160;lom2m.cpp']]],
  ['remote_5fcse_5ftype_880',['REMOTE_CSE_TYPE',['../lom2m_8h.html#a64f197cbc2dfe4e5304fd441550aea8d',1,'REMOTE_CSE_TYPE():&#160;lom2m.h'],['../lom2m_8cpp.html#a64f197cbc2dfe4e5304fd441550aea8d',1,'REMOTE_CSE_TYPE():&#160;lom2m.cpp']]],
  ['remotecse_881',['RemoteCse',['../struct_remote_cse.html#a9b19fe8560204e3420ea5e97ab8a2a06',1,'RemoteCse::RemoteCse()'],['../struct_remote_cse.html',1,'RemoteCse']]],
  ['remotecsemapper_882',['RemoteCseMapper',['../class_remote_cse_mapper.html',1,'']]],
  ['remove_883',['remove',['../struct_entity.html#a1230e094d1a103a5df0963dd0995c317',1,'Entity']]],
  ['removenotification_884',['removeNotification',['../class_notify_buffer.html#ac1547a8d44782cdfcef02eb972132d2d',1,'NotifyBuffer']]],
  ['representation_885',['REPRESENTATION',['../_short_names_8h.html#add29787620745778d976ee1dffcb900c',1,'REPRESENTATION():&#160;ShortNames.cpp'],['../_short_names_8cpp.html#add29787620745778d976ee1dffcb900c',1,'REPRESENTATION():&#160;ShortNames.cpp']]],
  ['req_886',['REQ',['../_short_names_8h.html#a13e829416f948ed179c7edbb9babce89',1,'REQ():&#160;ShortNames.cpp'],['../_short_names_8cpp.html#a13e829416f948ed179c7edbb9babce89',1,'REQ():&#160;ShortNames.cpp']]],
  ['req_5flvl_5ffeat_887',['REQ_LVL_FEAT',['../configuration_8h.html#a51ef088231d9d63b98dea8eb3b11a165',1,'configuration.h']]],
  ['request_5fcontent_888',['REQUEST_CONTENT',['../_short_names_8h.html#a23bbccbb68389e01810ea838ced5c3ca',1,'ShortNames.h']]],
  ['request_5fexpiration_5ftimestamp_889',['REQUEST_EXPIRATION_TIMESTAMP',['../_short_names_8cpp.html#afb20cfa0d35f53e5197e8630547f1765',1,'REQUEST_EXPIRATION_TIMESTAMP():&#160;ShortNames.cpp'],['../_short_names_8h.html#afb20cfa0d35f53e5197e8630547f1765',1,'REQUEST_EXPIRATION_TIMESTAMP():&#160;ShortNames.cpp']]],
  ['request_5fid_890',['REQUEST_ID',['../_short_names_8h.html#a697ad9db11a16c8696dcd82354eb2f72',1,'REQUEST_ID():&#160;ShortNames.cpp'],['../_short_names_8cpp.html#a697ad9db11a16c8696dcd82354eb2f72',1,'REQUEST_ID():&#160;ShortNames.cpp']]],
  ['request_5fidentifier_891',['REQUEST_IDENTIFIER',['../_short_names_8h.html#a678142cc6d8a393348fd108d319c9a7e',1,'REQUEST_IDENTIFIER():&#160;ShortNames.cpp'],['../_short_names_8cpp.html#a678142cc6d8a393348fd108d319c9a7e',1,'REQUEST_IDENTIFIER():&#160;ShortNames.cpp']]],
  ['request_5foperation_892',['REQUEST_OPERATION',['../_short_names_8h.html#a3a6f03dc230777dcd40189d3b38a1ac0',1,'REQUEST_OPERATION():&#160;ShortNames.cpp'],['../_short_names_8cpp.html#a3a6f03dc230777dcd40189d3b38a1ac0',1,'REQUEST_OPERATION():&#160;ShortNames.cpp']]],
  ['request_5fprimitive_893',['REQUEST_PRIMITIVE',['../_short_names_8h.html#a69a1298f3a3862e262a8949754d576f7',1,'REQUEST_PRIMITIVE():&#160;ShortNames.cpp'],['../_short_names_8cpp.html#a69a1298f3a3862e262a8949754d576f7',1,'REQUEST_PRIMITIVE():&#160;ShortNames.cpp']]],
  ['request_5freachability_894',['REQUEST_REACHABILITY',['../_short_names_8h.html#a690b5967900e7391c9c5a43edc6f00ca',1,'REQUEST_REACHABILITY():&#160;ShortNames.cpp'],['../_short_names_8cpp.html#a690b5967900e7391c9c5a43edc6f00ca',1,'REQUEST_REACHABILITY():&#160;ShortNames.cpp']]],
  ['request_5fstatus_895',['REQUEST_STATUS',['../_short_names_8h.html#adf013de087e1f18d2d8edcff64d5c812',1,'REQUEST_STATUS():&#160;ShortNames.cpp'],['../_short_names_8cpp.html#adf013de087e1f18d2d8edcff64d5c812',1,'REQUEST_STATUS():&#160;ShortNames.cpp']]],
  ['request_5ftimestamp_896',['REQUEST_TIMESTAMP',['../_short_names_8h.html#a00740d16df213c5d7d93d6f2760e402e',1,'REQUEST_TIMESTAMP():&#160;ShortNames.cpp'],['../_short_names_8cpp.html#a00740d16df213c5d7d93d6f2760e402e',1,'REQUEST_TIMESTAMP():&#160;ShortNames.cpp']]],
  ['requestprimitive_897',['RequestPrimitive',['../class_request_primitive.html',1,'RequestPrimitive'],['../class_request_primitive.html#ae6c5bdd9f0551873c9c57bd507acaaec',1,'RequestPrimitive::RequestPrimitive()']]],
  ['requestprimitive_2ecpp_898',['RequestPrimitive.cpp',['../_request_primitive_8cpp.html',1,'']]],
  ['requestprimitive_2eh_899',['RequestPrimitive.h',['../_request_primitive_8h.html',1,'']]],
  ['requestprimitivebuffer_900',['requestPrimitiveBuffer',['../struct_m_q_t_t_buffer.html#afd337318f0f218e0478011892fc0d866',1,'MQTTBuffer']]],
  ['requestreachability_901',['requestReachability',['../struct_remote_cse.html#afa5c58bf9a73c41251cae02b29e9fdde',1,'RemoteCse']]],
  ['requestreply_902',['requestReply',['../gateway_8cpp.html#a58daca2bb62ad69c35d4dd4848cb830f',1,'gateway.cpp']]],
  ['requesttosend_903',['requestToSend',['../_notify_8cpp.html#aadc7bcea9adbb7c0480c1bd2937d70da',1,'Notify.cpp']]],
  ['resource_904',['resource',['../struct_notification.html#a5ad3f34ba1feb8bda8666d849b9998a2',1,'Notification']]],
  ['resource_5fid_905',['RESOURCE_ID',['../_short_names_8h.html#aa6b49a57764bb21a9f686b0f90ef5f0e',1,'RESOURCE_ID():&#160;ShortNames.cpp'],['../_short_names_8cpp.html#aa6b49a57764bb21a9f686b0f90ef5f0e',1,'RESOURCE_ID():&#160;ShortNames.cpp']]],
  ['resource_5fname_906',['RESOURCE_NAME',['../_short_names_8cpp.html#a16212560dbd2b4a4a0e8c5ddb4ccf3fb',1,'RESOURCE_NAME():&#160;ShortNames.cpp'],['../_short_names_8h.html#a16212560dbd2b4a4a0e8c5ddb4ccf3fb',1,'RESOURCE_NAME():&#160;ShortNames.cpp']]],
  ['resource_5fstatus_907',['RESOURCE_STATUS',['../_short_names_8cpp.html#a958d581785b7b07bcb8efb671bbe181c',1,'RESOURCE_STATUS():&#160;ShortNames.cpp'],['../_short_names_8h.html#a958d581785b7b07bcb8efb671bbe181c',1,'RESOURCE_STATUS():&#160;ShortNames.cpp']]],
  ['resource_5ftype_908',['RESOURCE_TYPE',['../_short_names_8cpp.html#a5c02944e432e1ae19c39ff094e14429c',1,'RESOURCE_TYPE():&#160;ShortNames.cpp'],['../_short_names_8h.html#a5c02944e432e1ae19c39ff094e14429c',1,'RESOURCE_TYPE():&#160;ShortNames.cpp']]],
  ['resourcedataname_909',['resourceDataName',['../class_request_primitive.html#ac3579f0c1d53bc6955403bc135b4f2b9',1,'RequestPrimitive']]],
  ['resourcename_910',['resourceName',['../class_request_primitive.html#aa4d4018611f66ac78a76547283f55490',1,'RequestPrimitive']]],
  ['resourcetype_911',['resourceType',['../struct_filter_criteria.html#a329103ebd6ac788f252b956508e6dd99',1,'FilterCriteria']]],
  ['response_912',['response',['../_notify_8cpp.html#a528102f7451f2b706bac9e9ed302a5da',1,'Notify.cpp']]],
  ['response_913',['RESPONSE',['../gateway_8cpp.html#a9d596128a301809b2269c1eb6f3c6033',1,'gateway.cpp']]],
  ['response_5fprimitive_914',['RESPONSE_PRIMITIVE',['../_short_names_8cpp.html#a2cd19af5dfe56c0a5d9150064b988090',1,'RESPONSE_PRIMITIVE():&#160;ShortNames.cpp'],['../_short_names_8h.html#a2cd19af5dfe56c0a5d9150064b988090',1,'RESPONSE_PRIMITIVE():&#160;ShortNames.cpp']]],
  ['response_5fstatus_5fcode_915',['RESPONSE_STATUS_CODE',['../_short_names_8cpp.html#a90bd71b94c084bd07ad51f081fcd75e0',1,'RESPONSE_STATUS_CODE():&#160;ShortNames.cpp'],['../_short_names_8h.html#a90bd71b94c084bd07ad51f081fcd75e0',1,'RESPONSE_STATUS_CODE():&#160;ShortNames.cpp']]],
  ['response_5ftype_916',['RESPONSE_TYPE',['../_short_names_8cpp.html#ab7665125e5b4fc3a60be391781ab430e',1,'RESPONSE_TYPE():&#160;ShortNames.cpp'],['../_short_names_8h.html#ab7665125e5b4fc3a60be391781ab430e',1,'RESPONSE_TYPE():&#160;ShortNames.cpp']]],
  ['responseprimitive_917',['ResponsePrimitive',['../class_response_primitive.html#aca921780560446528459f3b9b3fa33db',1,'ResponsePrimitive::ResponsePrimitive()'],['../class_response_primitive.html#a5751c3385829e810da51116d4461116d',1,'ResponsePrimitive::ResponsePrimitive(const String &amp;ri, const String &amp;from, const String &amp;to, ResponseStatusCode rsc)'],['../class_response_primitive.html',1,'ResponsePrimitive']]],
  ['responseprimitive_2ecpp_918',['ResponsePrimitive.cpp',['../_response_primitive_8cpp.html',1,'']]],
  ['responseprimitive_2eh_919',['ResponsePrimitive.h',['../_response_primitive_8h.html',1,'']]],
  ['responseprimitivebuffer_920',['responsePrimitiveBuffer',['../struct_m_q_t_t_buffer.html#a27a75899707d03bb47a872cc61c0d3c1',1,'MQTTBuffer']]],
  ['responsestatuscode_921',['ResponseStatusCode',['../_response_primitive_8h.html#a84b382c64aaa1f5f8d0cbfa85ce34fbc',1,'ResponsePrimitive.h']]],
  ['result_5fcontent_922',['RESULT_CONTENT',['../_short_names_8h.html#af02951aa9ceeda3d34d2be7e1b35a130',1,'RESULT_CONTENT():&#160;ShortNames.cpp'],['../_short_names_8cpp.html#af02951aa9ceeda3d34d2be7e1b35a130',1,'RESULT_CONTENT():&#160;ShortNames.cpp']]],
  ['result_5fexpiration_5ftimestamp_923',['RESULT_EXPIRATION_TIMESTAMP',['../_short_names_8cpp.html#a3b791d997e6a0ea25c8cd9c7378cf26a',1,'RESULT_EXPIRATION_TIMESTAMP():&#160;ShortNames.cpp'],['../_short_names_8h.html#a3b791d997e6a0ea25c8cd9c7378cf26a',1,'RESULT_EXPIRATION_TIMESTAMP():&#160;ShortNames.cpp']]],
  ['result_5fpersistence_924',['RESULT_PERSISTENCE',['../_short_names_8h.html#a03e1a810f99c7123e951dadc8c6e4869',1,'RESULT_PERSISTENCE():&#160;ShortNames.cpp'],['../_short_names_8cpp.html#a03e1a810f99c7123e951dadc8c6e4869',1,'RESULT_PERSISTENCE():&#160;ShortNames.cpp']]],
  ['resultcontenttype_925',['ResultContentType',['../_enum_8h.html#a5d2a50fd0d73e6b3ef0d722454bd0f24',1,'Enum.h']]],
  ['retarget_926',['retarget',['../class_redirector.html#a0ba7830b08c0144a5f84d173ee0557c5',1,'Redirector']]],
  ['role_5fids_5ffrom_5facps_927',['ROLE_IDS_FROM_ACPS',['../_short_names_8h.html#a34b50730763e22f731dabea799428366',1,'ROLE_IDS_FROM_ACPS():&#160;ShortNames.cpp'],['../_short_names_8cpp.html#a34b50730763e22f731dabea799428366',1,'ROLE_IDS_FROM_ACPS():&#160;ShortNames.cpp']]],
  ['rootcse_928',['rootCSE',['../class_request_primitive.html#ab6b99fb67981c81a7a80ad1189269e46',1,'RequestPrimitive']]],
  ['rootname_929',['rootName',['../class_request_primitive.html#a03538b536629c1af1e3e5d87e48c3763',1,'RequestPrimitive']]],
  ['router_930',['Router',['../class_router.html',1,'']]],
  ['rqtype_931',['rqType',['../lom2m_8cpp.html#a27bfc98f51ac8fb4f7b0ba8c7f8fae01',1,'rqType(int ty):&#160;lom2m.cpp'],['../lom2m_8h.html#a27bfc98f51ac8fb4f7b0ba8c7f8fae01',1,'rqType(int ty):&#160;lom2m.cpp']]],
  ['rvi_932',['RVI',['../lom2m_8cpp.html#adec9aa0f92bb9cc2f793837c86b5fb2a',1,'lom2m.cpp']]]
];
