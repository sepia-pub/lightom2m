#!/bin/sh

set -eux ;

echo "Check data backup location...\n";
DATA_PATH=/usr/lom2m-data
if [ -z ${LOM2M_DATA_PATH+x} ]; then
  LOM2M_DATA_PATH=$DATA_PATH; 
else 
  echo "var is set to '$LOM2M_DATA_PATH'"; 
fi

# check existence of dir
if [ ! -d "$LOM2M_DATA_PATH" ]; then
  # Control will enter here if $LOM2M_DATA_PATH doesn't exist.
  mkdir -p $LOM2M_DATA_PATH ;
fi

echo "Start LOM2M...\n";
exec "$@" -P $LOM2M_DATA_PATH;
