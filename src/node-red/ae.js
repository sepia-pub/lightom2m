/*
------------------------------------------------------------

SOFTWARE EVALUATION LICENSE

LAAS research laboratory, Toulouse, France.
IRIT computer science research laboratory, Toulouse, France.
------------------------------------------------------------

Contact: Thierry Monteil, thierry.monteil@irit.fr

Definitions
SOFTWARE:  The LightOM2M software, version 1.0, in source code form, written by David Gauchard, Guillaume Garzone and Thierry Monteil
at the LAAS research laboratory.

LICENSOR: LE CENTRE NATIONAL DE LA RECHERCHE SCIENTIFIQUE, a public scientific and technical research establishment, having SIREN No. 180 089 013, APE code 7219 Z, having its registered office at 3, rue Michel-Ange, 75796 Paris cedex 16, France, acting in its own name and on its own behalf, and on behalf of Le laboratoire d'analyse et d'architecture des systèmes (LAAS), UPR n°8001.

1. INTENT/PURPOSE
This agreement determines the conditions in which the LICENSOR, who has the rights to the SOFTWARE, grants to the LICENSEE a license for research and evaluation purposes only, excluding any commercial use.

2. LICENSEE
Any person or organization who receives the SOFTWARE with a copy of this license.

3. RIGHTS GRANTED
The rights to use and copy the SOFTWARE, subject to the restrictions described in this agreement.
The rights to modify and compile the SOFTWARE when it's provided in source code form, subject to the restrictions described in this agreement.
For the SOFTWARE provided in binary form only, LICENSEE undertakes to not decompile, disassemble, decrypt, extract the components or perform reverse engineering except to the extent expressly provided by law.

5. SCOPE OF THE LICENSE
- NON-COMMERCIAL license for research and evaluation purposes ONLY.
- NO right to commercialize the SOFTWARE, or any derivative work, without separate agreement with the LICENSOR.

6. MODIFICATION OF THE SOFTWARE
License permits LICENSEE to modify the SOFTWARE provided in source code form for research and evaluation purposes only.

7. REDISTRIBUTION
- License permits LICENSEE to redistribute verbatim copies of the SOFTWARE, accompanied with a copy of this license.
- License DOES NOT permit LICENSEE to redistribute modified versions of the SOFTWARE provided in source code form.
- License DOES NOT permit LICENSEE to commercialize the SOFTWARE or any derivative work of the SOFTWARE.

8. FEE/ROYALTY
- LICENSEE pays no royalty for this license.
- LICENSEE and any third parties must enter a new agreement for any use beyond scope of license. Please contact the IRIT technology transfer office (email numerique@toulouse-tech-transfer.com) for further information.

9. NO WARRANTY
The SOFTWARE is provided "as is" without warranty of any kind, either expressed or implied, including, but not limited to, the implied warranties of merchantability and fitness for a particular purpose. The entire risk as to the quality and performance of the program is with the LICENSEE.

10. NO LIABILITY
In no event unless required by applicable law or agreed to in writing will any copyright owner be liable to LICENSEE for damages, including any general, special, incidental or consequential damages arising out of the use or inability to use the program (including but not limited to loss of data or data being rendered inaccurate or losses sustained by LICENSEE or third parties or a failure of the program to operate with other programs), even if such holder has been advised of the possibility of such damages.
*/


"use strict";
var request = require('request');
module.exports = function (RED) {

    function CreateAEHTTP(n) {
        RED.nodes.createNode(this, n);
        this.pointOfAccess = n.pointOfAccess;
        this.labels = n.labels;
        this.appId = n.appId;

        var node = this;
        node.status({});

        if (RED.settings.httpRequestTimeout) {
            this.reqTimeout = parseInt(RED.settings.httpRequestTimeout) || 120000;
        } else {
            this.reqTimeout = 120000;
        }

        this.on("input", function (msg) {
            node.CSE_CONFIG = RED.nodes.getNode(n.cseConfig);
            node.AE_CONFIG = RED.nodes.getNode(n.aeConfig);
            var origin = RED.nodes.getNode(node.CSE_CONFIG.adminOriginator).originator;
            var AEName = node.AE_CONFIG.aeName;
            var url = node.CSE_CONFIG.poa + "/~/" + node.CSE_CONFIG.cseId + '/' + node.CSE_CONFIG.cseName;

            if (n.acpConfig && n.acpConfig != "") {
                node.ACP_CONFIG = RED.nodes.getNode(n.acpConfig);
            }
            else {
                node.ACP_CONFIG = false;
            }

            node.status({
                fill: "blue",
                shape: "dot",
                text: "httpin.status.requesting"
            });

            console.log('1');
            // TODO GET
            var request = require('request');
            var ri = "NR-" + Date.now();
            var options = {
                'method': 'GET',
                'url': url + '/' + node.AE_CONFIG.aeName,
                'headers': {
                    'X-M2M-RI': ri,
                    'X-M2M-Origin': origin,
                    'accept': 'application/json'
                }
            };
            console.log('2');
            request(options, function (error, response, body) {

                node.status({});

                var requestMethod = 'POST';
                var contentType = 'application/json';

                if (error) {
                    console.error(error);
                    node.status({ shape: "ring", fill: "red", text: "error" });
                    msg.error = error;
                    msg.payload = error.code;
                    node.send(msg);
                    return;
                }
                else {
                    if (response.statusCode == 200) {
                        requestMethod = 'PUT';
                        console.log("AE Resource already exists, updating it...");
                    }
                    else {
                        node.status({ shape: "ring", fill: "red", text: 'rsc: ' + response.headers['x-m2m-rsc'] });
                        if (response.statusCode == 403) {
                            msg.error = response.statusCode;
                            msg.payload = response.headers['x-m2m-rsc'];
                            node.send(msg);
                            return;
                        }
                    }
                }
                console.log('3');
                var reqBody = {};

                // POST / PUT 

                if (!url) {
                    node.error(RED._("httpin.errors.no-url"), msg);
                    node.status({
                        fill: "red",
                        shape: "ring",
                        text: (RED._("httpin.errors.no-url"))
                    });
                    return;
                }

                var reqBody = { 'm2m:ae': { 'rr': false, 'srv': [] } };
                var aeObj = reqBody['m2m:ae'];

                if (requestMethod == 'POST') {
                    aeObj.rn = AEName;
                    aeObj.api = node.appId;
                    contentType = contentType + ';ty=2';
                }
                else
                {
                    url = url + '/' + AEName;
                }
                if (node.ACP_CONFIG && node.ACP_CONFIG.acpId && node.ACP_CONFIG.acpId != '') {
                    aeObj.acpi = []
                    aeObj.acpi[0] = node.ACP_CONFIG.acpId;
                }

                if (node.pointOfAccess && node.pointOfAccess != "") {
                    aeObj.poa = [];
                    // TODO update with list when implemented in HTML
                    aeObj.poa[0] = node.pointOfAccess;
                    aeObj.rr = true;
                }

                // possible to add this in HTML
                aeObj.srv = ['2a', '3'];

                // TODO TO UPDATE WHEN LIST IMPLEMENTED IN HTML
                if (node.labels && node.labels != "") {
                    aeObj.lbl = [];
                    aeObj.lbl[0] = node.labels;
                }

                var bodyString = JSON.stringify(reqBody);
                var ri = "NR-" + Date.now();
                console.log("4");
                var options2 = {
                    method: requestMethod,
                    url: url,
                    timeout: node.reqTimeout,
                    headers: {
                        "Content-type": contentType,
                        "X-M2M-Origin": origin,
                        "X-M2M-RVI": "3",
                        "X-M2M-RI": ri
                    },
                    body: bodyString
                };

                msg.headers = options.headers;
                msg.payload = bodyString;

                // TODO to remove
                console.log(options2);

                // SENDING POST / PUT REQUEST
                var request2 = require('request');
                request2(options2, function (error, response) {
                    node.status({});

                    if (error) {
                        node.error(error, msg);
                        msg.payload = error.toString() + " : " + url;
                        msg.statusCode = error.code;
                        node.send(msg);
                        node.status({
                            fill: "red",
                            shape: "ring",
                            text: error.code
                        });
                    } else {
                        msg.payload = response.body;
                        msg.headers = response.headers;
                        msg.statusCode = response.statusCode;
                        if (response.statusCode == 201 || response.statusCode == 200) {
                            node.status({ shape: "ring", fill: "green", text: 'rsc: ' + response.statusCode });
                        }
                        else {
                            node.status({ shape: "ring", fill: "red", text: 'rsc: ' + response.headers['x-m2m-rsc'] });
                        }
                        node.send(msg);
                    }
                });

            });

        });
    }

    RED.nodes.registerType("Application Entity", CreateAEHTTP);
}
