/*
------------------------------------------------------------

SOFTWARE EVALUATION LICENSE

LAAS research laboratory, Toulouse, France.
IRIT computer science research laboratory, Toulouse, France.
------------------------------------------------------------

Contact: Thierry Monteil, thierry.monteil@irit.fr

Definitions
SOFTWARE:  The LightOM2M software, version 1.0, in source code form, written by David Gauchard, Guillaume Garzone and Thierry Monteil
at the LAAS research laboratory.

LICENSOR: LE CENTRE NATIONAL DE LA RECHERCHE SCIENTIFIQUE, a public scientific and technical research establishment, having SIREN No. 180 089 013, APE code 7219 Z, having its registered office at 3, rue Michel-Ange, 75796 Paris cedex 16, France, acting in its own name and on its own behalf, and on behalf of Le laboratoire d'analyse et d'architecture des systèmes (LAAS), UPR n°8001.

1. INTENT/PURPOSE
This agreement determines the conditions in which the LICENSOR, who has the rights to the SOFTWARE, grants to the LICENSEE a license for research and evaluation purposes only, excluding any commercial use.

2. LICENSEE
Any person or organization who receives the SOFTWARE with a copy of this license.

3. RIGHTS GRANTED
The rights to use and copy the SOFTWARE, subject to the restrictions described in this agreement.
The rights to modify and compile the SOFTWARE when it's provided in source code form, subject to the restrictions described in this agreement.
For the SOFTWARE provided in binary form only, LICENSEE undertakes to not decompile, disassemble, decrypt, extract the components or perform reverse engineering except to the extent expressly provided by law.

5. SCOPE OF THE LICENSE
- NON-COMMERCIAL license for research and evaluation purposes ONLY.
- NO right to commercialize the SOFTWARE, or any derivative work, without separate agreement with the LICENSOR.

6. MODIFICATION OF THE SOFTWARE
License permits LICENSEE to modify the SOFTWARE provided in source code form for research and evaluation purposes only.

7. REDISTRIBUTION
- License permits LICENSEE to redistribute verbatim copies of the SOFTWARE, accompanied with a copy of this license.
- License DOES NOT permit LICENSEE to redistribute modified versions of the SOFTWARE provided in source code form.
- License DOES NOT permit LICENSEE to commercialize the SOFTWARE or any derivative work of the SOFTWARE.

8. FEE/ROYALTY
- LICENSEE pays no royalty for this license.
- LICENSEE and any third parties must enter a new agreement for any use beyond scope of license. Please contact the IRIT technology transfer office (email numerique@toulouse-tech-transfer.com) for further information.

9. NO WARRANTY
The SOFTWARE is provided "as is" without warranty of any kind, either expressed or implied, including, but not limited to, the implied warranties of merchantability and fitness for a particular purpose. The entire risk as to the quality and performance of the program is with the LICENSEE.

10. NO LIABILITY
In no event unless required by applicable law or agreed to in writing will any copyright owner be liable to LICENSEE for damages, including any general, special, incidental or consequential damages arising out of the use or inability to use the program (including but not limited to loss of data or data being rendered inaccurate or losses sustained by LICENSEE or third parties or a failure of the program to operate with other programs), even if such holder has been advised of the possibility of such damages.
*/


"use strict";
var request = require('request');
module.exports = function (RED) {

    function SubscriptionHTTPRequest(n) {
        RED.nodes.createNode(this, n);
        var node = this;

        this.on("input", function (msg) {
            try {
                this.CSE_CONFIG = RED.nodes.getNode(n.cseConfig);
                this.AE_CONFIG = RED.nodes.getNode(n.aeConfig);
                this.notificationUri = n.nu;
                this.cnt = n.cnt;
                this.SUB_CONFIG = RED.nodes.getNode(n.subConfig);

                node.status({
                    fill: "blue",
                    shape: "dot",
                    text: "requesting"
                });

                var url = node.CSE_CONFIG.poa + '/~/' + node.CSE_CONFIG.cseId + '/' + node.CSE_CONFIG.cseName;
                var adminOriginator = RED.nodes.getNode(node.CSE_CONFIG.adminOriginator).originator;
                var reqBody = {};
                var ri = "NR-" + Date.now();
                if (!url) {
                    node.status({
                        fill: "red",
                        shape: "ring",
                        text: (RED._("error.url"))
                    });
                    console.log("ERROR: no url\n");
                    return;
                }

                reqBody['m2m:sub'] = {};
                var subObj = reqBody['m2m:sub'];
                subObj['lbl'] = ["node-red/sub"];
                subObj['nu'] = [node.SUB_CONFIG.subNu];
                subObj['nct'] = 1;
                subObj['rn'] = node.SUB_CONFIG.subName;

                console.log(reqBody);

                var options = {
                    'method': 'POST',
                    'url': url + '/' + node.AE_CONFIG.aeName + '/' + node.cnt,
                    'headers': {
                        'X-M2M-RI': ri,
                        'X-M2M-Origin': adminOriginator,
                        'X-M2M-RVI': '3',
                        'accept': 'application/json',
                        'content-type': 'application/json;ty=23'
                    },
                    body: JSON.stringify(reqBody)
                };

                msg.payload = options;

                console.log(options);

                request(options, function (error, response) {
                    console.log(response.body);
                    if (error) {
                        console.error(error);
                        node.status({ shape: "ring", fill: "red", text: "error" });
                        msg.error = error;
                        msg.payload = error.code;
                    }
                    else {
                        msg.payload = response.body;
                        msg.headers = response.headers;
                        msg.statusCode = response.statusCode;
                        if (response.statusCode != 201) {
                            node.status({ shape: "ring", fill: "red", text: 'rsc: ' + response.headers['x-m2m-rsc'] });
                        }
                        else {
                            node.status({ shape: "ring", fill: "green", text: 'rsc: ' + response.statusCode });
                        }
                    }
                    node.send(msg);
                });

            } catch (e) {
                node.status({
                    fill: "red",
                    shape: "ring",
                    text: "error"
                });
                console.log(e.toString());
            }

        });
    }

    RED.nodes.registerType("SubscriptionNode", SubscriptionHTTPRequest);
}
