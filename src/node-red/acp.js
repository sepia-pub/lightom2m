/*
------------------------------------------------------------

SOFTWARE EVALUATION LICENSE

LAAS research laboratory, Toulouse, France.
IRIT computer science research laboratory, Toulouse, France.
------------------------------------------------------------

Contact: Thierry Monteil, thierry.monteil@irit.fr

Definitions
SOFTWARE:  The LightOM2M software, version 1.0, in source code form, written by David Gauchard, Guillaume Garzone and Thierry Monteil
at the LAAS research laboratory.

LICENSOR: LE CENTRE NATIONAL DE LA RECHERCHE SCIENTIFIQUE, a public scientific and technical research establishment, having SIREN No. 180 089 013, APE code 7219 Z, having its registered office at 3, rue Michel-Ange, 75796 Paris cedex 16, France, acting in its own name and on its own behalf, and on behalf of Le laboratoire d'analyse et d'architecture des systèmes (LAAS), UPR n°8001.

1. INTENT/PURPOSE
This agreement determines the conditions in which the LICENSOR, who has the rights to the SOFTWARE, grants to the LICENSEE a license for research and evaluation purposes only, excluding any commercial use.

2. LICENSEE
Any person or organization who receives the SOFTWARE with a copy of this license.

3. RIGHTS GRANTED
The rights to use and copy the SOFTWARE, subject to the restrictions described in this agreement.
The rights to modify and compile the SOFTWARE when it's provided in source code form, subject to the restrictions described in this agreement.
For the SOFTWARE provided in binary form only, LICENSEE undertakes to not decompile, disassemble, decrypt, extract the components or perform reverse engineering except to the extent expressly provided by law.

5. SCOPE OF THE LICENSE
- NON-COMMERCIAL license for research and evaluation purposes ONLY.
- NO right to commercialize the SOFTWARE, or any derivative work, without separate agreement with the LICENSOR.

6. MODIFICATION OF THE SOFTWARE
License permits LICENSEE to modify the SOFTWARE provided in source code form for research and evaluation purposes only.

7. REDISTRIBUTION
- License permits LICENSEE to redistribute verbatim copies of the SOFTWARE, accompanied with a copy of this license.
- License DOES NOT permit LICENSEE to redistribute modified versions of the SOFTWARE provided in source code form.
- License DOES NOT permit LICENSEE to commercialize the SOFTWARE or any derivative work of the SOFTWARE.

8. FEE/ROYALTY
- LICENSEE pays no royalty for this license.
- LICENSEE and any third parties must enter a new agreement for any use beyond scope of license. Please contact the IRIT technology transfer office (email numerique@toulouse-tech-transfer.com) for further information.

9. NO WARRANTY
The SOFTWARE is provided "as is" without warranty of any kind, either expressed or implied, including, but not limited to, the implied warranties of merchantability and fitness for a particular purpose. The entire risk as to the quality and performance of the program is with the LICENSEE.

10. NO LIABILITY
In no event unless required by applicable law or agreed to in writing will any copyright owner be liable to LICENSEE for damages, including any general, special, incidental or consequential damages arising out of the use or inability to use the program (including but not limited to loss of data or data being rendered inaccurate or losses sustained by LICENSEE or third parties or a failure of the program to operate with other programs), even if such holder has been advised of the possibility of such damages.
*/


module.exports = function (RED) {

    function CreateACPHTTP(n) {
        RED.nodes.createNode(this, n);

        this.acpConfig = RED.nodes.getNode(n.acpConfig);
        this.acrPv = RED.nodes.getNode(n.acrPv);
        this.acrPvs = RED.nodes.getNode(n.acrPvs);
        this.CSE_CONFIG = RED.nodes.getNode(n.cseConfig);
        var node = this;
        
        node.status({});

        var acpName = "";
        if(this.acpConfig)
        {
            acpName = this.acpConfig.acpName;
            this.acpConfig.acpId = '';
        }

        this.on('input', function (msg) {

            if (RED.settings.httpRequestTimeout) {
                this.reqTimeout = parseInt(RED.settings.httpRequestTimeout) || 120000;
            } else {
                this.reqTimeout = 120000;
            }
            node.status({
                fill: "blue",
                shape: "dot",
                text: "httpin.status.requesting"
            });

            var adminOriginator = RED.nodes.getNode(node.CSE_CONFIG.adminOriginator).originator ;

            var reqBody = {};
            reqBody['m2m:acp'] = {};
            var acpObj = reqBody['m2m:acp'];
            acpObj['pv'] = {};
            acpObj['pvs'] = {};

            // PRIVILEGES
            // TODO to updateRight with lists when implemented in HTML
            reqBody['m2m:acp'].pv = { "acr": [] };
            // TODO add for loop on acor here
            reqBody['m2m:acp'].pv.acr[0] = { "acor": [], "acop": 0 };
            reqBody['m2m:acp'].pv.acr[0].acor = [];
            reqBody['m2m:acp'].pv.acr[0].acor[0] = node.acrPv.acrOriginator.originator;
            var op = 0;
            if (node.acrPv.createRight) {
                op += 1;
            }
            if (node.acrPv.retrieveRight) {
                op += 2;
            }
            if (node.acrPv.updateRight) {
                op += 4;
            }
            if (node.acrPv.deleteRight) {
                op += 8;
            }
            if (node.acrPv.notifyRight) {
                op += 16;
            }
            if (node.acrPv.discoveryRight) {
                op += 32;
            }
            reqBody['m2m:acp'].pv.acr[0].acop = op;

            // SELF PRIVILEGES
            reqBody['m2m:acp'].pvs = { "acr": [] };
            // TODO add for loop on acr here when implemented in HTML
            reqBody['m2m:acp'].pvs.acr[0] = { "acor": [], "acop": 0 };
            // TODO add for loop on acor here when implemented in HTML
            reqBody['m2m:acp'].pvs.acr[0].acor[0] = node.acrPvs.acrOriginator.originator;
            var op2 = 0;
            if (node.acrPvs.createRight) {
                op2 += 1;
            }
            if (node.acrPvs.retrieveRight) {
                op2 += 2;
            }
            if (node.acrPvs.updateRight) {
                op2 += 4;
            }
            if (node.acrPvs.deleteRight) {
                op2 += 8;
            }
            if (node.acrPvs.notifyRight) {
                op2 += 16;
            }
            if (node.acrPvs.discoveryRight) {
                op2 += 32;
            }
            reqBody['m2m:acp'].pvs.acr[0].acop = op2;

            // preparing http request
            var url = node.CSE_CONFIG.poa + '/' + node.CSE_CONFIG.cseName;
            var request = require('request');
            var ri = "NR-" + Date.now();
            var options = {
                'method': 'GET',
                'url': url + '/' + acpName,
                'headers': {
                    'X-M2M-RI': ri,
                    'X-M2M-Origin': adminOriginator,
                    'accept': 'application/json'
                }
            };


            request(options, function(error, response) {
                var requestMethod = 'POST';
                var request2 = require('request');
                var contentType = 'content-type';

                if (error) {
                    console.error(error);
                    node.status({ shape: "ring", fill: "red", text: "error" });
                    msg.error = error;
                    msg.payload = error.code;
                }
                else {
                    if (response.statusCode == 200) {
                        requestMethod = 'PUT';
                    }
                }
                
                ri = "NR-" + Date.now();
                var urlAcp = url ;
                if (requestMethod == 'POST')
                {
                    contentType = contentType + ';ty=1';
                    acpObj['rn'] = acpName;
                }
                else
                {
                    urlAcp = urlAcp +'/'+acpName;
                    urlAcp = urlAcp + '?rcn=1';
                }

                var options2 = {
                    'method': requestMethod,
                    'url': urlAcp,
                    'headers': {
                        'X-M2M-RI': ri,
                        'X-M2M-Origin': adminOriginator,
                        "X-M2M-RVI": "3",
                        'accept': 'application/json',
                        'content-type' : contentType
                    },
                    body: JSON.stringify(reqBody)
                }

                request2(options2, function(error, response){
                    if (error) {
                        console.error(error);
                        node.status({ shape: "ring", fill: "red", text: "error" });
                        msg.error = error;
                        msg.payload = error.code;
                    }
                    else {
                        console.log(response.body);
                        try {
                            var responseObj = JSON.parse(response.body);
                            var acpobj = responseObj['m2m:acp'];
                            var riacp = acpobj['ri'];
                            node.acpConfig.acpId = riacp;
                            msg.payload = response.body;
                            msg.headers = response.headers;
                            msg.statusCode = response.statusCode;
                            if (response.statusCode == 201 || response.statusCode == 200) {
                                node.status({ shape: "ring", fill: "green", text: 'rsc: ' + response.statusCode });
                            }
                            else {
                                node.status({ shape: "ring", fill: "red", text: 'rsc: ' + response.headers['x-m2m-rsc'] });
                            }
                        } catch (e) {
                            console.error(error);
                            msg.error(error);
                        }
                    }
                    node.send(msg);
                }); // POST / PUT REQUEST

            }); // GET REQUEST

        }); // input

    }

    RED.nodes.registerType("ACP", CreateACPHTTP);
}
