/*
------------------------------------------------------------

SOFTWARE EVALUATION LICENSE

LAAS research laboratory, Toulouse, France.
IRIT computer science research laboratory, Toulouse, France.
------------------------------------------------------------

Contact: Thierry Monteil, thierry.monteil@irit.fr

Definitions
SOFTWARE:  The LightOM2M software, version 1.0, in source code form, written by David Gauchard, Guillaume Garzone and Thierry Monteil
at the LAAS research laboratory.

LICENSOR: LE CENTRE NATIONAL DE LA RECHERCHE SCIENTIFIQUE, a public scientific and technical research establishment, having SIREN No. 180 089 013, APE code 7219 Z, having its registered office at 3, rue Michel-Ange, 75796 Paris cedex 16, France, acting in its own name and on its own behalf, and on behalf of Le laboratoire d'analyse et d'architecture des systèmes (LAAS), UPR n°8001.

1. INTENT/PURPOSE
This agreement determines the conditions in which the LICENSOR, who has the rights to the SOFTWARE, grants to the LICENSEE a license for research and evaluation purposes only, excluding any commercial use.

2. LICENSEE
Any person or organization who receives the SOFTWARE with a copy of this license.

3. RIGHTS GRANTED
The rights to use and copy the SOFTWARE, subject to the restrictions described in this agreement.
The rights to modify and compile the SOFTWARE when it's provided in source code form, subject to the restrictions described in this agreement.
For the SOFTWARE provided in binary form only, LICENSEE undertakes to not decompile, disassemble, decrypt, extract the components or perform reverse engineering except to the extent expressly provided by law.

5. SCOPE OF THE LICENSE
- NON-COMMERCIAL license for research and evaluation purposes ONLY.
- NO right to commercialize the SOFTWARE, or any derivative work, without separate agreement with the LICENSOR.

6. MODIFICATION OF THE SOFTWARE
License permits LICENSEE to modify the SOFTWARE provided in source code form for research and evaluation purposes only.

7. REDISTRIBUTION
- License permits LICENSEE to redistribute verbatim copies of the SOFTWARE, accompanied with a copy of this license.
- License DOES NOT permit LICENSEE to redistribute modified versions of the SOFTWARE provided in source code form.
- License DOES NOT permit LICENSEE to commercialize the SOFTWARE or any derivative work of the SOFTWARE.

8. FEE/ROYALTY
- LICENSEE pays no royalty for this license.
- LICENSEE and any third parties must enter a new agreement for any use beyond scope of license. Please contact the IRIT technology transfer office (email numerique@toulouse-tech-transfer.com) for further information.

9. NO WARRANTY
The SOFTWARE is provided "as is" without warranty of any kind, either expressed or implied, including, but not limited to, the implied warranties of merchantability and fitness for a particular purpose. The entire risk as to the quality and performance of the program is with the LICENSEE.

10. NO LIABILITY
In no event unless required by applicable law or agreed to in writing will any copyright owner be liable to LICENSEE for damages, including any general, special, incidental or consequential damages arising out of the use or inability to use the program (including but not limited to loss of data or data being rendered inaccurate or losses sustained by LICENSEE or third parties or a failure of the program to operate with other programs), even if such holder has been advised of the possibility of such damages.
*/


var request = require('request');


module.exports = function (RED) {
    
    function CreateResources(node, msg, url, adminOriginator) {
        var AEName = node.AE_CONFIG.aeName;
        var method = "POST";
        if (!url) {
            node.error(RED._("httpin.errors.no-url"), msg);
            node.status({
                fill: "red",
                shape: "ring",
                text: (RED._("httpin.errors.no-url"))
            });
            return;
        }
    
        var reqBody = { 'm2m:ae': { 'rn': AEName, 'api': node.appId, 'rr': false, 'srv': [] } };
    
        if (node.pointOfAccess && node.pointOfAccess != "") {
            reqBody['m2m:ae'].poa = [];
            // TODO update with list when implemented in HTML
            reqBody['m2m:ae'].poa[0] = node.pointOfAccess;
            reqBody['m2m:ae'].rr = true;
        }
    
        // possible to add this in HTML
        reqBody['m2m:ae'].srv = ['2a', '3'];
    
        // TODO TO UPDATE WHEN LIST IMPLEMENTED IN HTML
        reqBody['m2m:ae'].lbl = [];
        reqBody['m2m:ae'].lbl[0] = "type/sensor";
        reqBody['m2m:ae'].lbl[1] = "type/" + node.sensorType;
        if (node.labels && node.labels != "") {
            reqBody['m2m:ae'].lbl[2] = node.labels;
        }
    
        var bodyString = JSON.stringify(reqBody);
        var ri = "NR" + Date.now();
    
        var options = {
            method: method,
            url: url,
            timeout: node.reqTimeout,
            headers: {
                "Content-type": "application/json;ty=2",
                "X-M2M-Origin": adminOriginator,
                "X-M2M-RVI": "3",
                "X-M2M-RI": ri
            },
            body: bodyString
        };
    
        console.log(options);
    
        request(options, function (error, response) {
            node.status({
                fill: "blue",
                shape: "dot",
                text: "httpin.status.requesting"
            });
    
            if (error) {
                node.error(error, msg);
                msg.payload = error.toString() + " : " + url;
                msg.statusCode = error.code;
                node.send(msg);
                node.status({
                    fill: "red",
                    shape: "ring",
                    text: error.code
                });
                return;
            } else {
                msg.payload = response.body;
                msg.headers = response.headers;
                msg.statusCode = response.statusCode;
                if (response.statusCode != 201) {
                    node.status({ shape: "ring", fill: "red", text: 'rsc: ' + response.headers['x-m2m-rsc'] });
                    node.send(msg);
                    return;
                }
                else {
                    node.status({ shape: "ring", fill: "green", text: 'rsc: ' + response.statusCode });
                }
            }
    
    
            // CREATE CNT DATA
            var bodyCnt = {};
            bodyCnt['m2m:cnt'] = { "rn": 'DATA', 'mni': node.mni };
    
            // preparing http request
            url = url + '/' + AEName;
            ri = "NR" + Date.now();
            var optionsData = {
                'method': 'POST',
                'url': url,
                'headers': {
                    'X-M2M-RI': ri,
                    'X-M2M-Origin': adminOriginator,
                    'Content-Type': 'application/json;ty=3'
                },
                body: JSON.stringify(bodyCnt)
    
            };
    
            console.log(optionsData);
    
            // sending the request
            request(optionsData, function (error, response) {
                if (error) {
                    console.error(error);
                    node.status({ shape: "ring", fill: "red", text: "error" });
                    msg.error = error;
                    msg.payload = error.code;
                    node.send(msg);
                    return;
                }
                else {
                    msg.payload = response.body;
                    msg.headers = response.headers;
                    msg.statusCode = response.statusCode;
                    if (response.statusCode != 201) {
                        node.status({ shape: "ring", fill: "red", text: 'rsc: ' + response.headers['x-m2m-rsc'] });
                        node.send(msg);
                        return;
                    }
                    else {
                        node.status({ shape: "ring", fill: "green", text: 'rsc: ' + response.statusCode });
                    }
                }
    
                node.setupDone = true;
    
                node.status({
                    fill: "blue",
                    shape: "dot",
                    text: "httpin.status.requesting"
                });
                if (!msg.sensorValue) {
                    msg.sensorValue = Math.random() * (node.maxValue - node.minValue);
                    if (node.minValue < 0) {
                        msg.sensorValue = msg.sensorValue - (-1 * node.minValue);
                    }
                    else {
                        msg.sensorValue = msg.sensorValue + (node.minValue);
                    }
                    msg.sensorValue = Math.round(msg.sensorValue * 100) / 100;
                }
                reqBody = {};
                reqBody['m2m:cin'] = { "cnf": node.contentFormat, "con": msg.sensorValue };
    
                // preparing http request
                url = url + '/DATA';
                ri = "NR-" + Date.now();
                var optionsCin = {
                    'method': 'POST',
                    'url': url,
                    'headers': {
                        'X-M2M-RI': ri,
                        'X-M2M-Origin': adminOriginator,
                        'Content-Type': 'application/json;ty=4'
                    },
                    body: JSON.stringify(reqBody)
                };
    
                console.log(optionsCin);
    
                // sending the request
                request(optionsCin, function (error, response) {
                    if (error) {
                        console.error(error);
                        node.status({ shape: "ring", fill: "red", text: "error" });
                        msg.error = error;
                        msg.payload = error.code;
                    }
                    else {
                        msg.payload = response.body;
                        msg.headers = response.headers;
                        msg.statusCode = response.statusCode;
                        if (response.statusCode != 201) {
                            node.status({ shape: "ring", fill: "red", text: 'rsc: ' + response.headers['x-m2m-rsc'] });
                        }
                        else {
                            node.status({ shape: "ring", fill: "green", text: 'rsc: ' + response.statusCode });
                        }
                    }
                    node.send(msg);
                });
    
                node.send(msg);
    
            }); // CREATE CNT DATA
        }); // CREATE AE
    }

    function SmartSensorHttp(n) {

        RED.nodes.createNode(this, n);


        this.minValue = n.minValue;
        this.maxValue = n.maxValue;
        this.sensorType = n.sensorType;

        this.pointOfAccess = n.pointOfAccess;
        this.labels = n.labels;
        this.appId = n.appId;
        this.CSE_CONFIG = RED.nodes.getNode(n.cseConfig);
        this.AE_CONFIG = RED.nodes.getNode(n.aeConfig);
        this.mni = n.mni;
        this.contentFormat = n.contentFormat;
        if (this.contentFormat == "") {
            this.contentFormat = "text/plain";
        }

        this.setupDone = false;
        this.deleteIfPresent = n.deleteIfPresent;

        var node = this;
        node.status({});

        if (!this.setupDone) {
            var url = this.CSE_CONFIG.poa + "/~/" + this.CSE_CONFIG.cseId + '/' + this.CSE_CONFIG.cseName;
            var adminOriginator = RED.nodes.getNode(this.CSE_CONFIG.adminOriginator).originator;
        }
        this.on('input', function (msg) {
            if (!this.CSE_CONFIG || !this.AE_CONFIG) {
                node.status({ shape: "ring", fill: "red", text: 'Error with CSE or AE CONFIG' });
                return;
            }
            var newValue = Math.random() * (this.maxValue - this.minValue);
            if (this.minValue < 0) {
                newValue = newValue - (-1 * this.minValue);
            }
            else {
                newValue = newValue + (this.minValue);
            }
            newValue = Math.round(newValue * 100) / 100;
            node.status({ shape: "ring", fill: "blue", text: newValue });

            if (RED.settings.httpRequestTimeout) {
                this.reqTimeout = parseInt(RED.settings.httpRequestTimeout) || 120000;
            } else {
                this.reqTimeout = 120000;
            }

            if (!this.setupDone) {

                if (this.deleteIfPresent) {
                    var optionsDelete = {
                        method: 'DELETE',
                        url: url + '/' + node.AE_CONFIG.aeName,
                        timeout: node.reqTimeout,
                        headers: {
                            "X-M2M-Origin": adminOriginator,
                            "X-M2M-RI": "NR" + Date.now()
                        }
                    };

                    console.log(optionsDelete);

                    request(optionsDelete, function (error, response) {
                        node.status({
                            fill: "blue",
                            shape: "dot",
                            text: "httpin.status.requesting"
                        });

                        if (error) {
                            node.error(error, msg);
                            msg.payload = error.toString() + " : " + url;
                            msg.statusCode = error.code;
                            node.send(msg);
                            node.status({
                                fill: "red",
                                shape: "ring",
                                text: error.code
                            });
                            return;
                        } else {
                            msg.payload = response.body;
                            msg.headers = response.headers;
                            msg.statusCode = response.statusCode;
                            if (response.statusCode != 200 && response.statusCode != 404) {
                                node.status({ shape: "ring", fill: "red", text: 'rsc: ' + response.headers['x-m2m-rsc'] });
                                node.send(msg);
                                return;
                            }
                            else {
                                // TODO NEXT STEPS
                                CreateResources(node, msg, url, adminOriginator);
                            }
                        }
                    });

                }
                else {
                    CreateResources(node, msg, url, adminOriginator);
                }
            }
            else {
                node.status({
                    fill: "blue",
                    shape: "dot",
                    text: "httpin.status.requesting"
                });
                if (!msg.sensorValue) {
                    msg.sensorValue = Math.random() * (this.maxValue - this.minValue);
                    if (this.minValue < 0) {
                        msg.sensorValue = msg.sensorValue - (-1 * this.minValue);
                    }
                    else {
                        msg.sensorValue = msg.sensorValue + (this.minValue);
                    }
                    msg.sensorValue = Math.round(msg.sensorValue * 100) / 100;
                }
                reqBody = {};
                reqBody['m2m:cin'] = { "cnf": this.contentFormat, "con": msg.sensorValue };

                // preparing http request
                url = this.CSE_CONFIG.poa + "/~/" + this.CSE_CONFIG.cseId + '/' + this.CSE_CONFIG.cseName + '/' + this.AE_CONFIG.aeName + '/DATA';

                var ri = "NR" + Date.now();
                var optionsCin = {
                    'method': 'POST',
                    'url': url,
                    'headers': {
                        'X-M2M-RI': ri,
                        'X-M2M-Origin': adminOriginator,
                        'Content-Type': 'application/json;ty=4'
                    },
                    body: JSON.stringify(reqBody)

                };

                console.log(optionsCin);


                // sending the request
                request(optionsCin, function (error, response) {
                    if (error) {
                        console.error(error);
                        node.status({ shape: "ring", fill: "red", text: "error" });
                        msg.error = error;
                        msg.payload = error.code;
                    }
                    else {
                        msg.payload = response.body;
                        msg.headers = response.headers;
                        msg.statusCode = response.statusCode;
                        if (response.statusCode != 201) {
                            node.status({ shape: "ring", fill: "red", text: 'rsc: ' + response.headers['x-m2m-rsc'] });
                        }
                        else {
                            node.status({ shape: "ring", fill: "green", text: 'rsc: ' + response.statusCode });
                            msg.payloadType = "m2m:cin";
                        }
                    }
                    node.send(msg);
                });
            }
        }); // input
    }

    RED.nodes.registerType("Smart Sensor", SmartSensorHttp);
}
