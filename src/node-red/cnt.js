/*
------------------------------------------------------------

SOFTWARE EVALUATION LICENSE

LAAS research laboratory, Toulouse, France.
IRIT computer science research laboratory, Toulouse, France.
------------------------------------------------------------

Contact: Thierry Monteil, thierry.monteil@irit.fr

Definitions
SOFTWARE:  The LightOM2M software, version 1.0, in source code form, written by David Gauchard, Guillaume Garzone and Thierry Monteil
at the LAAS research laboratory.

LICENSOR: LE CENTRE NATIONAL DE LA RECHERCHE SCIENTIFIQUE, a public scientific and technical research establishment, having SIREN No. 180 089 013, APE code 7219 Z, having its registered office at 3, rue Michel-Ange, 75796 Paris cedex 16, France, acting in its own name and on its own behalf, and on behalf of Le laboratoire d'analyse et d'architecture des systèmes (LAAS), UPR n°8001.

1. INTENT/PURPOSE
This agreement determines the conditions in which the LICENSOR, who has the rights to the SOFTWARE, grants to the LICENSEE a license for research and evaluation purposes only, excluding any commercial use.

2. LICENSEE
Any person or organization who receives the SOFTWARE with a copy of this license.

3. RIGHTS GRANTED
The rights to use and copy the SOFTWARE, subject to the restrictions described in this agreement.
The rights to modify and compile the SOFTWARE when it's provided in source code form, subject to the restrictions described in this agreement.
For the SOFTWARE provided in binary form only, LICENSEE undertakes to not decompile, disassemble, decrypt, extract the components or perform reverse engineering except to the extent expressly provided by law.

5. SCOPE OF THE LICENSE
- NON-COMMERCIAL license for research and evaluation purposes ONLY.
- NO right to commercialize the SOFTWARE, or any derivative work, without separate agreement with the LICENSOR.

6. MODIFICATION OF THE SOFTWARE
License permits LICENSEE to modify the SOFTWARE provided in source code form for research and evaluation purposes only.

7. REDISTRIBUTION
- License permits LICENSEE to redistribute verbatim copies of the SOFTWARE, accompanied with a copy of this license.
- License DOES NOT permit LICENSEE to redistribute modified versions of the SOFTWARE provided in source code form.
- License DOES NOT permit LICENSEE to commercialize the SOFTWARE or any derivative work of the SOFTWARE.

8. FEE/ROYALTY
- LICENSEE pays no royalty for this license.
- LICENSEE and any third parties must enter a new agreement for any use beyond scope of license. Please contact the IRIT technology transfer office (email numerique@toulouse-tech-transfer.com) for further information.

9. NO WARRANTY
The SOFTWARE is provided "as is" without warranty of any kind, either expressed or implied, including, but not limited to, the implied warranties of merchantability and fitness for a particular purpose. The entire risk as to the quality and performance of the program is with the LICENSEE.

10. NO LIABILITY
In no event unless required by applicable law or agreed to in writing will any copyright owner be liable to LICENSEE for damages, including any general, special, incidental or consequential damages arising out of the use or inability to use the program (including but not limited to loss of data or data being rendered inaccurate or losses sustained by LICENSEE or third parties or a failure of the program to operate with other programs), even if such holder has been advised of the possibility of such damages.
*/

"use strict";

module.exports = function (RED) {
    function CreateCntHttp(n) {
        RED.nodes.createNode(this, n);
        if (n.cseConfig && n.cseConfig != "") 
        { 
            this.CSE_CONFIG = RED.nodes.getNode(n.cseConfig); 
        }
        if (!n.cseConfig || n.cseConfig == "" || !this.CSE_CONFIG)
        {
            node.status({ shape: "ring", fill: "red", text: 'Issue with CSE config ' });
            return;
        }

        if (n.aeConfig && n.aeConfig != "") {
            this.AE_CONFIG = RED.nodes.getNode(n.aeConfig);
        }
        else {
            node.status({ shape: "ring", fill: "red", text: 'Issue with AE config' });
            return;
        }

        if (n.acpConfig && n.acpConfig != "") {
            this.ACP_CONFIG = RED.nodes.getNode(n.acpConfig);
        }
        else {
            this.ACP_CONFIG = false;
        }

        if (n.viewType == "other") {
            this.cntName = n.viewunid1;
        }
        else {
            this.cntName = n.cntName;
        }
        this.mni = n.mni;




        this.on('input', function (msg) {

            if (RED.settings.httpRequestTimeout) {
                this.reqTimeout = parseInt(RED.settings.httpRequestTimeout) || 120000;
            } else {
                this.reqTimeout = 120000;
            }
            var node = this;
            node.status({});
            var adminOriginator = RED.nodes.getNode(node.CSE_CONFIG.adminOriginator).originator ;
            node.status({
                fill: "blue",
                shape: "dot",
                text: "httpin.status.requesting"
            });

            var acpidtoadd = '';
            if (this.ACP_CONFIG) {
                // Retrieving the acp ID from the ACP CONFIG
                acpidtoadd = this.ACP_CONFIG.acpId;
            }

            // preparing http request
            var url = this.CSE_CONFIG.poa + "/~/" + this.CSE_CONFIG.cseId + '/' + this.CSE_CONFIG.cseName;
            if (this.AE_CONFIG) {
                url = url + '/' + this.AE_CONFIG.aeName;
            }
            
            var request = require('request');
            var ri = "NR-" + Date.now();
            var options = {
                'method': 'GET',
                'url': url + '/' + node.cntName,
                'headers': {
                    'X-M2M-RI': ri,
                    'X-M2M-Origin': adminOriginator,
                    'X-M2M-RVI': '3',
                    'accept': 'application/json'
                }
            };

            request(options, function(error, response) {

                var requestMethod = 'POST';
                var contentType = 'application/json';

                if (error) {
                    console.error(error);
                    node.status({ shape: "ring", fill: "red", text: "error" });
                    msg.error = error;
                    msg.payload = error.code;
                }
                else {
                    if (response.statusCode == 200) {
                        requestMethod = 'PUT';
                        console.log("CNT Resource already exists, updating it...");
                    }
                    else
                    {
                        node.status({ shape: "ring", fill: "red", text: 'rsc: ' + response.headers['x-m2m-rsc'] });
                        if (response.statusCode == 403)
                        {
                            msg.error = response.statusCode;
                            msg.payload = response.headers['x-m2m-rsc'];
                            node.send(msg);
                            return;
                        }
                    }
                }
                var reqBody = {};
                reqBody['m2m:cnt'] = { 'mni': this.mni };
                var cntObj = reqBody['m2m:cnt'];
                if (requestMethod == 'POST') {
                    cntObj['rn'] = node.cntName;
                    contentType = contentType + ';ty=3';
                }
                else { 
                    url = url + '/' + node.cntName; 
                }

                if (acpidtoadd && acpidtoadd != '') {
                    cntObj['acpi'] = [];
                    cntObj.acpi[0] = acpidtoadd;
                }

                // preparing http request

                var ri = "NR-" + Date.now();
                
                var options = {
                    'method': requestMethod,
                    'url': url,
                    'headers': {
                        'X-M2M-RI': ri,
                        'X-M2M-Origin': adminOriginator,
                        'Content-Type': contentType
                    },
                    body: JSON.stringify(reqBody)
                };

                // sending the request
                request(options, function (error, response) {
                    if (error) {
                        console.error(error);
                        node.status({ shape: "ring", fill: "red", text: "error" });
                        msg.error = error;
                        msg.payload = error.code;
                    }
                    else {
                        msg.payload = response.body;
                        msg.headers = response.headers;
                        msg.statusCode = response.statusCode;
                        if (response.statusCode == 201 || response.statusCode == 200) {
                            node.status({ shape: "ring", fill: "green", text: 'rsc: ' + response.statusCode });
                        }
                        else {
                            node.status({ shape: "ring", fill: "red", text: 'rsc: ' + response.headers['x-m2m-rsc'] });
                        }
                    }
                    node.send(msg);
                }); // POST / PUT
            }); // GET
        }); // input
    }

    RED.nodes.registerType("Container Node", CreateCntHttp);
}
