
if [ -z "$sketch" ]; then
    if [ -z "$1" ]; then
        echo "usage: $0 <sketch.ino> <serial-port-dev>"
        exit 1
    fi
    sketch=$1
    shift
fi

if [ -z "$termport" ]; then
    if [ -z "$1" ]; then
        esptoolport=""
        termport="- 115200"
    else
        esptoolport="--port $1"
        termport="$1 115200"
        shift
    fi
fi

[ -z "$ARDUINO" ] && { echo "empty variable ARDUINO"; exit=1; }
[ -z "$ESP8266ARDUINO" ] && { echo "empty variable ESP8266ARDUINO"; exit=1; }
[ -z "${exit}" ] || exit $exit

BUILDPATH=/tmp/lom2mespbuild/$sketch
mkdir -p $BUILDPATH $BUILDPATH/cache

pwd=$(pwd)
