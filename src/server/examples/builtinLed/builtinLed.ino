/*
------------------------------------------------------------

SOFTWARE EVALUATION LICENSE

LAAS research laboratory, Toulouse, France.
IRIT computer science research laboratory, Toulouse, France.
------------------------------------------------------------

Contact: Thierry Monteil, thierry.monteil@irit.fr

Definitions
SOFTWARE:  The LightOM2M software, version 1.0, in source code form, written by David Gauchard, Guillaume Garzone and Thierry Monteil 
at the LAAS research laboratory.

LICENSOR: LE CENTRE NATIONAL DE LA RECHERCHE SCIENTIFIQUE, a public scientific and technical research establishment, having SIREN No. 180 089 013, APE code 7219 Z, having its registered office at 3, rue Michel-Ange, 75796 Paris cedex 16, France, acting in its own name and on its own behalf, and on behalf of Le laboratoire d'analyse et d'architecture des systèmes (LAAS), UPR n°8001.

1. INTENT/PURPOSE 
This agreement determines the conditions in which the LICENSOR, who has the rights to the SOFTWARE, grants to the LICENSEE a license for research and evaluation purposes only, excluding any commercial use.

2. LICENSEE 
Any person or organization who receives the SOFTWARE with a copy of this license. 

3. RIGHTS GRANTED
The rights to use and copy the SOFTWARE, subject to the restrictions described in this agreement.
The rights to modify and compile the SOFTWARE when it's provided in source code form, subject to the restrictions described in this agreement. 
For the SOFTWARE provided in binary form only, LICENSEE undertakes to not decompile, disassemble, decrypt, extract the components or perform reverse engineering except to the extent expressly provided by law.

5. SCOPE OF THE LICENSE 
- NON-COMMERCIAL license for research and evaluation purposes ONLY. 
- NO right to commercialize the SOFTWARE, or any derivative work, without separate agreement with the LICENSOR. 

6. MODIFICATION OF THE SOFTWARE
License permits LICENSEE to modify the SOFTWARE provided in source code form for research and evaluation purposes only. 

7. REDISTRIBUTION
- License permits LICENSEE to redistribute verbatim copies of the SOFTWARE, accompanied with a copy of this license. 
- License DOES NOT permit LICENSEE to redistribute modified versions of the SOFTWARE provided in source code form. 
- License DOES NOT permit LICENSEE to commercialize the SOFTWARE or any derivative work of the SOFTWARE. 

8. FEE/ROYALTY 
- LICENSEE pays no royalty for this license. 
- LICENSEE and any third parties must enter a new agreement for any use beyond scope of license. Please contact the IRIT technology transfer office (email numerique@toulouse-tech-transfer.com) for further information. 

9. NO WARRANTY 
The SOFTWARE is provided "as is" without warranty of any kind, either expressed or implied, including, but not limited to, the implied warranties of merchantability and fitness for a particular purpose. The entire risk as to the quality and performance of the program is with the LICENSEE. 

10. NO LIABILITY
In no event unless required by applicable law or agreed to in writing will any copyright owner be liable to LICENSEE for damages, including any general, special, incidental or consequential damages arising out of the use or inability to use the program (including but not limited to loss of data or data being rendered inaccurate or losses sustained by LICENSEE or third parties or a failure of the program to operate with other programs), even if such holder has been advised of the possibility of such damages.
*/


#define MDNSNAME "espom2m"

#include "IPE.h"
#include "JsonDatamapper.h"
#include "lom2m-server-base.h"
#include "Entity.h"

#define PERSISTENCE_TEST 0

#if PERSISTENCE_TEST
#include <LittleFS.h>
#endif

Millis blinkDurationMs = 1000; // milliseconds
Millis lastBlinkMs = 0; // date of last change

BinaryActuator* led;

void user_initial_setup()
{
    log("builtin-led-demo");
}

void user_final_setup()
{
    IPE* ipeTest = new IPE("IPE_led_builtin");
    led = new BinaryActuator(ipeTest->getIpeAe(), LED_BUILTIN, "LED_1");
    ipeTest->addActuator(led);

// #if PERSISTENCE_TEST
//     {
//         // test persistence
//         DynamicJsonDocument doc(500000);
//         JsonObject test = doc.to<JsonObject>();
//         JsonObject csb = test["m2m:csb"].to<JsonObject>();
//         Mapper::mapGenericAttributes(csb, CseBase::getInstance(), 0, false, true);
//         Mapper::mapResourceAttributes(csb, CseBase::getInstance(), 0, false, true);
//         Mapper::mapChildResources(csb, CseBase::getInstance(), 0, false, true);

//         // test["titi"] = "toto";
//         // JsonObject sub = test["sub"].to<JsonObject>();
//         // sub["toto"] = "titi";
//         // JsonArray arr = sub["tab"].to<JsonArray>();
//         // arr.add(1);
//         // arr.add(true);
//         // arr.add("hey");
//         Serial.println("store: what's written:");
//         serializeJsonPretty(test, Serial);
//         Serial.printf("persistence test: store=%d\n", persistenceStore(test, "test.json"));
//         Serial.println("\nstore: done");

//     }
//     {
//         LittleFS.begin();
//         File r = LittleFS.open("test.json", "r");
//         if (r)
//         {
//             int c;
//             while ((c = r.read()) != -1)
//                 Serial.print((char)c);
//             r.close();
//         }
//         else
//             Serial.println("cannot open file?");
//         Serial.println();
//     }
//     {
//         DynamicJsonDocument test(500000);
//         Serial.printf("persistence test: load=%d\n", persistenceLoad(test, "test.json"));
//         Serial.println(">>>>> loaded:");
//         serializeJsonPretty(test, Serial);
//         Serial.println("\n<<<<<");
//     }
// #endif

}

void user_loop()
{
    Millis nowMs = millis();

    if (nowMs - lastBlinkMs > blinkDurationMs)
    {
        //led->toggle();
        lastBlinkMs = nowMs;
    }
}
