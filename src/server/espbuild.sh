
. espcommon.sh

MHz=80
#MHz=160

esp=esp8266com
hardw=~/.arduino15/packages
if [ "$(uname)" = Darwin ]; then
    esp=esp8266
    hardw=~/Library/Arduino15/packages
fi

$ARDUINO/arduino-builder \
    -compile \
    -logger=human \
    -hardware $ARDUINO/hardware \
    -hardware ${hardw} \
    -tools $ARDUINO/tools-builder \
    -tools $ARDUINO/hardware/tools/avr \
    -tools ${hardw} \
    -built-in-libraries $ARDUINO/libraries \
    -libraries $(pwd)/libraries \
    -fqbn=${esp}:esp8266:generic:xtal=${MHz},vt=flash,exception=legacy,ssl=all,ResetMethod=nodemcu,CrystalFreq=26,FlashFreq=40,FlashMode=qio,eesz=4M2M,led=2,ip=hb6f,dbg=Disabled,lvl=None____,wipe=none,baud=921600 \
    -build-path $BUILDPATH \
    -warnings=all \
    -build-cache $BUILDPATH/cache \
    -prefs=build.warn_data_percentage=75 -verbose \
    $sketch
