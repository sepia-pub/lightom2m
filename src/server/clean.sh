for i in libraries libraries/* examples/*; do
    (cd $i; rm -f *~ *.gcno *.gcda *.o ext-*/*.o)
done

[ -z "$ESP8266ARDUINO" ] && { echo "empty variable ESP8266ARDUINO"; exit 1; }

cd $ESP8266ARDUINO/tests/host/
make clean

