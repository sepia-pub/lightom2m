
. espcommon.sh

baud=460800
if [ "$(uname)" = Linux ]; then
    baud=115200
fi
python3 $ESP8266ARDUINO/tools/esptool/esptool.py $esptoolport --baud ${baud} write_flash 0 $BUILDPATH/*.bin && miniterm.py $termport
