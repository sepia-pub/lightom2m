
[ -z "$ESP8266ARDUINO" ] && { echo "empty variable ESP8266ARDUINO"; exit 1; }

[ -z "$ino" ] && ino=builtinLed/builtinLed

pwd=$(pwd)
(
	cd $ESP8266ARDUINO/tests/host/

	# hack hack hack hack
	# cross compilation for mongOH red (__arm__ is defined)
	# live patch SdFatConfig to force USE_FCNTL_H to 0
	(echo "#undef USE_FCNTL_H"; echo "#define USE_FCNTL_H 0"; ) >> ../../libraries/ESP8266SdFat/src/SdFatConfig.h

    VERBOSE=1
    DEBUG=1
    #PAR=-j

	make ${PAR} D=${DEBUG} V=${VERBOSE} FORCE32=0 ULIBDIRS=$pwd:$pwd/libraries/lom2m:$pwd/libraries/ext-ArduinoJson6:$pwd/libraries/ext-NTPClient:$pwd/libraries/ext-pubsub $pwd/examples/$ino 
)
