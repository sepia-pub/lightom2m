/*
------------------------------------------------------------

SOFTWARE EVALUATION LICENSE

LAAS research laboratory, Toulouse, France.
IRIT computer science research laboratory, Toulouse, France.
------------------------------------------------------------

Contact: Thierry Monteil, thierry.monteil@irit.fr

Definitions
SOFTWARE:  The LightOM2M software, version 1.0, in source code form, written by David Gauchard, Guillaume Garzone and Thierry Monteil
at the LAAS research laboratory.

LICENSOR: LE CENTRE NATIONAL DE LA RECHERCHE SCIENTIFIQUE, a public scientific and technical research establishment, having SIREN No. 180 089 013, APE code 7219 Z, having its registered office at 3, rue Michel-Ange, 75796 Paris cedex 16, France, acting in its own name and on its own behalf, and on behalf of Le laboratoire d'analyse et d'architecture des systèmes (LAAS), UPR n°8001.

1. INTENT/PURPOSE
This agreement determines the conditions in which the LICENSOR, who has the rights to the SOFTWARE, grants to the LICENSEE a license for research and evaluation purposes only, excluding any commercial use.

2. LICENSEE
Any person or organization who receives the SOFTWARE with a copy of this license.

3. RIGHTS GRANTED
The rights to use and copy the SOFTWARE, subject to the restrictions described in this agreement.
The rights to modify and compile the SOFTWARE when it's provided in source code form, subject to the restrictions described in this agreement.
For the SOFTWARE provided in binary form only, LICENSEE undertakes to not decompile, disassemble, decrypt, extract the components or perform reverse engineering except to the extent expressly provided by law.

5. SCOPE OF THE LICENSE
- NON-COMMERCIAL license for research and evaluation purposes ONLY.
- NO right to commercialize the SOFTWARE, or any derivative work, without separate agreement with the LICENSOR.

6. MODIFICATION OF THE SOFTWARE
License permits LICENSEE to modify the SOFTWARE provided in source code form for research and evaluation purposes only.

7. REDISTRIBUTION
- License permits LICENSEE to redistribute verbatim copies of the SOFTWARE, accompanied with a copy of this license.
- License DOES NOT permit LICENSEE to redistribute modified versions of the SOFTWARE provided in source code form.
- License DOES NOT permit LICENSEE to commercialize the SOFTWARE or any derivative work of the SOFTWARE.

8. FEE/ROYALTY
- LICENSEE pays no royalty for this license.
- LICENSEE and any third parties must enter a new agreement for any use beyond scope of license. Please contact the IRIT technology transfer office (email numerique@toulouse-tech-transfer.com) for further information.

9. NO WARRANTY
The SOFTWARE is provided "as is" without warranty of any kind, either expressed or implied, including, but not limited to, the implied warranties of merchantability and fitness for a particular purpose. The entire risk as to the quality and performance of the program is with the LICENSEE.

10. NO LIABILITY
In no event unless required by applicable law or agreed to in writing will any copyright owner be liable to LICENSEE for damages, including any general, special, incidental or consequential damages arising out of the use or inability to use the program (including but not limited to loss of data or data being rendered inaccurate or losses sustained by LICENSEE or third parties or a failure of the program to operate with other programs), even if such holder has been advised of the possibility of such damages.
*/


#include "Notify.h"
#include "RequestPrimitive.h"
#include "httpBinding.h"
#include "lom2m.h"
#include "ShortNames.h"
#include "RequestPrimitive.h"
#include "tools.h"

#if SUBSCRIPTION_FEAT

Notification::Notification()
{
    verificationRequest = false;
    subDeletion = false;
    net = NET_UPDATE_RES;
}

NotifyBuffer::NotifyBuffer()
{
    notificationBuffer.clear();
}

NotifyBuffer::~NotifyBuffer()
{
}

NotifyBuffer* NotifyBuffer::bufferInstance = nullptr;


/**
 * Add notifications to the buffer for the given parameters
 * @param subs - the subscriptions to check
 * @param entity - the target entity of the event
 * @param net - notification event type to check
 * @param operation - the operation received on the target entity
 */
void
Notifier::notify(std::list<Subscription*> subs, Entity* entity, NotificationEventType net, Operation operation)
{
    if (!entity)
    {
        return;
    }
    String representation;
    for (Subscription* sub : subs)
    {
#if DEBUG
#endif
        representation = emptyString;
        if (sub->getNotificationContentType() == NCT_ALL_ATTRIBUTES)
        {
            //case NCT_MODIFIED_ATTRIBUTES: // TODO TO UPDATE ONCE RCN 9 IMPLEMENTED
            jsonglobal.clear();
            RequestPrimitive req;
            req.createReport(entity, RCN_ATTR, 1, OP_NULL, true);
            if(!serializeJson(jsonglobal, representation))
            {
                Serial.printf("ERROR WITH JSON SERIALIZATION NOTIFICATION\n");
                return ;
            }
        }
        for (String url : sub->getNotificationURI())
        {
            // build notification
            Notification* notif = new Notification();
            NotificationToSend* toSend = new NotificationToSend();
            notif->setResource(representation);
            notif->setOriginator(ADMIN_ORIGINATOR);
            notif->setNet(net);
            notif->setOperation(operation);
            notif->setSubReference(sub->getResourceIdentifier());
            // add notification to buffer
            toSend->setNotif(notif);
            toSend->setUrl(url);
            toSend->setNct(sub->getNotificationContentType());
            NotifyBuffer::getInstance()->addNotification(toSend);
        }
    }

    // return
}


NotificationToSend* notif;
std::list<String> urls;
RequestPrimitive* requestToSend;
ResponsePrimitive* response;
/**
 * Handle pending notifications in the notification buffer
 * @param max - the maximum number of notifications handled before returning
 */
void
handleNotifications(int max)
{
    if (NotifyBuffer::getInstance()->isEmpty())
    {
        return;
    }
#if DEBUG
    printf("DEBUG: handling pending notifications...\n");
#endif
    // max defines maximum number of notifications to send in one loop.
    // pop notification from buffer
    notif = NotifyBuffer::getInstance()->popNotification();
    // send notification (request)
    urls.clear();
    String url = notif->getUrl();
    String aeid = emptyString;
    Entity* e = nullptr;
    if (url.startsWith("/"+CSE_NAME) && !url.startsWith("/"+CSE_ID))
    {
        e = Entity::getByHierUri(url);
        if (!e || !e->getApplication())
        {
            printf("DEBUG: error while sending notify, AE not found %s\n", notif->getUrl().c_str());
            delete (notif);
            return;
        }
        else
        {
            urls = (e->getApplication())->m_poa;
            aeid = e->getApplication()->getIdentifier();
        }
    }
    else if (url.startsWith("http://"))
    {
        urls.push_back(url);
    }
    else
    {
        e = Entity::getByIdentifier(url);
        // Application* ae =
        if (!e || !e->getApplication())
        {
            printf("DEBUG: error while sending notify, AE not found %s\n", notif->getUrl().c_str());
            delete (notif);
            return;
        }
        else
        {
            urls = (e->getApplication())->m_poa;
            aeid = e->getApplication()->getIdentifier();
        }
    }
    for (String url : urls)
    {
#if DEBUG
          printf("DEBUG: initialising request\n");
#endif
        requestToSend = new RequestPrimitive();
        // create request primitive
        requestToSend->init();
        response = new ResponsePrimitive();
        String result = requestToSend->createNotifyBody(notif->getNotif(), notif->getNct());
#if DEBUG
        printf("DEBUG: BODY %s\n", result.c_str());
#endif
        requestToSend->setContent(result);
        requestToSend->setFrom(CSE_ID);
#if DEBUG
        printf("DEBUG: Sending notify to: %s\n", url.c_str());
#endif
        // get string representation for json notification
        requestToSend->setResourceType(TY_NOTIFICATION);
        requestToSend->setRequestContentTypeRaw("application/json");
        requestToSend->setRequestId(String((long)get_utime()));
        requestToSend->setTo(url);
        requestToSend->setOperation(OP_NOTIFY);
#if DEBUG
        printf("DEBUG: sending notify...\n");
#endif
        if (url.startsWith(F("http")))
        {
            HTTPBinding::sendRequest(requestToSend, response);
        }
#if DEBUG
        printf("DEBUG: notify sent, analysing response\n");
#endif
        if (response->getResponseStatusCode() != R2000_OK || response->getResponseStatusCode() != R204_NO_CONTENT)
        {
            notif->increaseFailed();
            // TODO put notif back in buffer ?
        }
#if DEBUG
        printf("DEBUG: notify RSC : %d\n", response->getResponseStatusCode());
        if(response->getResponseStatusCode() == R2000_OK)
        {
            break;
        }
#endif 
    }
        // cleaning variables
        delete (response);
        delete (notif);
        delete (requestToSend);
#if DEBUG
    printf("DEBUG: notify, ended\n");
#endif
}

#endif // SUBSCRIPTION_FEAT
