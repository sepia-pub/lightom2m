/*
------------------------------------------------------------

SOFTWARE EVALUATION LICENSE

LAAS research laboratory, Toulouse, France.
IRIT computer science research laboratory, Toulouse, France.
------------------------------------------------------------

Contact: Thierry Monteil, thierry.monteil@irit.fr

Definitions
SOFTWARE:  The LightOM2M software, version 1.0, in source code form, written by David Gauchard, Guillaume Garzone and Thierry Monteil
at the LAAS research laboratory.

LICENSOR: LE CENTRE NATIONAL DE LA RECHERCHE SCIENTIFIQUE, a public scientific and technical research establishment, having SIREN No. 180 089 013, APE code 7219 Z, having its registered office at 3, rue Michel-Ange, 75796 Paris cedex 16, France, acting in its own name and on its own behalf, and on behalf of Le laboratoire d'analyse et d'architecture des systèmes (LAAS), UPR n°8001.

1. INTENT/PURPOSE
This agreement determines the conditions in which the LICENSOR, who has the rights to the SOFTWARE, grants to the LICENSEE a license for research and evaluation purposes only, excluding any commercial use.

2. LICENSEE
Any person or organization who receives the SOFTWARE with a copy of this license.

3. RIGHTS GRANTED
The rights to use and copy the SOFTWARE, subject to the restrictions described in this agreement.
The rights to modify and compile the SOFTWARE when it's provided in source code form, subject to the restrictions described in this agreement.
For the SOFTWARE provided in binary form only, LICENSEE undertakes to not decompile, disassemble, decrypt, extract the components or perform reverse engineering except to the extent expressly provided by law.

5. SCOPE OF THE LICENSE
- NON-COMMERCIAL license for research and evaluation purposes ONLY.
- NO right to commercialize the SOFTWARE, or any derivative work, without separate agreement with the LICENSOR.

6. MODIFICATION OF THE SOFTWARE
License permits LICENSEE to modify the SOFTWARE provided in source code form for research and evaluation purposes only.

7. REDISTRIBUTION
- License permits LICENSEE to redistribute verbatim copies of the SOFTWARE, accompanied with a copy of this license.
- License DOES NOT permit LICENSEE to redistribute modified versions of the SOFTWARE provided in source code form.
- License DOES NOT permit LICENSEE to commercialize the SOFTWARE or any derivative work of the SOFTWARE.

8. FEE/ROYALTY
- LICENSEE pays no royalty for this license.
- LICENSEE and any third parties must enter a new agreement for any use beyond scope of license. Please contact the IRIT technology transfer office (email numerique@toulouse-tech-transfer.com) for further information.

9. NO WARRANTY
The SOFTWARE is provided "as is" without warranty of any kind, either expressed or implied, including, but not limited to, the implied warranties of merchantability and fitness for a particular purpose. The entire risk as to the quality and performance of the program is with the LICENSEE.

10. NO LIABILITY
In no event unless required by applicable law or agreed to in writing will any copyright owner be liable to LICENSEE for damages, including any general, special, incidental or consequential damages arising out of the use or inability to use the program (including but not limited to loss of data or data being rendered inaccurate or losses sustained by LICENSEE or third parties or a failure of the program to operate with other programs), even if such holder has been advised of the possibility of such damages.
*/


#include "PersistenceHelper.h"
#include "Entity.h"
#include "JsonDatamapper.h"
#include "bsp.h"
#include "configuration.h"
#include "lom2m.h"
#include "tools.h"

#include <LittleFS.h>

#if PERSISTENCE_FEAT
/**
 * Load entity from json element loaded in memory
 * @param JsonObject root element of the object
 * @param Entity* target/parent of the entity to load, null if CSE BASE
 * @return int value different from 0 if error occured
 */
int
loadEntitiesFromJson(JsonObject root, Entity* target) //TODO REMOVE TARGET, USELESS
{
    int result = 0;
    if (root.containsKey(rqType(TY1_ACP)))
    {
        for (JsonObject o : (JsonArray)root[rqType(TY1_ACP)])
        {
#if DEBUG
            Serial.printf("DEBUG: Adding ACP name:%s, parent: %s, ri: %s\n", (const char*)o[RESOURCE_NAME], (const char*)o[PARENT_ID], (const char*)o[RESOURCE_ID]);
#endif
            AccessControlPolicy* a = ACPMapper::parseResource(o, (const String&)o[RESOURCE_NAME]);
            result += Mapper::parseGenericAttributes(o, a);
            CseBase::getInstance()->children.push_back(a);
        }
    }
    else if (root.containsKey(rqType(TY2_APPL_ENTITY)))
    {
        for (JsonObject o : (JsonArray)root[rqType(TY2_APPL_ENTITY)])
        {
#if DEBUG
            Serial.printf("DEBUG: Adding AE name:%s, parent: %s\n", (const char*)o[RESOURCE_NAME], (const char*)o[PARENT_ID]);
#endif
            Application* ae = AEMapper::parseResource(o, (const String&)o[RESOURCE_NAME]);
            result += Mapper::parseGenericAttributes(o, ae);
            CseBase::getInstance()->children.push_back(ae);
        }
    }
    else if (root.containsKey(rqType(TY3_CONTAINER)))
    {
        for (JsonObject o : (JsonArray)root[rqType(TY3_CONTAINER)])
        {
#if DEBUG
            Serial.printf("DEBUG: Adding CNT name:%s, parent: %s\n", (const char*)o[RESOURCE_NAME], (const char*)o[PARENT_ID]);
#endif
            target = Entity::getByIdentifier((const String&)o[PARENT_ID]);
            if (target)
            {
                Container* cnt = ContainerMapper::parseResource(o, (const String&)o[RESOURCE_NAME], target);
                result += Mapper::parseGenericAttributes(o, cnt);
                target->children.push_back(cnt);
            }
            else
            {
                result += 1;
            }
        }
    }
    else if (root.containsKey(rqType(TY4_CONTENT_INSTANCE)))
    {
        for (JsonObject o : (JsonArray)root[rqType(TY4_CONTENT_INSTANCE)])
        {
#if DEBUG
            Serial.printf("DEBUG: Adding CIN name:%s, parent: %s\n", (const char*)o[RESOURCE_NAME], (const char*)o[PARENT_ID]);
#endif
            target = Entity::getByIdentifier((const String&)o[PARENT_ID]);
            if (target)
            {
                ContentInstance* cin = ContentInstanceMapper::parseResource(o, (const String&)o[RESOURCE_NAME], target);
                result += Mapper::parseGenericAttributes(o, cin);
                target->children.push_back(cin);
            }
            else
            {
                result += 1;
            }
        }
    }
    else if (root.containsKey(rqType(TY5_CSEBASE)))
    {
#if DEBUG
        Serial.printf("DEBUG: Adding CSB\n");
#endif
        CseBase* csb = CseBase::getInstance();
        result += Mapper::parseGenericAttributes(root[rqType(TY5_CSEBASE)], csb);
    }
#if GROUP_FEAT
    else if (root.containsKey(rqType(TY9_GROUP)))
    {
        for (JsonObject o : (JsonArray)root[rqType(TY9_GROUP)])
        {
            target = Entity::getByIdentifier((const String&)o[PARENT_ID]);
            if (target)
            {
                Group* g = GroupMapper::parseResource(o, (const String&)o[RESOURCE_NAME], target);
                result += Mapper::parseGenericAttributes(o, g);
                target->children.push_back(g);
            }
            else
            {
                result += 1;
            }
        }
    }
#endif
    else if (root.containsKey(rqType(TY16_REMOTE_CSE)))
    {
        for (JsonObject o : (JsonArray)root[rqType(TY16_REMOTE_CSE)])
        {
#if DEBUG
            Serial.printf("DEBUG: Adding CSR name:%s, parent: %s\n", (const char*)o[RESOURCE_NAME], (const char*)o[PARENT_ID]);
#endif
            target = Entity::getByIdentifier((const String&)o[PARENT_ID]);
            if (target)
            {
                RemoteCse* csr = RemoteCseMapper::parseResource(o, (const String&)o[RESOURCE_NAME], target);
                result += Mapper::parseGenericAttributes(o, csr);
                target->children.push_back(csr);
            }
            else
            {
                result += 1;
            }
        }
    }
    else if (root.containsKey(rqType(TY23_SUBSCRIPTION)))
    {
        for (JsonObject o : (JsonArray)root[rqType(TY23_SUBSCRIPTION)])
        {
#if DEBUG
            Serial.printf("DEBUG: Adding SUB name:%s, parent: %s\n", (const char*)o[RESOURCE_NAME], (const char*)o[PARENT_ID]);
#endif
            target = Entity::getByIdentifier((const String&)o[PARENT_ID]);
            if (target)
            {
                Subscription* sub = SubscriptionMapper::parseResource(o, (const String&)o[RESOURCE_NAME], target);
                result += Mapper::parseGenericAttributes(o, sub);
                target->children.push_back(sub);
            }
            else
            {
                result += 1;
            }
        }
    }

    return result;
}

PersistenceHelper::PersistenceErrorCode
PersistenceHelper::persistAll()
{
    int res = 0;
    // test persistence
    for (int i : SUPPORTED_RESTYPE)
    {
#if INFO
        Serial.printf("INFO: PERSISTING ty : %d\n", i);
#endif // INFO
        jsonglobal.clear();
        JsonObject test = jsonglobal.to<JsonObject>();
        JsonObject csb = test[rqType(TY5_CSEBASE)].to<JsonObject>();
        if (i == 1)
        {
            Mapper::mapGenericAttributes(csb, CseBase::getInstance(), 0, OP_NULL, true);
            Mapper::mapResourceAttributes(csb, CseBase::getInstance(), 0, OP_NULL, true);
        }

        for (Entity* e : Entity::entities)
        {
            if (e->getType() != 5 && e->getType() == i)
            {
                JsonArray chArray;
                if (csb.containsKey(rqType(e->getType())))
                {
                    chArray = csb[rqType(e->getType())];
                }
                else
                {
                    chArray = csb[rqType(e->getType())].to<JsonArray>();
                }
                JsonObject o = chArray.createNestedObject();
                Mapper::mapGenericAttributes(o, e, 0, OP_NULL, true);
                Mapper::mapResourceAttributes(o, e, 0, OP_NULL, true);
            }
        }

#if INFO
        Serial.printf("INFO: persisting data...\n");
#endif
#if DEBUG
        Serial.printf("DEBUG: persistence store: what's written:");
        serializeJsonPretty(test, Serial);
#endif
        String filename = String(i) + ".json";
        res += persistenceStore(test, filename.c_str());
#if INFO
        Serial.printf("INFO: persistence test: store=%d\n", res);
    }
    Serial.printf("INFO: persistence store: done\n");
#endif
    if (res > 0)
        return PersistenceHelper::P_SUCCESS;
    else
        return PersistenceHelper::P_ERROR_STORING_DATA;
}

PersistenceHelper::PersistenceErrorCode
PersistenceHelper::loadAll()
{
#if INFO
    Serial.printf("INFO: loading persistence data...\n");
#endif
    for (int i : SUPPORTED_RESTYPE)
    {
        Serial.printf("INFO: loading persistence data type: %d\n", i);
        String filename = String(i) + ".json";
        LittleFS.begin();
        File r = LittleFS.open(filename.c_str(), "r");
        if (r)
        {
#if DEBUG
            int c;
            while ((c = r.read()) != -1)
                Serial.print((char)c);
            r.close();
#endif
        }
        else
        {
            Serial.printf("ERROR: cannot open persistence file?\n");
            return PersistenceHelper::P_FILE_NOT_FOUND;
        }
        jsonglobal.clear();
        printf("\n");
        JsonObject t = jsonglobal.to<JsonObject>();
        auto result = persistenceLoad(jsonglobal, filename.c_str());
#if DEBUG
        Serial.printf("\n\nDEBUG: loading return code %d\n", result);
#endif
        if (result != DeserializationError::Ok)
            return PersistenceHelper::P_ERROR_PARSING_JSON_FILE;
#if INFO
        Serial.printf("INFO: persistence... data loaded.\n");
#endif
#if DEBUG
        Serial.printf("INFO: persistence JSON loaded:\n");
        serializeJsonPretty(jsonglobal, Serial);
#endif
        // json loaded, now parsing it
        // look for CSB, must be present
        if (jsonglobal.containsKey(rqType(TY5_CSEBASE)))
        {
            Serial.printf("INFO: Loading resources...\n");
            JsonObject root = jsonglobal[rqType(TY5_CSEBASE)];
            Mapper::parseGenericAttributes(root, CseBase::getInstance());

            if (loadEntitiesFromJson(root, nullptr) != 0)
                return PersistenceHelper::P_ERROR_LOADING_DATA;
        }
        else
        {
            Serial.printf("ERROR: CSB key not found in json document\n");
            return PersistenceHelper::P_ERROR_LOADING_ROOT_ELEMENT;
        }
    }
    return PersistenceHelper::P_SUCCESS;
}

#endif // PERSISTENCE_FEAT
