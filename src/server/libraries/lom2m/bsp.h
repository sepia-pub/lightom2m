/*
------------------------------------------------------------

SOFTWARE EVALUATION LICENSE

LAAS research laboratory, Toulouse, France.
IRIT computer science research laboratory, Toulouse, France.
------------------------------------------------------------

Contact: Thierry Monteil, thierry.monteil@irit.fr

Definitions
SOFTWARE:  The LightOM2M software, version 1.0, in source code form, written by David Gauchard, Guillaume Garzone and Thierry Monteil
at the LAAS research laboratory.

LICENSOR: LE CENTRE NATIONAL DE LA RECHERCHE SCIENTIFIQUE, a public scientific and technical research establishment, having SIREN No. 180 089 013, APE code 7219 Z, having its registered office at 3, rue Michel-Ange, 75796 Paris cedex 16, France, acting in its own name and on its own behalf, and on behalf of Le laboratoire d'analyse et d'architecture des systèmes (LAAS), UPR n°8001.

1. INTENT/PURPOSE
This agreement determines the conditions in which the LICENSOR, who has the rights to the SOFTWARE, grants to the LICENSEE a license for research and evaluation purposes only, excluding any commercial use.

2. LICENSEE
Any person or organization who receives the SOFTWARE with a copy of this license.

3. RIGHTS GRANTED
The rights to use and copy the SOFTWARE, subject to the restrictions described in this agreement.
The rights to modify and compile the SOFTWARE when it's provided in source code form, subject to the restrictions described in this agreement.
For the SOFTWARE provided in binary form only, LICENSEE undertakes to not decompile, disassemble, decrypt, extract the components or perform reverse engineering except to the extent expressly provided by law.

5. SCOPE OF THE LICENSE
- NON-COMMERCIAL license for research and evaluation purposes ONLY.
- NO right to commercialize the SOFTWARE, or any derivative work, without separate agreement with the LICENSOR.

6. MODIFICATION OF THE SOFTWARE
License permits LICENSEE to modify the SOFTWARE provided in source code form for research and evaluation purposes only.

7. REDISTRIBUTION
- License permits LICENSEE to redistribute verbatim copies of the SOFTWARE, accompanied with a copy of this license.
- License DOES NOT permit LICENSEE to redistribute modified versions of the SOFTWARE provided in source code form.
- License DOES NOT permit LICENSEE to commercialize the SOFTWARE or any derivative work of the SOFTWARE.

8. FEE/ROYALTY
- LICENSEE pays no royalty for this license.
- LICENSEE and any third parties must enter a new agreement for any use beyond scope of license. Please contact the IRIT technology transfer office (email numerique@toulouse-tech-transfer.com) for further information.

9. NO WARRANTY
The SOFTWARE is provided "as is" without warranty of any kind, either expressed or implied, including, but not limited to, the implied warranties of merchantability and fitness for a particular purpose. The entire risk as to the quality and performance of the program is with the LICENSEE.

10. NO LIABILITY
In no event unless required by applicable law or agreed to in writing will any copyright owner be liable to LICENSEE for damages, including any general, special, incidental or consequential damages arising out of the use or inability to use the program (including but not limited to loss of data or data being rendered inaccurate or losses sustained by LICENSEE or third parties or a failure of the program to operate with other programs), even if such holder has been advised of the possibility of such damages.
*/


#ifndef __LOM2M_BSP_H
#define __LOM2M_BSP_H

#include <WString.h>
#include <ArduinoJson.h>
#include "ResponsePrimitive.h"

enum HTTPCode
{
    H000 = 0,
    H200_OK = 200,
    H201_Created = 201,
    H204_NoContent = 204,
    H400_BadRequest = 400,
    H401_Unauthorized = 401,
    H403_Forbidden = 403,
    H404_NotFound = 404,
    H405_NotAllowed = 405,
    H406_NotAcceptable = 406,
    H408_RequestTimeout = 408,
    H409_Conflict = 409,
    H415_Unsupported = 415,
    H500_InternalError = 500,
    H501_NotImplemented = 501,
    H503_Maintenance = 503,
};

enum Encoding
{
    ENC_NONE = -1,
    ENC_JSON,
};

// BSP must define:
//enum HTTPMethod { HTTP_ANY, HTTP_GET, HTTP_POST, HTTP_PUT, HTTP_PATCH, HTTP_DELETE, HTTP_OPTIONS };

#ifdef ESP8266
#include "bsp-esp8266.h"
#endif

class ResponsePrimitive;

const String HTTPCode2Human(HTTPCode code);

HTTPMethod method();

const char* method(HTTPMethod m);

[[deprecated("Replaced by the same function using Response Primitive")]]
void sendReply(HTTPCode code, const String& contentType, const String& message);

void sendReply(ResponsePrimitive& resp, const String& contentType, const String& message);

inline void sendReply(ResponsePrimitive& resp, const String& message)
{
    return sendReply(resp, String(F("text/plain")), message);
};

const String& HTTPContent();
template <typename T> const String& HTTPHeader(const T& attribute);
template <typename T> const String& HTTPArg(const T& key);
template <typename T> inline bool HTTPHasArg(const T& key);

/////////////////////////////////////////////
typedef uint64_t utime_t;
utime_t get_utime();
String get_ctime(time_t t);
/**
 * Get the time given in parameter.
 * Use the oneM2M format %04d%02d%02dT%02d%02d%02d
 * @param t - time as String in oneM2M format
 * @return time in the used structure
 */
utime_t getTimeFromString(String t);

extern const String emptyString;
extern utime_t default_expiration_duration;

constexpr char listSeparator = '#'; // internal common

void log_setup();
void log(const char* fmt, ...);

inline void logStream(Stream* in)
{
    while (in && in->available())
    {
        log("%c", in->read());
    }
}

/////////////////////////////////////////////////

void printHuman(Stream& to, int level, const char* tag, const char* data);
void printHumanSerial(int level, const char* tag, const char* data);
String getMac();

using printfmt = std::function<void(int level, const char* tag, const char* data)>;

/////////////////////////////////////////////////

using Millis = decltype(millis());

/////////////////////////////////////////////////

extern const char* defaultPersistenceName;

/**
 * Enable to open the file system
 * @return true if success, false if failure
 */
bool FSOpen ();
/**
 * Enable to close the file system
 * @return true if success, false if failure
 */
void FSClose ();

/**
 * Enable to store in a json file the whole json object provided
 * This open and close file system to do so
 * @param toStore json object to store in the file
 * @param name (optional) name of the file where the data will be stored
 * @return error code, -1: error, >= 0 size of written data
 */
int persistenceStore(JsonObject toStore, const char* name = defaultPersistenceName);

/**
 * Enable to load data from a file into a jsondocument (provided)
 * This open and close file system to do so
 * @return errorCode (see src/ArduinoJson/Deserialization/DeserializationError.hpp)
 *  - DeserializationError::Ok
 *  - DeserializationError::IncompleteInput
 *  - DeserializationError::InvalidInput
 *  - DeserializationError::NoMemory
 *  - DeserializationError::NotSupported  <-- + file or filesystem error
 *  - DeserializationError::TooDeep
 */
DeserializationError persistenceLoad(JsonDocument& toStore, const char* name = defaultPersistenceName);


/////////////////////////////////////////////////

#endif // __LOM2M_BSP_H
