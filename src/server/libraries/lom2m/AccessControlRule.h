/*
------------------------------------------------------------

SOFTWARE EVALUATION LICENSE

LAAS research laboratory, Toulouse, France.
IRIT computer science research laboratory, Toulouse, France.
------------------------------------------------------------

Contact: Thierry Monteil, thierry.monteil@irit.fr

Definitions
SOFTWARE:  The LightOM2M software, version 1.0, in source code form, written by David Gauchard, Guillaume Garzone and Thierry Monteil
at the LAAS research laboratory.

LICENSOR: LE CENTRE NATIONAL DE LA RECHERCHE SCIENTIFIQUE, a public scientific and technical research establishment, having SIREN No. 180 089 013, APE code 7219 Z, having its registered office at 3, rue Michel-Ange, 75796 Paris cedex 16, France, acting in its own name and on its own behalf, and on behalf of Le laboratoire d'analyse et d'architecture des systèmes (LAAS), UPR n°8001.

1. INTENT/PURPOSE
This agreement determines the conditions in which the LICENSOR, who has the rights to the SOFTWARE, grants to the LICENSEE a license for research and evaluation purposes only, excluding any commercial use.

2. LICENSEE
Any person or organization who receives the SOFTWARE with a copy of this license.

3. RIGHTS GRANTED
The rights to use and copy the SOFTWARE, subject to the restrictions described in this agreement.
The rights to modify and compile the SOFTWARE when it's provided in source code form, subject to the restrictions described in this agreement.
For the SOFTWARE provided in binary form only, LICENSEE undertakes to not decompile, disassemble, decrypt, extract the components or perform reverse engineering except to the extent expressly provided by law.

5. SCOPE OF THE LICENSE
- NON-COMMERCIAL license for research and evaluation purposes ONLY.
- NO right to commercialize the SOFTWARE, or any derivative work, without separate agreement with the LICENSOR.

6. MODIFICATION OF THE SOFTWARE
License permits LICENSEE to modify the SOFTWARE provided in source code form for research and evaluation purposes only.

7. REDISTRIBUTION
- License permits LICENSEE to redistribute verbatim copies of the SOFTWARE, accompanied with a copy of this license.
- License DOES NOT permit LICENSEE to redistribute modified versions of the SOFTWARE provided in source code form.
- License DOES NOT permit LICENSEE to commercialize the SOFTWARE or any derivative work of the SOFTWARE.

8. FEE/ROYALTY
- LICENSEE pays no royalty for this license.
- LICENSEE and any third parties must enter a new agreement for any use beyond scope of license. Please contact the IRIT technology transfer office (email numerique@toulouse-tech-transfer.com) for further information.

9. NO WARRANTY
The SOFTWARE is provided "as is" without warranty of any kind, either expressed or implied, including, but not limited to, the implied warranties of merchantability and fitness for a particular purpose. The entire risk as to the quality and performance of the program is with the LICENSEE.

10. NO LIABILITY
In no event unless required by applicable law or agreed to in writing will any copyright owner be liable to LICENSEE for damages, including any general, special, incidental or consequential damages arising out of the use or inability to use the program (including but not limited to loss of data or data being rendered inaccurate or losses sustained by LICENSEE or third parties or a failure of the program to operate with other programs), even if such holder has been advised of the possibility of such damages.
*/


#ifndef __ACR_LOM2M
#define __ACR_LOM2M

#include "configuration.h"

struct AccessControlRule;
/**
 * Get the int value corresponding to a specific Access Control Rule code
 * @param ruleEntity rule to convert
 */
static int getOperationFromACR(AccessControlRule& ruleEntity);
/**
 * Get the access control rule from operations (flags)
 * @param flags corresonding to rights to convert to an access control rule
 */
static void getARCFromOperation(int flags, AccessControlRule* rule);
/**
 * Simple struct with country codes and circular regions (coordinates)
 */
struct AccessControlLocationRegion
{
    std::list<String> countryCodes;
    std::list<String> circRegion;
};

/**
 * Access control object details
 */
struct AccessControlObjectDetails
{
    int m_resourceType;
    int m_specialization;
    std::list<int> m_childResourceTypes;
};

/**
 * Access control context with several criterias
 */
struct AccessControlContext
{
    std::list<String> m_accessControlTimeWindow;
    std::list<String> m_aclRegionCountryCode;
    std::list<float> m_aclRegionCirc;
    std::list<String> m_accessControlIP4Address;
    std::list<String> m_accessControlIP6Address;
};

/**
 * Access Controle Rule representation
 * Stores informations about access rule to apply
 */ 
struct AccessControlRule
{
    std::list<String> m_accessControlOriginators;
    std::list<AccessControlContext> m_contexts;
    bool m_create;
    bool m_retrieve;
    bool m_update;
    bool m_delete;
    bool m_notify;
    bool m_discovery;
    bool m_accessControlAuthenticationFlag;

    void init(){
        m_accessControlAuthenticationFlag = false;
        m_create = false;
        m_retrieve = false;
        m_update = false;
        m_delete = false;
        m_notify = false;
        m_discovery = false;
    }

public:
    /**
     * Constructor
     * @param rights - int specifying the rights to associate to the rule
     */
    AccessControlRule(int rights)
    {
        init();
        getARCFromOperation(rights, this);
    }
    /**
     * Constructor
     * Defines defaults rights (none)
     */
    AccessControlRule()
    {
        init();
    }

    void addContext(AccessControlContext& context)
    {
        this->m_contexts.push_back(context);
    }

    bool isCreate()
    {
        return this->m_create;
    }
    void setCreate(bool create)
    {
        this->m_create = create;
    }

    bool isRetrieve()
    {
        return this->m_retrieve;
    }
    void setRetrieve(bool retrieve)
    {
        this->m_retrieve = retrieve;
    }

    bool isUpdate()
    {
        return this->m_update;
    }
    void setUpdate(bool update)
    {
        this->m_update = update;
    }

    bool isDelete()
    {
        return this->m_delete;
    }
    void setDelete(bool deleteRight)
    {
        this->m_delete = deleteRight;
    }

    bool isNotify()
    {
        return this->m_notify;
    }
    void setNotify(bool notify)
    {
        this->m_notify = notify;
    }

    bool isDiscovery()
    {
        return this->m_discovery;
    }
    void setDiscovery(bool discovery)
    {
        this->m_discovery = discovery;
    }
};

static void
getARCFromOperation(int flags, AccessControlRule* rule)
{
    if ((flags & ACOP_CREATE) == ACOP_CREATE)
    {
        rule->setCreate(true);
    }
    if ((flags & ACOP_RETRIEVE) == ACOP_RETRIEVE)
    {
        rule->setRetrieve(true);
    }
    if ((flags & ACOP_UPDATE) == ACOP_UPDATE)
    {
        rule->setUpdate(true);
    }
    if ((flags & ACOP_DELETE) == ACOP_DELETE)
    {
        rule->setDelete(true);
    }
    if ((flags & ACOP_DISCOVERY) == ACOP_DISCOVERY)
    {
        rule->setDiscovery(true);
    }
    if ((flags & ACOP_NOTIFY) == ACOP_NOTIFY)
    {
        rule->setNotify(true);
    }
}

/**
 * Get the int value representing the operation rights from the access control rule
 * @param ruleEntity - the access control rule used
 * @return int value depending on the rights defined in ruleEntity
 */
static int getOperationFromACR(AccessControlRule& ruleEntity)
{
    int result = 0;
    if (ruleEntity.isCreate())
    {
        result += ACOP_CREATE;
    }
    if (ruleEntity.isRetrieve())
    {
        result += ACOP_RETRIEVE;
    }
    if (ruleEntity.isUpdate())
    {
        result += ACOP_UPDATE;
    }
    if (ruleEntity.isDelete())
    {
        result += ACOP_DELETE;
    }
    if (ruleEntity.isDiscovery())
    {
        result += ACOP_DISCOVERY;
    }
    if (ruleEntity.isNotify())
    {
        result += ACOP_NOTIFY;
    }
    return result;
}



#endif
