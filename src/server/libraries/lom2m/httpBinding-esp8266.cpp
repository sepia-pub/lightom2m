/*
------------------------------------------------------------

SOFTWARE EVALUATION LICENSE

LAAS research laboratory, Toulouse, France.
IRIT computer science research laboratory, Toulouse, France.
------------------------------------------------------------

Contact: Thierry Monteil, thierry.monteil@irit.fr

Definitions
SOFTWARE:  The LightOM2M software, version 1.0, in source code form, written by David Gauchard, Guillaume Garzone and Thierry Monteil
at the LAAS research laboratory.

LICENSOR: LE CENTRE NATIONAL DE LA RECHERCHE SCIENTIFIQUE, a public scientific and technical research establishment, having SIREN No. 180 089 013, APE code 7219 Z, having its registered office at 3, rue Michel-Ange, 75796 Paris cedex 16, France, acting in its own name and on its own behalf, and on behalf of Le laboratoire d'analyse et d'architecture des systèmes (LAAS), UPR n°8001.

1. INTENT/PURPOSE
This agreement determines the conditions in which the LICENSOR, who has the rights to the SOFTWARE, grants to the LICENSEE a license for research and evaluation purposes only, excluding any commercial use.

2. LICENSEE
Any person or organization who receives the SOFTWARE with a copy of this license.

3. RIGHTS GRANTED
The rights to use and copy the SOFTWARE, subject to the restrictions described in this agreement.
The rights to modify and compile the SOFTWARE when it's provided in source code form, subject to the restrictions described in this agreement.
For the SOFTWARE provided in binary form only, LICENSEE undertakes to not decompile, disassemble, decrypt, extract the components or perform reverse engineering except to the extent expressly provided by law.

5. SCOPE OF THE LICENSE
- NON-COMMERCIAL license for research and evaluation purposes ONLY.
- NO right to commercialize the SOFTWARE, or any derivative work, without separate agreement with the LICENSOR.

6. MODIFICATION OF THE SOFTWARE
License permits LICENSEE to modify the SOFTWARE provided in source code form for research and evaluation purposes only.

7. REDISTRIBUTION
- License permits LICENSEE to redistribute verbatim copies of the SOFTWARE, accompanied with a copy of this license.
- License DOES NOT permit LICENSEE to redistribute modified versions of the SOFTWARE provided in source code form.
- License DOES NOT permit LICENSEE to commercialize the SOFTWARE or any derivative work of the SOFTWARE.

8. FEE/ROYALTY
- LICENSEE pays no royalty for this license.
- LICENSEE and any third parties must enter a new agreement for any use beyond scope of license. Please contact the IRIT technology transfer office (email numerique@toulouse-tech-transfer.com) for further information.

9. NO WARRANTY
The SOFTWARE is provided "as is" without warranty of any kind, either expressed or implied, including, but not limited to, the implied warranties of merchantability and fitness for a particular purpose. The entire risk as to the quality and performance of the program is with the LICENSEE.

10. NO LIABILITY
In no event unless required by applicable law or agreed to in writing will any copyright owner be liable to LICENSEE for damages, including any general, special, incidental or consequential damages arising out of the use or inability to use the program (including but not limited to loss of data or data being rendered inaccurate or losses sustained by LICENSEE or third parties or a failure of the program to operate with other programs), even if such holder has been advised of the possibility of such damages.
*/


#include "ShortNames.h"
#include "MainController.h"
#include "httpBinding.h"
#include <ESP8266HTTPClient.h>
#include <ESP8266WebServer.h>

HTTPClient* http;
WiFiClient tcp2;
void
HTTPBinding::sendRequest(RequestPrimitive* requestPrimitive, ResponsePrimitive* responsePrimitive)
{
    //ResponsePrimitive* responsePrimitive = new ResponsePrimitive();
    responsePrimitive->init();
    http = new HTTPClient();
#if DEBUG
    printf("DEBUG: REST HANDLER, SENDING HTTP REQUEST\n");
#endif
    String url = requestPrimitive->getTo();
    String body = requestPrimitive->getContent();
    String qs = emptyString;
    // set the queryString
    if (requestPrimitive->getFilterCriteria().getFilterUsage() != FU_CONDITIONAL_RETRIEVAL)
    {
        qs += "&fu=" + String(requestPrimitive->getFilterCriteria().getFilterUsage());
    }
    if (!requestPrimitive->getFilterCriteria().getResourceType().empty())
    {
        for (int ty : requestPrimitive->getFilterCriteria().getResourceType())
        {
            qs += "&ty=";
            qs += String(ty);
        }
    }

    if (!requestPrimitive->getFilterCriteria().getLabels().empty())
    {
        for (String s : requestPrimitive->getFilterCriteria().getLabels())
        {
            qs += "&lbl=" + s;
        }
    }

    if (requestPrimitive->getFilterCriteria().getLevel() != -1 && requestPrimitive->getFilterCriteria().getLevel() != 1)
    {
        qs += "&lvl=";
        qs += String(requestPrimitive->getFilterCriteria().getLevel());
    }
    if (requestPrimitive->getFilterCriteria().getLimit() != -1)
    {
        qs += "&lim=";
        qs += String(requestPrimitive->getFilterCriteria().getLimit());
    }

    if (requestPrimitive->getResultContentType() != RCN_NULL)
    {
        qs += "&rcn=" + String(requestPrimitive->getResultContentType());
    }
    if (!qs.isEmpty())
    {
        qs = qs.substring(1, qs.length());
        qs = "?" + qs;
    }
#if DEBUG
    printf("DEBUG: REST CLIENT, QS: %s\n", qs.c_str());
#endif
    http->setReuse(false);
    if (!http->begin(tcp2, url + qs))
    {
        //return H000;
    }
    if (requestPrimitive->getRequestId().equals(emptyString))
    {
        http->addHeader(F("X-M2M-RI"), CSE_ID+"-R1");
    }
    else
    {
        http->addHeader(F("X-M2M-RI"), requestPrimitive->getRequestId());
    }
    if (!requestPrimitive->getWantedContentType() || requestPrimitive->getWantedContentType() == "" || requestPrimitive->getWantedContentType() == "*/*")
    {
#if DEBUG
        printf("DEBUG: accept header not present, using JSON as default\n");
#endif
        requestPrimitive->setWantedContentType(contentTypeEncoding(ENC_JSON));
    }
    http->addHeader(F("accept"), requestPrimitive->getWantedContentType());

    // Set the originator header
    if (requestPrimitive->getFrom())
    {
        http->addHeader("X-M2M-Origin", requestPrimitive->getFrom());
#if DEBUG
        Serial.printf("DEBUG: current originator: %s\n", requestPrimitive->getFrom().c_str());
#endif // DEBUG
    }

    // Set the request content type
    String contentTypeHeader = requestPrimitive->getRequestContentType();
    if (contentTypeHeader.equals(emptyString))
    {
        contentTypeHeader = "application/json";
    }
    // Add the content type header with the resource type for create operation
#if DEBUG
    printf("DEBUG: CONTENT TYPE HEADER TO SEND: %s\n", contentTypeHeader.c_str());
#endif // DEBUG
    if (requestPrimitive->getResourceType() && requestPrimitive->getOperation() == OP_CREATE)
    {
        contentTypeHeader += ";ty=" + String(requestPrimitive->getResourceType());
    }
#if DEBUG
    printf("DEBUG: CONTENT TYPE HEADER TO SEND: %s\n", contentTypeHeader.c_str());
#endif //debug
    http->addHeader("content-type", contentTypeHeader);

    // Set the operation
    int operation = requestPrimitive->getOperation();
    int httpCodeResponse;
    int NUMBER_OF_HEADERS = 6;
    const char* headersToCollect[] = { "Content-Location", "Content-Type", "X-M2M-RSC", "X-M2M-Origin", "X-M2M-RI", "X-M2M-RVI" };
    http->collectHeaders(headersToCollect, NUMBER_OF_HEADERS);

    switch (operation)
    {
    case OP_CREATE:
    case OP_NOTIFY:
        httpCodeResponse = http->POST(body);
        break;
    case OP_RETRIEVE:
    case OP_DISCOVERY:
        httpCodeResponse = http->GET();
        break;
    case OP_UPDATE:
        httpCodeResponse = http->PUT(body);
        break;
    case OP_DELETE:
        httpCodeResponse = http->sendRequest("DELETE");
        break;
    default:
        return;
    }

    //TODO fix header
    if (httpCodeResponse == -1)
    {
        responsePrimitive->setResponseStatusCode(R5103_TARGET_NOT_REACHABLE);
    }
    else
    {   
        bool rscSet = false;
        for (int i = 0; i < http->headers(); i++)
        {
            if (http->headerName(i).equalsIgnoreCase(F("X-M2M-RSC")))
            {
                if (http->header(i) != emptyString)
                {
                    responsePrimitive->setResponseStatusCodeString(http->header(i));
                    rscSet = true;
                }
            }
            else if (http->headerName(i).equalsIgnoreCase(F("content-type")))
            {
                if (http->header(i) != emptyString)
                {
                    responsePrimitive->setContentType(http->header(i));
                }
            }
            else if (http->headerName(i).equalsIgnoreCase(F("content-location")))
            {
                if (http->header(i) != emptyString)
                {
                    responsePrimitive->setLocation(http->header(i));
                }
            }
            else if (http->headerName(i).equalsIgnoreCase(F("X-M2M-RI")))
            {
                if (http->header(i) != emptyString)
                {
                    responsePrimitive->setRequestId(http->header(i));
                }
            }
            else if (http->headerName(i).equalsIgnoreCase(F("X-M2M-Origin")))
            {
                if (http->header(i) != emptyString)
                {
                    responsePrimitive->setFrom(http->header(i));
                }
            }
            else if (http->headerName(i).equalsIgnoreCase(F("X-M2M-RVI")))
            {
                if (http->header(i) != emptyString)
                {
                    responsePrimitive->setReleaseVersionIndicator(http->header(i));
                }
            }
        }
        if (!rscSet)
        {
            ResponseStatusCode rsc = HTTPBinding::getRSCFromHTTP(httpCodeResponse);
            responsePrimitive->setResponseStatusCode(rsc);
        }

        if (httpCodeResponse != H204_NoContent)
        {
            String payload = http->getString();
            
            if (payload && payload!= emptyString)
            {
                responsePrimitive->setContent(payload);
                Serial.printf("TRACE: setting response privimite content: \n\t%s\n", responsePrimitive->getContent().c_str());
            }

        }
    }
#if DEBUG
    printf("DEBUG: HTTP RESPONSE CODE %d\n", httpCodeResponse);
#endif
    http->end();
    delete (http);
    return;
};

/**
 * Initialize a Request Primitive (oneM2M) based on HTTP
 * @param req - the request primitive to init
 * @param uri - the target URI
 */
int
HTTPBinding::initRequestPrimitive(RequestPrimitive* req, const String& uri)
{
    int result = 0;
    if (!req)
    {
        req = new RequestPrimitive();
    }
    req->init();
    req->setTo(uri);
    req->setRequestId(HTTPHeader(F("X-M2M-RI")));
    req->setFrom(HTTPHeader(F("X-M2M-Origin")));
    const String& accept = HTTPHeader(F("Accept"));
    if (accept.indexOf("application/json") >= 0 || accept.equalsIgnoreCase(F("application/vnd.onem2m-res+json"))) // TODO TO IMPROVE
    {
        req->setWantedContentType("application/json");
    }
    const String& contentType = HTTPHeader(F("Content-Type")); //TODO to update
    if (contentType.equalsIgnoreCase("onem2m"))
    {
        if (contentType.indexOf("application/json") >= 0 || contentType.equalsIgnoreCase(F("application/vnd.onem2m-res+json"))) // TODO TO IMPROVE
        {
            req->setRequestContentTypeRaw("application/json");
        }
    }

    for (int i = 0; i < ArgsNumber(); i++)
    {
        if (HTTPArgNameByIndex(i).equalsIgnoreCase(FILTER_USAGE))
        {
            switch (atoi(HTTPArgValueByIndex(i).c_str()))
            {
            case FU_DISCOVERY_CRITERIA: case FU_CONDITIONAL_RETRIEVAL: case FU_IPE_ON_DEMAND_DISCOVERY:
                req->getFilterCriteria().setFilterUsage(atoi(HTTPArgValueByIndex(i).c_str()));
                break;
            default:
                result = 2;
                break;
            }
        }
        else if (HTTPArgNameByIndex(i).equalsIgnoreCase(LABELS))
        {
            req->getFilterCriteria().getLabels().push_back(HTTPArgValueByIndex(i));
        }
        else if (HTTPArgNameByIndex(i).equalsIgnoreCase(TYPE))
        {
            req->setQSty(atoi(HTTPArg(TYPE).c_str()));
            req->getFilterCriteria().addResourceType(atoi(HTTPArgValueByIndex(i).c_str()));
        }
        else if (HTTPArgNameByIndex(i).equalsIgnoreCase(LIMIT))
        {
            if (atoi(HTTPArgValueByIndex(i).c_str()) < 0)
            {
                result = 2;
            }
            req->getFilterCriteria().setLimit(atoi(HTTPArgValueByIndex(i).c_str()));
        }
        else if (HTTPArgNameByIndex(i).equalsIgnoreCase(CREATED_BEFORE))
        {
            req->getFilterCriteria().setCreatedBefore(HTTPArgValueByIndex(i));
            req->getFilterCriteria().unsupportedFilterOn(); // TODO TO REMOVE WHEN IMPLEMENTED
        }
        else if (HTTPArgNameByIndex(i).equalsIgnoreCase(CREATED_AFTER))
        {
            req->getFilterCriteria().setCreatedAfter(HTTPArgValueByIndex(i));
            req->getFilterCriteria().unsupportedFilterOn(); // TODO TO REMOVE WHEN IMPLEMENTED
        }
        else if (HTTPArgNameByIndex(i).equalsIgnoreCase(MODIFIED_SINCE))
        {
            req->getFilterCriteria().setModifiedSince(HTTPArgValueByIndex(i));
            req->getFilterCriteria().unsupportedFilterOn(); // TODO TO REMOVE WHEN IMPLEMENTED
        }
        else if (HTTPArgNameByIndex(i).equalsIgnoreCase(UNMODIFIED_SINCE))
        {
            req->getFilterCriteria().setUnmodifiedSince(HTTPArgValueByIndex(i));
            req->getFilterCriteria().unsupportedFilterOn(); // TODO TO REMOVE WHEN IMPLEMENTED
        }
        else if (HTTPArgNameByIndex(i).equalsIgnoreCase(STATETAG_SMALLER))
        {
            if (atoi(HTTPArgValueByIndex(i).c_str()) <= 0)
            {
                result = 2;
            }
            req->getFilterCriteria().setStateTagSmaller(atoi(HTTPArgValueByIndex(i).c_str()));
            req->getFilterCriteria().unsupportedFilterOn(); // TODO TO REMOVE WHEN IMPLEMENTED
        }
        else if (HTTPArgNameByIndex(i).equalsIgnoreCase(STATETAG_BIGGER))
        {
            if (atoi(HTTPArgValueByIndex(i).c_str()) < 0)
            {
                result = 2;
            }
            req->getFilterCriteria().setStateTagBigger(atoi(HTTPArgValueByIndex(i).c_str()));
            req->getFilterCriteria().unsupportedFilterOn(); // TODO TO REMOVE WHEN IMPLEMENTED
        }
        else if (HTTPArgNameByIndex(i).equalsIgnoreCase(EXPIRE_BEFORE))
        {
            req->getFilterCriteria().setExpireBefore(HTTPArgValueByIndex(i));
            req->getFilterCriteria().unsupportedFilterOn(); // TODO TO REMOVE WHEN IMPLEMENTED
        }
        else if (HTTPArgNameByIndex(i).equalsIgnoreCase(EXPIRE_AFTER))
        {
            req->getFilterCriteria().setExpireAfter(HTTPArgValueByIndex(i));
            req->getFilterCriteria().unsupportedFilterOn(); // TODO TO REMOVE WHEN IMPLEMENTED
        }
        else if (HTTPArgNameByIndex(i).equalsIgnoreCase(SIZE_ABOVE))
        {
            if (atoi(HTTPArgValueByIndex(i).c_str()) < 0)
            {
                result = 2;
            }
            req->getFilterCriteria().setSizeAbove(atoi(HTTPArgValueByIndex(i).c_str()));
            req->getFilterCriteria().unsupportedFilterOn(); // TODO TO REMOVE WHEN IMPLEMENTED
        }
        else if (HTTPArgNameByIndex(i).equalsIgnoreCase(SIZE_BELOW))
        {
            if (atoi(HTTPArgValueByIndex(i).c_str()) <= 0)
            {
                result = 2;
            }
            req->getFilterCriteria().setSizeBelow(atoi(HTTPArgValueByIndex(i).c_str()));
            req->getFilterCriteria().unsupportedFilterOn(); // TODO TO REMOVE WHEN IMPLEMENTED
        }
        else if (HTTPArgNameByIndex(i).equalsIgnoreCase(CONTENT_TYPE))
        {
            req->getFilterCriteria().setSizeBelow(atoi(HTTPArgValueByIndex(i).c_str()));
            req->getFilterCriteria().unsupportedFilterOn(); // TODO TO REMOVE WHEN IMPLEMENTED
        }
        else if (HTTPArgNameByIndex(i).equalsIgnoreCase(ATTRIBUTE))
        {
            req->getFilterCriteria().unsupportedFilterOn(); // TODO TO REMOVE WHEN IMPLEMENTED
        }
        else if (HTTPArgNameByIndex(i).equalsIgnoreCase(SEMANTICS_FILTER))
        {
            req->getFilterCriteria().unsupportedFilterOn(); // TODO TO REMOVE WHEN IMPLEMENTED
        }
        else if (HTTPArgNameByIndex(i).equalsIgnoreCase(FILTER_OPERATION))
        {
            req->getFilterCriteria().unsupportedFilterOn(); // TODO TO REMOVE WHEN IMPLEMENTED
        }
        else if (HTTPArgNameByIndex(i).equalsIgnoreCase(CONTENT_FILTER_SYNTAX))
        {
            req->getFilterCriteria().unsupportedFilterOn(); // TODO TO REMOVE WHEN IMPLEMENTED
        }
        else if (HTTPArgNameByIndex(i).equalsIgnoreCase(CONTENT_FILTER_QUERY))
        {
            req->getFilterCriteria().unsupportedFilterOn(); // TODO TO REMOVE WHEN IMPLEMENTED
        }
        else if (HTTPArgNameByIndex(i).equalsIgnoreCase(LEVEL))
        {
            if (atoi(HTTPArgValueByIndex(i).c_str()) <=0 )
            {
                result = 2;
            }
            req->getFilterCriteria().setLevel(atoi(HTTPArgValueByIndex(i).c_str()));
        }
        else if (HTTPArgNameByIndex(i).equalsIgnoreCase(OFFSET))
        {
            if (atoi(HTTPArgValueByIndex(i).c_str()) <=0 )
            {
                result = 2;
            }
            req->getFilterCriteria().unsupportedFilterOn(); // TODO TO REMOVE WHEN IMPLEMENTED
        }
    }

    if (req->getWantedContentType() == "" || req->getWantedContentType() == "*/*")
    {
#if DEBUG
        printf("DEBUG: accept header not present, using JSON as default\n");
#endif
        req->setWantedContentType(contentTypeEncoding(ENC_JSON));
    }

    if (method() == HTTP_GET)
    {
        bool discovery = HTTPHasArg(FILTER_USAGE);
        if (discovery)
        {
            req->setOperation(OP_DISCOVERY);
        }
        else
        {
            req->setOperation(OP_RETRIEVE);
        }
    }
    else if (method() == HTTP_PUT)
    {
        req->setOperation(OP_UPDATE);
    }
    else if (method() == HTTP_DELETE)
    {
        req->setOperation(OP_DELETE);
    }
    else if (method() == HTTP_POST)
    {
        // check if ty present in content-type
        int ty;
        int pos = contentType.indexOf(S_tyeq);
        if (pos >= 0)
        {
            ty = atoi(contentType.c_str() + pos + sizeof(S_tyeq) - 1);
        }
        if (ty)
        {
            req->setOperation(OP_CREATE);
        }
        else
        {
            req->setOperation(OP_NOTIFY);
        }
    }

    if (HTTPHasArg(RESULT_CONTENT))
    {
        switch (atoi(HTTPArg(RESULT_CONTENT).c_str()))
        {
        case RCN_NOTHING:
        case RCN_ATTR:
        case RCN_HIER_ADDR:
        case RCN_HIER_ADDR_ATTR:
        case RCN_ATTR_CHRES:
        case RCN_ATTR_CHREF:
        case RCN_CHREF:
        case RCN_ORIGINAL_RES:
        case RCN_CHRES:
        case RCN_MODIF_ATTR:
        case RCN_SEM_CONTENT:
            req->setResultContentType(HTTPArg(RESULT_CONTENT).c_str());
            break;
        default:
            result = 2;
            break;
        }
#if DEBUG
        printf("DEBUG: RCN value: %d\n", req->getResultContentType());
#endif
    }
    else if (req->getOperation() == OP_DELETE)
    {
        req->setResultContentType(RCN_NOTHING);
    }
    int pos = contentType.indexOf(S_tyeq);
    if (pos >= 0)
    {
        req->setResourceType(atoi(contentType.c_str() + pos + sizeof(S_tyeq) - 1));
    }

    if (!HTTPContent().isEmpty())
    {
        req->setContent(HTTPContent());
    }
    // TODO TO FIX
    //req->setRequestContentType(HTTPHeader(F("Content-Type")));
    return result;
}

void
HTTPBinding::sendResponse(const ResponsePrimitive& responsePrimitive)
{
    // RI
    om2msrv.sendHeader("X-M2M-RI", responsePrimitive.getRequestId(), false);

    // RSC
    bool rscHeader = true;
    HTTPCode httpCode;
    switch (responsePrimitive.getResponseStatusCode())
    {
    case R2000_OK:
    case R2002_DELETED:
    case R2004_UPDATED:
        httpCode = H200_OK;
        break;
    case R2001_CREATED:
        rscHeader = false;
        httpCode = H201_Created;
        break;
    case R4000_BAD_REQUEST:
        httpCode = H400_BadRequest;
        break;
    case R4103_ORIGINATOR_HAS_NO_PRIVILEGE:
        httpCode = H403_Forbidden;
        break;
    case R4004_NOT_FOUND:
    case R5103_TARGET_NOT_REACHABLE:
        httpCode = H404_NotFound;
        break;
    case R4005_OPERATION_NOT_ALLOWED:
        httpCode = H405_NotAllowed;
        break;
    case R4008_REQUEST_TIMEOUT:
        rscHeader = false;
        httpCode = H408_RequestTimeout;
        break;
    case R4105_CONFLICT:
    case R5106_ALREADY_EXISTS:
        httpCode = H409_Conflict;
        break;
    case R4015_UNSUPPORTED_MEDIA_TYPE:
        httpCode = H415_Unsupported;
        rscHeader = false;
        break;
    case R5000_INTERNAL_SERVER_ERROR:
        httpCode = H500_InternalError;
        break;
    case R5001_NOT_IMPLEMENTED:
    case R5206_NON_BLOCKING_SYNCH_REQUEST_NOT_SUPPORTED:
        httpCode = H501_NotImplemented;
        break;
    case R5207_NOT_ACCEPTABLE:
        rscHeader = false;
        httpCode = H406_NotAcceptable;
        break;
    default:
        httpCode = H000;
        break;
    }
    // if (rscHeader)
    // {
    om2msrv.sendHeader("X-M2M-RSC", String(responsePrimitive.getResponseStatusCode()), false);
    // }

    // RVI
    om2msrv.sendHeader("X-M2M-RVI", responsePrimitive.getReleaseVersionIndicator(), false);

    if (responsePrimitive.getResourceId() != emptyString)
    {
        om2msrv.sendHeader("Content-Location", responsePrimitive.getResourceId(), false);
    }
    om2msrv.sendHeader("X-M2M-Origin", responsePrimitive.getFrom(), false);

    // sending the response
    om2msrv.send(httpCode, responsePrimitive.getContentType(), responsePrimitive.getContent());
}

boolean
handleOptions()
{
    if (method() == HTTP_OPTIONS)
    {
        om2msrv.sendHeader("Access-Control-Allow-Methods", "POST, GET, PUT, DELETE, OPTIONS", false);
        om2msrv.sendHeader("Access-Control-Allow-Headers", "X-M2M-RI, X-M2M-Origin, Content-Type, Accept", false);
        om2msrv.sendHeader("Access-Control-Allow-Origin", "*", false);
        om2msrv.sendHeader("Access-Control-Max-Age", "86400", false);
        om2msrv.send(H204_NoContent, emptyString, emptyString);
        return true;
    }
    else
    {
        return false;
    }
}

// TODO update this by using performRequest from Router, move serveOM2M to HTTP binding
void
HTTPBinding::serveOM2M(const String& uri)
{
    Encoding returnContentType = ENC_JSON;
    Encoding bodyContentType = ENC_NONE;
    RequestPrimitive* req = new RequestPrimitive();
    ResponsePrimitive* response = new ResponsePrimitive;
    // check if OPTIONS
    if (handleOptions())
    {
        Serial.printf("INFO: RECEIVED OPTIONS REQUEST\n");
        return;
    }

    String message = emptyString;
    // clear response RequestPrimitive
    Serial.printf("INFO: serve LOM2M, init request primitive \n");
    int res = HTTPBinding::initRequestPrimitive(req, uri);
    // init response primitive from request ///XXXFIXME resp is global
    Serial.printf("INFO: serve LOM2M, init response primitive \n");
    Router::initResp(*req, response);

    if (res == 2)
    {
        Serial.printf("INFO: BAD VALUES used in parameters, rejecting request\n");
        response->setResponseStatusCode(R4000_BAD_REQUEST);
    }
    else
    {
        // handle request
        Serial.printf("INFO: Router process request primitive \n");
        Router::processRequest(*req, response);
    }
    

    Serial.printf("INFO: Sending response \n");
    HTTPBinding::sendResponse(*response);

    delete (req);
    delete (response);
}
