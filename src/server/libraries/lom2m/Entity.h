/*
------------------------------------------------------------

SOFTWARE EVALUATION LICENSE

LAAS research laboratory, Toulouse, France.
IRIT computer science research laboratory, Toulouse, France.
------------------------------------------------------------

Contact: Thierry Monteil, thierry.monteil@irit.fr

Definitions
SOFTWARE:  The LightOM2M software, version 1.0, in source code form, written by David Gauchard, Guillaume Garzone and Thierry Monteil
at the LAAS research laboratory.

LICENSOR: LE CENTRE NATIONAL DE LA RECHERCHE SCIENTIFIQUE, a public scientific and technical research establishment, having SIREN No. 180 089 013, APE code 7219 Z, having its registered office at 3, rue Michel-Ange, 75796 Paris cedex 16, France, acting in its own name and on its own behalf, and on behalf of Le laboratoire d'analyse et d'architecture des systèmes (LAAS), UPR n°8001.

1. INTENT/PURPOSE
This agreement determines the conditions in which the LICENSOR, who has the rights to the SOFTWARE, grants to the LICENSEE a license for research and evaluation purposes only, excluding any commercial use.

2. LICENSEE
Any person or organization who receives the SOFTWARE with a copy of this license.

3. RIGHTS GRANTED
The rights to use and copy the SOFTWARE, subject to the restrictions described in this agreement.
The rights to modify and compile the SOFTWARE when it's provided in source code form, subject to the restrictions described in this agreement.
For the SOFTWARE provided in binary form only, LICENSEE undertakes to not decompile, disassemble, decrypt, extract the components or perform reverse engineering except to the extent expressly provided by law.

5. SCOPE OF THE LICENSE
- NON-COMMERCIAL license for research and evaluation purposes ONLY.
- NO right to commercialize the SOFTWARE, or any derivative work, without separate agreement with the LICENSOR.

6. MODIFICATION OF THE SOFTWARE
License permits LICENSEE to modify the SOFTWARE provided in source code form for research and evaluation purposes only.

7. REDISTRIBUTION
- License permits LICENSEE to redistribute verbatim copies of the SOFTWARE, accompanied with a copy of this license.
- License DOES NOT permit LICENSEE to redistribute modified versions of the SOFTWARE provided in source code form.
- License DOES NOT permit LICENSEE to commercialize the SOFTWARE or any derivative work of the SOFTWARE.

8. FEE/ROYALTY
- LICENSEE pays no royalty for this license.
- LICENSEE and any third parties must enter a new agreement for any use beyond scope of license. Please contact the IRIT technology transfer office (email numerique@toulouse-tech-transfer.com) for further information.

9. NO WARRANTY
The SOFTWARE is provided "as is" without warranty of any kind, either expressed or implied, including, but not limited to, the implied warranties of merchantability and fitness for a particular purpose. The entire risk as to the quality and performance of the program is with the LICENSEE.

10. NO LIABILITY
In no event unless required by applicable law or agreed to in writing will any copyright owner be liable to LICENSEE for damages, including any general, special, incidental or consequential damages arising out of the use or inability to use the program (including but not limited to loss of data or data being rendered inaccurate or losses sustained by LICENSEE or third parties or a failure of the program to operate with other programs), even if such holder has been advised of the possibility of such damages.
*/


#ifndef __LOM2M_ENTITY_H
#define __LOM2M_ENTITY_H

#include <list>

#include "lom2m.h"
#include "bsp.h"
#include "stdio.h"
#include "Enum.h"
#include "Observer.h"
#include "AccessControlRule.h"
#include "configuration.h"

struct AccessControlPolicy;
struct Application;
struct Container;
struct ContentInstance;
struct CseBase;
struct RemoteCse;
struct Subscription;
#if GROUP_FEAT
struct Group;
#endif
struct FilterCriteria;

/**
 * Filter Criteria is used to perform operations such as discovery.
 * Enables features to filter results of requests such as resource type
 * or labels.
 */
struct FilterCriteria
{
protected:
    String createdBefore;
    String createdAfter;
    String modifiedSince;
    String unmodifiedSince;
    int stateTagSmaller;
    int stateTagBigger;
    String expireBefore;
    String expireAfter;
    std::list<String> labels;
    std::list<int> resourceType;
    int sizeAbove;
    int sizeBelow;
    std::list<String> contentType;
    // TODO to add when supported
    //std::list<Attribute> attribute;
    int filterUsage = -1;
    int limit = -1;
    int level = 1;
    int offset = -1;
    bool unsupportedFilter = false;

    void init()
    {
        resourceType.clear();
        stateTagSmaller = -1;
        stateTagBigger = -1;
        sizeAbove = -1;
        sizeBelow = -1;
        filterUsage = -1;
        limit = -1;
        level = 1;
        offset = -1;
        filterUsage = FU_CONDITIONAL_RETRIEVAL;
    }

public:
    /** Constructor */
    FilterCriteria()
    {
        this->init();
    }
    void unsupportedFilterOn()
    {
        this->unsupportedFilter = true;
    }
    bool hasUnsupportedFilter()
    {
        return this->unsupportedFilter;
    }
    int getFilterUsage()
    {
        return this->filterUsage;
    }
    void setFilterUsage(int filterUsage)
    {
        this->filterUsage = filterUsage;
    }
    int getLimit()
    {
        return this->limit;
    }
    void setLimit(int limit)
    {
        this->limit = limit;
    }
    int getLevel()
    {
        return this->level;
    }
    void setLevel(int level)
    {
        this->level = level;
    }
    int getOffset()
    {
        return this->offset;
    }
    void setOffset(int offset)
    {
        this->offset = offset;
    }
    int getSizeBelow()
    {
        return this->sizeBelow;
    }
    void setSizeBelow(int sizeBelow)
    {
        this->sizeBelow = sizeBelow;
    }
    std::list<String> getContentType()
    {
        return this->contentType;
    }
    void setContentType(std::list<String> contentType)
    {
        this->contentType = contentType;
    }
    int getStateTagSmaller()
    {
        return this->stateTagSmaller;
    }
    void setStateTagSmaller(int stateTagSmaller)
    {
        this->stateTagSmaller = stateTagSmaller;
    }
    int getStateTagBigger()
    {
        return this->stateTagBigger;
    }
    void setStateTagBigger(int stateTagBigger)
    {
        this->stateTagBigger = stateTagBigger;
    }
    const String&getExpireBefore()
    {
        return this->expireBefore;
    }
    void setExpireBefore(const String&expireBefore)
    {
        this->expireBefore = expireBefore;
    }
    const String&getExpireAfter()
    {
        return this->expireAfter;
    }
    void setExpireAfter(const String&expireAfter)
    {
        this->expireAfter = expireAfter;
    }
    std::list<String>& getLabels()
    {
        return this->labels;
    }
    void setLabels(std::list<String>& labels)
    {
        this->labels = labels;
    }
    std::list<int> getResourceType()
    {
        return this->resourceType;
    }
    void addResourceType(int resourceType)
    {
        this->resourceType.push_back(resourceType);
    }
    int getSizeAbove()
    {
        return this->sizeAbove;
    }
    void setSizeAbove(int sizeAbove)
    {
        this->sizeAbove = sizeAbove;
    }
    const String& getCreatedBefore()
    {
        return this->createdBefore;
    }
    void setCreatedBefore(const String& createdBefore)
    {
        this->createdBefore = createdBefore;
    }
    const String& getCreatedAfter()
    {
        return this->createdAfter;
    }
    void setCreatedAfter(const String& createdAfter)
    {
        this->createdAfter = createdAfter;
    }
    const String& getModifiedSince()
    {
        return this->modifiedSince;
    }
    void setModifiedSince(const String& modifiedSince)
    {
        this->modifiedSince = modifiedSince;
    }
    const String& getUnmodifiedSince()
    {
        return this->unmodifiedSince;
    }
    void setUnmodifiedSince(const String& unmodifiedSince)
    {
        this->unmodifiedSince = unmodifiedSince;
    }
};

/**
 * Generic struc, define commons attributes for all entities
 */
struct Entity
{
private:
    std::vector<std::reference_wrapper<Observer>> observers;

public:
    /**
     * Add internal subscription and registration
     */
    void register_observer(Observer& o)
    {
        observers.push_back(o);
    }
    /**
     * Notify all subscribed entities
     */ 
    void notify_observers()
    {
        for (Observer& o : observers)
        {
            o.notify(this->m_name);
        }
    }
    /**
     * Get an entity from hierarchical uri
     * @param uri - hierarchical uri
     */
    static Entity* getByHierUri(const String& uri);

    using collection = std::list<Entity*>;
    using collectionIt = collection::iterator;
    using Children = std::list<Entity*>;
    using Child = Children::iterator;

    Children children;
    String m_resourceId;
    String m_name;
    String m_parentID;
    int m_type; // discriminator (dynamic_cast<> not allowed without rtti)
    time_t m_time_creation, m_time_modification, m_time_expiration;
    Entity* m_parent;
    std::list<String> m_labels;
    String m_creator;
    std::list<AccessControlPolicy*> m_acps;

    Entity(){};
    Entity(const String& name, int type, Entity* parent);
    virtual ~Entity();

    virtual const __FlashStringHelper* headerStr() const
    {
        return nullptr;
    }
    void init();            // from constructor
    virtual void clear();   // not from constuctor
    void printTo(printfmt to, int level = 0) const;
    virtual void printLocalTo(printfmt to, int level) const
    {
        (void)to;
        (void)level;
    }
    void setExpirationTime(String et);
    void setResourceID(String id)
    {
        m_resourceId = id;
    }

    const String& getCreator() const
    {
        return this->m_creator;
    }
    void setCreator(String creator)
    {
        this->m_creator = creator;
    }
    static String generateName(int type)
    {
        return String(type) + "_" + String((long)get_utime());
    }
    const String getParentID() const
    {
        if (m_parent)
        {
            #if DEBUG
            printf("DEBUG: getting parent RI: %s\n", m_parent->getResourceIdentifier().c_str());
            #endif
            return m_parent->getResourceIdentifier();
        }
        else
        {
            return emptyString;
        }
    }
    const String& getName() const
    {
        return m_name;
    }
    int getType() const
    {
        return m_type;
    }
    String getResourceIdentifier();
    String getIdentifier();
    String getFullName();

    bool addChild(Entity* e);

    Children getChildren()
    {
        return children;
    };

    //// no rtti: emulate it
    AccessControlPolicy* getAcp();
    Application* getApplication();
    Container* getContainer();
    ContentInstance* getInstance();
    #if GROUP_FEAT
    Group* getGroup();
    #endif
    CseBase* getCseBase();
    RemoteCse* getRemoteCse();
    Subscription* getSubscription();

    Application* getApplicationParent()
    {
        return m_parent ? m_parent->getApplication() : nullptr;
    }
    Container* getContainerParent()
    {
        return m_parent ? m_parent->getContainer() : nullptr;
    }

    //// static:

    static Entity* getByName(const String& name);
    static Entity* getByName(const String& name, const String& parentName);
    static Entity* getByIdentifier(const String& identifier);

    static collection entities;
    static collectionIt findByName(const String& name);
    static collectionIt findByResource(const String& name);

    //static Entity* addNew (const String& name, int type);

    static ContentInstance* addInstance(const String& name, Container* parent);
    static Container* addContainer(const String& name, Entity* parent);
    static Application* addApplication(const String& name);
    #if GROUP_FEAT
    static Group* addGroup(const String& name, Entity* parent);
    #endif
    static RemoteCse* addRemoteCse(const String& name, CseBase* parent);
    #if SUBSCRIPTION_FEAT
    static Subscription* addSubscription(const String& name, Entity* parent);
    #endif
    static bool remove(const String& name, int ty = TY_NONE);

    static void deleteEntity(Entity* e, bool r = false);

private:
    void clearTimes();
};

/**
 * CSE Base representation, extends entity struct
 * Implements singleton design pattern for CSEBase instance
 */
struct CseBase : Entity
{
    String m_cseId;
    int m_cst;
    std::list<int> m_supportedResTypes;
    std::list<String> m_poas;
    std::list<String> m_contentSerializationTypes;
    std::list<String> m_supportedReleaseVersions;
    /**
     * constructor
     * Uses generic consctructor for entity with specific attributes
     */
    CseBase() : Entity(CSE_NAME, TY5_CSEBASE, nullptr)
    {
        init();
        m_cseId = CSE_ID;
        m_cst = CSE_TYPE;
        #if HTTP_BINDING
        m_poas.push_back("http://" + IP + ":" + PORT);
        #endif // HTTP_BINDING
        m_supportedReleaseVersions = SUPPORTED_RELEASE_VERSIONS;
        m_supportedResTypes = SUPPORTED_RESTYPE;
        m_contentSerializationTypes = SERIALISATION_TYPES;
        m_parentID = emptyString;
    }
    static CseBase* csbInstance;



public:
    /**
     * Singleton design pattern. Get CSE Base unique instance of resource.
     */
    static CseBase* getInstance()
    {
        if (!csbInstance)
        {
            csbInstance = arduino_new(CseBase);
        }
        return csbInstance;
    }

    void init();    // from constructor
    void clear();   // not from constructor
    String getResourceIdentifier();
    const String& getParentID() const
    {
        return emptyString;
    }
};

/**
 * Entity representing Remote CSE resource, from generic entity.
 */
struct RemoteCse : Entity
{
    int cseType;
    std::list<String> poas;
    String cseBase;
    String cseID;
    // M2M EXT identifier
    // TRIGGER RECIPIENT ID
    bool requestReachability;
    // nodelink
    std::list<String> contentSerializationTypes;
    // E2ESECINFO
    // trigger reference getMaxNumberOfInstances
    std::list<String> descendantCses;
    // multicast capability
    // external group ID
    // trigger ENABLE+ activity pattern ELEMENTS
    std::list<String> supportedReleaseVersions;
    /**
     * constructor
     * @param name - name of the resource to create
     * @param parent - parent entity of the remote CSE resource (should be CSE Base)
     */
    RemoteCse(const String& name, Entity* parent) : Entity(name, TY16_REMOTE_CSE, (Entity*)parent)
    {
        this->descendantCses.clear();
        init();
    }

    void init();  // from constructor
    void clear(); // from not constructor
    static RemoteCse* getByName(const String& name);
    static RemoteCse* getByIdentifier(const String& identifier);

    const std::list<String>& getSupportedReleaseVersions() const
    {
        return this->supportedReleaseVersions;
    }

    void setSupportedReleaseVersions(std::list<String>&& supportedReleaseVersions)
    {
        this->supportedReleaseVersions = std::move(supportedReleaseVersions);
    }

    const std::list<String>& getDescendantCses() const
    {
        return this->descendantCses;
    }

    void setDescendantCses(std::list<String>&& descendantCses)
    {
        this->descendantCses = std::move(descendantCses);
    }

    const std::list<String>& getContentSerializationTypes() const
    {
        return this->contentSerializationTypes;
    }

    void setContentSerializationTypes(std::list<String>&& contentSerializationTypes)
    {
        this->contentSerializationTypes = std::move(contentSerializationTypes);
    }

    bool getRequestReachability() const
    {
        return this->requestReachability;
    }

    void setRequestReachability(bool requestReachability)
    {
        this->requestReachability = requestReachability;
    }

    const String& getCseID() const
    {
        return this->cseID;
    }

    void setCseID(const String& cseID)
    {
        this->cseID = cseID;
    }

    const String& getCseBase() const
    {
        return this->cseBase;
    }

    void setCseBase(const String& cseBase)
    {
        this->cseBase = cseBase;
    }

    const std::list<String>& getPoas() const
    {
        return this->poas;
    }

    void setPoas(std::list<String>&& poas)
    {
        this->poas = std::move(poas);
    }

    int getCseType() const
    {
        return this->cseType;
    }

    void setCseType(int cseType)
    {
        this->cseType = cseType;
    }
};

/**
 * Container resource as defined by oneM2M
 */
struct Container : Entity
{
public:
    int m_maxNumberOfInstances;
    int m_maxByteSize;

    int m_maxInstanceAge;
    // currentNrInstances -> generated
    // currentByteSize -> generated
    String m_locationID; // used when considering localization dynamic_cast
    String m_ontologyRef;
    bool m_disableRetrieval;
    unsigned int m_stateTag;

    int getCurrentNumberOfInstances();
    int getCurrentByteSize();

    int getMaxInstanceAge() const
    {
        return m_maxInstanceAge;
    }

    void setMaxInstanceAge(int mia)
    {
        m_maxInstanceAge = mia;
    }

    int getMaxNumberOfInstances() const
    {
        return this->m_maxNumberOfInstances;
    }

    void setMaxNumberOfInstances(int maxNumberOfInstances)
    {
        this->m_maxNumberOfInstances = maxNumberOfInstances;
    }

    int getMaxByteSize()
    {
        return this->m_maxByteSize;
    }

    void setMaxByteSize(int m_maxByteSize)
    {
        this->m_maxByteSize = m_maxByteSize;
    }

    const String& getLocationID() const
    {
        return this->m_locationID;
    }

    void setLocationID(const String& locationID)
    {
        this->m_locationID = locationID;
    }

    const String& getOntologyRef() const
    {
        return this->m_ontologyRef;
    }

    void setOntologyRef(const String& ontologyRef)
    {
        this->m_ontologyRef = ontologyRef;
    }

    bool getDisableRetrieval() const
    {
        return this->m_disableRetrieval;
    }

    void setDisableRetrieval(bool disableRetrieval)
    {
        this->m_disableRetrieval = disableRetrieval;
    }

    virtual const __FlashStringHelper* headerStr() const
    {
        return F("cnt-");
    }
    /**
     * Constructor
     * @param name - name of the Container Resource to create
     * @param parent - parent entity of the Container
     */
    Container(const String& name, Entity* parent) : Entity(name, TY3_CONTAINER, (Entity*)parent)
    {
        m_maxNumberOfInstances = MAX_NUMBER_OF_INSTANCES_DEFAULT ;
        m_maxByteSize = -1;
        m_maxInstanceAge = -1;
        m_disableRetrieval = false;
        m_stateTag = 0;
    }

    void incrementStateTag()
    {
        if (m_stateTag == UINT_MAX-1)
        {
            m_stateTag = 0;
        }
        m_stateTag++;
    }

    unsigned int getStateTag()
    {
        return m_stateTag;
    }

    virtual ~Container();

    void init();    // from constructor
    void clear();   // not from constructor
    virtual void printLocalTo(printfmt to, int level) const;

    static Container* getByIdentifier(const String& identifier);
    static Container* getByName(const String& name);
};

/**
 * Content Instance resource
 */
struct ContentInstance: Entity
{
    String m_contentFormat;
    String m_content;
    unsigned int m_stateTag = 0;

    virtual const __FlashStringHelper* headerStr() const
    {
        return F("cin-");
    }

    /**
     * Constructor
     * @param name - name of the resource
     * @param parent - link to the parent container
     */
    ContentInstance(const String& name, Container* parent) : Entity(name, TY4_CONTENT_INSTANCE, parent) {}
    virtual ~ContentInstance();

    void init();        // from constructor
    //void clear ();    // not from constructor
    virtual void printLocalTo(printfmt to, int level) const;

    unsigned int getStateTag() {
    	return this->m_stateTag;
    }
    void setStateTag(unsigned int stateTag) {
    	this->m_stateTag = stateTag;
    }

    const String& getContent() const
    {
        return m_content;
    }

    void setContent(String content)
    {
        m_content = content;
    }

    const String& getContentFormat() const
    {
        return m_contentFormat;
    }

    void setContentFormat(const String& contentFormat)
    {
        m_contentFormat = contentFormat;
    }

    static ContentInstance* getByName(const String& name);
    static ContentInstance* getByIdentifier(const String& identifier);
};

/**
 * Application Entity resource
 */
struct Application : Entity
{
    String m_api; // M
    String m_apn;
    std::list<String> m_poa;
    String m_ontologyRef;
    bool m_rr; // M
    std::list<String> m_contentSerialisation;
    // E2ESECINFO
    // ACTIVITY PATTERN ELEMENTS
    // TRIGGER ENABLE
    // SESSION CAPABILITIES
    std::list<String> m_supportedReleaseVersions; // M

    virtual const __FlashStringHelper* headerStr() const
    {
        return F("ae-");
    };

    /**
     * Constructor
     * requestReachability is set to false by default
     * @param name - name of the AE resource to create
     */
    Application(const String& name): Entity(name, TY2_APPL_ENTITY, CseBase::getInstance())
    {
        this->m_rr = false;
    }

    virtual ~Application();

    void init();        // from constructor
    //void clear ();    // not from constructor
    //const String& getParentIdentifier () const;
    virtual void printLocalTo(printfmt to, int level) const;

    static Application* getByName(const String& name);
    static Application* getByIdentifier(const String& identifier);
};

#if SUBSCRIPTION_FEAT
/**
 * Subscription resource
 */
struct Subscription : Entity
{
    static Subscription* getByName(const String& name);
    static Subscription* getByIdentifier(const String& identifier);

    // EventNotificationCriteria eventNotificationCriteria;
    int expirationCounter;
    std::list<String> notificationURI;
    String groupID;
    String notificationForwardingURI;
    int preSubscriptionNotify;
    int pendingNotification;
    int notificationStoragePriority;
    bool latestNotify;
    NotificationContentType notificationContentType;
    String notificationEventCat;
    String creator;
    String subscriberURI;

    /**
     * constructor
     * @param name - name of the subscription resource to create
     * @param parent - parent entity of the subscription
     */
    Subscription(const String& name, Entity* parent) : Entity(name, TY23_SUBSCRIPTION, (Entity*)parent)
    {
        notificationContentType = NCT_ALL_ATTRIBUTES;
        expirationCounter = -1;
        preSubscriptionNotify = -1;
        pendingNotification = 0;
        notificationStoragePriority = -1;
    }

    virtual ~Subscription();

    int getExpirationCounter() const
    {
        return this->expirationCounter;
    }

    void setExpirationCounter(int expirationCounter)
    {
        this->expirationCounter = expirationCounter;
    }

    const String& getNotificationForwardingURI() const
    {
        return this->notificationForwardingURI;
    }

    void setNotificationForwardingURI(String notificationForwardingURI)
    {
        this->notificationForwardingURI = notificationForwardingURI;
    }

    // BatchNotify batchNotify;
    // RateLimit rateLimit;

    int getPreSubscriptionNotify() const
    {
        return this->preSubscriptionNotify;
    }

    void setPreSubscriptionNotify(int preSubscriptionNotify)
    {
        this->preSubscriptionNotify = preSubscriptionNotify;
    }

    int getPendingNotification() const
    {
        return this->pendingNotification;
    }

    void setPendingNotification(int pendingNotification)
    {
        this->pendingNotification = pendingNotification;
    }

    int getNotificationStoragePriority() const
    {
        return this->notificationStoragePriority;
    }

    void setNotificationStoragePriority(int notificationStoragePriority)
    {
        this->notificationStoragePriority = notificationStoragePriority;
    }

    bool getLatestNotify() const
    {
        return this->latestNotify;
    }

    void setLatestNotify(bool latestNotify)
    {
        this->latestNotify = latestNotify;
    }

    NotificationContentType getNotificationContentType() const
    {
        return this->notificationContentType;
    }

    void setNotificationContentType(NotificationContentType notificationContentType)
    {
        this->notificationContentType = notificationContentType;
    }

    void setNotificationContentType(int nct)
    {
        this->notificationContentType = static_cast<NotificationContentType>(nct);
    }

    const String& getNotificationEventCat() const
    {
        return this->notificationEventCat;
    }

    void setNotificationEventCat(const String& notificationEventCat)
    {
        this->notificationEventCat = notificationEventCat;
    }

    const String& getCreator() const
    {
        return this->creator;
    }

    void setCreator(const String& creator)
    {
        this->creator = creator;
    }

    const String& getSubscriberURI() const
    {
        return this->subscriberURI;
    }

    void setSubscriberURI(const String& subscriberURI)
    {
        this->subscriberURI = subscriberURI;
    }

    std::list<String> getNotificationURI()
    {
        return this->notificationURI;
    }

    void setNotificationURI(std::list<String>&& notificationURI)
    {
        this->notificationURI = std::move(notificationURI);
    }
};
#endif // SUBSCRIPTION_FEAT

/**
 * Access control policy resource to check access rights in the system
 */
struct AccessControlPolicy : Entity
{
    std::list<AccessControlRule> m_privileges;
    std::list<AccessControlRule> m_selfPrivileges;
    // authorization Decision ResourceIDs
    // authorization Politcy ResourcesIDS
    // authorization Information ResourceIDs
public:
    /**
     * Constructor
     * @param name - name of the ACP resource to create
     */
    AccessControlPolicy(const String& name)
      : Entity(name, TY1_ACP, (Entity*)CseBase::getInstance())
    {
    }
};

/**
 * Administrator access control policy specific singleton
 */
struct AcpAdmin : AccessControlPolicy
{

    AcpAdmin()
      : AccessControlPolicy("ACP_ADMIN")
    {
        init();
    }
    static AccessControlPolicy* acpInstance;

public:
    /**
     * Get the unique instance of the admin acp (singleton)
     */
    static AccessControlPolicy* getInstance()
    {
        if (!acpInstance)
        {
            Entity* e = Entity::getByHierUri("/" + CSE_ID + "/" + CSE_NAME + "/" + ACP_ADMIN_NAME);
            if (e)
            {
                // Entity::deleteEntity(e);
                acpInstance = e->getAcp();
            }
            else
            {
                acpInstance = arduino_new(AcpAdmin);
            }
        }
        return acpInstance;
    }
    void init();
};

#if GROUP_FEAT
/**
 * Group resource
 * Can be used to perform different operations / requests on multiple resources
 */
struct Group : Entity
{
    std::list<String> m_memberIds;
    virtual const __FlashStringHelper* headerStr() const
    {
        return F("grp-");
    };
    /**
     * Constructor
     * @param name - name of the groupe resource to create
     * @param parent - parent entity of the groupe to create (CSEBase, AE, remoteCSE)
     */
    Group(const String& name, Entity* parent) : Entity(name, TY9_GROUP, parent) {
        m_memberIds.clear();
    }
    virtual ~Group();

    void add(Entity* e);
    virtual void printLocalTo(printfmt to, int level) const;
    void addMemberId(const String& id)
    {
        m_memberIds.push_back(id);
    }
    const std::list<String>& getMemberIds()
    {
        return m_memberIds;
    }
    static Group* getByName(const String& name);
    static Group* getByIdentifier(const String& identifier);
};

#endif // GROUP FEATURE

#endif // __LOM2M_ENTITY_H
