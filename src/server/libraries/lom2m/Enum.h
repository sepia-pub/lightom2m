/*
------------------------------------------------------------

SOFTWARE EVALUATION LICENSE

LAAS research laboratory, Toulouse, France.
IRIT computer science research laboratory, Toulouse, France.
------------------------------------------------------------

Contact: Thierry Monteil, thierry.monteil@irit.fr

Definitions
SOFTWARE:  The LightOM2M software, version 1.0, in source code form, written by David Gauchard, Guillaume Garzone and Thierry Monteil
at the LAAS research laboratory.

LICENSOR: LE CENTRE NATIONAL DE LA RECHERCHE SCIENTIFIQUE, a public scientific and technical research establishment, having SIREN No. 180 089 013, APE code 7219 Z, having its registered office at 3, rue Michel-Ange, 75796 Paris cedex 16, France, acting in its own name and on its own behalf, and on behalf of Le laboratoire d'analyse et d'architecture des systèmes (LAAS), UPR n°8001.

1. INTENT/PURPOSE
This agreement determines the conditions in which the LICENSOR, who has the rights to the SOFTWARE, grants to the LICENSEE a license for research and evaluation purposes only, excluding any commercial use.

2. LICENSEE
Any person or organization who receives the SOFTWARE with a copy of this license.

3. RIGHTS GRANTED
The rights to use and copy the SOFTWARE, subject to the restrictions described in this agreement.
The rights to modify and compile the SOFTWARE when it's provided in source code form, subject to the restrictions described in this agreement.
For the SOFTWARE provided in binary form only, LICENSEE undertakes to not decompile, disassemble, decrypt, extract the components or perform reverse engineering except to the extent expressly provided by law.

5. SCOPE OF THE LICENSE
- NON-COMMERCIAL license for research and evaluation purposes ONLY.
- NO right to commercialize the SOFTWARE, or any derivative work, without separate agreement with the LICENSOR.

6. MODIFICATION OF THE SOFTWARE
License permits LICENSEE to modify the SOFTWARE provided in source code form for research and evaluation purposes only.

7. REDISTRIBUTION
- License permits LICENSEE to redistribute verbatim copies of the SOFTWARE, accompanied with a copy of this license.
- License DOES NOT permit LICENSEE to redistribute modified versions of the SOFTWARE provided in source code form.
- License DOES NOT permit LICENSEE to commercialize the SOFTWARE or any derivative work of the SOFTWARE.

8. FEE/ROYALTY
- LICENSEE pays no royalty for this license.
- LICENSEE and any third parties must enter a new agreement for any use beyond scope of license. Please contact the IRIT technology transfer office (email numerique@toulouse-tech-transfer.com) for further information.

9. NO WARRANTY
The SOFTWARE is provided "as is" without warranty of any kind, either expressed or implied, including, but not limited to, the implied warranties of merchantability and fitness for a particular purpose. The entire risk as to the quality and performance of the program is with the LICENSEE.

10. NO LIABILITY
In no event unless required by applicable law or agreed to in writing will any copyright owner be liable to LICENSEE for damages, including any general, special, incidental or consequential damages arising out of the use or inability to use the program (including but not limited to loss of data or data being rendered inaccurate or losses sustained by LICENSEE or third parties or a failure of the program to operate with other programs), even if such holder has been advised of the possibility of such damages.
*/

#ifndef __ENUM_LOM2M
#define __ENUM_LOM2M

/**
 * Notification event type defines which type of event should trigger a Notification
 */
enum NotificationEventType
{
    NET_UPDATE_RES = 1, /**< event update resource */
    NET_DELETE_RES = 2, /**< event delete resource */
    NET_CREATE_DIRECT_CHILD = 3, /**< event direct child resource creation */
    NET_DELETE_DIRECT_CHILD = 4, /**< event delete direct child resource */
    NET_RETRIEVE_CNT_WITH_NO_CHILD = 5, /**< event retrieve container with no child resource */
    NET_TRIGGER_RECEIVE_FOR_AE = 6, /**< trigger receive for application entity */
    NET_BLOCKING_UPDATE = 7 /**< event blocking update */
};
/**
 * Define the content type of the notification body
 */
enum NotificationContentType
{
    NCT_NULL_VRQ = 0, /**< used only for initialisation */
    NCT_ALL_ATTRIBUTES = 1, /**< all atributes of concerned resource */
    NCT_MODIFIED_ATTRIBUTES = 2, /**< modified attributes only */
    NCT_RESOURCE_ID = 3, /**< resource ID only */
    NCT_TRIGGER_PAYLOAD = 4 /**< ??? */
};

/**
 * Enum for oneM2M operation code
 */
enum Operation
{
    OP_NULL = -1,
    OP_CREATE = 1,
    OP_RETRIEVE = 2,
    OP_UPDATE = 3,
    OP_DELETE = 4,
    OP_NOTIFY = 5,
    OP_DISCOVERY = 6
};

/**
 * Enum for result content type parameter (query string in HTTP)
 */
enum ResultContentType
{
    RCN_NULL = -1,
    RCN_NOTHING = 0, /**< result content nothing */
    RCN_ATTR = 1, /**< result content only attributes of the target resource */
    RCN_HIER_ADDR = 2, /**< result content with hierarchical address only */
    RCN_HIER_ADDR_ATTR = 3, /**< hierarchical address + attributes of the resource */
    RCN_ATTR_CHRES = 4, /**< child resources attributes + resource attributes */
    RCN_ATTR_CHREF = 5, /**< child resources references + resource attributes */
    RCN_CHREF = 6, /**< child resources references only */
    RCN_ORIGINAL_RES = 7, /**< original resource */
    RCN_CHRES = 8, /**< child resources attributes only */
    RCN_MODIF_ATTR = 9, /**< modified attributes of the resource only */
    RCN_SEM_CONTENT = 10 /**< semantic content only */
};

/**
 * Enum for access control operations IDs.
 * To use multiple rights, sum the operations.
 */
enum ACCESS_CONTROL_OPERATIONS
{
    ACOP_CREATE = 1,
    ACOP_RETRIEVE = 2,
    ACOP_UPDATE = 4,
    ACOP_DELETE = 8,
    ACOP_NOTIFY = 16,
    ACOP_DISCOVERY = 32,
    ACOP_ALL = 63
};

/**
 * Possible values for FILTER USAGE attribute of Filter Criteria (used in discovery)
 */
enum FILTER_USAGE
{
    FU_DISCOVERY_CRITERIA = 1,
    FU_CONDITIONAL_RETRIEVAL = 2,
    FU_IPE_ON_DEMAND_DISCOVERY = 3
};

#endif
