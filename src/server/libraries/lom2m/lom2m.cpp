/*
------------------------------------------------------------

SOFTWARE EVALUATION LICENSE

LAAS research laboratory, Toulouse, France.
IRIT computer science research laboratory, Toulouse, France.
------------------------------------------------------------

Contact: Thierry Monteil, thierry.monteil@irit.fr

Definitions
SOFTWARE:  The LightOM2M software, version 1.0, in source code form, written by David Gauchard, Guillaume Garzone and Thierry Monteil
at the LAAS research laboratory.

LICENSOR: LE CENTRE NATIONAL DE LA RECHERCHE SCIENTIFIQUE, a public scientific and technical research establishment, having SIREN No. 180 089 013, APE code 7219 Z, having its registered office at 3, rue Michel-Ange, 75796 Paris cedex 16, France, acting in its own name and on its own behalf, and on behalf of Le laboratoire d'analyse et d'architecture des systèmes (LAAS), UPR n°8001.

1. INTENT/PURPOSE
This agreement determines the conditions in which the LICENSOR, who has the rights to the SOFTWARE, grants to the LICENSEE a license for research and evaluation purposes only, excluding any commercial use.

2. LICENSEE
Any person or organization who receives the SOFTWARE with a copy of this license.

3. RIGHTS GRANTED
The rights to use and copy the SOFTWARE, subject to the restrictions described in this agreement.
The rights to modify and compile the SOFTWARE when it's provided in source code form, subject to the restrictions described in this agreement.
For the SOFTWARE provided in binary form only, LICENSEE undertakes to not decompile, disassemble, decrypt, extract the components or perform reverse engineering except to the extent expressly provided by law.

5. SCOPE OF THE LICENSE
- NON-COMMERCIAL license for research and evaluation purposes ONLY.
- NO right to commercialize the SOFTWARE, or any derivative work, without separate agreement with the LICENSOR.

6. MODIFICATION OF THE SOFTWARE
License permits LICENSEE to modify the SOFTWARE provided in source code form for research and evaluation purposes only.

7. REDISTRIBUTION
- License permits LICENSEE to redistribute verbatim copies of the SOFTWARE, accompanied with a copy of this license.
- License DOES NOT permit LICENSEE to redistribute modified versions of the SOFTWARE provided in source code form.
- License DOES NOT permit LICENSEE to commercialize the SOFTWARE or any derivative work of the SOFTWARE.

8. FEE/ROYALTY
- LICENSEE pays no royalty for this license.
- LICENSEE and any third parties must enter a new agreement for any use beyond scope of license. Please contact the IRIT technology transfer office (email numerique@toulouse-tech-transfer.com) for further information.

9. NO WARRANTY
The SOFTWARE is provided "as is" without warranty of any kind, either expressed or implied, including, but not limited to, the implied warranties of merchantability and fitness for a particular purpose. The entire risk as to the quality and performance of the program is with the LICENSEE.

10. NO LIABILITY
In no event unless required by applicable law or agreed to in writing will any copyright owner be liable to LICENSEE for damages, including any general, special, incidental or consequential damages arising out of the use or inability to use the program (including but not limited to loss of data or data being rendered inaccurate or losses sustained by LICENSEE or third parties or a failure of the program to operate with other programs), even if such holder has been advised of the possibility of such damages.
*/


#include "lom2m.h"

String CSE_ID = "mn-cse";
String CSE_NAME = "mn-name";
String ACP_ADMIN_NAME = "ACP_ADMIN";
int CSE_TYPE = 2;
String PORT = "8282";
String IP = "127.0.0.1";
String RVI = "3";
String DEFAULT_PROTOCOL = "http";
#if HTTP_BINDING
int HTTP_BINDING_ENABLED = HTTP_BINDING_ENABLED_DEFAULT;
#endif // HTTP_BINDING
#if PERSISTENCE_FEAT
int BACKUP_PERIOD = PERSIST_PERIOD_MS;
int BACKUP_ENABLED = 1;
#endif
int MAX_NUMBER_OF_INSTANCES_DEFAULT = MAX_CIN_PER_CNT;
int globalResourcesThreshold = GLOBAL_MAX_NUMBER_RESOURCES;

std::list<int> SUPPORTED_RESTYPE = {1, 2, 3, 4, 5, 16, 23};
std::list<String> SUPPORTED_RELEASE_VERSIONS = {"2a", "3"};
std::list<String> SERIALISATION_TYPES = {"application/json"};

const char S_slash [] PROGMEM = "/";
const char S_date [] PROGMEM = "date";
const char S_value [] PROGMEM = "value";
const char S_creation [] PROGMEM = "creation";
const char S_modification [] PROGMEM = "modification";
const char S_expiration [] PROGMEM = "expiration";
const char S_xml [] PROGMEM = "xml";
const char S_json [] PROGMEM = "json";
const char S_tyeq [] PROGMEM = "ty=";
const char S_rn [] PROGMEM = "\r\n";
const char S_INCSE [] PROGMEM = "(in CSE)";

// any unique string which will be translated as xml/json bools
const char ENCODED_TRUE [] PROGMEM = "#TR#";
const char ENCODED_FALSE [] PROGMEM = "#FLS#";

extern String REMOTE_CSE_POA = CONF_REMOTE_POA;
extern String REMOTE_CSE_ID = CONF_REMOTE_CSE_ID;
extern String REMOTE_CSE_NAME = CONF_REMOTE_CSE_NAME;
extern const String M2M_SP_ID = "lom2m.fr";
extern String ADMIN_ORIGINATOR = CONF_ADMIN_ORIGINATOR;
extern int REMOTE_CSE_TYPE = 1;

utime_t default_expiration_duration = utime_t(1) * 365 * 24 * 60 * 60 * 1000000;

const char* contentTypeEncoding(Encoding enc)
{
    switch (enc)
    {
    //case ENC_XML:  return PSTR("application/xml");
    case ENC_JSON: return PSTR("application/json");
    case ENC_NONE: return PSTR("text/plain");
    }
    return emptyString.c_str();
}

const char* rqType(int ty)
{
    switch (ty)
    {
    case TY_NONE:              return PSTR("(ty:none)");
    case TY1_ACP:              return PSTR("m2m:acp");
    case TY2_APPL_ENTITY:      return PSTR("m2m:ae");
    case TY3_CONTAINER:        return PSTR("m2m:cnt");
    case TY4_CONTENT_INSTANCE: return PSTR("m2m:cin");
    case TY5_CSEBASE:          return PSTR("m2m:cb");
    case TY9_GROUP:            return PSTR("m2m:grp");
    case TY16_REMOTE_CSE:      return PSTR("m2m:csr");
    case TY23_SUBSCRIPTION:    return PSTR("m2m:sub");
    case TY_NOTIFICATION:       return PSTR("m2m:sgn");

    case TY_URIL:              return PSTR("m2m:uril");

    default:                   return PSTR("m2m:mx"); // Mixed??
    }
}

bool dataUpdated = false;
bool descendantCsesToUpdate = false;
#if FEAT_RCN_9
std::list<String> modifiedAttributes ;
#endif //FEAT_RCN_9

String FROM_ORIGINATOR = emptyString;
