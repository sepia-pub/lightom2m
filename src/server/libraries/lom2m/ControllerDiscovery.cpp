/*
------------------------------------------------------------

SOFTWARE EVALUATION LICENSE

LAAS research laboratory, Toulouse, France.
IRIT computer science research laboratory, Toulouse, France.
------------------------------------------------------------

Contact: Thierry Monteil, thierry.monteil@irit.fr

Definitions
SOFTWARE:  The LightOM2M software, version 1.0, in source code form, written by David Gauchard, Guillaume Garzone and Thierry Monteil
at the LAAS research laboratory.

LICENSOR: LE CENTRE NATIONAL DE LA RECHERCHE SCIENTIFIQUE, a public scientific and technical research establishment, having SIREN No. 180 089 013, APE code 7219 Z, having its registered office at 3, rue Michel-Ange, 75796 Paris cedex 16, France, acting in its own name and on its own behalf, and on behalf of Le laboratoire d'analyse et d'architecture des systèmes (LAAS), UPR n°8001.

1. INTENT/PURPOSE
This agreement determines the conditions in which the LICENSOR, who has the rights to the SOFTWARE, grants to the LICENSEE a license for research and evaluation purposes only, excluding any commercial use.

2. LICENSEE
Any person or organization who receives the SOFTWARE with a copy of this license.

3. RIGHTS GRANTED
The rights to use and copy the SOFTWARE, subject to the restrictions described in this agreement.
The rights to modify and compile the SOFTWARE when it's provided in source code form, subject to the restrictions described in this agreement.
For the SOFTWARE provided in binary form only, LICENSEE undertakes to not decompile, disassemble, decrypt, extract the components or perform reverse engineering except to the extent expressly provided by law.

5. SCOPE OF THE LICENSE
- NON-COMMERCIAL license for research and evaluation purposes ONLY.
- NO right to commercialize the SOFTWARE, or any derivative work, without separate agreement with the LICENSOR.

6. MODIFICATION OF THE SOFTWARE
License permits LICENSEE to modify the SOFTWARE provided in source code form for research and evaluation purposes only.

7. REDISTRIBUTION
- License permits LICENSEE to redistribute verbatim copies of the SOFTWARE, accompanied with a copy of this license.
- License DOES NOT permit LICENSEE to redistribute modified versions of the SOFTWARE provided in source code form.
- License DOES NOT permit LICENSEE to commercialize the SOFTWARE or any derivative work of the SOFTWARE.

8. FEE/ROYALTY
- LICENSEE pays no royalty for this license.
- LICENSEE and any third parties must enter a new agreement for any use beyond scope of license. Please contact the IRIT technology transfer office (email numerique@toulouse-tech-transfer.com) for further information.

9. NO WARRANTY
The SOFTWARE is provided "as is" without warranty of any kind, either expressed or implied, including, but not limited to, the implied warranties of merchantability and fitness for a particular purpose. The entire risk as to the quality and performance of the program is with the LICENSEE.

10. NO LIABILITY
In no event unless required by applicable law or agreed to in writing will any copyright owner be liable to LICENSEE for damages, including any general, special, incidental or consequential damages arising out of the use or inability to use the program (including but not limited to loss of data or data being rendered inaccurate or losses sustained by LICENSEE or third parties or a failure of the program to operate with other programs), even if such holder has been advised of the possibility of such damages.
*/


#include "Controller.h"
#include "AccessControlController.h"
#include "Entity.h"

void DiscoveryController::performDiscovery(RequestPrimitive& req, ResponsePrimitive* resp, Entity* targetEntity)
{
    FilterCriteria fc = req.getFilterCriteria();
    if (fc.hasUnsupportedFilter())
    {
        resp->setResponseStatusCode(R5001_NOT_IMPLEMENTED);
        return;
    }
    jsonglobal.clear();
    JsonArray a;
#if TARGET_UNIX
    try
    {
#endif
        a = jsonglobal[rqType(TY_URIL)].to<JsonArray>();
#if DEBUG
        Serial.println("DEBUG: FILTER CRITERIA TY:");
        for (int toPrint: fc.getResourceType())
        {
            Serial.println(toPrint);
        }
#endif
        if (!targetEntity)
        {
            for (auto& e : Entity::entities)
            {
                if (!fc.getResourceType().empty())
                {
                    bool ok = false;
                    for (int ty : fc.getResourceType())
                    {
                        if (ty == e->m_type)
                        {
                            ok = true;
                        }
                    }
                    if (!ok)
                    {
                        continue;
                    }
                }
                if (e->getName().length() > 0)
                {
                    if (fc.getLabels().size() > 0)
                    {
                        bool toAdd = true;
                        bool toAddRecall = false;
                        for (String lbl : fc.getLabels())
                        {
                            toAddRecall = false;
                            for (String l : e->m_labels)
                            {
                                if (lbl.equals(l))
                                {
                                    toAddRecall = true;
                                }
                            }
                            toAdd = toAdd && toAddRecall;
                        }
                        if (toAdd)
                        {
                            a.add(e->getFullName());
                        }
                    }
                    else
                    {
                        a.add(e->getFullName());
                    }
                }
                if (fc.getLimit() != -1 && a.size() >= fc.getLimit())
                {
                    break;
                }
            }
        }
        else
        {
            bool limitReached = false;
            std::list<Entity*> toExplore;
            toExplore.clear();
            for(Entity* child : targetEntity->getChildren())
            {
                toExplore.push_back(child);
            }
            while (!limitReached && !toExplore.empty())
            {
                Entity* toDiscover = toExplore.front();
                toExplore.pop_front();
                for (Entity* child : toDiscover->getChildren())
                {
                    toExplore.push_back(child);
                }
                if (!checkAccessRights(toDiscover, req))
                {
                    continue;
                }
                if (!fc.getResourceType().empty())
                {
                    bool ok = false;
                    for (int ty : fc.getResourceType())
                    {
                        if (ty == toDiscover->m_type)
                        {
                            ok = true;
                        }
                    }
                    if (!ok)
                    {
                        continue;
                    }
                }
                if (toDiscover->getName().length() > 0)
                {
                    if (fc.getLabels().size() > 0)
                    {
                        bool toAdd = true;
                        bool toAddRecall = false;
                        for (String lbl : fc.getLabels())
                        {
                            toAddRecall = false;
                            for (String l : toDiscover->m_labels)
                            {
                                if (lbl.equals(l))
                                {
                                    toAddRecall = true;
                                }
                            }
                            toAdd = toAdd && toAddRecall;
                        }
                        if (toAdd)
                        {
                            a.add(toDiscover->getFullName());
                        }
                    }
                    else
                    {
                        a.add(toDiscover->getFullName());
                    }
                }
                if (fc.getLimit() != -1 && a.size() >= fc.getLimit())
                {
                    limitReached = true;
                }
            }
            toExplore.clear();
        }
        String payload;
        serializeJson(jsonglobal, payload);
        resp->setContent(payload);
        resp->setContentType("application/json");
#if TARGET_UNIX
    }
    catch (std::exception& e)
    {
        resp->setResponseStatusCode(R5000_INTERNAL_SERVER_ERROR);
    }
#endif
    resp->setResponseStatusCode(R2000_OK);
    return;
}
