/*
------------------------------------------------------------

SOFTWARE EVALUATION LICENSE

LAAS research laboratory, Toulouse, France.
IRIT computer science research laboratory, Toulouse, France.
------------------------------------------------------------

Contact: Thierry Monteil, thierry.monteil@irit.fr

Definitions
SOFTWARE:  The LightOM2M software, version 1.0, in source code form, written by David Gauchard, Guillaume Garzone and Thierry Monteil
at the LAAS research laboratory.

LICENSOR: LE CENTRE NATIONAL DE LA RECHERCHE SCIENTIFIQUE, a public scientific and technical research establishment, having SIREN No. 180 089 013, APE code 7219 Z, having its registered office at 3, rue Michel-Ange, 75796 Paris cedex 16, France, acting in its own name and on its own behalf, and on behalf of Le laboratoire d'analyse et d'architecture des systèmes (LAAS), UPR n°8001.

1. INTENT/PURPOSE
This agreement determines the conditions in which the LICENSOR, who has the rights to the SOFTWARE, grants to the LICENSEE a license for research and evaluation purposes only, excluding any commercial use.

2. LICENSEE
Any person or organization who receives the SOFTWARE with a copy of this license.

3. RIGHTS GRANTED
The rights to use and copy the SOFTWARE, subject to the restrictions described in this agreement.
The rights to modify and compile the SOFTWARE when it's provided in source code form, subject to the restrictions described in this agreement.
For the SOFTWARE provided in binary form only, LICENSEE undertakes to not decompile, disassemble, decrypt, extract the components or perform reverse engineering except to the extent expressly provided by law.

5. SCOPE OF THE LICENSE
- NON-COMMERCIAL license for research and evaluation purposes ONLY.
- NO right to commercialize the SOFTWARE, or any derivative work, without separate agreement with the LICENSOR.

6. MODIFICATION OF THE SOFTWARE
License permits LICENSEE to modify the SOFTWARE provided in source code form for research and evaluation purposes only.

7. REDISTRIBUTION
- License permits LICENSEE to redistribute verbatim copies of the SOFTWARE, accompanied with a copy of this license.
- License DOES NOT permit LICENSEE to redistribute modified versions of the SOFTWARE provided in source code form.
- License DOES NOT permit LICENSEE to commercialize the SOFTWARE or any derivative work of the SOFTWARE.

8. FEE/ROYALTY
- LICENSEE pays no royalty for this license.
- LICENSEE and any third parties must enter a new agreement for any use beyond scope of license. Please contact the IRIT technology transfer office (email numerique@toulouse-tech-transfer.com) for further information.

9. NO WARRANTY
The SOFTWARE is provided "as is" without warranty of any kind, either expressed or implied, including, but not limited to, the implied warranties of merchantability and fitness for a particular purpose. The entire risk as to the quality and performance of the program is with the LICENSEE.

10. NO LIABILITY
In no event unless required by applicable law or agreed to in writing will any copyright owner be liable to LICENSEE for damages, including any general, special, incidental or consequential damages arising out of the use or inability to use the program (including but not limited to loss of data or data being rendered inaccurate or losses sustained by LICENSEE or third parties or a failure of the program to operate with other programs), even if such holder has been advised of the possibility of such damages.
*/


#ifndef __LOM2M_CONFIG
#define __LOM2M_CONFIG

#include "configurationTarget.h"
#include <Arduino.h>

/** WiFi connection information, useful for ESP target */
#define mySSID "Cisco38658"
#define myPSK ""

//////////////////// LOG LEVEL CONFIGURATION ////////////////////////
#define DEBUG 0 // define here for enabling DEBUG logs
#if DEBUG
#define INFO 1
#define TRACE 1
#else 
#define INFO 1 // define here for INFO logs
#define TRACE 1 // define here for TRACE logs
#endif
/////////////////////////////////////////////////////////////////////


/////////////////////// ENABLED FEATURES ////////////////////////////
#ifndef TARGET_UNIX
#define TARGET_UNIX 0
#endif
#if TARGET_UNIX
//  TARGET (UNIX BASED)
#define ACCESS_CONTROL_FEAT 1
#define PERSISTENCE_FEAT 1
#define SUBSCRIPTION_FEAT 1
#define REQ_LVL_FEAT 1
#define JSONMAXSIZE_OUTPUT 65536
#define GROUP_FEAT 0
#define HTTP_BINDING 1
#define HTTP_BINDING_ENABLED_DEFAULT 1
#define FEAT_RCN_9 1

#else // DEFAULT
// FEATURES TO BE ENABLED ON MICROCONTROLLER ESP 8266
#define ACCESS_CONTROL_FEAT 1
#define PERSISTENCE_FEAT 0
#define SUBSCRIPTION_FEAT 1
#define REQ_LVL_FEAT 0
#define JSONMAXSIZE_OUTPUT 7168
#define GROUP_FEAT 0
#define HTTP_BINDING 1
#define HTTP_BINDING_ENABLED_DEFAULT 1
#define FEAT_RCN_9 1

#endif

/** PERSISTENCE FEATURE */
#if PERSISTENCE_FEAT
// default backup period, can be set through env var LOM2M_BACKUP_PERIOD
#define PERSIST_PERIOD_MS 60000
#endif

/** RESOURCES THRESHOLDS */
/**
 * Use this section to define thresholds in terms of 
 * max number of existing resources
 */
#if TARGET_UNIX

// Global Threshold
#define GLOBAL_MAX_NUMBER_RESOURCES -1
// CIN Specific threshold (per CNT)
#define MAX_CIN_PER_CNT 10

#else

#define GLOBAL_MAX_NUMBER_RESOURCES 30
#define MAX_CIN_PER_CNT 1
#define DEFAULT_PROTOCOL_HTTP 1

#endif

/////////////////////////////////////////////////////////////////////


/////////////////// CSE CONNECTIVITY CONFIG /////////////////////////
#define CONF_REMOTE_POA "http://192.168.1.19:8282"
#define CONF_REMOTE_CSE_ID "lom2m-cse-1"
#define CONF_REMOTE_CSE_NAME "lom2m-mn-1"
#define CONF_ADMIN_ORIGINATOR "admin:admin"
#define CONF_LOCAL_POA "http://"+IP+":"+PORT
/////////////////////////////////////////////////////////////////////

//////////////// DEVELOPMENT DEBUG CONFIGURATION ////////////////////
//////////// !!! ALL SHOULD BE SET TO 0 IN PROD !!! /////////////////
/** Used for test purpose only: enable verbose answers on errors */
#define VERBOSE 0
/** Used for test purpose only: skip verification request (SUB) */
#define SKIP_VRQ 0
/** Used for Debug & test only: should be set to true otherwise */
#define DISABLE_RI_CHECK 0
/** Used to work with a remote CSE using release 1, dev purpose */
#define REL_1 0
/////////////////////////////////////////////////////////////////////

#endif // __LOM2M_CONFIG
