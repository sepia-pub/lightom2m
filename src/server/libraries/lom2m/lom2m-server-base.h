/*
------------------------------------------------------------

SOFTWARE EVALUATION LICENSE

LAAS research laboratory, Toulouse, France.
IRIT computer science research laboratory, Toulouse, France.
------------------------------------------------------------

Contact: Thierry Monteil, thierry.monteil@irit.fr

Definitions
SOFTWARE:  The LightOM2M software, version 1.0, in source code form, written by David Gauchard, Guillaume Garzone and Thierry Monteil
at the LAAS research laboratory.

LICENSOR: LE CENTRE NATIONAL DE LA RECHERCHE SCIENTIFIQUE, a public scientific and technical research establishment, having SIREN No. 180 089 013, APE code 7219 Z, having its registered office at 3, rue Michel-Ange, 75796 Paris cedex 16, France, acting in its own name and on its own behalf, and on behalf of Le laboratoire d'analyse et d'architecture des systèmes (LAAS), UPR n°8001.

1. INTENT/PURPOSE
This agreement determines the conditions in which the LICENSOR, who has the rights to the SOFTWARE, grants to the LICENSEE a license for research and evaluation purposes only, excluding any commercial use.

2. LICENSEE
Any person or organization who receives the SOFTWARE with a copy of this license.

3. RIGHTS GRANTED
The rights to use and copy the SOFTWARE, subject to the restrictions described in this agreement.
The rights to modify and compile the SOFTWARE when it's provided in source code form, subject to the restrictions described in this agreement.
For the SOFTWARE provided in binary form only, LICENSEE undertakes to not decompile, disassemble, decrypt, extract the components or perform reverse engineering except to the extent expressly provided by law.

5. SCOPE OF THE LICENSE
- NON-COMMERCIAL license for research and evaluation purposes ONLY.
- NO right to commercialize the SOFTWARE, or any derivative work, without separate agreement with the LICENSOR.

6. MODIFICATION OF THE SOFTWARE
License permits LICENSEE to modify the SOFTWARE provided in source code form for research and evaluation purposes only.

7. REDISTRIBUTION
- License permits LICENSEE to redistribute verbatim copies of the SOFTWARE, accompanied with a copy of this license.
- License DOES NOT permit LICENSEE to redistribute modified versions of the SOFTWARE provided in source code form.
- License DOES NOT permit LICENSEE to commercialize the SOFTWARE or any derivative work of the SOFTWARE.

8. FEE/ROYALTY
- LICENSEE pays no royalty for this license.
- LICENSEE and any third parties must enter a new agreement for any use beyond scope of license. Please contact the IRIT technology transfer office (email numerique@toulouse-tech-transfer.com) for further information.

9. NO WARRANTY
The SOFTWARE is provided "as is" without warranty of any kind, either expressed or implied, including, but not limited to, the implied warranties of merchantability and fitness for a particular purpose. The entire risk as to the quality and performance of the program is with the LICENSEE.

10. NO LIABILITY
In no event unless required by applicable law or agreed to in writing will any copyright owner be liable to LICENSEE for damages, including any general, special, incidental or consequential damages arising out of the use or inability to use the program (including but not limited to loss of data or data being rendered inaccurate or losses sustained by LICENSEE or third parties or a failure of the program to operate with other programs), even if such holder has been advised of the possibility of such damages.
*/


#ifndef LOM2M_SERVER_BASE_H
#define LOM2M_SERVER_BASE_H

#include "configuration.h"

#include <ESP8266HTTPUpdateServer.h>
#include <ESP8266mDNS.h>
#include <LittleFS.h>
#include <assert.h>
#include <map>

#include "Entity.h"
#include "IPE.h"
#include "PersistenceHelper.h"
#include "bsp.h"
#include "MainController.h"
#include "lom2m.h"
#include "httpBinding.h"
#include "tools.h"

#define HEAP_TRACE_MS 5000 // define to 0 to disable

WiFiClient tcp;
String mDNSName = MDNSNAME;
ESP8266WebServer om2msrv(8282);
#if !CORE_MOCK
ESP8266HTTPUpdateServer httpUpdater; // <= OTA
#endif

void
user_initial_setup();
void
user_final_setup();
void
user_loop();

#if TARGET_UNIX
void
init_env_variables();
#endif

bool registered = false;

/////////////////////////////////////////////////////////////////////
// adm

///////////////////////// HTTP SPECIFIC /////////////////////////////
#if HTTP_BINDING
void
serveFallback()
{
    log("----> NOT SERVED URI = %s\n", om2msrv.uri().c_str());

    String uri = om2msrv.uri().c_str();
    uri.replace(F("//"), F("/"));
    HTTPBinding::serveOM2M(uri);
}

bool
serveFile(String path)
{
    if (path.endsWith("/"))
    {
        path += F("index.html");
    }
    String pathgz = path + F(".gz");
    if (LittleFS.exists(pathgz))
    {
        path = pathgz;
    }
    if (LittleFS.exists(path))
    {
        File file = LittleFS.open(path, "r");
        om2msrv.streamFile(file, contentType(path));
        file.close();
        return true;
    }
    return false;
}

#define printHttpCode(code) _printHttpCode(code, #code)
HTTPCode
_printHttpCode(HTTPCode code, const char* desc)
{
    log("\n----> %s: request returned %d (%s)\n\n", desc, code, HTTPCode2Human(code).c_str());
    return code;
}
#endif
/////////////////////////////////////////////////////////////////////
void
setup()
{
    Serial.begin(115200);
    Serial.printf("\n\nESP826/Arduino - %s\n", ESP.getFullVersion().c_str());
    Serial.printf("host name = " MDNSNAME "\n");
    Serial.printf("mac address = %s\n", WiFi.macAddress().c_str());
    Serial.printf("\n\n---- LOM2M server ----\n\n");
    Serial.printf("Connecting to '%s'", mySSID);
    WiFi.hostname(mDNSName);
#if TARGET_UNIX
    const char* ssid = std::getenv("LOM2M_SSID");
    const char* psk = std::getenv("LOM2M_PSK");
    if (ssid && psk)
    {
        WiFi.begin(ssid, psk);
    }
    else
    {
        WiFi.begin(mySSID, myPSK);
    }
    delete ssid;
    delete psk;
#else
    WiFi.begin(mySSID, myPSK);
#endif
    while (WiFi.status() != WL_CONNECTED)
    {
        Serial.print(".");
        delay(1000);
    }
    log_setup();
    log("\nConnected\n");

    log("Try me at these addresses:");
    log("(with 'telnet <addr> or 'nc -u <addr> 23')");

    IP = WiFi.localIP().toString();
    uint8_t mac[6];
    char ret[14];
    WiFi.macAddress(mac);
    sprintf(ret, "%02X%02X%02X%02X%02X%02X", mac[0], mac[1], mac[2], mac[3], mac[4], mac[5]);
    #ifdef CONF_LOCAL_CSE_ID
    CSE_ID = CONF_LOCAL_CSE_ID;
    #else
    CSE_ID = "mn-cse-" + String(ret);
    #endif // CONF_LOCAL_CSE_ID
    #ifdef CONF_LOCAL_CSE_NAME
    CSE_NAME = CONF_LOCAL_CSE_NAME;
    #else
    CSE_NAME = "mn-" + String(ret);
    #endif // CONF_LOCAL_CSE_NAME
#if TARGET_UNIX
    init_env_variables();
#endif

    user_initial_setup();

#if 1 //CORE_MOCK
    log("IP=%s\n", IP.c_str());
#else
    for (auto a : addrList)
    {
        log("IF='%s' IPv6=%d local=%d hostname='%s' addr= %s",
            a.ifname().c_str(),
            a.isV6(),
            a.isLocal(),
            a.ifhostname(),
            a.toString().c_str());

        if (a.isLegacy())
            log("        mask:%s / gw:%s",
                a.netmask().toString().c_str(),
                a.gw().toString().c_str());
    }
#endif

#if 0 //!CORE_MOCK // works but too noisy for debug
    MDNS.begin(mDNSName);
    MDNS.addService("http", "tcp", "port", 8282);
#endif

#if !CORE_MOCK
    httpUpdater.setup(&om2msrv); // <= setup OTA before om2msrv.begin();
#endif

#if HTTP_BINDING
    // enables the HTTP server to store specified headers
    // otherwise they are discarded for
    if (HTTP_BINDING_ENABLED)
    {
        const char* headers[] = { "Content-Type", "X-M2M-Origin", "Accept", "X-M2M-RI", "X-M2M-RVI" };
        om2msrv.collectHeaders(headers, sizeof headers / sizeof headers[0]);
        om2msrv.begin();
        om2msrv.onNotFound([]() {
            if (!serveFile(om2msrv.uri()))
            {
                serveFallback();
            }
        });
    }
#endif // HTTP_BINDING

#if PERSISTENCE_FEAT
    // LOAD DATA FROM PERSISTENCE FILE
    if (BACKUP_ENABLED)
    {
        int error = PersistenceHelper::loadAll();
        if (error != PersistenceHelper::P_SUCCESS && error != PersistenceHelper::P_FILE_NOT_FOUND)
        {
            Serial.println("\n\nERROR: error while loading data from persistence.\n");
        }
    }
#endif

    user_final_setup();
    // sending CSE REGISTRATION
    registered = registerCSE();
}

#if HEAP_TRACE_MS
Millis heapLastMs = 0;
#endif // HEAP_TRACE_MS
#if PERSISTENCE_FEAT
Millis lastPersistMs = 0;
#endif // PERSISTENCE_FEAT

void
loop()
{

    Millis nowMs = millis();

#if DEBUG
    if (nowMs - heapLastMs > HEAP_TRACE_MS)
    {
        heapLastMs = nowMs;
        log("-- free heap: %d bytes", ESP.getFreeHeap());
    }
#endif
#if HTTP_BINDING
    if (HTTP_BINDING_ENABLED)
    {
        om2msrv.handleClient();
    }
#endif // HTTP_BINDING

#if 0 //!CORE_MOCK
    MDNS.update();
#endif
    user_loop();
#if SUBSCRIPTION_FEAT
    handleNotifications(3);
#endif // SUBSCRIPTION_FEAT

    // ADD other conditions here if necessary to update the registration
    if (registered && descendantCsesToUpdate) 
    {
        #if TRACE
        Serial.printf("TRACE: UPDATING REGISTRATION...\n");
        #endif // TRACE
        ResponseStatusCode rsc = MainController::updateRegistration();
        if (rsc == R2004_UPDATED)
        {
            #if INFO
            Serial.printf("INFO: Registration upadted (descendant CSEs)\n");
            #endif
        }
        else
        {
            Serial.printf("ERROR during update of registration (descendant CSEs)\n");
        }
        // See if needed to resend the update of registration   
        descendantCsesToUpdate = false;
    }


#if PERSISTENCE_FEAT
    if (BACKUP_ENABLED)
    {
        if ((nowMs - lastPersistMs > BACKUP_PERIOD) && dataUpdated)
        {
            int result = PersistenceHelper::persistAll();
            Serial.printf("DEBUG: persitence, result code: %d\n", result);
            dataUpdated = false;
            lastPersistMs = nowMs;
        }
    }
#endif
}

/**
 * Loads configuration values from system environement variables if set
 */
#if TARGET_UNIX
void
init_env_variables()
{
    if (const char* poa = std::getenv("LOM2M_REMOTE_POA"))
    {
        REMOTE_CSE_POA = String(poa);
    }
    if (const char* cseId = std::getenv("LOM2M_REMOTE_CSE_ID"))
    {
        REMOTE_CSE_ID = String(cseId);
    }
    if (const char* cseName = std::getenv("LOM2M_REMOTE_CSE_NAME"))
    {
        REMOTE_CSE_NAME = String(cseName);
    }
    if (const char* cseName = std::getenv("LOM2M_CSE_NAME"))
    {
        CSE_NAME = String(cseName);
    }
    if (const char* cseId = std::getenv("LOM2M_CSE_ID"))
    {
        CSE_ID = String(cseId);
    }
    if (const char* adminOriginator = std::getenv("LOM2M_ADMIN_ORIGINATOR"))
    {
        ADMIN_ORIGINATOR = String(adminOriginator);
    }
    if (const char* envIp = std::getenv("LOM2M_LOCAL_POA_IP"))
    {
        IP = String(envIp);
    }
    if (const char* envPort = std::getenv("LOM2M_LOCAL_POA_PORT"))
    {
        PORT = String(envPort);
    }
    #if PERSISTENCE_FEAT
    if (const char* envBackup = std::getenv("LOM2M_BACKUP_PERIOD"))
    {
        BACKUP_PERIOD = std::atoi(envBackup);
    }
    if (const char* envBackup = std::getenv("LOM2M_BACKUP_ENABLED"))
    {
        BACKUP_ENABLED = std::atoi(envBackup);
    }
    #endif
    if (const char* in = std::getenv("LOM2M_MAX_NUMBER_OF_INSTANCES_DEFAULT"))
    {
        MAX_NUMBER_OF_INSTANCES_DEFAULT = std::atoi(in);
    }
    if (const char* in = std::getenv("LOM2M_MAX_NUMBER_RESOURCES"))
    {
        globalResourcesThreshold = std::atoi(in);
    }
    #if HTTP_BINDING
    if (const char* in = std::getenv("LOM2M_HTTP_BINDING"))
    {
        HTTP_BINDING_ENABLED = std::atoi(in);
    }
    #endif // HTTP_BINDING
    if (const char* in = std::getenv("LOM2M_DEFAULT_PROTOCOL"))
    {
        DEFAULT_PROTOCOL = String(in);
    }
    // Enable regsitration all originators
    if (const char* in = std::getenv("LOM2M_ENABLE_REGISTRATION_ALL"))
    {
        if (in)
        {
            if (std::atoi(in) == 1)
            {
                AccessControlPolicy* acp = AcpAdmin::getInstance();
                AccessControlRule* ruleAll = new AccessControlRule(15);
                ruleAll->m_accessControlOriginators.push_back("all");
                acp->m_privileges.push_back(*ruleAll);
                delete ruleAll;
            }
        }
    }

}
#endif

#endif // LOM2M_SERVER_BASE_H
