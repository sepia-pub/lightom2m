/*
------------------------------------------------------------

SOFTWARE EVALUATION LICENSE

LAAS research laboratory, Toulouse, France.
IRIT computer science research laboratory, Toulouse, France.
------------------------------------------------------------

Contact: Thierry Monteil, thierry.monteil@irit.fr

Definitions
SOFTWARE:  The LightOM2M software, version 1.0, in source code form, written by David Gauchard, Guillaume Garzone and Thierry Monteil
at the LAAS research laboratory.

LICENSOR: LE CENTRE NATIONAL DE LA RECHERCHE SCIENTIFIQUE, a public scientific and technical research establishment, having SIREN No. 180 089 013, APE code 7219 Z, having its registered office at 3, rue Michel-Ange, 75796 Paris cedex 16, France, acting in its own name and on its own behalf, and on behalf of Le laboratoire d'analyse et d'architecture des systèmes (LAAS), UPR n°8001.

1. INTENT/PURPOSE
This agreement determines the conditions in which the LICENSOR, who has the rights to the SOFTWARE, grants to the LICENSEE a license for research and evaluation purposes only, excluding any commercial use.

2. LICENSEE
Any person or organization who receives the SOFTWARE with a copy of this license.

3. RIGHTS GRANTED
The rights to use and copy the SOFTWARE, subject to the restrictions described in this agreement.
The rights to modify and compile the SOFTWARE when it's provided in source code form, subject to the restrictions described in this agreement.
For the SOFTWARE provided in binary form only, LICENSEE undertakes to not decompile, disassemble, decrypt, extract the components or perform reverse engineering except to the extent expressly provided by law.

5. SCOPE OF THE LICENSE
- NON-COMMERCIAL license for research and evaluation purposes ONLY.
- NO right to commercialize the SOFTWARE, or any derivative work, without separate agreement with the LICENSOR.

6. MODIFICATION OF THE SOFTWARE
License permits LICENSEE to modify the SOFTWARE provided in source code form for research and evaluation purposes only.

7. REDISTRIBUTION
- License permits LICENSEE to redistribute verbatim copies of the SOFTWARE, accompanied with a copy of this license.
- License DOES NOT permit LICENSEE to redistribute modified versions of the SOFTWARE provided in source code form.
- License DOES NOT permit LICENSEE to commercialize the SOFTWARE or any derivative work of the SOFTWARE.

8. FEE/ROYALTY
- LICENSEE pays no royalty for this license.
- LICENSEE and any third parties must enter a new agreement for any use beyond scope of license. Please contact the IRIT technology transfer office (email numerique@toulouse-tech-transfer.com) for further information.

9. NO WARRANTY
The SOFTWARE is provided "as is" without warranty of any kind, either expressed or implied, including, but not limited to, the implied warranties of merchantability and fitness for a particular purpose. The entire risk as to the quality and performance of the program is with the LICENSEE.

10. NO LIABILITY
In no event unless required by applicable law or agreed to in writing will any copyright owner be liable to LICENSEE for damages, including any general, special, incidental or consequential damages arising out of the use or inability to use the program (including but not limited to loss of data or data being rendered inaccurate or losses sustained by LICENSEE or third parties or a failure of the program to operate with other programs), even if such holder has been advised of the possibility of such damages.
*/


#include "Controller.h"
#if SUBSCRIPTION_FEAT
#if HTTP_BINDING
#include "httpBinding.h"
#endif

ResponseStatusCode
SubscriptionController::performVerificationRequest(const RequestPrimitive& request, Subscription& sub)
{
    std::list<String> urls;
    ResponsePrimitive* response;
    RequestPrimitive* requestToSend;
    Notification* notif;
#if DEBUG
    printf("DEBUG: PERFORMING VRQ\n");
#endif
    ResponseStatusCode result = R5103_TARGET_NOT_REACHABLE; //  TO UPDATE
    notif = new Notification();

#if DEBUG
    Serial.printf("DEBUG BEFORE FOR URI\n");
#endif
    for (String uri : sub.notificationURI)
    {
        urls.clear();
        String aeid = emptyString;
        String targetId = emptyString;
        if (uri.startsWith("http://"))
        {
            urls.push_back(uri);
        }
        else
        {
            UriType uriType = URI_CSE_RELATIVE;
#if DEBUG
            Serial.printf("DEBUG: looking for AE: %s\n", uri.c_str());
#endif // DEBUG
            if (!uri.startsWith(S_slash))
            {
                uri = String(S_slash) + CSE_ID + String(S_slash) + uri;
            }
            if (uri.startsWith(S_slash + M2M_SP_ID))
            {
                uriType = URI_ABSOLUTE;
            }
            else if (uri.startsWith(S_slash + CSE_ID))
            {
                uriType = URI_SP_RELATIVE;
            }
            Entity* e = AbstractController::findEntityFromUri(uri, uriType);
            // Application* ae =
            if (!e || !e->getApplication())
            {
#if DEBUG
                printf("DEBUG: error while sending notify, AE not found %s\n", uri.c_str());
#endif // DEBUG
                delete (notif);
                return R4004_NOT_FOUND;
            }
            else
            {
                urls = (e->getApplication())->m_poa;
                targetId = CSE_ID;
                aeid = (e->getApplication())->getIdentifier();
            }
        }
        for (String url : urls)
        {
#if DEBUG
            Serial.printf("DEBUG IN FOR URI\n");
#endif
            if (!url.equals(request.getFrom()))
            {
#if DEBUG
                Serial.printf("DEBUG: checking uri %s\n", url.c_str());
#endif
                notif = new Notification();
                notif->setCreator(sub.getCreator());
                notif->setVerificationRequest(true);
                notif->setSubReference(sub.getFullName());
                notif->setSubDeletion(false);
                requestToSend = new RequestPrimitive();
                requestToSend->init();
#if DEBUG
                printf("DEBUG: creating report\n");
#endif
                requestToSend->setResourceType(TY_NOTIFICATION);
                String content = requestToSend->createNotifyBody(notif, NCT_NULL_VRQ);
                requestToSend->setContent(content);
                requestToSend->setFrom("/" + CSE_ID);
                requestToSend->setTo(url);
                requestToSend->setOperation(OP_NOTIFY);
                requestToSend->setRequestContentType();
                requestToSend->setRequestId(String((long)get_utime()));
#if DEBUG
                Serial.printf("DEBUG: sending verification request to %s\n", url.c_str());
#endif
                response = new ResponsePrimitive();
                if (url.startsWith(F("http://")) && HTTP_BINDING_ENABLED)
                {
#if HTTP_BINDING
                    HTTPBinding::sendRequest(requestToSend, response);
#endif
                }

                // TODO add case where NU is AE URI
#if DEBUG
                printf("DEBUG: received response: RSC: %s, RC: %d\n", String(response->getResponseStatusCode()).c_str());
#endif // DEBUG
                if (response->getResponseStatusCode() == R2000_OK || response->getResponseStatusCode() == R204_NO_CONTENT)
                {
#if INFO
                    printf("INFO: Verification request sent successfully.\n");
#endif
                    result = R2000_OK;
                    break;
                }
            }
        }
    }
    delete notif;
    delete response;
    delete requestToSend;

    return result;
}

#endif
