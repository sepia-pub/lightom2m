/*
------------------------------------------------------------

SOFTWARE EVALUATION LICENSE

LAAS research laboratory, Toulouse, France.
IRIT computer science research laboratory, Toulouse, France.
------------------------------------------------------------

Contact: Thierry Monteil, thierry.monteil@irit.fr

Definitions
SOFTWARE:  The LightOM2M software, version 1.0, in source code form, written by David Gauchard, Guillaume Garzone and Thierry Monteil
at the LAAS research laboratory.

LICENSOR: LE CENTRE NATIONAL DE LA RECHERCHE SCIENTIFIQUE, a public scientific and technical research establishment, having SIREN No. 180 089 013, APE code 7219 Z, having its registered office at 3, rue Michel-Ange, 75796 Paris cedex 16, France, acting in its own name and on its own behalf, and on behalf of Le laboratoire d'analyse et d'architecture des systèmes (LAAS), UPR n°8001.

1. INTENT/PURPOSE
This agreement determines the conditions in which the LICENSOR, who has the rights to the SOFTWARE, grants to the LICENSEE a license for research and evaluation purposes only, excluding any commercial use.

2. LICENSEE
Any person or organization who receives the SOFTWARE with a copy of this license.

3. RIGHTS GRANTED
The rights to use and copy the SOFTWARE, subject to the restrictions described in this agreement.
The rights to modify and compile the SOFTWARE when it's provided in source code form, subject to the restrictions described in this agreement.
For the SOFTWARE provided in binary form only, LICENSEE undertakes to not decompile, disassemble, decrypt, extract the components or perform reverse engineering except to the extent expressly provided by law.

5. SCOPE OF THE LICENSE
- NON-COMMERCIAL license for research and evaluation purposes ONLY.
- NO right to commercialize the SOFTWARE, or any derivative work, without separate agreement with the LICENSOR.

6. MODIFICATION OF THE SOFTWARE
License permits LICENSEE to modify the SOFTWARE provided in source code form for research and evaluation purposes only.

7. REDISTRIBUTION
- License permits LICENSEE to redistribute verbatim copies of the SOFTWARE, accompanied with a copy of this license.
- License DOES NOT permit LICENSEE to redistribute modified versions of the SOFTWARE provided in source code form.
- License DOES NOT permit LICENSEE to commercialize the SOFTWARE or any derivative work of the SOFTWARE.

8. FEE/ROYALTY
- LICENSEE pays no royalty for this license.
- LICENSEE and any third parties must enter a new agreement for any use beyond scope of license. Please contact the IRIT technology transfer office (email numerique@toulouse-tech-transfer.com) for further information.

9. NO WARRANTY
The SOFTWARE is provided "as is" without warranty of any kind, either expressed or implied, including, but not limited to, the implied warranties of merchantability and fitness for a particular purpose. The entire risk as to the quality and performance of the program is with the LICENSEE.

10. NO LIABILITY
In no event unless required by applicable law or agreed to in writing will any copyright owner be liable to LICENSEE for damages, including any general, special, incidental or consequential damages arising out of the use or inability to use the program (including but not limited to loss of data or data being rendered inaccurate or losses sustained by LICENSEE or third parties or a failure of the program to operate with other programs), even if such holder has been advised of the possibility of such damages.
*/


#include "MainController.h"

#include "AccessControlController.h"
#include "Controller.h"
#include "JsonDatamapper.h"
#include "Redirector.h"
#include "RequestPrimitive.h"
#include "ShortNames.h"
#if HTTP_BINDING
#include "httpBinding.h"
#endif // HTTP_BINDING
#include "stdlib.h"
#include "tools.h"
#include "lom2m.h"

#ifndef DEBUG
#define DEBUG false
#endif

/////////////////////////////////////////////////////////////////////
// generic

String httpmessage;
HTTPCode httpretcode;
std::vector<String> uriSplit;
ResponsePrimitive* RESPONSE = nullptr;
RequestPrimitive builder; // used to build request body to send, to be updated when mapping process improved

/**
 * Send an HTTP response
 * @param code - the oneM2M response status code (will add also the HTTP code)`
 * @param message - can be empty, it is the body message of the response
 */
void
requestReply(ResponseStatusCode code, const String& message)
{
#if DEBUG
    log("answer: %d => %s\n", (int)code, message.c_str());
#endif
    RESPONSE->setResponseStatusCode(code);
#if VERBOSE
    RESPONSE->setContentType(F("text/plain"));
    RESPONSE->setContent(message);
#endif
    #if HTTP_BINDING
    HTTPBinding::sendResponse(*RESPONSE);
    #endif
    delete (RESPONSE);
}

/////////////////////////////////////////////////////////////////////
// OM2M entry point

void
Router::initResp(RequestPrimitive& req, ResponsePrimitive* resp)
{
    if (!resp)
    {
        resp = new ResponsePrimitive();
    }
    else
    {
        resp->init();
    }
    resp->setTo(req.getFrom());
    resp->setFrom("/" + CSE_ID);
    resp->setRequestId(req.getRequestId());
}

int
Router::processRequest(RequestPrimitive& req, ResponsePrimitive* resp)
{
    FROM_ORIGINATOR = emptyString;
    CseBase* csb = CseBase::getInstance();
    String message = emptyString;
    String uri = req.getTo();
    initResp(req, resp);
    #if FEAT_RCN_9
    modifiedAttributes.clear();
    #endif // FEAT_RCN_9

    // handle request PRIMITIVE

    // check M attributes
    ResponseStatusCode rsc = AbstractController::checkValidityRequestPri(req);
    if (rsc != R2000_OK)
    {
        resp->setResponseStatusCode(rsc);
        return 0;
    }
    
    #if !DISABLE_RI_CHECK
    if (!req.getRequestId() || req.getRequestId() == emptyString || req.getRequestId() == "*/*")
    {
        resp->setResponseStatusCode(R4000_BAD_REQUEST);
#if VERBOSE
        message = "RI is mandatory";
        resp->setContentType(F("text/plain"));
        resp->setContent(message);
#endif
        return 0;
    }
#endif
#if INFO
    log("---- method=%s -------------------------", method(method()));
#endif
    // check originator // TODO (optionnal if CREATE)
    // updated: taken into account later w/ checkACP
    const String& cred = req.getFrom();
    const String& acceptHeader = req.getWantedContentType();
    const String& contentType = req.getRequestContentType();
#if INFO
    log("uri=%s", uri.c_str());
#endif

    int ty = TY_NONE;
    // checking return content type
    if (!acceptHeader.equalsIgnoreCase("application/json")) // TODO USE CONSTANT
    {
        resp->setResponseStatusCode(R4015_UNSUPPORTED_MEDIA_TYPE);
#if VERBOSE
        resp->setContentType(F("text/plain"));
        resp->setContent("wanted content type: " + acceptHeader + " not supported\n");
#endif
        return 0;
    }

    bool cseTarget = false;
    Entity* targetEntity = nullptr;

    // split uri by '/' removing '~'
    uriSplit.clear();
    //printf("DEBUG: uriSplit: %s\n", uriSplit[0].c_str());
    for (int sl, idx = 0; idx < (int)uri.length(); idx = sl + 1)
    {
        sl = uri.indexOf('/', idx);
        if (sl < 0)
        {
            sl = uri.length(); //break;
        }
        if (sl <= idx + 1)
        {
            //idx = sl;
            if (sl < uri.length() - 1)
            {
                continue;
            }
        }
        String name = uri.substring(idx, sl);
#if DEBUG
        printf("DEBUG: urisplit name: %s\n", name.c_str());
#endif
        if (name.equals("~") || name.equals("/") || name.equals("_"))
        {
            continue;
        }
        uriSplit.emplace_back(name);
    }

    String uri2;
    if (uri.startsWith("/~/"))
    {
        req.setUriType(URI_SP_RELATIVE);
    }
    else if (uri.startsWith("/_/"))
    {
        req.setUriType(URI_ABSOLUTE);
    }
    if (uri.startsWith("/~/") || uri.startsWith("/_/"))
    {
        uri2 = uri.substring(2);
    }
    else if (uri.startsWith("/"))
    {
        uri2 = uri;//.substring(1);
    }
    if (uri2.endsWith("/"))
    {
        uri2.remove(uri2.length() - 1);
    }
#if TRACE
    printf("TRACE: full URI CLEANED: %s\n", uri2.c_str());
#endif
    // check if uri is hierarchical or not
    bool hierarchical = (uriSplit.size() >= 2 && uriSplit[1] == CSE_NAME) ;
    hierarchical = hierarchical || (uriSplit[0] == CSE_NAME);
    if (hierarchical)
    {
#if DEBUG
        printf("DEBUG: hierachical URI\n");
#endif
        cseTarget = (uriSplit.size() == 2 && uriSplit[0] == CSE_ID);
        cseTarget = cseTarget || (uriSplit.size() == 1 && uriSplit[0] == CSE_NAME);
#if DEBUG
        if (cseTarget)
        {
            printf("DEBUG: CSE TARGET\n");
        }
#endif
    }
    else if (uriSplit[0] == CSE_ID)
    {
#if DEBUG
        printf("DEBUG: non hierachical URI, SP-RELATIVE case\n");
#endif
        if (uriSplit.size() == 3 && (uriSplit[2].equalsIgnoreCase(LATEST) || uriSplit[2].equalsIgnoreCase(OLDEST)))
        {
            #if TRACE
            Serial.printf("TRACE: NON HIERARCHICAL URI, SP-REL, using /la or /ol\n");
            #endif
        }
        else if (uriSplit.size() > 2)
        {
            resp->setResponseStatusCode(R4000_BAD_REQUEST);
#if VERBOSE
            resp->setContent("malformed URI\n");
            resp->setContentType("text/plain");
#endif
            return 0;
        }
        if (uriSplit.size() == 1 && uriSplit[0] == CSE_ID)
        {
            cseTarget = true;
        }
    }
    else if (
        (uriSplit.size() == 1 && Entity::getByIdentifier(uriSplit[0])) 
        || (uriSplit.size() == 2 && Entity::getByIdentifier(uriSplit[0]) && uriSplit[1].equalsIgnoreCase(LATEST))
        || (uriSplit.size() == 2 && Entity::getByIdentifier(uriSplit[0]) && uriSplit[1].equalsIgnoreCase(OLDEST))
        )
    {
        // CSE RELATIVE CASE with CSE RELATIVE RESOURCE ID
        #if DEBUG
        Serial.printf("DEBUG: non hierarchical, CSE-RELATIVE CASE \n");
        #endif // DEBUG
        targetEntity = Entity::getByIdentifier(uriSplit[0]);
    }
    else
    {
        // CSE BASE is not the target > redirect the request
#if INFO
        printf("INFO: redirecting request...\n");
#endif
        ResponsePrimitive* response = new ResponsePrimitive();
        Redirector::retarget(&req, response);
        *resp = *response;
        delete response;
        return 0;
    }

    if (!uri2.startsWith("/"))
    {
        uri2 = "/" + uri2;
    }

    // entity targeted by the request
    if (!targetEntity) // case the target has not been found yet
    {
        if (cseTarget)
        {
            targetEntity = csb;
        }
        else
        {
            if (hierarchical)
            {
                if (uri2.startsWith("/" + CSE_NAME) && !uri2.startsWith("/" + CSE_ID))
                {
                    Serial.printf("DEBUG: uri starts with CSE name, adding CSE ID\n");
                    uri2 = CSE_ID + uri2;
                    uri2 = "/" + uri2;
                }
                if (uriSplit[uriSplit.size() - 1].equalsIgnoreCase(LATEST) || uriSplit[uriSplit.size() - 1].equalsIgnoreCase(OLDEST))
                {
                    uri2.remove(uri2.length() - 3);
                    targetEntity = Entity::getByHierUri(uri2);
                    if (!targetEntity)
                    {
                        resp->setResponseStatusCode(R4004_NOT_FOUND);
#if VERBOSE
                        resp->setContent(String(F("resource not found\n")));
                        resp->setContentType(String(F("text/plain")));
#endif
                        return 0;
                    }
                    Container* cnt = targetEntity->getContainer();
                    if (cnt)
                    {
                        std::list<Entity*> ch = cnt->getChildren();
                        targetEntity = nullptr;
                        if (uriSplit[uriSplit.size() - 1].equalsIgnoreCase(LATEST))
                        {
#if DEBUG
                            printf("DEBUG: LATEST case\n");
#endif
#if DEBUG
                            printf("DEBUG: size of children: %d\n", cnt->getChildren().size());
#endif
                            if (!cnt->getChildren().empty())
                            {
                                for (std::list<Entity*>::reverse_iterator rit = ch.rbegin(); rit != ch.rend(); rit++)
                                {
                                    //printf("DEBUG: iterating from end of children: %s\n", (*rit)->getName().c_str());

                                    if ((*rit))
                                    {
                                        if ((*rit)->getType() == TY4_CONTENT_INSTANCE)
                                        {
#if DEBUG
                                            printf("DEBUG: found la\n");
#endif
                                            targetEntity = *rit;
                                            break;
                                        }
                                    }
                                }
                            }
                        }
                        else if (uriSplit[uriSplit.size() - 1].equalsIgnoreCase(OLDEST))
                        {
                            for (Entity* ent : ch)
                            {
                                if (ent->getType() == TY4_CONTENT_INSTANCE)
                                {
                                    targetEntity = ent;
                                    break;
                                }
                            }
                        }
                    }
                }
                else
                {
                    #if DEBUG
                    Serial.printf("DEBUG: looking for resource by hier uri with: %s\n", uri2.c_str());
                    #endif
                    targetEntity = Entity::getByHierUri(uri2);
                }
            }
            else if (!hierarchical)
            {
#if DEBUG
                printf("DEBUG: looking for resource: %s\n", uriSplit[1].c_str());
#endif
                targetEntity = Entity::getByIdentifier(uriSplit[1]);
                if (uriSplit[uriSplit.size() - 1].equalsIgnoreCase(LATEST) || uriSplit[uriSplit.size() - 1].equalsIgnoreCase(OLDEST))
                {
                    if (!targetEntity)
                    {
                        resp->setResponseStatusCode(R4004_NOT_FOUND);
#if VERBOSE
                        resp->setContent(String(F("resource not found\n")));
                        resp->setContentType(String(F("text/plain")));
#endif
                        return 0;
                    }
                    Container* cnt = targetEntity->getContainer();
                    if (cnt)
                    {
                        targetEntity = nullptr;
                        std::list<Entity*> ch = cnt->getChildren();
                        if (uriSplit[uriSplit.size() - 1].equalsIgnoreCase(LATEST))
                        {
#if DEBUG
                            printf("DEBUG: LATEST case\n");
#endif
#if DEBUG
                            printf("DEBUG: size of children: %d\n", cnt->getChildren().size());
#endif
                            if (!cnt->getChildren().empty())
                            {
                                for (std::list<Entity*>::reverse_iterator rit = ch.rbegin(); rit != ch.rend(); rit++)
                                {
                                    //printf("DEBUG: iterating from end of children: %s\n", (*rit)->getName().c_str());

                                    if ((*rit))
                                    {
                                        if ((*rit)->getType() == TY4_CONTENT_INSTANCE)
                                        {
#if DEBUG
                                            printf("DEBUG: found la\n");
#endif
                                            targetEntity = *rit;
                                            break;
                                        }
                                    }
                                }
                            }
                        }
                        else if (uriSplit[uriSplit.size() - 1].equalsIgnoreCase(OLDEST))
                        {
                            for (Entity* ent : ch)
                            {
                                if (ent->getType() == TY4_CONTENT_INSTANCE)
                                {
                                    targetEntity = ent;
                                    break;
                                }
                            }
                        }
                    }
                }
            }
        }
    }
    else
    {
        if (uriSplit[uriSplit.size() - 1].equalsIgnoreCase(LATEST) || uriSplit[uriSplit.size() - 1].equalsIgnoreCase(OLDEST))
        {
            if (!targetEntity)
            {
                resp->setResponseStatusCode(R4004_NOT_FOUND);
#if VERBOSE
                resp->setContent(String(F("resource not found\n")));
                resp->setContentType(String(F("text/plain")));
#endif
                return 0;
            }
            Container* cnt = targetEntity->getContainer();
            if (cnt)
            {
                targetEntity = nullptr;
                std::list<Entity*> ch = cnt->getChildren();
                if (uriSplit[uriSplit.size() - 1].equalsIgnoreCase(LATEST))
                {
#if DEBUG
                    printf("DEBUG: LATEST case\n");
#endif
#if DEBUG
                    printf("DEBUG: size of children: %d\n", cnt->getChildren().size());
#endif
                    if (!cnt->getChildren().empty())
                    {
                        for (std::list<Entity*>::reverse_iterator rit = ch.rbegin(); rit != ch.rend(); rit++)
                        {
                            //printf("DEBUG: iterating from end of children: %s\n", (*rit)->getName().c_str());

                            if ((*rit))
                            {
                                if ((*rit)->getType() == TY4_CONTENT_INSTANCE)
                                {
#if DEBUG
                                    printf("DEBUG: found la\n");
#endif
                                    targetEntity = *rit;
                                    break;
                                }
                            }
                        }
                    }
                }
                else if (uriSplit[uriSplit.size() - 1].equalsIgnoreCase(OLDEST))
                {
                    for (Entity* ent : ch)
                    {
                        if (ent->getType() == TY4_CONTENT_INSTANCE)
                        {
                            targetEntity = ent;
                            break;
                        }
                    }
                }
            }
        }
    }
    if (!targetEntity)
    {
        resp->setResponseStatusCode(R4004_NOT_FOUND);
#if VERBOSE
        resp->setContent(String(F("resource not found\n")));
        resp->setContentType(String(F("text/plain")));
#endif
        return 0;
    }

    if (req.getOperation() != OP_DISCOVERY)
    {
        if (!checkAccessRights(targetEntity, req))
        {
            resp->setResponseStatusCode(R4103_ORIGINATOR_HAS_NO_PRIVILEGE);
#if VERBOSE
            resp->setContent(String(F("originator not found\n")));
            resp->setContentType(String(F("text/plain")));
#endif
            return 0;
        }
    }
    FROM_ORIGINATOR = req.getFrom();
    // check operation

    if (req.getOperation() == OP_DISCOVERY)
    {
#if DEBUG
        log("DEBUG: DISCOVERY\n");
#endif
// TODO to improve
// http://www.onem2m.org/application-developer-guide/implementation/discovery
#if INFO
        log((PGM_P)F("INFO: discovery (fu)\n"));
#endif
        DiscoveryController::performDiscovery(req, resp, targetEntity);
        return 0;
    }
    else if (req.getOperation() == OP_RETRIEVE)
    {
#if DEBUG
        log("DEBUG: RETRIEVE\n");
        //unused: const String &type = rqType(ty);
        log("ty=%s", rqType(ty));
#endif
        // GET WITH OTHER RESOURCE THAN CSE
#if DEBUG
        printf("DEBUG: GET WITH OTHER RESOURCE THAN CSE\n");
#endif
        String content = emptyString;
        jsonglobal.clear();
        // serialize target entity
        if (req.getResultContentType() != RCN_NOTHING)
        {
            req.createReport(targetEntity, req.getResultContentType(), req.getFilterCriteria().getLevel(), req.getOperationEnum());
            if (!serializeJson(jsonglobal, content))
            {
                resp->setResponseStatusCode(R5000_INTERNAL_SERVER_ERROR);
                return 1;
            }
            resp->setContent(content);
            resp->setContentType(F("application/json"));
        }
        resp->setResponseStatusCode(R2000_OK);
        return 0;
    }
    else if (req.getOperation() == OP_CREATE)
    {
        if (globalResourcesThreshold >= 0 && Entity::entities.size() >= globalResourcesThreshold)
        {
            resp->setResponseStatusCode(R5207_NOT_ACCEPTABLE);
#if VERBOSE
            resp->setContent("CSE is full: no longer accept resource creation\n");
            resp->setContentType("text/plain");
#endif
            return 0;
        }

        if (uriSplit[uriSplit.size() - 1].equalsIgnoreCase(LATEST) || uriSplit[uriSplit.size() - 1].equalsIgnoreCase(OLDEST))
        {
            resp->setResponseStatusCode(R4005_OPERATION_NOT_ALLOWED);
#if VERBOSE
            resp->setContent(F("POST not authorized on la/ol virtual resource\n"));
            resp->setContentType(String(F("text/plain")));
#endif //VERBOSE
            return 0;
        }
        if (req.getResourceType() == TY_NONE)
        {
            resp->setResponseStatusCode(R4000_BAD_REQUEST);
#if VERBOSE
            resp->setContent(String(F("ty missing, needed for resource creation\r\n")));
            resp->setContentType(String(F("text/plain")));
#endif
            return 0;
        }
        
        ty = req.getResourceType();
        lastError = emptyString;
        if (!jsonDecode(req.getContent())) //, rqType(ty), keyval))
        {
            resp->setResponseStatusCode(R4000_BAD_REQUEST);
#if VERBOSE
            resp->setContent(lastError);
            resp->setContentType(String(F("text/plain")));
#endif
            return 0;
        }
        String name;
        JsonObject o = jsonglobal[rqType(ty)];
        if (!o.containsKey(RESOURCE_NAME))
        {
            name = Entity::generateName(ty);
            o[RESOURCE_NAME] = name;
        }
        else
        {
            name = (const String&)o[RESOURCE_NAME];
            if (name.equalsIgnoreCase(LATEST) || name.equalsIgnoreCase(OLDEST))
            {
                 resp->setResponseStatusCode(R4005_OPERATION_NOT_ALLOWED);
#if VERBOSE
                resp->setContent(String(F("la / ol as name is not allowed")));
                resp->setContentType(String(F("text/plain")));
#endif
                return 0;
            }
            else if (Entity::getByHierUri(targetEntity->getFullName() +"/"+ (const String&)o[RESOURCE_NAME]))
            {
                resp->setResponseStatusCode(R5106_ALREADY_EXISTS);
#if VERBOSE
                resp->setContent(String(F("entity name already exists")));
                resp->setContentType(String(F("text/plain")));
#endif
                return 0;
            }
        }
#if INFO
        Serial.printf("INFO: POST rn='%s' ty=%s\n", name.c_str(), rqType(ty));
#endif
            // NOTIFY
            // IF parent has sub -> notify creation of child
#if SUBSCRIPTION_FEAT
            std::list<Subscription*> subs;
            for (auto ch : targetEntity->getChildren())
            {
                if (ch->getSubscription())
                {
                    subs.push_back(ch->getSubscription());
                }
            }
            #endif
        Entity* res = AbstractController::createResource(req, resp, targetEntity, o);
        if (res && resp->getResponseStatusCode() == R2001_CREATED)
        {
            dataUpdated = true;
#if SUBSCRIPTION_FEAT
            // adding notifications to buffer if needed
            if (!subs.empty())
            {
                Notifier::notify(subs, res, NET_CREATE_DIRECT_CHILD, OP_CREATE);
            }
#endif // SUBSCRIPTION_FEAT

            if (req.getResultContentType() == RCN_NOTHING)
            {
                resp->setContent(emptyString);
                return 0;
            }
            else
            {
                req.createReport(res, req.getResultContentType(), 1);
                String payload = emptyString;
                if (serializeJson(jsonglobal, payload))
                {
                    resp->setContent(payload);
                    resp->setContentType(F("application/json"));
                    return 0;
                }
                else
                {
                    resp->setResponseStatusCode(R5000_INTERNAL_SERVER_ERROR);
                    return 1;
                }
            }
        }
        else
        {
            return 1;
        }
        //return createReport(returnContentType, R2001_CREATED, ret); //g ? (Entity*)g : cnt ? (Entity*)cnt : (Entity*)ae);
    }
    else if (req.getOperation() == OP_UPDATE)
    {
        if (cseTarget)
        {
            resp->setResponseStatusCode(R4005_OPERATION_NOT_ALLOWED);
#if VERBOSE
            resp->setContent(String(F("UPDATE operation not allowed on CSB")));
            resp->setContentType(String(F("text/plain")));
#endif
            return 0;
        }
        else
        {
            if (uriSplit[uriSplit.size() - 1].equalsIgnoreCase(LATEST) || uriSplit[uriSplit.size() - 1].equalsIgnoreCase(OLDEST))
            {
                resp->setResponseStatusCode(R4005_OPERATION_NOT_ALLOWED);
#if VERBOSE
                resp->setContent(F("UPDATE not authorized on la/ol virtual resource\n"));
                resp->setContentType(String(F("text/plain")));
#endif //VERBOSE
                return 0;
            }
            lastError = emptyString;
            if (!jsonDecode(req.getContent())) //, rqType(ty), keyval))
            {
                resp->setResponseStatusCode(R4000_BAD_REQUEST);
#if VERBOSE
                resp->setContent(lastError);
                resp->setContentType(String(F("text/plain")));
#endif
                return 0;
            }

#if INFO
            Serial.printf("UPDATE rn='%s'\n", targetEntity->getName().c_str());
#endif            
            JsonObject o;
            // updating resource specific attributes
            Entity* res = targetEntity;
            if (jsonglobal.containsKey(rqType(TY1_ACP)) && targetEntity->getAcp())
            {
                o = jsonglobal[rqType(TY1_ACP)];
                res = ACPMapper::parseResource(o, emptyString, targetEntity);
            }
            else if (jsonglobal.containsKey(rqType(TY2_APPL_ENTITY)) && targetEntity->getApplication())
            {
                o = jsonglobal[rqType(TY2_APPL_ENTITY)];
                res = AEMapper::parseResource(o, emptyString, targetEntity);
            }
            else if (jsonglobal.containsKey(rqType(TY3_CONTAINER)) && targetEntity->getContainer())
            {
                o = jsonglobal[rqType(TY3_CONTAINER)];
                res = ContainerMapper::parseResource(o, emptyString, nullptr, targetEntity);
            }
            else if (jsonglobal.containsKey(rqType(TY4_CONTENT_INSTANCE)) && targetEntity->getInstance())
            {
                resp->setResponseStatusCode(R4005_OPERATION_NOT_ALLOWED);
                return 0;
            }
#if GROUP_FEAT
            else if (jsonglobal.containsKey(rqType(TY9_GROUP)) && targetEntity->getGroup())
            {
                resp->setResponseStatusCode(R5001_NOT_IMPLEMENTED);
                return 0;
            }
#endif // GROUP_FEAT
            else if (jsonglobal.containsKey(rqType(TY16_REMOTE_CSE)) && targetEntity->getRemoteCse())
            {
                o = jsonglobal[rqType(TY16_REMOTE_CSE)];
                res = RemoteCseMapper::parseResource(o, emptyString, nullptr, targetEntity);
            }
#if SUBSCRIPTION_FEAT
            else if (jsonglobal.containsKey(rqType(TY23_SUBSCRIPTION)) && targetEntity->getSubscription())
            {
                o = jsonglobal[rqType(TY23_SUBSCRIPTION)];
                res = SubscriptionMapper::parseResource(o, emptyString, nullptr, targetEntity);
            }
#endif // SUBSCRIPTION_FEAT
            else
            {
                res = nullptr;
                resp->setResponseStatusCode(R5001_NOT_IMPLEMENTED);
                return 0;
            }

            // updating generic attributes
            if (!res || Mapper::parseGenericAttributes(o, targetEntity) != 0)
            {
                resp->setResponseStatusCode(R4000_BAD_REQUEST);
#if VERBOSE
                resp->setContent(String(F("error while parsing body")));
                resp->setContentType(String(F("text/plain")));
#endif
                return 0;
            }
            else
            {
                if (res->getContainer())
                {
                    res->getContainer()->incrementStateTag();
                    #if FEAT_RCN_9
                    modifiedAttributes.push_back(STATETAG);
                    #endif
                }
                dataUpdated = true;
                resp->setResponseStatusCode(R2004_UPDATED);

                // NOTIFY
                // IF target has sub -> notify update
#if SUBSCRIPTION_FEAT
                std::list<Subscription*> subs;
                for (auto ch : res->getChildren())
                {
                    if (ch->getSubscription())
                    {
                        subs.push_back(ch->getSubscription());
                    }
                }
                // adding notifications to buffer if needed
                if (!subs.empty())
                {
                    Notifier::notify(subs, res, NET_UPDATE_RES, req.getOperationEnum());
                }
#endif // SUBSCRIPTION_FEAT

                if (req.getResultContentType() == RCN_NOTHING)
                {
                    resp->setContent(emptyString);
                    return 0;
                }
                else
                {
                    #if DEBUG && FEAT_RCN_9
                    Serial.printf("DEBUG: size of modified attributes: %d\n", modifiedAttributes.size());
                    #endif
                    req.createReport(res, req.getResultContentType(), 1, OP_NULL);
                    String payload = emptyString;
                    if (serializeJson(jsonglobal, payload))
                    {
                        resp->setContent(payload);
                        resp->setContentType(F("application/json"));
                        return 0;
                    }
                    else
                    {
                        resp->setResponseStatusCode(R5000_INTERNAL_SERVER_ERROR);
                        return 1;
                    }
                }
            }
            return 0;
        }
    }
    else if (req.getOperation() == OP_DELETE)
    {
        if (!cseTarget)
        {
            // IF resource has SUB -> notify
            // IF parent has sub -> notify ?
            int returnCode = 0;
            if (req.getResultContentType() == RCN_NOTHING)
            {
                resp->setContent(emptyString);
                resp->setContentType(emptyString);
            }
            else
            {
                req.createReport(targetEntity, req.getResultContentType(), 1);
                String payload = emptyString;
                if (serializeJson(jsonglobal, payload))
                {
                    resp->setContent(payload);
                    resp->setContentType(F("application/json"));
                }
                else
                {
                    resp->setResponseStatusCode(R5000_INTERNAL_SERVER_ERROR);
                    returnCode = 1;
                }
            }
            if (targetEntity->getRemoteCse())
            {
                descendantCsesToUpdate = true;
            }
            Entity::deleteEntity(targetEntity);
            dataUpdated = true;
            resp->setResponseStatusCode(R2002_DELETED);
            
            return returnCode;
        }
        else
        {
            resp->setResponseStatusCode(R4005_OPERATION_NOT_ALLOWED);
#if VERBOSE
            resp->setContent(String(F("DELETE operation is not allowed on CSB")));
            resp->setContentType(String(F("text/plain")));
#endif
            return 0;
        }
    }
    else
    {
        resp->setResponseStatusCode(R4005_OPERATION_NOT_ALLOWED);
#if VERBOSE
        resp->setContent(String(F("bad operation")));
        resp->setContentType(String(F("text/plain")));
#endif
        return 0;
    }

    resp->setResponseStatusCode(R5000_INTERNAL_SERVER_ERROR);
#if VERBOSE
    resp->setContent(String(F("request not handled")));
    resp->setContentType(String(F("text/plain")));
#endif
    return 1;
}

/**
 * Register the current CSE to the remote CSE specified in configuration
 */
bool
registerCSE()
{
    RequestPrimitive* requestToSend;
    // create request primitive
    ResponsePrimitive* response = new ResponsePrimitive();
    builder.init();

    // create CSR resource to send
    RemoteCse* csr = new RemoteCse(CSE_NAME, nullptr);
    csr->poas.push_back(CONF_LOCAL_POA);
    csr->cseID = "/" + CSE_ID;
    csr->cseBase = "//" + M2M_SP_ID + csr->cseID;
    csr->requestReachability = true;
    csr->contentSerializationTypes.push_back("application/json");
    csr->supportedReleaseVersions.push_back("2a");
    csr->supportedReleaseVersions.push_back("3");
    builder.createReport(csr, RCN_ATTR, OP_CREATE);
    String result;
    builder.buildRequest(ENC_JSON, result);
#if DEBUG
    printf("DEBUG: BODY %s\n", result.c_str());
#endif
    String url = REMOTE_CSE_POA;
    bool registered = false;
    bool issue = false;
    int iterations = 0;
    while (!registered & (iterations<5))
    {
        requestToSend = new RequestPrimitive();
        requestToSend->init();
        requestToSend->setFrom(CSE_ID);
        requestToSend->setResourceType(TY16_REMOTE_CSE);
        requestToSend->setRequestContentTypeRaw("application/json");
        requestToSend->setRequestId(String((long)get_utime()));
        requestToSend->setContent(result);
        requestToSend->setOperation(OP_CREATE);
        #if HTTP_BINDING
        if (HTTP_BINDING_ENABLED && DEFAULT_PROTOCOL.equalsIgnoreCase(F("http")))
        {
            requestToSend->setTo(url+ "/~/" + REMOTE_CSE_ID + "/" + REMOTE_CSE_NAME);
            Serial.printf("DEBUG: SENDING WITH HTTP\n");
            HTTPBinding::sendRequest(requestToSend, response);
        }
        #endif
        if (response->getResponseStatusCode() == R2001_CREATED)
        {
            registered = true;
            issue = false;
        }
        else if (response->getResponseStatusCode() == R4105_CONFLICT)
        {
            Serial.printf("ERROR: CONFLICT WHILE REGISTERING.\n");
            requestToSend = new RequestPrimitive();
            requestToSend->init();
            requestToSend->setFrom(CSE_ID);
            requestToSend->setOperation(OP_DELETE);
            response->init();
            #if HTTP_BINDING
            if (HTTP_BINDING_ENABLED && DEFAULT_PROTOCOL.equalsIgnoreCase(F("http")))
            {
                requestToSend->setTo(url+ "/~/" + REMOTE_CSE_ID + "/" + REMOTE_CSE_NAME +"/"+ CSE_NAME);
                HTTPBinding::sendRequest(requestToSend, response);
            }
            #endif //HTTP_BINDING
            if (response->getResponseStatusCode() != R2000_OK)
            {
                Serial.printf("ERROR: during DELETE of old registration\n");
                issue = true;
            }
            else
            {
                #if DEBUG
                Serial.printf("REGISTRATION: DELETE OLD CSR OK\n");
                #endif
                issue = false;
            }
        }
        else
        {
            Serial.printf("ERROR: during REGISTRATION %s\n", String(response->getResponseStatusCode()).c_str());
            issue = true;
        }
        iterations++;
    }
    if (issue)
    {
        Serial.printf("ERROR: while registering CSE\n");
    }
    //assert(registered);
    delete (requestToSend);
    delete (response);
    delete csr;
    // create local CSR resource to represent distant CSE
    Entity* e = Entity::getByHierUri("/" + CSE_ID + "/" + CSE_NAME + "/" + REMOTE_CSE_NAME);
    if (e)
    {
        Entity::deleteEntity(e);
    }
    if (!issue)
    {
        csr = Entity::addRemoteCse(REMOTE_CSE_NAME, CseBase::getInstance());
        csr->setCseType(REMOTE_CSE_TYPE);
        csr->setCseBase("/" + REMOTE_CSE_ID);
        csr->setCseID("/" + REMOTE_CSE_ID);
        csr->setRequestReachability(true);
        csr->poas.push_back(REMOTE_CSE_POA);
        csr->contentSerializationTypes.push_back("application/json");
        csr->supportedReleaseVersions = SUPPORTED_RELEASE_VERSIONS;
        csr->m_acps.push_back(AcpAdmin::getInstance());
        CseBase::getInstance()->addChild(csr);
    }

    return !issue;
}


/**
 * Update the current remote CSE with the actual state
 */
ResponseStatusCode
MainController::updateRegistration()
{
    Serial.printf("TRACE: Updating registration...\n");
    ResponseStatusCode result = R4008_REQUEST_TIMEOUT;
    RequestPrimitive* requestToSend = new RequestPrimitive();
    builder.init();
    ResponsePrimitive* response = new ResponsePrimitive();
    String url = REMOTE_CSE_POA;
    RemoteCse* csr = new RemoteCse(CSE_NAME, nullptr);
    csr->requestReachability = true;
    csr->poas.push_back(CONF_LOCAL_POA);
    csr->contentSerializationTypes.push_back("application/json");
    csr->supportedReleaseVersions.push_back("2a");
    csr->supportedReleaseVersions.push_back("3");
    if (descendantCsesToUpdate)
    {
        for (Entity* e : Entity::entities)
        {
            if(e->getType() == TY16_REMOTE_CSE)
            { 
                RemoteCse* csrToAdd = e->getRemoteCse();
                if (csrToAdd && !csrToAdd->getName().equals(csr->getName()) && !csrToAdd->getName().equals(REMOTE_CSE_NAME))
                {
                    csr->descendantCses.push_back(csrToAdd->getCseID());
                    if (csrToAdd && !csrToAdd->getDescendantCses().empty())
                    {
                        for (String dcseId : csrToAdd->getDescendantCses())
                        {
                            csr->descendantCses.push_back(dcseId);
                        }
                    }
                }
            }
        }
    }

    // create CSR resource to send
    builder.createReport(csr, RCN_ATTR, OP_UPDATE);
    String body;
    builder.buildRequest(ENC_JSON, body);
    requestToSend->setOperation(OP_UPDATE);
    requestToSend->setContent(body);
    requestToSend->setFrom(CSE_ID);
    requestToSend->setRequestId("update-regisration-"+CSE_ID);

    Serial.printf("%s\n", body.c_str());
#if HTTP_BINDING
    if (HTTP_BINDING_ENABLED && DEFAULT_PROTOCOL.equalsIgnoreCase(F("http")))
    {
        requestToSend->setTo(url + "/~/" + REMOTE_CSE_ID + "/" + REMOTE_CSE_NAME + "/" + CSE_NAME);
        Serial.printf("TRACE: SENDING WITH HTTP\n");
        HTTPBinding::sendRequest(requestToSend, response);
    }
#endif
    result = response->getResponseStatusCode();
    if (result != R2000_OK || result != R2004_UPDATED)
    {
        Serial.printf("ERROR: during UPDATE of registration\n");
    }
    else
    {
#if DEBUG
        Serial.printf("DEBUG: REGISTRATION: UPDATE CSR OK\n");
#endif
    }

    delete (requestToSend);
    delete (response);
    delete csr;

    return result;
}
