/*
------------------------------------------------------------

SOFTWARE EVALUATION LICENSE

LAAS research laboratory, Toulouse, France.
IRIT computer science research laboratory, Toulouse, France.
------------------------------------------------------------

Contact: Thierry Monteil, thierry.monteil@irit.fr

Definitions
SOFTWARE:  The LightOM2M software, version 1.0, in source code form, written by David Gauchard, Guillaume Garzone and Thierry Monteil
at the LAAS research laboratory.

LICENSOR: LE CENTRE NATIONAL DE LA RECHERCHE SCIENTIFIQUE, a public scientific and technical research establishment, having SIREN No. 180 089 013, APE code 7219 Z, having its registered office at 3, rue Michel-Ange, 75796 Paris cedex 16, France, acting in its own name and on its own behalf, and on behalf of Le laboratoire d'analyse et d'architecture des systèmes (LAAS), UPR n°8001.

1. INTENT/PURPOSE
This agreement determines the conditions in which the LICENSOR, who has the rights to the SOFTWARE, grants to the LICENSEE a license for research and evaluation purposes only, excluding any commercial use.

2. LICENSEE
Any person or organization who receives the SOFTWARE with a copy of this license.

3. RIGHTS GRANTED
The rights to use and copy the SOFTWARE, subject to the restrictions described in this agreement.
The rights to modify and compile the SOFTWARE when it's provided in source code form, subject to the restrictions described in this agreement.
For the SOFTWARE provided in binary form only, LICENSEE undertakes to not decompile, disassemble, decrypt, extract the components or perform reverse engineering except to the extent expressly provided by law.

5. SCOPE OF THE LICENSE
- NON-COMMERCIAL license for research and evaluation purposes ONLY.
- NO right to commercialize the SOFTWARE, or any derivative work, without separate agreement with the LICENSOR.

6. MODIFICATION OF THE SOFTWARE
License permits LICENSEE to modify the SOFTWARE provided in source code form for research and evaluation purposes only.

7. REDISTRIBUTION
- License permits LICENSEE to redistribute verbatim copies of the SOFTWARE, accompanied with a copy of this license.
- License DOES NOT permit LICENSEE to redistribute modified versions of the SOFTWARE provided in source code form.
- License DOES NOT permit LICENSEE to commercialize the SOFTWARE or any derivative work of the SOFTWARE.

8. FEE/ROYALTY
- LICENSEE pays no royalty for this license.
- LICENSEE and any third parties must enter a new agreement for any use beyond scope of license. Please contact the IRIT technology transfer office (email numerique@toulouse-tech-transfer.com) for further information.

9. NO WARRANTY
The SOFTWARE is provided "as is" without warranty of any kind, either expressed or implied, including, but not limited to, the implied warranties of merchantability and fitness for a particular purpose. The entire risk as to the quality and performance of the program is with the LICENSEE.

10. NO LIABILITY
In no event unless required by applicable law or agreed to in writing will any copyright owner be liable to LICENSEE for damages, including any general, special, incidental or consequential damages arising out of the use or inability to use the program (including but not limited to loss of data or data being rendered inaccurate or losses sustained by LICENSEE or third parties or a failure of the program to operate with other programs), even if such holder has been advised of the possibility of such damages.
*/


#ifndef __LOM2M_SHORTNAMES
#define __LOM2M_SHORTNAMES

#include "Arduino.h"

extern const String SUPPORTED_REL_VERSIONS ;
extern const String CONTENT_SERIALIZATION ;

/** Short name for AccessControl Policy resource */
extern const String ACP ;
/** Short name for AccessControlPolicyAnnc resource */
extern const String ACPA ;
/** Short name for ApplicationEntity resource */
extern const String AE ;
/** Short name for ApplicationEntityAnnc resource */
extern const String AEA ;
/** Short name for Container resource */
extern const String CNT ;
/** Short name for ContainerAnnc resource */
extern const String CNTA ;
/** Short name for DynamicAuthorizationConsultation resource */
extern const String DAC ;
/** Short name for FlexContainer resource*/
extern const String FCNT ;
/** Short name for FlexContainerAnnc resource */
extern const String FCNTA ;
/** Short name for Content Instance resource */
extern const String CIN ;
/** Short name for ContentInstanceAnnc resource */
extern const String CINA ;
/** Short name for CseBase resource */
extern const String CSE_BASE ;
/** Short name for Delivery resource */
extern const String DELIVERY ;
/** Short name for EventConfig resource */
extern const String EVENTCONFIG ;
/** Short name for ExecInstance resource */
extern const String EXECINSTANCE ;
/** Short name for FanOutPoint resource */
extern const String FANOUTPOINT ;
/** Short name for Group resource */
extern const String GROUP ;
/** Short name for GroupAnnc resource */
extern const String GROUPA ;
/** Short name for LocationPolicy resource */
extern const String LOCATIONPOLICY ;
/** Short name for LocationPolicyAnnc resource */
extern const String LOCATIONPOLICYA ;
/** Short name for M2mServiceSubscriptionProfile resource */
extern const String MSSP ;
/** Short name for MgmtCmd resource */
extern const String MGC ;
/** Short name for MgmtObj resource */
extern const String MGO ;
/** Short name for MgmtObjAnnc resource */
extern const String MGOA ;
/** Short name for Node resource */
extern const String NODE ;
/** Short name for NodeAnnc resource */
extern const String NODE_ANNC ;
/** Short name for PollingChannel resource */
extern const String PCH ;
/** Short name for PollingChannelUri resource */
extern const String POLLING_CHANNEL_URI ;
/** Short name for RemoteCse resource */
extern const String REMOTE_CSE ;
/** Short name for RemoteCseAnnc resource */
extern const String CSRA ;
/** Short name for Request resource */
extern const String REQ ;
/** Short name for Schedule resource */
extern const String SCHEDULE ;
/** Short name for ScheduleAnnc resource */
extern const String SCHA ;
/** Short name for Service subscribedAppRule resource */
extern const String ASAR ;
/** Short name for ServiceSubscribedNode resource */
extern const String SVSN ;
/** Short name for StatsCollect resource */
extern const String STCL ;
/** Short name for StatsConfig resource */
extern const String STCG ;
/** Short name for Subscription resource */
extern const String SUB ;

// Resource attributes short names
/** Short name for Resource ID attribute */
extern const String RESOURCE_ID ;
/** Short name for CreationTime attribute */
extern const String RESOURCE_TYPE ;
/** Short name for CreationTime attribute */
extern const String CREATION_TIME ;
/** Short name for LastModifiedTime attribute */
extern const String LAST_MODIFIED_TIME ;
/** Short name for ParentID attribute */
extern const String PARENT_ID ;
/** Short name for ResourceName attribute */
extern const String RESOURCE_NAME ;
/** Short name for Labels attribute */
extern const String LABELS ;
/** Short name for ExpirationTime attribute */
extern const String EXPIRATION_TIME ;
/** Short name for AnnounceTo attribute */
extern const String ANNOUNCE_TO ;
/** Short name for AnnouncedAttribute attribute */
extern const String ANNOUNCED_ATTRIBUTE ;
/** Short name for PointOfAccess attribute */
extern const String POA ;
/** Short name for NodeLink attribute */
extern const String NODE_LINK ;
/** Short name for NodeLink attribute */
extern const String ACP_IDS ;
/** Short name for Child Resource */
extern const String CHILD_RESOURCE ;
/** Short name for DynamicAuthorizationConsultationIDs attribute */
extern const String DAC_IDS ;

// Attributes for Child Resources
/** Short name for name attribute of a ChildResourceRef */
extern const String CHILD_RESOURCE_NAME ;
/** Short name for type attribute of a ChildResourceRef */
extern const String CHILD_RESOURCE_TYPE ;
/** Short name for spid attribute of a ChildResourceRef */
extern const String CHILD_RESOURCE_SPID ;
/** Short name for val attribute of a ChildResourceRef */
extern const String CHILD_RESOURCE_VALUE ;

// Attributes for CSEBase Entity
/** Short name for SupportedResourceTypes attribute */
extern const String SRT ;
/** Short name for CSE-ID attribute */
extern const String SN_CSE_ID ;
/** Short name for CseType attribute */
extern const String SN_CSE_TYPE ;

// Specific attributes for AccessControlPolicy Entity
/** Short name for privileges attribute */
extern const String PRIVILEGES ;
/** Short name for privileges attribute */
extern const String SELF_PRIVILEGES ;
/** Short name for acr attribute */
extern const String ACR ;
/** Short name for access control originators attribute */
extern const String ACOR ;
/** Short name for access control operations attribute */
extern const String ACOP ;
/** Short name for access control contexts attribute */
extern const String ACCO ;
/** Short name for access control window attribute */
extern const String ACTW ;
/** Short name for access control ip adresses attribute */
extern const String ACIP ;
/** Short name for ipv4 address attribute */
extern const String IPV4 ;
/** Short name for ipv6 address attribute */
extern const String IPV6 ;
/** Short name for access control location region attribute */
extern const String ACLR ;
/** Short name for country code attribute */
extern const String ACCC ;
/** Short name for circ region attribute */
extern const String ACCR ;
/** Short name for access control authentication flag */
extern const String ACAF ;

// Attributes for Application Entity
/** Short name for App Name Attribute */
extern const String APP_NAME ;
/** Short name for App-ID Attribute */
extern const String APP_ID ;
/** Short name for AE-ID Attribute */
extern const String AE_ID ;
/** Short name for Ontology Reference Attribute */
extern const String ONTOLOGY_REF ;

// Attributes for Container Entity
/** Short name for Creator attribute */
extern const String CREATOR ;
/** Short name for StateTag attribute */
extern const String STATETAG ;
/** Short name for Max Number Of Instances attribute */
extern const String MAX_NR_OF_INSTANCES ;
/** Short name for Max Byte Size attribute */
extern const String MAX_BYTE_SIZE ;
/** Short name for Max Instance Age attribute */
extern const String MAX_INSTANCE_AGE ;
/** Short name for Current Byte Size attribute */
extern const String CURRENT_BYTE_SIZE ;
/** Short name for location ID attribute */
extern const String LOCATION_ID ;
/** Short name for Latest attribute */
extern const String LATEST ;
/** Short name for Oldest attribute */
extern const String OLDEST ;
/** Short name for current number of instances attribute */
extern const String CURRENT_NUMBER_OF_INSTANCES ;
/** Short name for disable retrieval attribute */
extern const String DISABLE_RETRIEVAL ;

// Attributes for FlexContainerEntity
/** Short name for ContainerDefinition attribute */
extern const String CONTAINER_DEFINITION ;

// Attributes for Content Instance
/** Short name for ContentSize attribute */
extern const String CONTENT_SIZE ;
/** Short name for ContentInfo attribute */
extern const String CONTENT_INFO ;
/** Short name for Content attribute */
extern const String CONTENT ;
/** Short Name for expiration counter */
extern const String EXPIRATION_COUNTER ;

extern const String NOTIFICATION_URI ;
extern const String NOTIFICATION_FORWARDING_URI ;
/** Short name for resource type filter criteria */
extern const String TYPE ;

// specific attributes for Group resource
/** Short Name for the member type attribute */
extern const String MEMBER_TYPE ;
/** Short Name for the current number of members attribute */
extern const String CURRENT_NUM_MEMBERS ;
/** Short Name for the member type attribute */
extern const String MAX_NUM_MEMBERS ;
/** Short Name for the memberID type attribute */
extern const String MEMBER_ID ;
/** Short Name for the member acp id attribute */
extern const String MEMBER_ACP_ID ;
/** Short Name for the member type validated attribute */
extern const String MEMBER_TYPE_VALIDATED ;
/** Short Name for the consistency strategy attribute */
extern const String CONSISTENCY_STRATEGY ;
/** Short Name for the consistency strategy attribute */
extern const String GROUP_NAME ;

// Specific attributes for remoteCSE resource
/** Short Name for the M2M-EXT-ID attribute */
extern const String M2M_EXT_ID ;
extern const String TRIGGER_RECIPIENT_ID ;
extern const String REQUEST_REACHABILITY ;
extern const String REMOTE_CSE_CSEBASE ;
extern const String SN_DESCENDANT_CSE ;

// Specific attributes for Subscription resource
/** Short Name for the Event Notification Criteria attribute */
extern const String EVENT_NOTIFICATION_CRITERIA ;
/** Short name for group id */
extern const String GROUP_ID ;
/** Short name for the batch notify attribute */
extern const String BATCH_NOTIFY ;
/** Short name for rate limit */
extern const String RATE_LIMIT ;
/** Short name for pre subscription notify */
extern const String PRE_SUBSCRIPTION_NOTIFY ;
/** Short name for pending notification */
extern const String PENDING_NOTIFICATION ;
/** Short name for notification storage priority */
extern const String NOTIFICATION_STORAGE_PRIORITY ;
/** Short name for latest notify */
extern const String LATEST_NOTIFY ;
/** Short name for notification content type */
extern const String NOTIFICATION_CONTENT_TYPE ;
/** Short name for Notification event cat */
extern const String NOTIFICATION_EVENT_CAT ;
extern const String NOTIFICATION_EVENT_TYPE;
/** Short name for subscriber uri */
extern const String SUBSCRIBER_URI ;

// attributes for rate limit
/** short name for max number of notify */
extern const String MAX_NR_OF_NOTIFY ;
/** short name for time window */
extern const String TIME_WINDOW ;

// attributes for Schedule
/** short name for schedule element */
extern const String SCHEDULE_ELEMENT ;
/** short name for schedule entry */
extern const String SCHEDULE_ENTRY ;

// attributes for announced resource
extern const String LINK ;
extern const String AE_ANNC ;
extern const String CNT_ANNC ;

// attributes for request resource
extern const String OPERATION ;
extern const String TO ;
extern const String FROM ;
extern const String NAME ;
extern const String REQUEST_CONTENT ;
extern const String ORIGINATING_TIMESTAMP ;
extern const String REQUEST_EXPIRATION_TIMESTAMP ;
extern const String RESULT_EXPIRATION_TIMESTAMP ;
extern const String OPERATION_EXECUTION_TIME ;
extern const String RESPONSE_TYPE ;
extern const String RESULT_PERSISTENCE ;
extern const String RESULT_CONTENT ;
extern const String EVENT_CATEGORY ;
extern const String DELIVERY_AGGREGATION ;
extern const String GROUP_REQUEST_IDENTIFIER ;
extern const String FILTER_CRITERIA ;
extern const String CREATED_BEFORE ;
extern const String CREATED_AFTER ;
extern const String MODIFIED_SINCE ;
extern const String UNMODIFIED_SINCE ;
extern const String STATETAG_SMALLER ;
extern const String STATETAG_BIGGER ;
extern const String EXPIRE_BEFORE ;
extern const String EXPIRE_AFTER ;
extern const String FILTER_RESOURCETYPE ;
extern const String CONTENT_TYPE ;
extern const String LIMIT ;
extern const String LEVEL ;
extern const String OFFSET ;
extern const String ATTRIBUTE ;
extern const String FILTER_USAGE ;
extern const String DISCOVERY_RESULT_TYPE ;
extern const String RESPONSE_STATUS_CODE ;
extern const String ORIGINATOR ;
extern const String META_INFORMATION ;
extern const String REQUEST_STATUS ;
extern const String OPERATION_RESULT ;
extern const String REQUEST_OPERATION ;
extern const String REQUEST_ID ;
extern const String TARGET ;
extern const String EVENT_CAT_TYPE ;
extern const String EVENT_CAT_NO ;
extern const String PRIMITIVE_CONTENT ;
extern const String RELEASE_VERSION_INDICATOR ;
extern const String AUTHORIZATION_SIGNATURE_REQUEST_INFO ;
extern const String AUTHORIZATION_SIGNATURE_INDICATOR ;
extern const String AUTHORIZATION_RELATIONSHIP_INDICATOR ;
extern const String VENDOR_INFO ;
extern const String CONTENT_OFFSET ;
extern const String CONTENT_STATUS ;
extern const String SEMANTIC_QUERY_INDICATOR ;
extern const String SEMANTICS_FILTER ;
extern const String FILTER_OPERATION ;
extern const String CONTENT_FILTER_SYNTAX ;
extern const String CONTENT_FILTER_QUERY ;


// attributes for filter criteria
extern const String SIZE_ABOVE ;
extern const String SIZE_BELOW ;

// Attributes for Event Notification Criteria
extern const String OPERATION_MONITOR ;
extern const String RESOURCE_STATUS ;

// Attributes for Batch Notify
extern const String NUMBER ;
extern const String DURATION ;

// Attributes for Notification
extern const String NOTIFICATION ;
extern const String NOTIFICATION_EVENT ;
extern const String VERIFICATION_REQUEST ;
extern const String SUBSCRIPTION_DELETION ;
extern const String SUBSCRIPTION_REFERENCE ;

// Attribute for Notification Event
extern const String REPRESENTATION ;

// Attributes for OperationMonitor
extern const String OM_OPERATION ;
extern const String OM_ORIGINATOR ;

extern const String AGGREGATED_RESPONSE ;
extern const String RESPONSE_PRIMITIVE ;
extern const String REQUEST_PRIMITIVE ;
extern const String REQUEST_IDENTIFIER ;

// Attributes for Node
extern const String NODE_ID ;
extern const String HOSTED_CSE_LINK ;
extern const String HOSTED_SRV_LINK ;

// Short names for mgmt objects generic attributes
extern const String DESCRIPTION ;
extern const String MGMT_DEF ;
extern const String OBJ_IDS ;
extern const String OBJ_PATHS ;

// Short names for mgmt objects specialization
extern const String AREA_NWK_DEVICE_INFO ;
extern const String AREA_NWK_INFO ;
extern const String BATTERY ;
extern const String DEVICE_CAPABILITY ;
extern const String DEVICE_INFO ;
extern const String EVENT_LOG ;
extern const String FIRMWARE ;
extern const String MEMORY ;
extern const String REBOOT ;
extern const String SOFTWARE ;

extern const String AREA_NWK_DEVICE_INFO_ANNC ;
extern const String AREA_NWK_INFO_ANNC ;
extern const String BATTERY_ANNC ;
extern const String DEVICE_CAPABILITY_ANNC ;
extern const String DEVICE_INFO_ANNC ;
extern const String EVENT_LOG_ANNC ;
extern const String FIRMWARE_ANNC ;
extern const String MEMORY_ANNC ;
extern const String REBOOT_ANNC ;
extern const String SOFTWARE_ANNC ;

extern const String ACTIVE_CMDH_POLICY ;
extern const String CMDH_POLICY ;
extern const String CMDH_DEFAULTS ;
extern const String CMDH_DEF_EC_VALUE ;
extern const String CMDH_EC_DEF_PARAM_VALUES ;
extern const String CMDH_LIMITS ;
extern const String CMDH_NETWORK_ACCESS_RULES ;
extern const String CMDH_NW_ACCESS_RULE ;
extern const String CMDH_BUFFER ;

// Short names for Custom Attributes
extern const String CUSTOM_ATTRIBUTE_NAME ;
extern const String CUSTOM_ATTRIBUTE_TYPE ;
extern const String CUSTOM_ATTRIBUTE_VALUE ;

// short names for area nwk info
extern const String AREA_NWK_TYPE ;
extern const String LIST_DEVICES ;

// short names for area nwk device info
extern const String SN_STATUS ;
extern const String DEV_ID ;
extern const String DEV_TYPE ;
extern const String AREA_NWK_ID ;
extern const String SLEEP_INTERVAL ;
extern const String SLEEP_DURATION ;
extern const String LIST_OF_NEIGHBORS ;

// short names for device info
extern const String DEVICE_LABEL ;
extern const String DEVICE_TYPE ;
extern const String DEVICE_MODEL ;
extern const String MANUFACTURER ;
extern const String FW_VERSION ;
extern const String SW_VERSION ;
extern const String HW_VERSION ;
extern const String OS_VERSION ;
extern const String MANUF_DET_LINKS ;
extern const String MANUF_DATE ;
extern const String DEVICE_SUB_MODEL ;
extern const String DEVICE_NAME ;
extern const String COUNTRY ;
extern const String LOCATION ;
extern const String SYS_TIME ;
extern const String SUPPORT_URL ;
extern const String PRES_URL ;
extern const String PROTOCOL ;

// short names for battery
extern const String BATTERY_LEVEL ;
extern const String BATTERY_STATUS ;

// short name for URI List
extern const String URI_LIST ;

// short names for DynamicAuthorizationConsultation
extern const String DYNAMIC_AUTHORIZATION_ENABLED ;
extern const String DYNAMIC_AUTHORIZATION_PoA ;
extern const String DYNAMIC_AUTHORIZATION_LIFETIME ;

// short names for DynAuthDasRequest
extern const String TARGETED_RESOURCE_TYPE ;
extern const String TARGETED_RESOURCE_ID ;
extern const String ORIGINATOR_IP ;
extern const String ORIGINATOR_LOCATION ;
extern const String ORIGINATOR_ROLE_IDS ;
extern const String REQUEST_TIMESTAMP ;
extern const String PROPOSED_PRIVILEDGES_LIFETIME ;
extern const String ROLE_IDS_FROM_ACPS ;
extern const String TOKEN_IDS ;

// short names for DynamicAuthTokenReqInfo
extern const String DYNAMIC_AUTHORIZATION_TOKEN_REQ_INFO ;
extern const String TOKEN_REQ_INFO ;
extern const String DAS_INFO ;
extern const String URI ;
extern const String DAS_REQUEST ;

#endif
