/*
------------------------------------------------------------

SOFTWARE EVALUATION LICENSE

LAAS research laboratory, Toulouse, France.
IRIT computer science research laboratory, Toulouse, France.
------------------------------------------------------------

Contact: Thierry Monteil, thierry.monteil@irit.fr

Definitions
SOFTWARE:  The LightOM2M software, version 1.0, in source code form, written by David Gauchard, Guillaume Garzone and Thierry Monteil
at the LAAS research laboratory.

LICENSOR: LE CENTRE NATIONAL DE LA RECHERCHE SCIENTIFIQUE, a public scientific and technical research establishment, having SIREN No. 180 089 013, APE code 7219 Z, having its registered office at 3, rue Michel-Ange, 75796 Paris cedex 16, France, acting in its own name and on its own behalf, and on behalf of Le laboratoire d'analyse et d'architecture des systèmes (LAAS), UPR n°8001.

1. INTENT/PURPOSE
This agreement determines the conditions in which the LICENSOR, who has the rights to the SOFTWARE, grants to the LICENSEE a license for research and evaluation purposes only, excluding any commercial use.

2. LICENSEE
Any person or organization who receives the SOFTWARE with a copy of this license.

3. RIGHTS GRANTED
The rights to use and copy the SOFTWARE, subject to the restrictions described in this agreement.
The rights to modify and compile the SOFTWARE when it's provided in source code form, subject to the restrictions described in this agreement.
For the SOFTWARE provided in binary form only, LICENSEE undertakes to not decompile, disassemble, decrypt, extract the components or perform reverse engineering except to the extent expressly provided by law.

5. SCOPE OF THE LICENSE
- NON-COMMERCIAL license for research and evaluation purposes ONLY.
- NO right to commercialize the SOFTWARE, or any derivative work, without separate agreement with the LICENSOR.

6. MODIFICATION OF THE SOFTWARE
License permits LICENSEE to modify the SOFTWARE provided in source code form for research and evaluation purposes only.

7. REDISTRIBUTION
- License permits LICENSEE to redistribute verbatim copies of the SOFTWARE, accompanied with a copy of this license.
- License DOES NOT permit LICENSEE to redistribute modified versions of the SOFTWARE provided in source code form.
- License DOES NOT permit LICENSEE to commercialize the SOFTWARE or any derivative work of the SOFTWARE.

8. FEE/ROYALTY
- LICENSEE pays no royalty for this license.
- LICENSEE and any third parties must enter a new agreement for any use beyond scope of license. Please contact the IRIT technology transfer office (email numerique@toulouse-tech-transfer.com) for further information.

9. NO WARRANTY
The SOFTWARE is provided "as is" without warranty of any kind, either expressed or implied, including, but not limited to, the implied warranties of merchantability and fitness for a particular purpose. The entire risk as to the quality and performance of the program is with the LICENSEE.

10. NO LIABILITY
In no event unless required by applicable law or agreed to in writing will any copyright owner be liable to LICENSEE for damages, including any general, special, incidental or consequential damages arising out of the use or inability to use the program (including but not limited to loss of data or data being rendered inaccurate or losses sustained by LICENSEE or third parties or a failure of the program to operate with other programs), even if such holder has been advised of the possibility of such damages.
*/


#include "bsp-esp8266.h"

const char* contentType(const String& filename)
{
    for (int i = 0; i < (int)mime::maxType; i++)
        if (filename.endsWith(mime::mimeTable[i].endsWith))
        {
            return mime::mimeTable[i].mimeType;
        }
    return mime::mimeTable[mime::none].mimeType;
}

utime_t get_utime()
{
    timeval t;
    if (gettimeofday(&t, NULL) != 0)
    {
        perror("gettimeofday");
    }
    return (utime_t)(t.tv_sec) * 1000000 + (utime_t)(t.tv_usec);
}

String get_ctime(time_t t)
{
    t /= 1000000;
    struct tm* time = gmtime(&t);
    char fmt[20];
    snprintf(fmt, sizeof(fmt), "%04d%02d%02dT%02d%02d%02d",
             1900 + time->tm_year, 1 + time->tm_mon, time->tm_mday,
             time->tm_hour, time->tm_min, time->tm_sec);
    return String(fmt);
}

utime_t getTimeFromString(String et)
{
    int year, month, day, hours, minutes, seconds;
    struct tm* time = new tm();
    time->tm_year = year - 1900;
    time->tm_mon = month - 1;
    time->tm_mday = day;
    time->tm_hour = hours;
    time->tm_min = minutes;
    time->tm_sec = seconds;
    time_t t = mktime(time);
    t *= 1000000; // due to t /= 10000000 in get_ctime()
    return t;
}

void printHuman(Stream& to, int level, const char* tag, const char* data)
{
    for (int i = 0; i < level; i++)
    {
        to.printf("    ");
    }
    if (tag)
    {
        to.printf("%s: ", tag);
    }
    if (data)
    {
        to.printf("%s", data);
    }
    to.printf("\n");
}

void printHumanSerial(int level, const char* tag, const char* data)
{
    printHuman(Serial, level, tag, data);
}

String getMac()
{
    char ret[14];
    uint8_t mac[6];
    WiFi.macAddress(mac);
    sprintf(ret, "%02X%02X%02X%02X%02X%02X", mac[0], mac[1], mac[2], mac[3], mac[4], mac[5]);
    return ret;
}

static WiFiServer server(23);
static std::vector<Print*> logs;

void log_setup()
{
    logs.reserve(4);
    logs.push_back(&Serial);
    server.begin();
}

void log(const char* fmt, ...)
{
    va_list ap;
    va_start(ap, fmt);
    char line[512];
    int len = vsnprintf(line, sizeof line, fmt, ap);
    if (len > (int)sizeof(line) - 5)
    {
        ::printf((PGM_P)F("log(): BUFFER TOO SMALL\n"));
#if CORE_MOCK
        abort();
#endif
    }

    // check new client
    if (server.hasClient())
    {
        logs.emplace_back(new WiFiClient(server.available()));
    }

    Serial.println(line);
#if 0 //XXXFIXMERTTI
    for (auto l = logs.begin(); l != logs.end();)
    {
        (*l)->write(line);

        // check dead client
        WiFiClient* cli = dynamic_cast<WiFiClient*>(*l);
        if (cli && (!*cli || !cli->connected()))
            //logs.erase(l++); <- for other containers
        {
            logs.erase(l);    // <- for vector
        }
        else
        {
            l++;
        }
    }
#endif
}


/////////////////////////////////////////////////

#include <LittleFS.h>

FS* filesystem = &LittleFS;

bool FSOpen ()
{
    FSInfo64 info64;
    return // already opened
           filesystem->info64(info64)
           // or, start OK
        || filesystem->begin()
           // or, format OK and start OK
        || (filesystem->format() && filesystem->begin());
}

void FSClose ()
{
    filesystem->end();
}

int persistenceStore(JsonObject toStore, const char* name)
{
    if (!FSOpen())
        return -1;

    File f = filesystem->open(name, "w");
    if (!f)
    {
        FSClose();
        return -1;
    }

    size_t ret = serializeJson(toStore, f);

    f.close();
    FSClose();

    return (int)ret;
}

DeserializationError persistenceLoad(JsonDocument& toStore, const char* name)
{
    if (!FSOpen())
        return DeserializationError::NotSupported;
    File f = filesystem->open(name, "r");
    if (!f)
    {
        FSClose();
        return DeserializationError::NotSupported;
    }

            
    auto ret = deserializeJson(toStore, f, DeserializationOption::NestingLimit(20));

    f.close();
    FSClose();
    return ret;
}
