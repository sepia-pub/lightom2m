/*
------------------------------------------------------------

SOFTWARE EVALUATION LICENSE

LAAS research laboratory, Toulouse, France.
IRIT computer science research laboratory, Toulouse, France.
------------------------------------------------------------

Contact: Thierry Monteil, thierry.monteil@irit.fr

Definitions
SOFTWARE:  The LightOM2M software, version 1.0, in source code form, written by David Gauchard, Guillaume Garzone and Thierry Monteil
at the LAAS research laboratory.

LICENSOR: LE CENTRE NATIONAL DE LA RECHERCHE SCIENTIFIQUE, a public scientific and technical research establishment, having SIREN No. 180 089 013, APE code 7219 Z, having its registered office at 3, rue Michel-Ange, 75796 Paris cedex 16, France, acting in its own name and on its own behalf, and on behalf of Le laboratoire d'analyse et d'architecture des systèmes (LAAS), UPR n°8001.

1. INTENT/PURPOSE
This agreement determines the conditions in which the LICENSOR, who has the rights to the SOFTWARE, grants to the LICENSEE a license for research and evaluation purposes only, excluding any commercial use.

2. LICENSEE
Any person or organization who receives the SOFTWARE with a copy of this license.

3. RIGHTS GRANTED
The rights to use and copy the SOFTWARE, subject to the restrictions described in this agreement.
The rights to modify and compile the SOFTWARE when it's provided in source code form, subject to the restrictions described in this agreement.
For the SOFTWARE provided in binary form only, LICENSEE undertakes to not decompile, disassemble, decrypt, extract the components or perform reverse engineering except to the extent expressly provided by law.

5. SCOPE OF THE LICENSE
- NON-COMMERCIAL license for research and evaluation purposes ONLY.
- NO right to commercialize the SOFTWARE, or any derivative work, without separate agreement with the LICENSOR.

6. MODIFICATION OF THE SOFTWARE
License permits LICENSEE to modify the SOFTWARE provided in source code form for research and evaluation purposes only.

7. REDISTRIBUTION
- License permits LICENSEE to redistribute verbatim copies of the SOFTWARE, accompanied with a copy of this license.
- License DOES NOT permit LICENSEE to redistribute modified versions of the SOFTWARE provided in source code form.
- License DOES NOT permit LICENSEE to commercialize the SOFTWARE or any derivative work of the SOFTWARE.

8. FEE/ROYALTY
- LICENSEE pays no royalty for this license.
- LICENSEE and any third parties must enter a new agreement for any use beyond scope of license. Please contact the IRIT technology transfer office (email numerique@toulouse-tech-transfer.com) for further information.

9. NO WARRANTY
The SOFTWARE is provided "as is" without warranty of any kind, either expressed or implied, including, but not limited to, the implied warranties of merchantability and fitness for a particular purpose. The entire risk as to the quality and performance of the program is with the LICENSEE.

10. NO LIABILITY
In no event unless required by applicable law or agreed to in writing will any copyright owner be liable to LICENSEE for damages, including any general, special, incidental or consequential damages arising out of the use or inability to use the program (including but not limited to loss of data or data being rendered inaccurate or losses sustained by LICENSEE or third parties or a failure of the program to operate with other programs), even if such holder has been advised of the possibility of such damages.
*/


#include "Entity.h"
#include "Notify.h"
#include <iostream>
#include <sstream>
#include <locale>
#include <iomanip>

Entity::collection Entity::entities;

CseBase* CseBase::csbInstance = nullptr;

#if !!__cpp_rtti

CseBase* Entity::getCseBase()
{
    return dynamic_cast<CseBase*>(this) : nullptr;
}
AccessControlPolicy* Entity::getAcp()
{
    return dynamic_cast<AccessControlPolicy*>(this) : nullptr;
}

RemoteCse* Entity::getRemoteCse()
{
    return dynamic_cast<RemoteCse*>(this) : nullptr;
}

Application* Entity::getApplication()
{
    return dynamic_cast<Application*>(this) : nullptr;
}

Container* Entity::getContainer()
{
    return dynamic_cast<Container*>(this) : nullptr;
}

ContentInstance* Entity::getInstance()
{
    return dynamic_cast<ContentInstance*>(this) : nullptr;
}
#if GROUP_FEAT
Group* Entity::getGroup()
{
    return dynamic_cast<Group*>(this) : nullptr;
}
#endif
#if SUBSCRIPTION_FEAT
Subscription* Entity::getSubscription()
{
    return dynamic_cast<Subscription*>(this) : nullptr;
}
#endif // SUBSCRIPTION_FEAT
#else // cannot use dynamic_cast<>

CseBase* Entity::getCseBase()
{
    return m_type == TY5_CSEBASE ? static_cast<CseBase*>(this) : nullptr;
}
AccessControlPolicy* Entity::getAcp()
{
    return m_type == TY1_ACP ? static_cast<AccessControlPolicy*>(this) : nullptr;
}

RemoteCse* Entity::getRemoteCse()
{
    return m_type == TY16_REMOTE_CSE ? static_cast<RemoteCse*>(this) : nullptr;
}

Application* Entity::getApplication()
{
    return m_type == TY2_APPL_ENTITY ? static_cast<Application*>(this) : nullptr;
}

Container* Entity::getContainer()
{
    return m_type == TY3_CONTAINER ? static_cast<Container*>(this) : nullptr;
}

ContentInstance* Entity::getInstance()
{
    return m_type == TY4_CONTENT_INSTANCE ? static_cast<ContentInstance*>(this) : nullptr;
}
#if GROUP_FEAT
Group* Entity::getGroup()
{
    return m_type == TY9_GROUP ? static_cast<Group*>(this) : nullptr;
}
#endif

#if SUBSCRIPTION_FEAT
Subscription* Entity::getSubscription()
{
    return m_type == TY23_SUBSCRIPTION ? static_cast<Subscription*>(this) : nullptr;
}
#endif // SUBSCRIPTION_FEAT

#endif // rtti

////////////////
// Entity

Entity::collectionIt Entity::findByName(const String& name)
{
    for (auto it = entities.begin(); it != entities.end(); it++)
        if ((*it)->m_name == name)
        {
            return it;
        }
    return entities.end();
}

Entity::collectionIt Entity::findByResource(const String& name)
{
    for (auto it = entities.begin(); it != entities.end(); it++)
        if ((*it)->getIdentifier() == name)
        {
            return it;
        }
    return entities.end();
}

Entity::Entity(const String& name, int type, Entity* parent): m_name(name), m_type(type), m_parent(parent)
{
    clearTimes();
    m_resourceId = emptyString;
    entities.emplace_back(this);
}

Entity* Entity::getByHierUri(const String& uri)
{
    String uriToFind =  uri;
    String cse = "/"+CSE_ID;
    if (uriToFind.startsWith(cse))
    {
        uriToFind.remove(0, cse.length());
    }
    for (Entity* e : entities)
    {
        //printf("DEBUG: uri checked: %s\n", e->getFullName().c_str());
        if (e->getFullName().equals(uriToFind))
        {
            return e;
        }
    }
    return nullptr;
}

String Entity::getIdentifier()
{
    if (!m_resourceId || m_resourceId == emptyString)
    {
        m_resourceId = String((String(headerStr()) + m_time_creation + '-' + this->m_type).c_str());
    }
    return m_resourceId;
}

String Entity::getResourceIdentifier()
{
    if (m_type == TY5_CSEBASE)
    {
        return CSE_ID;
    }
    else
    {
        return getIdentifier();
    }
}

String Entity::getFullName()
{
    String out = emptyString;
    if (m_parent)
    {
        out = m_parent->getFullName();
    }
    if (this->getName().length() > 0)
    {
        String name = this->getName();
        out = out + '/' + name;
    }
    return out;
}

void Entity::clearTimes()
{
    m_time_creation = get_utime();
    m_time_modification = m_time_creation;
    m_time_expiration = m_time_creation + default_expiration_duration;
}

void Entity::setExpirationTime(String et)
{
    m_time_expiration = getTimeFromString(et);
}

void Entity::init()
{
    m_type = TY_NONE;
}

void Entity::clear()
{
    init();
    m_name = emptyString;
    clearTimes();
    children.clear();
}

Entity* Entity::getByName(const String& name)
{
    for (auto& e : Entity::entities)
        if (e->getName() == name)
        {
            return &*e;
        }
    return nullptr;
}
Entity* Entity::getByName(const String& name, const String& parentName)
{
    Entity* res = nullptr;
    for (auto& e : Entity::entities)
        if (e->getName() == name)
        {
            if (e->m_parent->getName() == parentName)
            {
                res = e;
            }
        }
    return res;
}

Entity* Entity::getByIdentifier(const String& identifier)
{
    String id = identifier;
    String cse = String('/') + CSE_ID + '/';
    if (id.startsWith(cse))
    {
        id.remove(0, cse.length());
    }
    for (Entity* e : Entity::entities)
    {
        if (e->getIdentifier().equals(id))
        {
            return &*e;
        }
    }
    return nullptr;
}

void Entity::printTo(printfmt to, int level) const
{
    to(level, (PGM_P)rqType(getType()), m_name.length() > 0 ? m_name.c_str() : S_INCSE);
    to(level + 1, S_creation, get_ctime(m_time_creation).c_str());
    to(level + 1, S_modification, get_ctime(m_time_modification).c_str());
    to(level + 1, S_expiration, get_ctime(m_time_expiration).c_str());
    printLocalTo(to, level);
    for (auto& e : Entity::entities)
        if (e->m_parent == this)
        {
            e->printTo(to, level + 1);
        }
}

Entity::~Entity()
{
    for (auto it = entities.begin(); it != entities.end(); it++)
    {
        if (*it == this)
        {
            entities.erase(it);
            break;
        }
    }
}

void Entity::deleteEntity(Entity* e, bool r)
{
    Serial.println("DEBUG: START DELETE ENTITY...");
    // NOTIFY
    // IF target has sub -> notify delete
#if SUBSCRIPTION_FEAT
    std::list<Subscription*> subs;
    if (!r)
    {
        for (auto ch : e->getChildren())
        {
            if (ch->getSubscription())
            {
                subs.push_back(ch->getSubscription());
            }
        }
        // adding notifications to buffer if needed
        if (!subs.empty())
        {
            Serial.println("DEBUG: ADD NOTIFY DELETE RES...");
            Notifier::notify(subs, e, NET_DELETE_RES, OP_DELETE);
        }
    }
#endif // SUBSCRIPTION_FEAT
    for (Entity* toDelete : e->children)
    {
        Entity::deleteEntity(toDelete, 1);
    }
    Entity::entities.remove(e);
    if (!r)
    {
        if (e->m_parent)
        {
            Entity* p = e->m_parent;
            p->children.remove(e);
        }
    }
    delete e;
}

Container* Entity::addContainer(const String& name, Entity* parent)
{
    return new Container(name, parent);
}

Application* Entity::addApplication(const String& name)
{
    Application* ret = new Application(name);
    ret->m_acps.push_back(AcpAdmin::getInstance());
    return ret;
}

RemoteCse* Entity::addRemoteCse(const String& name, CseBase* parent)
{
    return new RemoteCse(name, parent);
}


ContentInstance* Entity::addInstance(const String& name, Container* parent)
{
    return new ContentInstance(name, parent);
}

#if SUBSCRIPTION_FEAT
Subscription* Entity::addSubscription(const String& name, Entity* parent)
{
    return new Subscription(name, parent);
}
#endif // SUBSCRIPTION_FEAT

bool Entity::addChild(Entity* e)
{
    // adding created entity to parent children
    children.push_back(e);

    // ensuring that container MNI is respected
    if (this->getContainer() && e->m_type == TY4_CONTENT_INSTANCE)
    {
        Container* cnt = (Container*)this;
        e->getInstance()->setStateTag(cnt->getStateTag());
        while (cnt->getMaxNumberOfInstances() != -1 && cnt->getCurrentNumberOfInstances() > cnt->getMaxNumberOfInstances())
        {
            for (Entity* ent : this->children)
            {
                if (ent->m_type == TY4_CONTENT_INSTANCE)
                {
                    this->children.remove(ent);
                    delete ent;
                    break;
                }
            }
        }
        while (cnt->getMaxByteSize() != -1 && cnt->getCurrentByteSize() > 0 && cnt->getMaxByteSize() < cnt->getCurrentByteSize())
        {
            for (Entity* ent : this->children)
            {
                if (ent->m_type == TY4_CONTENT_INSTANCE)
                {
                    this->children.remove(ent);
                    delete ent;
                    break;
                }
            }
        }
    }
    this->notify_observers();

    return true;
}

////////////////
// Application

Application::~Application()
{
}

Application* Application::getByName(const String& name)
{
    Entity* e = Entity::getByName(name);
    return e ? e->getApplication() : nullptr;
}

Application* Application::getByIdentifier(const String& identifier)
{
    Entity* e = Entity::getByIdentifier(identifier);
    return e ? e->getApplication() : nullptr;
}

void Application::printLocalTo(printfmt to, int level) const
{
    to(level + 1, (PGM_P)F("api"), m_api.length() ? m_api.c_str() : S_INCSE);
    to(level + 1, (PGM_P)F("labels"), nullptr);
    if (m_labels.size())
        for (const auto& l : m_labels)
        {
            to(level + 1, nullptr, (l + ' ').c_str());
        }
}

////////////////
// CseBase

String CseBase::getResourceIdentifier()
{
    return m_cseId;
}

void CseBase::init()
{
}

void CseBase::clear()
{
    Entity::clear();
    m_name = emptyString;
    children.clear();
    m_supportedResTypes.clear();
    m_poas.clear();
    m_contentSerializationTypes.clear();
    m_supportedReleaseVersions.clear();
}


////////////////
// RemoteCse

void RemoteCse::init()
{
    this->cseType = 0;
}

void RemoteCse::clear()
{
    init();
    Entity::clear();
    this->contentSerializationTypes.clear();
    this->poas.clear();
    this->descendantCses.clear();
    this->contentSerializationTypes.clear();
    this->supportedReleaseVersions.clear();
}

RemoteCse* RemoteCse::getByName(const String& name)
{
    Entity* e = Entity::getByName(name);
    return e ? e->getRemoteCse() : nullptr;
}

RemoteCse* RemoteCse::getByIdentifier(const String& identifier)
{
    Entity* e = Entity::getByIdentifier(identifier);
    return e ? e->getRemoteCse() : nullptr;
}

////////////////
// Container

Container::~Container()
{
}

Container* Container::getByIdentifier(const String& identifier)
{
    Entity* e = Entity::getByIdentifier(identifier);
    return e ? e->getContainer() : nullptr;
}

void Container::clear()
{
    Entity::clear();
    m_parent = nullptr;
    //instances.clear();
}

Container* Container::getByName(const String& name)
{
    Entity* e = Entity::getByName(name);
    return e ? e->getContainer() : nullptr;
}

void Container::printLocalTo(printfmt to, int level) const
{
    //printToChildren(to, level + 1);
    to(level + 1, "container", "glok");
}

int Container::getCurrentNumberOfInstances()
{
    int res = 0;
    for (Entity* e : this->children)
    {
        if (e->m_type == TY4_CONTENT_INSTANCE)
        {
            res++;
        }
    }
    return res;
}

int Container::getCurrentByteSize()
{
    int res = 0;
    for (Entity* e : this->getChildren())
    {
        if (e->getType() == TY4_CONTENT_INSTANCE)
        {
            res = res + e->getInstance()->getContent().length();
        }
    }
    return res;
}

////////////////
// ContentInstance

ContentInstance* ContentInstance::getByIdentifier(const String& identifier)
{
    Entity* e = Entity::getByIdentifier(identifier);
    return e ? e->getInstance() : nullptr;
}

ContentInstance::~ContentInstance()
{
}

ContentInstance* ContentInstance::getByName(const String& name)
{
    Entity* e = Entity::getByName(name);
    return e ? e->getInstance() : nullptr;
}

void ContentInstance::printLocalTo(printfmt to, int level) const
{
    //to(level, nullptr, (m_name + " (" + get_ctime(m_time_creation) + ")").c_str());
    to(level + 1, "instance", "glok");
}

#if SUBSCRIPTION_FEAT
////////////////
// Subscription
Subscription::~Subscription() 
{
}


Subscription* Subscription::getByIdentifier(const String& identifier)
{
    Entity* e = Entity::getByIdentifier(identifier);
    return e ? e->getSubscription() : nullptr;
}

Subscription* Subscription::getByName(const String& name)
{
    Entity* e = Entity::getByName(name);
    return e ? e->getSubscription() : nullptr;
}
#endif // SUBSCRIPTION_FEAT

AccessControlPolicy* AcpAdmin::acpInstance = nullptr;

void AcpAdmin::init()
{
    AccessControlRule* rule = new AccessControlRule(63);
    rule->m_accessControlOriginators.push_back(ADMIN_ORIGINATOR);
    rule->m_accessControlOriginators.push_back("/"+CSE_ID);
    this->m_privileges.push_back(*rule);
    // PVS
    AccessControlRule* rulePvs = new AccessControlRule(63);
    rulePvs->m_accessControlOriginators.push_back(ADMIN_ORIGINATOR);
    rulePvs->m_accessControlOriginators.push_back("/"+CSE_ID);

    this->m_selfPrivileges.push_back(*rulePvs);
    delete rule;
    delete rulePvs;
    CseBase::getInstance()->addChild(this);
    CseBase::getInstance()->m_acps.push_back(this);
}
////////////////
