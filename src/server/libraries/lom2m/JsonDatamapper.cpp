/*
------------------------------------------------------------

SOFTWARE EVALUATION LICENSE

LAAS research laboratory, Toulouse, France.
IRIT computer science research laboratory, Toulouse, France.
------------------------------------------------------------

Contact: Thierry Monteil, thierry.monteil@irit.fr

Definitions
SOFTWARE:  The LightOM2M software, version 1.0, in source code form, written by David Gauchard, Guillaume Garzone and Thierry Monteil
at the LAAS research laboratory.

LICENSOR: LE CENTRE NATIONAL DE LA RECHERCHE SCIENTIFIQUE, a public scientific and technical research establishment, having SIREN No. 180 089 013, APE code 7219 Z, having its registered office at 3, rue Michel-Ange, 75796 Paris cedex 16, France, acting in its own name and on its own behalf, and on behalf of Le laboratoire d'analyse et d'architecture des systèmes (LAAS), UPR n°8001.

1. INTENT/PURPOSE
This agreement determines the conditions in which the LICENSOR, who has the rights to the SOFTWARE, grants to the LICENSEE a license for research and evaluation purposes only, excluding any commercial use.

2. LICENSEE
Any person or organization who receives the SOFTWARE with a copy of this license.

3. RIGHTS GRANTED
The rights to use and copy the SOFTWARE, subject to the restrictions described in this agreement.
The rights to modify and compile the SOFTWARE when it's provided in source code form, subject to the restrictions described in this agreement.
For the SOFTWARE provided in binary form only, LICENSEE undertakes to not decompile, disassemble, decrypt, extract the components or perform reverse engineering except to the extent expressly provided by law.

5. SCOPE OF THE LICENSE
- NON-COMMERCIAL license for research and evaluation purposes ONLY.
- NO right to commercialize the SOFTWARE, or any derivative work, without separate agreement with the LICENSOR.

6. MODIFICATION OF THE SOFTWARE
License permits LICENSEE to modify the SOFTWARE provided in source code form for research and evaluation purposes only.

7. REDISTRIBUTION
- License permits LICENSEE to redistribute verbatim copies of the SOFTWARE, accompanied with a copy of this license.
- License DOES NOT permit LICENSEE to redistribute modified versions of the SOFTWARE provided in source code form.
- License DOES NOT permit LICENSEE to commercialize the SOFTWARE or any derivative work of the SOFTWARE.

8. FEE/ROYALTY
- LICENSEE pays no royalty for this license.
- LICENSEE and any third parties must enter a new agreement for any use beyond scope of license. Please contact the IRIT technology transfer office (email numerique@toulouse-tech-transfer.com) for further information.

9. NO WARRANTY
The SOFTWARE is provided "as is" without warranty of any kind, either expressed or implied, including, but not limited to, the implied warranties of merchantability and fitness for a particular purpose. The entire risk as to the quality and performance of the program is with the LICENSEE.

10. NO LIABILITY
In no event unless required by applicable law or agreed to in writing will any copyright owner be liable to LICENSEE for damages, including any general, special, incidental or consequential damages arising out of the use or inability to use the program (including but not limited to loss of data or data being rendered inaccurate or losses sustained by LICENSEE or third parties or a failure of the program to operate with other programs), even if such holder has been advised of the possibility of such damages.
*/


#include "JsonDatamapper.h"
#include "lom2m.h"
#include "AccessControlController.h"

int
Mapper::mapResourceAttributes(JsonObject jsonObj, Entity* e, int level, Operation op, bool mapAll)
{
    if (level < 0 && !mapAll)
    {
        return 0;
    }
    int resourceType = e->getType();

    // Mapping specific attributes
    switch (resourceType)
    {
    case TY1_ACP:
    {
        ACPMapper::mapAttributes(jsonObj, e->getAcp(), level, op, mapAll);
        break;
    }
    case TY2_APPL_ENTITY:
    {
        AEMapper::mapAttributes(jsonObj, e->getApplication(), level, op, mapAll);
        break;
    }
    case TY3_CONTAINER:
    {
        ContainerMapper::mapAttributes(jsonObj, e->getContainer(), level, op, mapAll);
        break;
    }
    case TY4_CONTENT_INSTANCE:
    {
        ContentInstanceMapper::mapAttributes(jsonObj, e->getInstance(), level, op, mapAll);
        break;
    }
    case TY5_CSEBASE:
    {
        CseBaseMapper::mapAttributes(jsonObj, e->getCseBase());
        break;
    }
#if GROUP_FEAT
    case TY9_GROUP:
    {
        GroupMapper::mapAttributes(jsonObj, e->getGroup(), level, op, mapAll);
        break;
    }
#endif
    case TY16_REMOTE_CSE:
    {
        RemoteCseMapper::mapAttributes(jsonObj, e->getRemoteCse(), level, op, mapAll);
        break;
    }
#if SUBSCRIPTION_FEAT
    case TY23_SUBSCRIPTION:
    {
        SubscriptionMapper::mapAttributes(jsonObj, e->getSubscription(), level, op, mapAll);
        break;
    }
#endif // SUBSCRIPTION_FEAT
    default:
        return 0;
    }

    return 1;
}

int
Mapper::mapChildResourcesRef(JsonObject jsonObj, Entity* e, int level, Operation op, bool mapAll)
{
    if (level < 0 && !mapAll)
    {
        return 0;
    }
    if (!e->getChildren().empty())
    {
        JsonArray chArray = jsonObj[CHILD_RESOURCE].to<JsonArray>();
        for (auto ch : e->getChildren())
        {
            //"ch": [ { "rn": "resource_name", "ty": resourceType, "value": "resourceId" } ]
            JsonObject o = chArray.createNestedObject();

#if REL_1
            o[RESOURCE_NAME] = ch->getName();
            o[RESOURCE_TYPE] = ch->getType(); // String()?
            o["value"] = ch->getResourceIdentifier();
#else
            o[CHILD_RESOURCE_NAME] = ch->getName();
            o[CHILD_RESOURCE_TYPE] = ch->getType(); // String()?
            o[CHILD_RESOURCE_VALUE] = ch->getResourceIdentifier();
#endif
        }
    }
    return 0;
}

int
Mapper::mapChildResources(JsonObject jsonObj, Entity* e, int level, Operation op, bool mapAll)
{
    if (level <= 0 && !mapAll)
    {
        return 0;
    }
#if DEBUG
    Serial.printf("DEBUG: mapping child resources\n");
#endif
    if (!e->getChildren().empty())
    {
        for (auto ch : e->getChildren())
        {
            if (op == OP_RETRIEVE && !checkAccessRights(ch, FROM_ORIGINATOR, op))
            {
                continue;
            }
            JsonArray chArray;
            if (jsonObj.containsKey(rqType(ch->getType())))
            {
                chArray = jsonObj[rqType(ch->getType())];
            }
            else
            {
                chArray = jsonObj[rqType(ch->getType())].to<JsonArray>();
            }
            JsonObject o = chArray.createNestedObject();
#if DEBUG
            Serial.printf("DEBUG: adding child %s\n", ch->getName().c_str());
#endif
            Mapper::mapGenericAttributes(o, ch, level - 1, op, mapAll);
            Mapper::mapResourceAttributes(o, ch, level - 1, op, mapAll);
            Mapper::mapChildResources(o, ch, level - 1, op, mapAll);
        }
    }
    return 0;
}

int
Mapper::mapGenericAttributes(JsonObject jsonObj, Entity* e, int level, Operation op, bool mapAll)
{
    if (level < 0 && !mapAll)
    {
        return 0;
    }
    if (op != OP_UPDATE)
    {
        jsonObj[RESOURCE_NAME] = e->m_name;
    }
    
    if (op != OP_CREATE && op != OP_UPDATE)
    {
        jsonObj[RESOURCE_TYPE] = e->getType();
        if (!e->getAcp())
        {
            if (!e->m_acps.empty())
            {
                JsonArray a;
                a = jsonObj[ACP_IDS].to<JsonArray>();
                for (AccessControlPolicy* acp : e->m_acps)
                {
                    a.add(acp->getResourceIdentifier());
                }
            }
        }
        jsonObj[CREATION_TIME] = get_ctime(e->m_time_creation);
        jsonObj[LAST_MODIFIED_TIME] = get_ctime(e->m_time_modification);
        if (!e->getCseBase())
        {
            jsonObj[EXPIRATION_TIME] = get_ctime(e->m_time_expiration);
        }
        jsonObj[RESOURCE_ID] = e->getResourceIdentifier();
        jsonObj[PARENT_ID] = e->getParentID();
    }
    JsonArray a;
    if (!e->m_labels.empty())
    {
        a = jsonObj[LABELS].to<JsonArray>();
        for (auto s : e->m_labels)
            a.add(s);
    }
    return 0;
}
#if FEAT_RCN_9
int
Mapper::mapGenericModifiedAttributes(JsonObject jsonObj, Entity* e)
{
    for (String key : modifiedAttributes)
    {
        if (key.equals(EXPIRATION_TIME))
        {
            jsonObj[EXPIRATION_TIME] = get_ctime(e->m_time_expiration);
        }
        else if (key.equals(LABELS))
        {
            if (!e->m_labels.empty())
            {
                JsonArray a;
                a = jsonObj[LABELS].to<JsonArray>();
                for (auto s : e->m_labels)
                    a.add(s);
            }
        }
        else if (key.equals(ACP_IDS))
        {
            if (!e->getAcp())
            {
                if (!e->m_acps.empty())
                {
                    JsonArray a;
                    a = jsonObj[ACP_IDS].to<JsonArray>();
                    for (AccessControlPolicy* acp : e->m_acps)
                    {
                        a.add(acp->getResourceIdentifier());
                    }
                }
            }
        }
    }
    jsonObj[LAST_MODIFIED_TIME] = get_ctime(e->m_time_modification);
    return 0;
}
#endif

int
ACPMapper::mapAttributes(JsonObject jsonObj, AccessControlPolicy* acp, int level, Operation op, bool mapAll)
{
    JsonObject rule;
    JsonObject pv;
    JsonArray acr;
#if FEAT_RCN_9
    bool pvBool = false; 
    bool pvsBool = false;
    for (String s : modifiedAttributes)
    {
        if (s.equals(PRIVILEGES))
        {
            pvBool = true;
        }
        else if (s.equals(SELF_PRIVILEGES))
        {
            pvsBool = true;
        }
    }
    if (modifiedAttributes.empty() || pvBool)
    {
#endif
        pv = jsonObj[PRIVILEGES].to<JsonObject>();
        acr = pv[ACR].to<JsonArray>();
        for (AccessControlRule r : acp->m_privileges)
        {
            JsonObject rule = acr.createNestedObject();
            JsonArray acor = rule[ACOR].to<JsonArray>();
            for (auto o : r.m_accessControlOriginators)
            {
                acor.add(o);
            }
            rule[ACOP] = getOperationFromACR(r);
        }
#if FEAT_RCN_9
    }
    if (modifiedAttributes.empty() || pvsBool)
    {
#endif
        pv = jsonObj[SELF_PRIVILEGES].to<JsonObject>();
        acr = pv[ACR].to<JsonArray>();
        for (AccessControlRule r : acp->m_selfPrivileges)
        {
            JsonObject rule = acr.createNestedObject();
            JsonArray acor = rule[ACOR].to<JsonArray>();
            for (auto o : r.m_accessControlOriginators)
            {
                acor.add(o);
            }
            rule[ACOP] = getOperationFromACR(r);
            if (!r.m_contexts.empty())
            {
                JsonArray a = rule[ACCO].to<JsonArray>();
                for (AccessControlContext c : r.m_contexts)
                {
                    if (!c.m_accessControlTimeWindow.empty())
                    {
                        JsonObject actw = a.createNestedObject();
                        JsonArray actwArray = actw[ACTW].to<JsonArray>();
                        for (auto s : c.m_accessControlTimeWindow)
                        {
                            actwArray.add(s);
                        }
                    }
                    if (!c.m_accessControlIP4Address.empty() || !c.m_accessControlIP6Address.empty())
                    {
                        JsonObject acipo = a.createNestedObject();
                        if (!c.m_accessControlIP4Address.empty())
                        {
                            JsonObject ipv4 = acipo[ACIP].to<JsonObject>();
                            JsonArray ip4 = ipv4[IPV4].to<JsonArray>();
                            for (auto ip : c.m_accessControlIP4Address)
                            {
                                ip4.add(ip);
                            }
                        }
                        if (!c.m_accessControlIP6Address.empty())
                        {
                            JsonObject ipv6 = acipo[ACIP].to<JsonObject>();
                            JsonArray ip6 = ipv6[IPV6].to<JsonArray>();
                            for (auto ip : c.m_accessControlIP6Address)
                            {
                                ip6.add(ip);
                            }
                        }
                    }
                    if (!c.m_aclRegionCountryCode.empty() || !c.m_aclRegionCirc.empty())
                    {
                        JsonObject aclr = a.createNestedObject();
                        if (!c.m_aclRegionCountryCode.empty())
                        {
                            JsonObject accc = aclr[ACLR].to<JsonObject>();
                            JsonArray acccList = accc[ACCC].to<JsonArray>();
                            for (auto s : c.m_aclRegionCountryCode)
                            {
                                acccList.add(s);
                            }
                        }
                        else if (!c.m_aclRegionCirc.empty())
                        {
                            JsonObject accr = aclr[ACLR].to<JsonObject>();
                            JsonArray accrList = accr[ACCR].to<JsonArray>();
                            for (auto s : c.m_aclRegionCountryCode)
                            {
                                accrList.add(s);
                            }
                        }
                    }
                }
            }
            if (r.m_accessControlAuthenticationFlag)
            {
                rule[ACAF] = String(r.m_accessControlAuthenticationFlag);
            }
        }
#if FEAT_RCN_9
    }
#endif
}

AccessControlPolicy*
ACPMapper::parseResource(JsonObject o, const String& name, Entity* entityToFill)
{
    AccessControlPolicy* acp;
    if (entityToFill)
    {
        acp = entityToFill->getAcp();
    }
    else
    {
        acp = new AccessControlPolicy(name);
    }
    JsonObject pvs;
    // Privileges
    if (o.containsKey(SELF_PRIVILEGES))
    {
#if FEAT_RCN_9
        modifiedAttributes.push_back(SELF_PRIVILEGES);
#endif
        pvs = o[SELF_PRIVILEGES];
        acp->m_selfPrivileges.clear();
        // mapping ACR
        for (JsonObject acr : (JsonArray)pvs[ACR])
        {
            AccessControlRule* rule = new AccessControlRule();
            // acor
            if (acr.containsKey(ACOR))
            {
                for (auto o : (JsonArray)acr[ACOR])
                {
                    rule->m_accessControlOriginators.push_back(o);
                }
            }
            // acop
            if (acr.containsKey(ACOP))
            {
                getARCFromOperation(acr[ACOP], rule);
            }

            // acco
            if (acr.containsKey(ACCO))
            {
                for (JsonObject acco : (JsonArray)acr[ACCO])
                {
                    AccessControlContext* context = new AccessControlContext();
                    if (acco.containsKey(ACTW))
                    {
                        for (auto s : (JsonArray)acco[ACTW])
                        {
                            context->m_accessControlTimeWindow.push_back(s);
                        }
                    }
                    if (acco.containsKey(IPV4))
                    {
                        for (auto s : (JsonArray)acco[IPV4])
                        {
                            context->m_accessControlIP4Address.push_back(s);
                        }
                    }
                    if (acco.containsKey(IPV6))
                    {
                        for (auto s : (JsonArray)acco[IPV6])
                        {
                            context->m_accessControlIP6Address.push_back(s);
                        }
                    }
                    if (acco.containsKey(ACLR))
                    {
                        if (((JsonObject)acco[ACLR]).containsKey(ACCC))
                        {
                            for (auto s : (JsonArray)((JsonObject)acco[ACLR])[ACCC])
                            {
                                context->m_aclRegionCountryCode.push_back(s);
                            }
                        }
                        else if (((JsonObject)acco[ACLR]).containsKey(ACCR))
                        {
                            for (auto s : (JsonArray)((JsonObject)acco[ACLR])[ACCR])
                            {
                                context->m_aclRegionCirc.push_back(s);
                            }
                        }
                    }

                    rule->m_contexts.push_back(*context);
                }
            }

            // acaf
            if (acr.containsKey(ACAF))
            {
                rule->m_accessControlAuthenticationFlag = ((const String&)acr[ACAF]).equalsIgnoreCase("true");
            }

            acp->m_selfPrivileges.push_back(*rule);
            delete rule;
        }
    }
    // Privileges
    if (o.containsKey(PRIVILEGES))
    {
#if FEAT_RCN_9
        modifiedAttributes.push_back(PRIVILEGES);
#endif
        pvs = o[PRIVILEGES];
        if (pvs.containsKey(ACR))
        {
            acp->m_privileges.clear();
            // ACR
            for (JsonObject acr : (JsonArray)pvs[ACR])
            {
                AccessControlRule* rule = new AccessControlRule();
                // acor
                if (acr.containsKey(ACOR))
                {
                    for (auto o : (JsonArray)acr[ACOR])
                    {
                        rule->m_accessControlOriginators.push_back(o);
                    }
                }
                // acop
                if (acr.containsKey(ACOP))
                {
                    getARCFromOperation(acr[ACOP], rule);
                }

                // acco
                if (acr.containsKey(ACCO))
                {
                    for (JsonObject acco : (JsonArray)acr[ACCO])
                    {
                        AccessControlContext* context = new AccessControlContext();
                        if (acco.containsKey(ACTW))
                        {
                            for (auto s : (JsonArray)acco[ACTW])
                            {
                                context->m_accessControlTimeWindow.push_back(s);
                            }
                        }
                        if (acco.containsKey(IPV4))
                        {
                            for (auto s : (JsonArray)acco[IPV4])
                            {
                                context->m_accessControlIP4Address.push_back(s);
                            }
                        }
                        if (acco.containsKey(IPV6))
                        {
                            for (auto s : (JsonArray)acco[IPV6])
                            {
                                context->m_accessControlIP6Address.push_back(s);
                            }
                        }
                        if (acco.containsKey(ACLR))
                        {
                            if (((JsonObject)acco[ACLR]).containsKey(ACCC))
                            {
                                for (auto s : (JsonArray)((JsonObject)acco[ACLR])[ACCC])
                                {
                                    context->m_aclRegionCountryCode.push_back(s);
                                }
                            }
                            else if (((JsonObject)acco[ACLR]).containsKey(ACCR))
                            {
                                for (auto s : (JsonArray)((JsonObject)acco[ACLR])[ACCR])
                                {
                                    context->m_aclRegionCirc.push_back(s);
                                }
                            }
                        }

                        rule->m_contexts.push_back(*context);
                    }
                }

                // acaf
                if (acr.containsKey(ACAF))
                {
                    rule->m_accessControlAuthenticationFlag = ((const String&)acr[ACAF]).equalsIgnoreCase("true");
                }

                acp->m_privileges.push_back(*rule);
                delete rule;
            }
        }
    }

    // HERE add other attributes when implemented
    // Authorization decision resource IDS
    // Authorization Policy resource IDS
    // Authorization Information Resource IDS
    return acp;
}

Application*
AEMapper::parseResource(JsonObject o, const String& name, Entity* toUpdate)
{
    Application* ae;
    if (!toUpdate)
    {
        ae = Entity::addApplication(name);
    }
    else
    {
        ae = toUpdate->getApplication();
    }
    // HERE ADD OTHER ATTRIBUTES when implemented
    if (ae == nullptr)
    {
        return nullptr;
    }
    if (o.containsKey(REQUEST_REACHABILITY))
    {
        ae->m_rr = ((const String&)o[REQUEST_REACHABILITY]).equalsIgnoreCase("true");
#if FEAT_RCN_9
        modifiedAttributes.push_back(REQUEST_REACHABILITY);
#endif
    }
    if (o.containsKey(POA))
    {
        JsonArray a = o[POA];
        ae->m_poa.clear();
        for (String s : a)
        {
            ae->m_poa.push_back(s);
        }
#if FEAT_RCN_9
        modifiedAttributes.push_back(POA);
#endif
    }
    if (o.containsKey(APP_ID))
    {
        ae->m_api = (const String&)o[APP_ID];
    }
    if (o.containsKey(APP_NAME))
    {
        ae->m_apn = (const String&)o[APP_NAME];
#if FEAT_RCN_9
        modifiedAttributes.push_back(APP_NAME);
#endif
    }
    if (o.containsKey(SUPPORTED_REL_VERSIONS))
    {
        JsonArray a = o[SUPPORTED_REL_VERSIONS];
        ae->m_supportedReleaseVersions.clear();
        for (String s : a)
        {
            ae->m_supportedReleaseVersions.push_back(s);
        }
#if FEAT_RCN_9
        modifiedAttributes.push_back(SUPPORTED_REL_VERSIONS);
#endif
    }
    if (o.containsKey(CONTENT_SERIALIZATION))
    {
        JsonArray a = o[CONTENT_SERIALIZATION];
        ae->m_contentSerialisation.clear();
        for (String s : a)
        {
            ae->m_contentSerialisation.push_back(s);
        }
#if FEAT_RCN_9
        modifiedAttributes.push_back(CONTENT_SERIALIZATION);
#endif
    }
    if (o.containsKey(ONTOLOGY_REF))
    {
        ae->m_ontologyRef = ((const String&)o[ONTOLOGY_REF]);
#if FEAT_RCN_9
        modifiedAttributes.push_back(ONTOLOGY_REF);
#endif
    }
    return ae;
}

int
AEMapper::mapAttributes(JsonObject jsonObj, Application* ae, int level, Operation op, bool mapAll)
{
    if (level < 0 && !mapAll)
    {
        return 0;
    }
#if FEAT_RCN_9
    bool apnBool = false;
    bool poaBool = false;
    bool rrBool = false;
    bool cszBool = false;
    bool srvBool = false;
    bool orBool = false;
    for (String s : modifiedAttributes)
    {
        if (s.equals(APP_NAME))
        {
            apnBool = true;
        }
        else if (s.equals(POA))
        {
            poaBool = true;
        }
        else if (s.equals(REQUEST_REACHABILITY))
        {
            rrBool = true;
        }
        else if (s.equals(CONTENT_SERIALIZATION))
        {
            cszBool = true;
        }
        else if (s.equals(SUPPORTED_REL_VERSIONS))
        {
            srvBool = true;
        }
        else if (s.equals(ONTOLOGY_REF))
        {
            orBool = true;
        }
    }
    if (modifiedAttributes.empty())
    {
#endif
        if (ae->m_api.length() > 0)
            jsonObj[APP_ID] = ae->m_api;
        if (ae->getIdentifier().length())
            jsonObj[AE_ID] = ae->getIdentifier();
#if FEAT_RCN_9
    }
    if (modifiedAttributes.empty() || apnBool)
    {
#endif
        if (ae->m_apn.length() > 0)
            jsonObj[APP_NAME] = ae->m_apn;
#if FEAT_RCN_9
    }
#endif
    JsonArray a;
#if FEAT_RCN_9
    if (modifiedAttributes.empty() || poaBool)
    {
#endif
        if (!ae->m_poa.empty())
        {
            a = jsonObj[POA].to<JsonArray>();
            for (auto s : ae->m_poa)
                a.add(s);
        }
#if FEAT_RCN_9
    }
    if (modifiedAttributes.empty() || orBool)
    {
#endif
        if (ae->m_ontologyRef.length() > 0)
            jsonObj[ONTOLOGY_REF] = ae->m_ontologyRef;
#if FEAT_RCN_9
    }
    if (modifiedAttributes.empty() || rrBool)
    {
#endif
        jsonObj[REQUEST_REACHABILITY] = ae->m_rr;
#if FEAT_RCN_9
    }
    if (modifiedAttributes.empty() || cszBool)
    {
#endif
        if (!ae->m_contentSerialisation.empty())
        {
            a = jsonObj[CONTENT_SERIALIZATION].to<JsonArray>();
            for (auto s : ae->m_contentSerialisation)
                a.add(s);
        }
#if FEAT_RCN_9
    }
    if (modifiedAttributes.empty() || srvBool)
    {
#endif
        if (!ae->m_supportedReleaseVersions.empty())
        {
            a = jsonObj[SUPPORTED_REL_VERSIONS].to<JsonArray>();
            for (auto s : ae->m_supportedReleaseVersions)
                a.add(s);
        }
#if FEAT_RCN_9
    }
#endif
}

Container*
ContainerMapper::parseResource(JsonObject o, const String& name, Entity* targetEntity, Entity* toUpdate)
{
    Container* cnt;
    if (toUpdate)
    {
        cnt = toUpdate->getContainer();
    }
    else
    {
        cnt = Entity::addContainer(name, targetEntity);
    }

    if (o.containsKey(ONTOLOGY_REF))
    {
        cnt->setOntologyRef(o[ONTOLOGY_REF]);
#if FEAT_RCN_9
        modifiedAttributes.push_back(ONTOLOGY_REF);
#endif
    }
    if (o.containsKey(MAX_NR_OF_INSTANCES))
    {
        if (!MAX_NUMBER_OF_INSTANCES_DEFAULT)
        {
            cnt->setMaxNumberOfInstances((int)o[MAX_NR_OF_INSTANCES]);
        }
        else if ((int)o[MAX_NR_OF_INSTANCES] < MAX_NUMBER_OF_INSTANCES_DEFAULT || MAX_NUMBER_OF_INSTANCES_DEFAULT == -1)
        {
            cnt->setMaxNumberOfInstances((int)o[MAX_NR_OF_INSTANCES]);
        }
#if FEAT_RCN_9
        modifiedAttributes.push_back(MAX_NR_OF_INSTANCES);
#endif
    }
    if (o.containsKey(MAX_BYTE_SIZE))
    {
        cnt->setMaxByteSize((int)o[MAX_BYTE_SIZE]);
#if FEAT_RCN_9
        modifiedAttributes.push_back(MAX_BYTE_SIZE);
#endif
    }
    // ADD DISABLED RETRIEVAL when supported
    return cnt;
}

ContentInstance*
ContentInstanceMapper::parseResource(JsonObject o, const String& name, Entity* targetEntity)
{

    ContentInstance* cin = Entity::addInstance(name, targetEntity->getContainer());
    if (o.containsKey(CONTENT_INFO))
    {
        cin->setContentFormat(o[CONTENT_INFO]);
    }
    cin->setContent(o[CONTENT]);
    return cin;
}

int
ContainerMapper::mapAttributes(JsonObject jsonObj, Container* cnt, int level, Operation op, bool mapAll)
{
    if (level < 0 && !mapAll)
    {
        return 0;
    }
#if FEAT_RCN_9
    bool mniB = false;
    bool mbsB = false;
    bool miaB = false;
    bool orB = false;
    bool drB = false;
    bool stB = false;
    for (String s : modifiedAttributes)
    {
        if (s.equals(MAX_NR_OF_INSTANCES))
        {
            mniB = true;
        }
        else if (s.equals(MAX_BYTE_SIZE))
        {
            mbsB = true;
        }
        else if (s.equals(MAX_INSTANCE_AGE))
        {
            miaB = true;
        }
        else if (s.equals(ONTOLOGY_REF))
        {
            orB = true;
        }
        else if (s.equals(STATETAG))
        {
            stB = true;
        }
    }
    if (modifiedAttributes.empty())
    {
#endif // FEAT_RCN_9
        jsonObj[CURRENT_NUMBER_OF_INSTANCES] = cnt->getCurrentNumberOfInstances();
        jsonObj[CURRENT_BYTE_SIZE] = cnt->getCurrentByteSize();
        jsonObj[DISABLE_RETRIEVAL] = cnt->getDisableRetrieval();
        if (cnt->getCreator() && !cnt->getCreator().isEmpty())
        {
            jsonObj[CREATOR] = cnt->getCreator();
        }
        if (cnt->getLocationID().length())
            jsonObj[LOCATION_ID] = cnt->getLocationID();
#if FEAT_RCN_9
    }
    if (modifiedAttributes.empty() || mniB)
    {
#endif // FEAT_RCN_9
        if (cnt->getMaxNumberOfInstances() != -1)
            jsonObj[MAX_NR_OF_INSTANCES] = cnt->getMaxNumberOfInstances();
#if FEAT_RCN_9
    }
    if (modifiedAttributes.empty() || mbsB)
    {
#endif // FEAT_RCN_9
        if (cnt->getMaxByteSize() != -1)
            jsonObj[MAX_BYTE_SIZE] = cnt->getMaxByteSize();
#if FEAT_RCN_9
    }
    if (modifiedAttributes.empty() || miaB)
    {
#endif // FEAT_RCN_9
        if (cnt->getMaxInstanceAge() != -1)
            jsonObj[MAX_INSTANCE_AGE] = cnt->getMaxInstanceAge();
#if FEAT_RCN_9
    }
    if (modifiedAttributes.empty() || orB)
    {
#endif // FEAT_RCN_9
        if (cnt->getOntologyRef().length())
            jsonObj[ONTOLOGY_REF] = cnt->getOntologyRef();
#if FEAT_RCN_9
    }
    if (modifiedAttributes.empty() || stB)
    {
#endif // FEAT_RCN_9
        if (cnt->getStateTag() >= 0)
        {
            jsonObj[STATETAG] = cnt->getStateTag();
        }
#if FEAT_RCN_9
    }
#endif // FEAT_RCN_9
}

int
ContentInstanceMapper::mapAttributes(JsonObject jsonObj, ContentInstance* cin, int level, Operation op, bool mapAll)
{
    if (level < 0 && !mapAll)
    {
        return 0;
    }
    if (cin->getContentFormat() && !cin->getContentFormat().isEmpty())
    {
        jsonObj[CONTENT_INFO] = cin->getContentFormat();
    }
    jsonObj[CONTENT] = cin->getContent();
    jsonObj[CONTENT_SIZE] = cin->getContent().length();
    if (cin->getStateTag() > 0)
    {
        jsonObj[STATETAG] = cin->getStateTag();
    }
    return 0;
}

int
CseBaseMapper::mapAttributes(JsonObject jsonObj, CseBase* csb)
{
    jsonObj[SN_CSE_TYPE] = csb->m_cst; // String()? 1:IN-CSE 2:MN-CSE 3:ASN-CSE
    String csi = "/" + csb->m_cseId;
    jsonObj[SN_CSE_ID] = csi;

    JsonArray a;

    a = jsonObj[SRT].to<JsonArray>();
    for (int i : csb->m_supportedResTypes)
        a.add(i);

    a = jsonObj[POA].to<JsonArray>();
    for (auto s : csb->m_poas)
        a.add(s);

    a = jsonObj[CONTENT_SERIALIZATION].to<JsonArray>();
    for (auto s : csb->m_contentSerializationTypes)
        a.add(s);

    a = jsonObj[SUPPORTED_REL_VERSIONS].to<JsonArray>();
    for (auto s : csb->m_supportedReleaseVersions)
        a.add(s);

    return 0;
}

#if GROUP_FEAT
int
GroupMapper::mapAttributes(JsonObject jsonObj, Group* group, int level, Operation op, bool mapAll)
{
    if (level < 0 && !mapAll)
    {
        return 0;
    }
    JsonArray a;
    a = jsonObj[MEMBER_ID].to<JsonArray>();
    for (auto id : group->getMemberIds())
    {
        printf("\n\nDEBUG: adding MID %s\n", id.c_str());
        a.add(id);
    }
    // for (auto child : group->children)
    // {
    //     a.add(child->getResourceIdentifier());
    // } // the identifers above are real:
    jsonObj[MEMBER_TYPE_VALIDATED] = ENCODED_TRUE;
    if (group->getApplicationParent())
    {
        jsonObj[FANOUTPOINT] = String('/') + CSE_ID + '/' + CSE_NAME + '/' + group->getApplicationParent()->getName() + '/' + group->getName() + F("/fopt");
    }
    return 0;
}
#endif

int
RemoteCseMapper::mapAttributes(JsonObject jsonObj, RemoteCse* csr, int level, Operation op, bool mapAll)
{
    if (level < 0 && !mapAll)
    {
        return 0;
    }
    JsonArray a;
#if FEAT_RCN_9
    bool poaB = false;
    bool m2mextidB = false;
    bool rrB = false;
    bool cszB = false;
    bool dcseB = false;
    bool srvB = false;
    for (String s : modifiedAttributes)
    {
        if (s.equals(POA))
        {
            poaB = true;
        }
        else if (s.equals(M2M_EXT_ID))
        {
            m2mextidB = true;
        }
        else if (s.equals(REQUEST_REACHABILITY))
        {
            rrB = true;
        }
        else if (s.equals(CONTENT_SERIALIZATION))
        {
            cszB = true;
        }
        else if (s.equals(SN_DESCENDANT_CSE))
        {
            dcseB = true;
        }
        else if (s.equals(SUPPORTED_REL_VERSIONS))
        {
            srvB = true;
        }
    }
#endif //FEAT_RCN_9

// commons attr. with CSE BASE
#if FEAT_RCN_9
    if (modifiedAttributes.empty() && op != OP_UPDATE)
    {
#endif //FEAT_RCN_9
        if (csr->getCseType() != 0)
            jsonObj[SN_CSE_TYPE] = csr->getCseType();
        jsonObj[SN_CSE_ID] = csr->getCseID();
        jsonObj[REMOTE_CSE_CSEBASE] = csr->getCseBase();
#if FEAT_RCN_9
    }
    if (modifiedAttributes.empty() || poaB)
    {
#endif //FEAT_RCN_9
        if (!csr->poas.empty())
        {
            a = jsonObj[POA].to<JsonArray>();
            for (auto s : csr->poas)
            {
                a.add(s);
            }
        }
#if FEAT_RCN_9
    }
    if (modifiedAttributes.empty() || m2mextidB)
    {
        // M2M_EXT_ID
    }
    // TRIGGER RECIPIENT ID
    if (modifiedAttributes.empty() || rrB)
    {
#endif                                                                 //FEAT_RCN_9
        jsonObj[REQUEST_REACHABILITY] = csr->getRequestReachability(); // bool
#if FEAT_RCN_9
    }
    // node link
    // trigger ref number
    if (modifiedAttributes.empty() || cszB)
    {
#endif //FEAT_RCN_9
        if (!csr->getContentSerializationTypes().empty())
        {
            a = jsonObj[CONTENT_SERIALIZATION].to<JsonArray>();
            for (auto s : csr->getContentSerializationTypes())
            {
                a.add(s);
            }
        }
#if FEAT_RCN_9
    }
    if (modifiedAttributes.empty() || dcseB)
    {
#endif //FEAT_RCN_9
        if (!csr->getDescendantCses().empty())
        {
            a = jsonObj[SN_DESCENDANT_CSE].to<JsonArray>();
            for (auto s : csr->getDescendantCses())
                a.add(s);
        }
#if FEAT_RCN_9
    }
    if (modifiedAttributes.empty() || srvB)
    {
#endif //FEAT_RCN_9
        if (!csr->getSupportedReleaseVersions().empty())
        {
            a = jsonObj[SUPPORTED_REL_VERSIONS].to<JsonArray>();
            for (auto s : csr->getSupportedReleaseVersions())
            {
                a.add(s);
            }
        }
#if FEAT_RCN_9
    }
#endif //FEAT_RCN_9

    return 0;
}

#if SUBSCRIPTION_FEAT
int
SubscriptionMapper::mapAttributes(JsonObject jsonObj, Subscription* sub, int level, Operation op, bool mapAll)
{
    if (level < 0 && !mapAll)
    {
        return 0;
    }
    JsonArray a;
#if FEAT_RCN_9
    bool encB = false;
    bool ecB = false;
    bool nuB = false;
    bool gpiB = false;
    bool nfuB = false;
    bool bnB = false;
    bool rlB = false;
    bool psnB = false;
    bool pnB = false;
    bool nspB = false;
    bool lnB = false;
    bool nctB = false;
    bool necB = false;
    // associated cross resource sub

    for (String s : modifiedAttributes)
    {
        if (s.equals(EVENT_NOTIFICATION_CRITERIA))
        {
            encB = true;
        }
        else if (s.equals(EXPIRATION_COUNTER))
        {
            ecB = true;
        }
        else if (s.equals(NOTIFICATION_URI))
        {
            nuB = true;
        }
        else if (s.equals(GROUP_ID))
        {
            gpiB = true;
        }
        else if (s.equals(NOTIFICATION_FORWARDING_URI))
        {
            nfuB = true;
        }
        else if (s.equals(BATCH_NOTIFY))
        {
            bnB = true;
        }
        else if (s.equals(RATE_LIMIT))
        {
            rlB = true;
        }
        else if (s.equals(PENDING_NOTIFICATION))
        {
            pnB = true;
        }
        else if (s.equals(NOTIFICATION_STORAGE_PRIORITY))
        {
            nspB = true;
        }
        else if (s.equals(NOTIFICATION_CONTENT_TYPE))
        {
            nctB = true;
        }
        else if (s.equals(NOTIFICATION_EVENT_CAT))
        {
            necB = true;
        }
        // ASSOCIATED CROSS RESOURCE SUB
    }

    if (modifiedAttributes.empty())
    {
#endif // FEAT_RCN_9
        if (sub->getExpirationCounter() != -1)
        {
            jsonObj[EXPIRATION_COUNTER] = sub->getExpirationCounter();
        }
        if (!sub->getCreator().isEmpty())
        {
            jsonObj[CREATOR] = sub->getCreator();
        }
        if (!sub->getSubscriberURI().isEmpty())
        {
            jsonObj[SUBSCRIBER_URI] = sub->getSubscriberURI();
        }
#if FEAT_RCN_9
    }
    if (modifiedAttributes.empty() || nuB)
    {
#endif //FEAT_RCN_9
        a = jsonObj[NOTIFICATION_URI].to<JsonArray>();
        for (auto s : sub->getNotificationURI())
            a.add(s);
#if FEAT_RCN_9
    }
    if (modifiedAttributes.empty() || psnB)
    {
#endif //FEAT_RCN_9
        if (sub->getPreSubscriptionNotify() != -1)
        {
            jsonObj[PRE_SUBSCRIPTION_NOTIFY] = sub->getPreSubscriptionNotify();
        }
#if FEAT_RCN_9
    }
    if (modifiedAttributes.empty() || pnB)
    {
#endif //FEAT_RCN_9

        jsonObj[PENDING_NOTIFICATION] = sub->getPendingNotification();
#if FEAT_RCN_9
    }
    if (modifiedAttributes.empty() || nspB)
    {
#endif //FEAT_RCN_9
        if (sub->getNotificationStoragePriority() != -1)
        {
            jsonObj[NOTIFICATION_STORAGE_PRIORITY] = sub->getNotificationStoragePriority();
        }
#if FEAT_RCN_9
    }
    if (modifiedAttributes.empty() || lnB)
    {
#endif //FEAT_RCN_9
        jsonObj[LATEST_NOTIFY] = sub->getLatestNotify();
#if FEAT_RCN_9
    }
    if (modifiedAttributes.empty() || nctB)
    {
#endif //FEAT_RCN_9
        jsonObj[NOTIFICATION_CONTENT_TYPE] = sub->getNotificationContentType();
#if FEAT_RCN_9
    }
    if (modifiedAttributes.empty() || necB)
    {
#endif //FEAT_RCN_9
        if (!sub->getNotificationEventCat().isEmpty())
        {
            jsonObj[NOTIFICATION_EVENT_CAT] = sub->getNotificationEventCat();
        }
#if FEAT_RCN_9
    }
#endif //FEAT_RCN_9 \
       // not implemented: schedule
}
#endif // SUBSCRIPTION_FEAT

RemoteCse*
RemoteCseMapper::parseResource(JsonObject o, const String& name, Entity* targetEntity, Entity* toUpdate)
{
    RemoteCse* csr;
    if (toUpdate)
    {
        csr = toUpdate->getRemoteCse();
    }
    else
    {
        Serial.printf("TRACE: creating new CSR\n");
        csr = Entity::addRemoteCse(name, targetEntity->getCseBase());
    }
    if (o.containsKey(SN_CSE_TYPE))
    {
        csr->setCseType((int)o[SN_CSE_TYPE]);
    }
    if (o.containsKey(REMOTE_CSE_CSEBASE))
    {
        csr->setCseBase(o[REMOTE_CSE_CSEBASE]);
    }
    if (o.containsKey(SN_CSE_ID))
    {
        csr->setCseID(o[SN_CSE_ID]);
    }
    if (o.containsKey(REQUEST_REACHABILITY))
    {
        const String& s_rr = o[REQUEST_REACHABILITY];
        csr->setRequestReachability(s_rr.equalsIgnoreCase("true"));
#if FEAT_RCN_9
        modifiedAttributes.push_back(REQUEST_REACHABILITY);
#endif //FEAT_RCN_9
    }
    if (o.containsKey(POA))
    {
        JsonArray a = o[POA];
        csr->poas.clear();
        for (String s : a)
        {
            csr->poas.push_back(s);
        }
#if FEAT_RCN_9
        modifiedAttributes.push_back(POA);
#endif //FEAT_RCN_9
    }
    if (o.containsKey(CONTENT_SERIALIZATION))
    {
        JsonArray a = o[CONTENT_SERIALIZATION];
        csr->contentSerializationTypes.clear();
        for (String s : a)
        {
            csr->contentSerializationTypes.push_back(s);
        }
#if FEAT_RCN_9
        modifiedAttributes.push_back(CONTENT_SERIALIZATION);
#endif //FEAT_RCN_9
    }
    if (o.containsKey(SN_DESCENDANT_CSE))
    {
        descendantCsesToUpdate = true;
        JsonArray a = o[SN_DESCENDANT_CSE];
        csr->descendantCses.clear();
        for (String s : a)
        {
            csr->descendantCses.push_back(s);
        }
#if FEAT_RCN_9
        modifiedAttributes.push_back(SN_DESCENDANT_CSE);
#endif //FEAT_RCN_9
    }
    if (o.containsKey(SUPPORTED_REL_VERSIONS))
    {
        JsonArray a = o[SUPPORTED_REL_VERSIONS];
        csr->supportedReleaseVersions.clear();
        for (String s : a)
        {
            csr->supportedReleaseVersions.push_back(s);
        }
#if FEAT_RCN_9
        modifiedAttributes.push_back(SUPPORTED_REL_VERSIONS);
#endif //FEAT_RCN_9
    }
    return csr;
}

#if SUBSCRIPTION_FEAT
Subscription*
SubscriptionMapper::parseResource(JsonObject o, const String& name, Entity* targetEntity, Entity* toUpdate)
{
    Subscription* sub = nullptr;
    if (toUpdate)
    {
        sub = toUpdate->getSubscription();
    }
    else
    {
        sub = Entity::addSubscription(name, targetEntity);
    }
    if (o.containsKey(EXPIRATION_COUNTER))
    {
        sub->setExpirationCounter((int)o[EXPIRATION_COUNTER]);
#if FEAT_RCN_9
        modifiedAttributes.push_back(EXPIRATION_COUNTER);
#endif
    }
    if (o.containsKey(NOTIFICATION_URI))
    {
        JsonArray a = o[NOTIFICATION_URI];
        sub->notificationURI.clear();
        for (String s : a)
        {
#if DEBUG
            printf("DEBUG adding uri to SUB: %s\n", s.c_str());
#endif
            sub->notificationURI.push_back(s);
        }
#if FEAT_RCN_9
        modifiedAttributes.push_back(NOTIFICATION_URI);
#endif
    }
    if (o.containsKey(GROUP_ID))
    {
// missing implem: GROUP ID SUB
#if FEAT_RCN_9
        modifiedAttributes.push_back(GROUP_ID);
#endif
    }
    if (o.containsKey(NOTIFICATION_FORWARDING_URI))
    {
// missing implem: NOTIFICATION FWURI SUB
#if FEAT_RCN_9
        modifiedAttributes.push_back(NOTIFICATION_FORWARDING_URI);
#endif
    }
    if (o.containsKey(BATCH_NOTIFY))
    {
// missing implem: BATCH NOTIFY
#if FEAT_RCN_9
        modifiedAttributes.push_back(BATCH_NOTIFY);
#endif
    }
    if (o.containsKey(RATE_LIMIT))
    {
// missing implem: RATE LIMIT
#if FEAT_RCN_9
        modifiedAttributes.push_back(RATE_LIMIT);
#endif
    }
    if (o.containsKey(PRE_SUBSCRIPTION_NOTIFY))
    {
        sub->setPreSubscriptionNotify((int)o[PRE_SUBSCRIPTION_NOTIFY]);
    }
    if (o.containsKey(NOTIFICATION_STORAGE_PRIORITY))
    {
        sub->setNotificationStoragePriority((int)o[NOTIFICATION_STORAGE_PRIORITY]);
#if FEAT_RCN_9
        modifiedAttributes.push_back(NOTIFICATION_STORAGE_PRIORITY);
#endif
    }
    if (o.containsKey(LATEST_NOTIFY))
    {
        const String& latestNotify = o[LATEST_NOTIFY];
        bool ln;
        ln = latestNotify.equalsIgnoreCase("true");
        sub->setLatestNotify(ln);
#if FEAT_RCN_9
        modifiedAttributes.push_back(LATEST_NOTIFY);
#endif
    }
    if (o.containsKey(NOTIFICATION_CONTENT_TYPE))
    {
#if DEBUG
        printf("DEBUG: setting NCT\n");
#endif
        sub->setNotificationContentType((int)o[NOTIFICATION_CONTENT_TYPE]);
#if FEAT_RCN_9
        modifiedAttributes.push_back(NOTIFICATION_CONTENT_TYPE);
#endif
    }
    if (o.containsKey(NOTIFICATION_EVENT_CAT))
    {
        sub->setNotificationEventCat(o[NOTIFICATION_EVENT_CAT]);
#if FEAT_RCN_9
        modifiedAttributes.push_back(NOTIFICATION_EVENT_CAT);
#endif
    }
    if (o.containsKey(CREATOR))
    {
        sub->setCreator(o[CREATOR]);
    }
    // missing implem: schedule
    return sub;
}
#endif // SUBSCRIPTION_FEAT

#if GROUP_FEAT
Group*
GroupMapper::parseResource(JsonObject o, const String& name, Entity* entity)
{
    Group* g = Entity::addGroup(name, entity);

    for (String id : (JsonArray)o[MEMBER_ID])
    {
        g->addMemberId(id);
    }
    // missing implem of some attributes

    // const String& entityList = o[MEMBER_ID];
    // int from = 0;
    // int index;
    // while ((unsigned int)from < entityList.length() && (index = entityList.indexOf(listSeparator, from)) >= 0)
    // {
    //     if (from != index)
    //     {
    //         const String& ei = entityList.substring(from, index);
    //         // TODO id should be checked however it could be a resource hosted on a remote CSE
    //         printf("\n\nDBEUG: adding member id %s\n", ei.c_str());
    //         g->addMemberId(ei);
    //         // Entity* e = Entity::getByIdentifier(ei);
    //         // if (!e)
    //         // {
    //         //     log("add in group '%s': '%s' not found\n", g->getName().c_str(), ei.c_str());
    //         // }
    //         // else
    //         // {
    //         //     //g->add(e);
    //         //     #if DEBUG
    //         //     printf("DEBUG: addind member in group %s\n", e->m_name.c_str());
    //         //     #endif
    //         // }
    //     }
    //     from = index + 1;
    // }
    return g;
}
#endif

int
Mapper::parseGenericAttributes(JsonObject jsonObj, Entity* e)
{
    String name;
    int type;
    if (jsonObj.containsKey(RESOURCE_NAME))
    {
        name = (const String&)jsonObj[RESOURCE_NAME];
        e->m_name = name;
    }
    if (jsonObj.containsKey(RESOURCE_TYPE))
    {
        type = (int)jsonObj[RESOURCE_TYPE];
    }
    if (jsonObj.containsKey(CREATION_TIME))
    {
        e->m_time_creation = jsonObj[CREATION_TIME];
    }
    if (jsonObj.containsKey(LAST_MODIFIED_TIME))
    {
        e->m_time_modification = jsonObj[LAST_MODIFIED_TIME];
    }
    if (jsonObj.containsKey(EXPIRATION_TIME))
    {
        e->setExpirationTime(jsonObj[EXPIRATION_TIME]);
#if FEAT_RCN_9
        modifiedAttributes.push_back(EXPIRATION_TIME);
#endif
    }
    if (jsonObj.containsKey(RESOURCE_ID))
    {
        String id = (const String&)jsonObj[RESOURCE_ID];
        id.replace(S_slash + CSE_ID + S_slash, emptyString);
        // e->m_resourceId = String(id);
        e->setResourceID(id);
    }
    if (jsonObj.containsKey(PARENT_ID))
    {
        e->m_parentID = (const String&)jsonObj[PARENT_ID];
    }
    if (jsonObj.containsKey(LABELS))
    {
        e->m_labels.clear();
        JsonArray a = jsonObj[LABELS];
        for (String s : a)
        {
            e->m_labels.push_back(s);
        }
#if FEAT_RCN_9
        modifiedAttributes.push_back(LABELS);
#endif
    }
    if (jsonObj.containsKey(ACP_IDS))
    {
        e->m_acps.clear();
        for (String s : (JsonArray)jsonObj[ACP_IDS])
        {
            Serial.printf("DEBUG: TRYING TO GET ACP %s\n", s.c_str());
            Entity* acp = Entity::getByIdentifier(s);
            if (acp && acp->getAcp())
            {
                e->m_acps.push_back(acp->getAcp());
            }
            else
            {
                Serial.printf("DEBUG: ACP NoT FOUND \n");
                return -1;
            }
        }
#if FEAT_RCN_9
        modifiedAttributes.push_back(ACP_IDS);
#endif
    }
    return 0;
}

int
PrimitiveMapper::mapResponsePrimitive(ResponsePrimitive& resp, JsonObject o)
{
    int r = 1;
    if (resp.getResponseStatusCode())
    {
        o[RESPONSE_STATUS_CODE] = String(resp.getResponseStatusCode());
    }
    if (resp.getRequestId() && !resp.getRequestId().isEmpty())
    {
        o[REQUEST_ID] = resp.getRequestId();
    }
    if (resp.getTo() && !resp.getTo().isEmpty())
    {
        o[TO] = resp.getTo();
    }
    if (resp.getFrom() && !resp.getFrom().isEmpty())
    {
        o[FROM] = resp.getFrom();
    }
    if (resp.getContent() && !resp.getContent().isEmpty())
    {
        o[PRIMITIVE_CONTENT] = resp.getContent();
    }
    if (resp.getOriginatingTimetsamp() && !resp.getOriginatingTimetsamp())
    {
        o[ORIGINATING_TIMESTAMP] = resp.getOriginatingTimetsamp();
    }
    if (resp.getResultExpirationTimestamp() && !resp.getResultExpirationTimestamp())
    {
        o[RESULT_EXPIRATION_TIMESTAMP] = resp.getResultExpirationTimestamp();
    }

    // if (resp.getEventCategory())
    // {
    //     o[EVENT_CATEGORY] = resp.getEventCategory();
    // }
    // TODO TOKEN REQ INFO
    // TODO ASSIGNED TOKEN IDs
    if (resp.getContentStatus() != CS_NULL)
    {
        o[CONTENT_STATUS] = resp.getContentStatus();
    }
    if (resp.getContentOffset() != -1)
    {
        o[CONTENT_OFFSET] = resp.getContentOffset();
    }
    // TODO assigned token identifiers
    // TODO token request information
    // TODO authorization signature request information

    if (resp.getAuthorizationSignatureRequestInformationInt() != -1)
    {
        o[AUTHORIZATION_SIGNATURE_REQUEST_INFO] = resp.getAuthorizationSignatureRequestInformation();
    }
    if (resp.getReleaseVersionIndicator() && !resp.getReleaseVersionIndicator().isEmpty())
    {
        o[RELEASE_VERSION_INDICATOR] = resp.getReleaseVersionIndicator();
    }

    if (resp.getVendorInformation() && !resp.getVendorInformation().isEmpty())
    {
        o[VENDOR_INFO] = resp.getVendorInformation();
    }
    r = 0;
    return r;
}

int
PrimitiveMapper::mapRequestPrimitive(RequestPrimitive& req, JsonObject o)
{
    int r = 1;
    o[OPERATION] = req.getOperation();
    o[TO] = req.getTo();
    o[FROM] = req.getFrom();
    o[REQUEST_IDENTIFIER] = req.getRequestId();
    if (req.getResourceType() != -1)
    {
        o[RESOURCE_TYPE] = req.getResourceType();
    }
    if (req.getContent() && !req.getContent().isEmpty())
    {
        o[CONTENT] = req.getContent();
    }
    // TODO roles IDS
    if (req.getOriginatingTimestamp() && !req.getOriginatingTimestamp().isEmpty())
    {
        o[ORIGINATING_TIMESTAMP] = req.getOriginatingTimestamp();
    }
    // TODO EXPIRATION TIMESTAMP
    // TODO o[RESULT_EXPIRATION_TIMESTAMP]
    // TODO o[OPERATION_EXECUTION_TIME]
    // TODO o[RESPONSE_TYPE] = req.getResponseType();
    // TODO RESULT PERSISTENCE
    // TODO o[RESULT_CONTENT] = req.getResultContent();
    // TODO EVENT_CATEGORY
    if (req.getDeliveryAggregationInt() != -1)
    {
        if (req.getDeliveryAggregation())
        {
            o[DELIVERY_AGGREGATION] = F("true");
        }
        else
        {
            o[DELIVERY_AGGREGATION] = F("false");
        }
    }
    if (req.getGroupRequestIdentifier() && !req.getGroupRequestIdentifier().isEmpty())
    {
        o[GROUP_REQUEST_IDENTIFIER] = req.getGroupRequestIdentifier();
    }

    // TODO FILTER CRITERIA
    if (req.getFilterCriteria().getLimit() != -1)
    {
        JsonObject fc = o[FILTER_CRITERIA].to<JsonObject>();
        fc[LIMIT] = req.getFilterCriteria().getLimit();
    }
    if (req.getFilterCriteria().getLevel() != -1)
    {
        JsonObject fc = o[FILTER_CRITERIA].to<JsonObject>();
        fc[LEVEL] = req.getFilterCriteria().getLevel();
    }
    if (!req.getFilterCriteria().getLabels().empty())
    {
        JsonObject fc = o[FILTER_CRITERIA].to<JsonObject>();
        JsonArray a;
        a = fc[LABELS].to<JsonArray>();
        for (String label : req.getFilterCriteria().getLabels())
        {
            a.add(label);
        }
    }

    // TODO DESIRED IDENTIFIER RESULT TYPE
    // TODO TOKENS
    // TODO TOKENS IDS
    // TODO TOKEN REQUEST INDICATOR
    // TODO LOCAL TOKEN IDS
    // TODO GROUP REQUEST TARGET MEMBERS
    if (req.getAuthorizationSignatureIndicatorInt() != -1)
    {
        if (req.getAuthorizationSignatureIndicator())
        {
            o[AUTHORIZATION_SIGNATURE_INDICATOR] = F("true");
        }
        else
        {
            o[AUTHORIZATION_SIGNATURE_INDICATOR] = F("false");
        }
    }
    // TODO AUTHORIZATION SIGNATURE

    if (req.getAuthorizationRelationshipIndicatorInt() != -1)
    {
        if (req.getAuthorizationRelationshipIndicator())
        {
            o[AUTHORIZATION_RELATIONSHIP_INDICATOR] = F("true");
        }
        else
        {
            o[AUTHORIZATION_RELATIONSHIP_INDICATOR] = F("false");
        }
    }
    // TODO SEMANTIC QUERY INDICATOR
    if (req.getReleaseVersionIndicator() && !req.getReleaseVersionIndicator().isEmpty())
    {
        o[RELEASE_VERSION_INDICATOR] = req.getReleaseVersionIndicator();
    }
    if (req.getVendorInformation() && !req.getVendorInformation().isEmpty())
    {
        o[VENDOR_INFO] = req.getVendorInformation();
    }
    r = 0;
    return r;
}

int
PrimitiveMapper::parseRequestPrimitive(JsonObject o, RequestPrimitive* req)
{
#if TARGET_UNIX
    try
    {
#endif
        if (o.containsKey(OPERATION))
        {
            req->setOperation((int)o[OPERATION]);
        }
        if (o.containsKey(TO))
        {
            Serial.println("SETTING TO IN REQ");
            req->setTo((const String&)o[TO]);
        }
        if (o.containsKey(FROM))
        {
            req->setFrom((const String&)o[FROM]);
        }
        if (o.containsKey(REQUEST_IDENTIFIER))
        {
            req->setRequestId((const String&)o[REQUEST_IDENTIFIER]);
        }
        if (o.containsKey(RESOURCE_TYPE))
        {
            req->setResourceType((int)o[RESOURCE_TYPE]);
        }
        if (o.containsKey(PRIMITIVE_CONTENT))
        {
            req->setContent((const String&)o[PRIMITIVE_CONTENT]);
        }
        if (o.containsKey(ORIGINATING_TIMESTAMP))
        {
            req->setOriginatingTimestamp((const String&)o[ORIGINATING_TIMESTAMP]);
        }
        // TODO RESULT CONTENT
        if (o.containsKey(DELIVERY_AGGREGATION))
        {
            const String& s = (const String&)o[DELIVERY_AGGREGATION];
            bool b = s.equalsIgnoreCase(F("true"));
            req->setDeliveryAggregation(b);
        }
        if (o.containsKey(AUTHORIZATION_SIGNATURE_INDICATOR))
        {
            const String& s = (const String&)o[AUTHORIZATION_SIGNATURE_INDICATOR];
            bool b = s.equalsIgnoreCase(F("true"));
            req->setAuthorizationSignatureIndicator(b);
        }
        if (o.containsKey(AUTHORIZATION_RELATIONSHIP_INDICATOR))
        {
            const String& s = (const String&)o[AUTHORIZATION_RELATIONSHIP_INDICATOR];
            bool b = s.equalsIgnoreCase(F("true"));
            req->setAuthorizationRelationshipIndicator(b);
        }
        if (o.containsKey(SEMANTIC_QUERY_INDICATOR))
        {
            const String& s = (const String&)o[SEMANTIC_QUERY_INDICATOR];
            bool b = s.equalsIgnoreCase(F("true"));
            req->setSemanticQueryIndicator(b);
        }
        if (o.containsKey(FILTER_CRITERIA))
        {
            FilterCriteria fc;
            JsonObject f = (JsonObject)o[FILTER_CRITERIA];

            if (f.containsKey(CREATED_BEFORE))
            {
                fc.unsupportedFilterOn();
            }
            if (f.containsKey(CREATED_AFTER))
            {
                fc.unsupportedFilterOn();
            }
            if (f.containsKey(MODIFIED_SINCE))
            {
                fc.unsupportedFilterOn();
            }
            if (f.containsKey(UNMODIFIED_SINCE))
            {
                fc.unsupportedFilterOn();
            }
            if (f.containsKey(STATETAG_SMALLER))
            {
                if ((int)f[STATETAG_SMALLER] <= 0)
                {
                    return 2;
                }
                fc.unsupportedFilterOn();
            }
            if (f.containsKey(STATETAG_BIGGER))
            {
                if ((int)f[STATETAG_BIGGER] < 0)
                {
                    return 2;
                }
                fc.unsupportedFilterOn();
            }
            if (f.containsKey(EXPIRE_BEFORE))
            {
                fc.unsupportedFilterOn();
            }
            if (f.containsKey(EXPIRE_AFTER))
            {
                fc.unsupportedFilterOn();
            }

            if (f.containsKey(LABELS))
            {
                for (String label : (JsonArray)f[LABELS])
                {
                    fc.getLabels().push_back(label);
                }
            }
            if (f.containsKey(RESOURCE_TYPE))
            {
                for (int ty : (JsonArray)f[RESOURCE_TYPE])
                {
                    fc.addResourceType(ty);
                }
            }
            if (f.containsKey(SIZE_ABOVE))
            {
                if ((int)f[SIZE_ABOVE] < 0)
                {
                    return 2;
                }
                fc.unsupportedFilterOn();
            }
            if (f.containsKey(SIZE_BELOW))
            {
                if ((int)f[SIZE_BELOW] <= 0)
                {
                    return 2;
                }
                fc.unsupportedFilterOn();
            }
            if (f.containsKey(CONTENT_TYPE))
            {
                fc.unsupportedFilterOn();
            }
            if (f.containsKey(LIMIT))
            {
                if ((int)f[LIMIT] <= 0)
                {
                    return 2;
                }
                fc.setLimit((int)f[LIMIT]);
            }
            if (f.containsKey(ATTRIBUTE))
            {
                fc.unsupportedFilterOn();
            }
            if (f.containsKey(SEMANTICS_FILTER))
            {
                fc.unsupportedFilterOn();
            }
            if (f.containsKey(FILTER_OPERATION))
            {
                fc.unsupportedFilterOn();
            }
            if (f.containsKey(CONTENT_FILTER_SYNTAX))
            {
                fc.unsupportedFilterOn();
            }
            if (f.containsKey(CONTENT_FILTER_QUERY))
            {
                fc.unsupportedFilterOn();
            }
            if (f.containsKey(LEVEL))
            {
                if ((int)f[LEVEL] <= 0)
                {
                    return 2;
                }
                fc.setLevel((int)f[LEVEL]);
            }
            if (f.containsKey(OFFSET))
            {
                if ((int)f[OFFSET] <= 0)
                {
                    return 2;
                }
                fc.unsupportedFilterOn();
            }
            // TODO ADD OTHER ATTRIBUTES TO HAVE UNSUPPORTED TAG LIFT
            req->setFilterCriteria(fc);
        }
#if TARGET_UNIX
    }
    catch (const std::exception& e)
    {
        Serial.println("ERROR occured while parsing request primitive.");
        Serial.println(e.what());
        return 1;
    }
#endif
    return 0;
}

int
PrimitiveMapper::parseResponsePrimitive(JsonObject o, ResponsePrimitive* resp)
{
#if TARGET_UNIX
    try
    {
#endif
        if (o.containsKey(RESPONSE_STATUS_CODE))
        {
            resp->setResponseStatusCodeString((const String&)o[RESPONSE_STATUS_CODE]);
        }
        if (o.containsKey(REQUEST_IDENTIFIER))
        {
            resp->setRequestId((const String&)o[REQUEST_IDENTIFIER]);
        }
        if (o.containsKey(PRIMITIVE_CONTENT))
        {
            resp->setContent((const String&)o[PRIMITIVE_CONTENT]);
        }
        if (o.containsKey(TO))
        {
            resp->setTo((const String&)o[TO]);
        }
        if (o.containsKey(FROM))
        {
            resp->setFrom((const String&)o[FROM]);
        }

        // TODO PARSE MISSING ATTRIBUTES

        if (o.containsKey(RELEASE_VERSION_INDICATOR))
        {
            resp->setReleaseVersionIndicator((const String&)o[RELEASE_VERSION_INDICATOR]);
        }
#if TARGET_UNIX
    }
    catch (const std::exception& e)
    {
        Serial.println("ERROR occured while parsing response primitive.");
        Serial.println(e.what());
        return 1;
    }
#endif
    return 0;
}
