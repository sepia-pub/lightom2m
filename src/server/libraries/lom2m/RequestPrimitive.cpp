/*
------------------------------------------------------------

SOFTWARE EVALUATION LICENSE

LAAS research laboratory, Toulouse, France.
IRIT computer science research laboratory, Toulouse, France.
------------------------------------------------------------

Contact: Thierry Monteil, thierry.monteil@irit.fr

Definitions
SOFTWARE:  The LightOM2M software, version 1.0, in source code form, written by David Gauchard, Guillaume Garzone and Thierry Monteil
at the LAAS research laboratory.

LICENSOR: LE CENTRE NATIONAL DE LA RECHERCHE SCIENTIFIQUE, a public scientific and technical research establishment, having SIREN No. 180 089 013, APE code 7219 Z, having its registered office at 3, rue Michel-Ange, 75796 Paris cedex 16, France, acting in its own name and on its own behalf, and on behalf of Le laboratoire d'analyse et d'architecture des systèmes (LAAS), UPR n°8001.

1. INTENT/PURPOSE
This agreement determines the conditions in which the LICENSOR, who has the rights to the SOFTWARE, grants to the LICENSEE a license for research and evaluation purposes only, excluding any commercial use.

2. LICENSEE
Any person or organization who receives the SOFTWARE with a copy of this license.

3. RIGHTS GRANTED
The rights to use and copy the SOFTWARE, subject to the restrictions described in this agreement.
The rights to modify and compile the SOFTWARE when it's provided in source code form, subject to the restrictions described in this agreement.
For the SOFTWARE provided in binary form only, LICENSEE undertakes to not decompile, disassemble, decrypt, extract the components or perform reverse engineering except to the extent expressly provided by law.

5. SCOPE OF THE LICENSE
- NON-COMMERCIAL license for research and evaluation purposes ONLY.
- NO right to commercialize the SOFTWARE, or any derivative work, without separate agreement with the LICENSOR.

6. MODIFICATION OF THE SOFTWARE
License permits LICENSEE to modify the SOFTWARE provided in source code form for research and evaluation purposes only.

7. REDISTRIBUTION
- License permits LICENSEE to redistribute verbatim copies of the SOFTWARE, accompanied with a copy of this license.
- License DOES NOT permit LICENSEE to redistribute modified versions of the SOFTWARE provided in source code form.
- License DOES NOT permit LICENSEE to commercialize the SOFTWARE or any derivative work of the SOFTWARE.

8. FEE/ROYALTY
- LICENSEE pays no royalty for this license.
- LICENSEE and any third parties must enter a new agreement for any use beyond scope of license. Please contact the IRIT technology transfer office (email numerique@toulouse-tech-transfer.com) for further information.

9. NO WARRANTY
The SOFTWARE is provided "as is" without warranty of any kind, either expressed or implied, including, but not limited to, the implied warranties of merchantability and fitness for a particular purpose. The entire risk as to the quality and performance of the program is with the LICENSEE.

10. NO LIABILITY
In no event unless required by applicable law or agreed to in writing will any copyright owner be liable to LICENSEE for damages, including any general, special, incidental or consequential damages arising out of the use or inability to use the program (including but not limited to loss of data or data being rendered inaccurate or losses sustained by LICENSEE or third parties or a failure of the program to operate with other programs), even if such holder has been advised of the possibility of such damages.
*/


//#include <sys/time.h>

//#include <ESP8266WiFi.h>
//#include <WiFiClient.h>
//#include <WiFiServer.h>

#include "RequestPrimitive.h"
#include "ShortNames.h"
#include "JsonDatamapper.h"
#include "AccessControlController.h"

#ifndef DEBUG
#define DEBUG 1
#endif
#define REL_1 0

////////////////

void RequestPrimitive::init()
{
    m_operation = OP_NULL;
    m_to = emptyString;
    m_from = emptyString;
    m_ri = emptyString;

    m_url = emptyString;
    m_contentType = emptyString;
    m_resourceType = TY_NONE;
    m_rcn = RCN_NULL;
    qs_ty = -1;
    qs_fu = false;
    qs_lbl.clear();
    qs_lvl = 1;
    m_authorizationRelationshipIndicator = -1;
    m_deliveryAggregation = -1;
    m_authorizationSignatureIndicator = -1;
}

void RequestPrimitive::setRequestContentType()
{
    m_contentType = String(F("application/json")) + (m_resourceType == TY_NONE ? emptyString : String(F(";ty=")) + String((int)getResourceType(), DEC));
}

void RequestPrimitive::setRequestContentTypeRaw(const String& contentType)
{
    m_contentType = contentType;
}

void RequestPrimitive::setResultContentType(const char* rcn)
{
    int rcnValue = atoi(rcn);
    if (rcnValue == 0)
    {
        m_rcn = RCN_NOTHING;
    }
    else if (rcnValue == 1)
    {
        m_rcn = RCN_ATTR;
    }
    else if (rcnValue == 2)
    {
        m_rcn = RCN_HIER_ADDR;
    }
    else if (rcnValue == 3)
    {
        m_rcn = RCN_HIER_ADDR_ATTR;
    }
    else if (rcnValue == 4)
    {
        m_rcn = RCN_ATTR_CHRES;
    }
    else if (rcnValue == 5)
    {
        m_rcn = RCN_ATTR_CHREF;
    }
    else if (rcnValue == 6)
    {
        m_rcn = RCN_CHREF;
    }
    else if (rcnValue == 7)
    {
        m_rcn = RCN_ORIGINAL_RES;
    }
    else if (rcnValue == 8)
    {
        m_rcn = RCN_CHRES;
    }
    else if (rcnValue == 9)
    {
        m_rcn = RCN_MODIF_ATTR;
    }
    else if (rcnValue == 10)
    {
        m_rcn = RCN_SEM_CONTENT;
    }
    else
    {
        #if DEBUG
        printf("DEBUG: default RCN\n");
        #endif
        m_rcn = RCN_ATTR;
    }
}

void RequestPrimitive::setURL(const String& resourceName, const String& resourceDataName)
{
    int ty = getResourceType();
    m_url = F("~/");
    m_url += CSE_ID;

    switch (ty)
    {
    case TY3_CONTAINER:
    case TY4_CONTENT_INSTANCE:
        m_url += S_slash;
        m_url += CSE_NAME;
        break;
    default:;
    }

    if (resourceName.length())
    {
        m_url += S_slash;
        m_url += resourceName;

        if (resourceDataName.length())
        {
            m_url += S_slash;
            m_url += resourceDataName;
        }
    }
}

/////////////////////////////////////////////////

RequestPrimitive::RequestPrimitive()
{
    m_encoding = ENC_JSON;
}

void RequestPrimitive::createEntity()
{
    init();
    setResourceType(TY2_APPL_ENTITY);
    setURL();
    jsonglobal[RESOURCE_NAME] = resourceName;
    jsonglobal[APP_ID] = api;
    jsonglobal[REQUEST_REACHABILITY] = false;
    JsonArray a = jsonglobal[LABELS].to<JsonArray>();
    a.add(String(F("Type/")) + type);
    a.add(String(F("Category/")) + category);
    a.add(String(F("Location/")) + location);
}

void RequestPrimitive::createContainer(const String& name)
{
    init();
    setResourceType(TY3_CONTAINER);
    setURL(resourceName);
    jsonglobal[RESOURCE_NAME] = name;
}

void RequestPrimitive::addDescriptionInstance()
{
    init();
    setResourceType(TY4_CONTENT_INSTANCE);
    setURL(resourceName, descriptor);
    jsonglobal[CONTENT] = resourceName;
}

void RequestPrimitive::addDataInstance(const String& value)
{
    init();
    setResourceType(TY4_CONTENT_INSTANCE);
    setURL(resourceName, resourceDataName);
    jsonglobal[CONTENT] = value;
}

// TODO to remove
void RequestPrimitive::createCSEBase()
{
    init();
    CseBase* csb = CseBase::getInstance();
    setResourceType(csb->getType());
    setURL();

    JsonArray a;


    jsonglobal[RESOURCE_NAME] = csb->getName();
    jsonglobal[RESOURCE_ID] = csb->getResourceIdentifier();
    jsonglobal[SN_CSE_TYPE] = CSE_TYPE; // 1:IN-CSE 2:MN-CSE 3:ASN-CSE
    jsonglobal[SN_CSE_ID] = CSE_ID;
    jsonglobal[CONTENT_SERIALIZATION] = F("application/json");
    jsonglobal[SUPPORTED_REL_VERSIONS] = 3;

    a = jsonglobal[ACP_IDS].to<JsonArray>();
    a.add(F("[TODO_WHEN_ACP_IMPLEMENTED]")); // TODO update when ACP supported

    a = jsonglobal[SRT].to<JsonArray>();
    for (int i : SUPPORTED_RESTYPE)
        a.add(i);

    a = jsonglobal[POA].to<JsonArray>();
    a.add(String(F("http://<ip>:")) + PORT);
}

void RequestPrimitive::initReport()
{
    jsonglobal.clear();
}

void RequestPrimitive::createReport(Entity *e, ResultContentType rcn, int lvl, Operation op, bool notify)
{
    initReport();
    if (rcn == RCN_NOTHING)
    {
        setResourceType(TY_NONE);
        return;
    }
    else if (rcn == RCN_HIER_ADDR || rcn == RCN_HIER_ADDR_ATTR)
    {
        // TODO check the short name for resource wrapper
        JsonObject json2 = jsonglobal["m2m:re"].to<JsonObject>();
        createReportLvl(json2, e, rcn, lvl, op);
    }
    else
    {
        JsonObject jsonglobal2 ;
        if (notify)
        {
            JsonObject o = jsonglobal[REPRESENTATION].to<JsonObject>();
            jsonglobal2 = o[rqType(e->getType())].to<JsonObject>();
        }
        else
        {
            jsonglobal2 = jsonglobal[rqType(e->getType())].to<JsonObject>();
        }
        createReportLvl(jsonglobal2, e, rcn, lvl, op);
    }
}

void RequestPrimitive::createReportLvl(JsonObject rootElement, Entity* e, ResultContentType rcn, int lvl, Operation op)
{
    #if !REQ_LVL_FEAT
    lvl = 0;
    #endif
    if (op == OP_RETRIEVE)
    {
        if (!checkAccessRights(e, FROM_ORIGINATOR, op))
        {
            return;
        }
    }
    if (rcn == RCN_NOTHING)
    {
        setResourceType(TY_NONE);
        return;
    }
    else
    {
        setResourceType(e->getType());
    }
    JsonObject jsonglobal2 = rootElement;

    #if FEAT_RCN_9
    if (rcn == RCN_MODIF_ATTR)
    {
        Mapper::mapGenericModifiedAttributes(jsonglobal2, e);
        Mapper::mapResourceAttributes(jsonglobal2, e, lvl, op);
    }
    else
    {
        modifiedAttributes.clear();
    }
    #endif
    
    if (rcn == RCN_CHREF || rcn == RCN_ATTR_CHREF)
    {
        Mapper::mapChildResourcesRef(jsonglobal2, e, lvl, op);
    }
    else if (rcn == RCN_CHRES || rcn == RCN_ATTR_CHRES)
    {
        #if DEBUG
        printf("DEBUG: MAPPING CHILD RESOURCE OF %s, type %d\n", e->getName().c_str(), e->getType());
        #endif
        Mapper::mapChildResources(jsonglobal2, e, lvl, op);
    }
    if (rcn == RCN_ATTR_CHRES || rcn == RCN_ATTR || rcn == RCN_ATTR_CHREF || rcn == RCN_NULL)
    {
        #if DEBUG
        printf("DEBUG: GET RESOURCE %s, type %d\n", e->getName().c_str(), e->getType());
        #endif
        Mapper::mapResourceAttributes(jsonglobal2, e, lvl, op);
        Mapper::mapGenericAttributes(jsonglobal2, e, lvl, op);
    } 
    if (rcn == RCN_HIER_ADDR || rcn == RCN_HIER_ADDR_ATTR)
    {
        jsonglobal2["m2m:uri"] = e->getFullName();
    }
    if (rcn == RCN_HIER_ADDR_ATTR)
    {
        JsonObject o = jsonglobal2[rqType(e->getType())].to<JsonObject>();
        Mapper::mapResourceAttributes(o, e, lvl, op);
        Mapper::mapGenericAttributes(o, e, lvl, op);
    }

}

#if SUBSCRIPTION_FEAT
void RequestPrimitive::createNotifyReport(Notification* n, NotificationContentType nct)
{
    initReport();
    setResourceType(TY_NOTIFICATION);
    JsonObject jsonglobal2 = jsonglobal[rqType(TY_NOTIFICATION)].to<JsonObject>();

    jsonglobal2[NOTIFICATION_EVENT] = "TODO SUB RESOURCE";

    jsonglobal2[REPRESENTATION] = "TODO SUB RESOURCE";

    jsonglobal2[NOTIFICATION_EVENT_TYPE] = n->getNet();
    if (n->getCreator().length())
        jsonglobal2[CREATOR] = n->getCreator();

    jsonglobal2[VERIFICATION_REQUEST] = n->getVerificationRequest();
    jsonglobal2[SUBSCRIPTION_DELETION] = n->getSubDeletion();
    jsonglobal2[SUBSCRIBER_URI] = n->getSubReference();
}

String RequestPrimitive::createNotifyBody(Notification* notif, NotificationContentType nct)
{
    String result;
    initReport();
    JsonObject sgn = jsonglobal[rqType(TY_NOTIFICATION)].to<JsonObject>();
    //jsonglobal.remove(rqType(notif->getResource()->getType()));
    if (!notif->getVerificationRequest())
    {
        JsonObject nev = sgn[NOTIFICATION_EVENT].to<JsonObject>();
        jsonTemp.clear();
        deserializeJson(jsonTemp, notif->getResource().c_str());
        nev[REPRESENTATION] = jsonTemp[REPRESENTATION];
        if (nct == NCT_ALL_ATTRIBUTES || nct == NCT_MODIFIED_ATTRIBUTES)
        {
            nev[REQUEST_STATUS] = 1; //TODO TO CHECK
        }
    }
    if (notif->getNet())
    {
        sgn[NOTIFICATION_EVENT_TYPE] = String(notif->getNet());
    }
    sgn[SUBSCRIPTION_DELETION] = notif->getSubDeletion();
    sgn[SUBSCRIBER_URI] = notif->getSubReference();
    sgn[VERIFICATION_REQUEST] = notif->getVerificationRequest();

    buildRequest(ENC_JSON, result);
    // TODO to improve
    return result;
}
#endif

ResponseStatusCode RequestPrimitive::createURIList(int ty, String& lbl)
{
    #if DEBUG
    printf("DEBUG: CREATE URI LIST.");
    #endif
    init();
    setResourceType(TY_URIL);
    setURL();
    jsonglobal.clear();
    ResponseStatusCode returnCode = R2000_OK;

    JsonArray a;
    a = jsonglobal[rqType(TY_URIL)].to<JsonArray>();
    switch (ty)
    {
    case TY1_ACP:
    case TY2_APPL_ENTITY:
    case TY3_CONTAINER:
    case TY4_CONTENT_INSTANCE:
    case TY5_CSEBASE:
    case TY16_REMOTE_CSE:
    #if SUBSCRIPTION_FEAT
    case TY23_SUBSCRIPTION:
    #endif
    case TY9_GROUP:
        #if DEBUG
        printf("DEBUG: ty received\n");
        #endif
        for (auto& e : Entity::entities)
            if (e->m_type == ty && e->getName().length() > 0)
            {
                if (lbl != emptyString)
                {
                    for (String l : e->m_labels)
                    {
                        if (lbl.equals(l))
                        {
                            a.add(e->getFullName());
                            break;
                        }
                    }
                }
                else
                {
                    a.add(e->getFullName());
                }
            }
        break;
    case TY_NONE:
        #if DEBUG
        printf("DEBUG: ty none\n");
        #endif
        for (Entity* e : Entity::entities)
        {
            if (e->getName().length() <= 0)
            {
                continue;
            }
            if (lbl != emptyString)
            {
                for (String l : e->m_labels)
                {
                    if (lbl.equals(l))
                    {
                        a.add(e->getFullName());
                        break;
                    }
                }
            }
            else
            {
                a.add(e->getFullName());
            }
        }
        break;
    default:
        log("URIL: ty=%d not managed\n", ty);
    }
    return returnCode;
}

void RequestPrimitive::buildRequest(Encoding enc, String& req)
{
    setRequestContentType();
    req = emptyString;
    serializeJson(jsonglobal, req);
}
