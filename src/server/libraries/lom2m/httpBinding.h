/*
------------------------------------------------------------

SOFTWARE EVALUATION LICENSE

LAAS research laboratory, Toulouse, France.
IRIT computer science research laboratory, Toulouse, France.
------------------------------------------------------------

Contact: Thierry Monteil, thierry.monteil@irit.fr

Definitions
SOFTWARE:  The LightOM2M software, version 1.0, in source code form, written by David Gauchard, Guillaume Garzone and Thierry Monteil
at the LAAS research laboratory.

LICENSOR: LE CENTRE NATIONAL DE LA RECHERCHE SCIENTIFIQUE, a public scientific and technical research establishment, having SIREN No. 180 089 013, APE code 7219 Z, having its registered office at 3, rue Michel-Ange, 75796 Paris cedex 16, France, acting in its own name and on its own behalf, and on behalf of Le laboratoire d'analyse et d'architecture des systèmes (LAAS), UPR n°8001.

1. INTENT/PURPOSE
This agreement determines the conditions in which the LICENSOR, who has the rights to the SOFTWARE, grants to the LICENSEE a license for research and evaluation purposes only, excluding any commercial use.

2. LICENSEE
Any person or organization who receives the SOFTWARE with a copy of this license.

3. RIGHTS GRANTED
The rights to use and copy the SOFTWARE, subject to the restrictions described in this agreement.
The rights to modify and compile the SOFTWARE when it's provided in source code form, subject to the restrictions described in this agreement.
For the SOFTWARE provided in binary form only, LICENSEE undertakes to not decompile, disassemble, decrypt, extract the components or perform reverse engineering except to the extent expressly provided by law.

5. SCOPE OF THE LICENSE
- NON-COMMERCIAL license for research and evaluation purposes ONLY.
- NO right to commercialize the SOFTWARE, or any derivative work, without separate agreement with the LICENSOR.

6. MODIFICATION OF THE SOFTWARE
License permits LICENSEE to modify the SOFTWARE provided in source code form for research and evaluation purposes only.

7. REDISTRIBUTION
- License permits LICENSEE to redistribute verbatim copies of the SOFTWARE, accompanied with a copy of this license.
- License DOES NOT permit LICENSEE to redistribute modified versions of the SOFTWARE provided in source code form.
- License DOES NOT permit LICENSEE to commercialize the SOFTWARE or any derivative work of the SOFTWARE.

8. FEE/ROYALTY
- LICENSEE pays no royalty for this license.
- LICENSEE and any third parties must enter a new agreement for any use beyond scope of license. Please contact the IRIT technology transfer office (email numerique@toulouse-tech-transfer.com) for further information.

9. NO WARRANTY
The SOFTWARE is provided "as is" without warranty of any kind, either expressed or implied, including, but not limited to, the implied warranties of merchantability and fitness for a particular purpose. The entire risk as to the quality and performance of the program is with the LICENSEE.

10. NO LIABILITY
In no event unless required by applicable law or agreed to in writing will any copyright owner be liable to LICENSEE for damages, including any general, special, incidental or consequential damages arising out of the use or inability to use the program (including but not limited to loss of data or data being rendered inaccurate or losses sustained by LICENSEE or third parties or a failure of the program to operate with other programs), even if such holder has been advised of the possibility of such damages.
*/


#ifndef __LOM2M_REST_HANDLER
#define __LOM2M_REST_HANDLER

#include "configuration.h"

#include "ResponsePrimitive.h"
#include "RequestPrimitive.h"

class HTTPBinding
{
public:
    static void sendRequest(RequestPrimitive* requestPrimitive, ResponsePrimitive* responsePrimitive);
    static int initRequestPrimitive(RequestPrimitive* requestPrimitive, const String& uri);
    static void sendResponse(const ResponsePrimitive& responsePrimitive);
    static void serveOM2M(const String& uri);
    static ResponseStatusCode getRSCFromHTTP(int httpResponseCode)
    {
        switch (httpResponseCode)
        {
        case H200_OK: case H204_NoContent:
            return R2000_OK;
        case H201_Created:
            return R2001_CREATED;
        case H400_BadRequest:
            return R4000_BAD_REQUEST;
        case H401_Unauthorized: case H403_Forbidden:
            return R4103_ORIGINATOR_HAS_NO_PRIVILEGE;
        case H404_NotFound:
            return R4004_NOT_FOUND;
        case H405_NotAllowed:
            return R4005_OPERATION_NOT_ALLOWED;
        case H406_NotAcceptable:
            return R5207_NOT_ACCEPTABLE;
        case H408_RequestTimeout:
            return R4008_REQUEST_TIMEOUT;
        case H409_Conflict:
            return R4105_CONFLICT;
        case H415_Unsupported:
            return R4015_UNSUPPORTED_MEDIA_TYPE;
        case H500_InternalError:
            return R5000_INTERNAL_SERVER_ERROR;
        case H501_NotImplemented:
            return R5001_NOT_IMPLEMENTED;
        case H503_Maintenance: 
        default:
            return R5103_TARGET_NOT_REACHABLE;
        }
    }
};

#endif
