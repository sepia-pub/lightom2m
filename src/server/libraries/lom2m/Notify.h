/*
------------------------------------------------------------

SOFTWARE EVALUATION LICENSE

LAAS research laboratory, Toulouse, France.
IRIT computer science research laboratory, Toulouse, France.
------------------------------------------------------------

Contact: Thierry Monteil, thierry.monteil@irit.fr

Definitions
SOFTWARE:  The LightOM2M software, version 1.0, in source code form, written by David Gauchard, Guillaume Garzone and Thierry Monteil
at the LAAS research laboratory.

LICENSOR: LE CENTRE NATIONAL DE LA RECHERCHE SCIENTIFIQUE, a public scientific and technical research establishment, having SIREN No. 180 089 013, APE code 7219 Z, having its registered office at 3, rue Michel-Ange, 75796 Paris cedex 16, France, acting in its own name and on its own behalf, and on behalf of Le laboratoire d'analyse et d'architecture des systèmes (LAAS), UPR n°8001.

1. INTENT/PURPOSE
This agreement determines the conditions in which the LICENSOR, who has the rights to the SOFTWARE, grants to the LICENSEE a license for research and evaluation purposes only, excluding any commercial use.

2. LICENSEE
Any person or organization who receives the SOFTWARE with a copy of this license.

3. RIGHTS GRANTED
The rights to use and copy the SOFTWARE, subject to the restrictions described in this agreement.
The rights to modify and compile the SOFTWARE when it's provided in source code form, subject to the restrictions described in this agreement.
For the SOFTWARE provided in binary form only, LICENSEE undertakes to not decompile, disassemble, decrypt, extract the components or perform reverse engineering except to the extent expressly provided by law.

5. SCOPE OF THE LICENSE
- NON-COMMERCIAL license for research and evaluation purposes ONLY.
- NO right to commercialize the SOFTWARE, or any derivative work, without separate agreement with the LICENSOR.

6. MODIFICATION OF THE SOFTWARE
License permits LICENSEE to modify the SOFTWARE provided in source code form for research and evaluation purposes only.

7. REDISTRIBUTION
- License permits LICENSEE to redistribute verbatim copies of the SOFTWARE, accompanied with a copy of this license.
- License DOES NOT permit LICENSEE to redistribute modified versions of the SOFTWARE provided in source code form.
- License DOES NOT permit LICENSEE to commercialize the SOFTWARE or any derivative work of the SOFTWARE.

8. FEE/ROYALTY
- LICENSEE pays no royalty for this license.
- LICENSEE and any third parties must enter a new agreement for any use beyond scope of license. Please contact the IRIT technology transfer office (email numerique@toulouse-tech-transfer.com) for further information.

9. NO WARRANTY
The SOFTWARE is provided "as is" without warranty of any kind, either expressed or implied, including, but not limited to, the implied warranties of merchantability and fitness for a particular purpose. The entire risk as to the quality and performance of the program is with the LICENSEE.

10. NO LIABILITY
In no event unless required by applicable law or agreed to in writing will any copyright owner be liable to LICENSEE for damages, including any general, special, incidental or consequential damages arising out of the use or inability to use the program (including but not limited to loss of data or data being rendered inaccurate or losses sustained by LICENSEE or third parties or a failure of the program to operate with other programs), even if such holder has been advised of the possibility of such damages.
*/


#ifndef __LOM2M_NOTIFY_H
#define __LOM2M_NOTIFY_H

#include "configuration.h"

#if SUBSCRIPTION_FEAT

#include "Entity.h"
#include "Enum.h"
#include "Arduino.h"

struct NotificationToSend;
struct Notification;

// NOTIFY BUFFER
//std::list<Notification> notificationBuffer;
//std::list<RequestPrimitive> NotifyBuffer;

class Notifier
{
public:
    Notifier() {};
    ~Notifier() {};
    static void notify(std::list<Subscription*> subs, Entity* entity, NotificationEventType net, Operation operation);
};

struct NotificationToSend
{
private:
    Notification* notif;
    int failed;
    String url;
    String contentType;
    NotificationContentType nct;


public:
    NotificationContentType getNct()
    {
        return this->nct;
    }
    void setNct(NotificationContentType nct)
    {
        this->nct = nct;
    }
    void increaseFailed()
    {
        failed++;
    }
    const String& getContentType()
    {
        return this->contentType;
    }
    void setContentType(const String& contentType)
    {
        this->contentType = contentType;
    }
    const String& getUrl()
    {
        return this->url;
    }
    void setUrl(const String& url)
    {
        this->url = url;
    }
    Notification* getNotif()
    {
        return this->notif;
    }
    void setNotif(Notification* notif)
    {
        this->notif = notif;
    }
    int getFailed()
    {
        return this->failed;
    }
    void setFailed(int failed)
    {
        this->failed = failed;
    }

    NotificationToSend()
    {
        failed = 0;
    }
};

// NOTIFY RESOURCE
struct Notification
{
    NotificationEventType net;
    bool verificationRequest;
    bool subDeletion;
    String subReference;
    String creator;
    String notitificationForwardingUri;
    // Notification Envent
    String resource;
    // Operation Monitor
    Operation operation;
    String Originator;

    Notification();

    NotificationEventType& getNet()
    {
        return this->net;
    }
    void setNet(NotificationEventType& net)
    {
        this->net = net;
    }

    bool getVerificationRequest()
    {
        return this->verificationRequest;
    }
    void setVerificationRequest(bool verificationRequest)
    {
        this->verificationRequest = verificationRequest;
    }

    bool getSubDeletion()
    {
        return this->subDeletion;
    }
    void setSubDeletion(bool subDeletion)
    {
        this->subDeletion = subDeletion;
    }

    String getSubReference()
    {
        return this->subReference;
    }
    void setSubReference(const String& subReference)
    {
        this->subReference = subReference;
    }

    const String& getCreator()
    {
        return this->creator;
    }
    void setCreator(const String& creator)
    {
        this->creator = creator;
    }

    const String& getNotitificationForwardingUri()
    {
        return this->notitificationForwardingUri;
    }
    void setNotitificationForwardingUri(const String& notitificationForwardingUri)
    {
        this->notitificationForwardingUri = notitificationForwardingUri;
    }

    String& getResource()
    {
        return this->resource;
    }
    void setResource(String& resource)
    {
        this->resource = resource;
    }

    Operation& getOperation()
    {
        return this->operation;
    }
    void setOperation(Operation& operation)
    {
        this->operation = operation;
    }

    const String& getOriginator()
    {
        return this->Originator;
    }
    void setOriginator(const String& Originator)
    {
        this->Originator = Originator;
    }
};

/**
 * Simple buffer to queue pending notifications to send
 */
class NotifyBuffer
{
    std::list<NotificationToSend*> notificationBuffer;
    static NotifyBuffer* bufferInstance;
public:
    NotifyBuffer();
    ~NotifyBuffer();

    static void addNotification(NotificationToSend* notification)
    {
        getInstance()->notificationBuffer.push_back(notification);
#if DEBUG
        printf("DEBUG: notification added from SUB %s\n", notification->getNotif()->getSubReference().c_str());
#endif
    }

    static bool isEmpty()
    {
        return getInstance()->notificationBuffer.empty();
    }

    static NotificationToSend* popNotification()
    {
        if (!getInstance()->notificationBuffer.empty())
        {
            NotificationToSend* result = getInstance()->notificationBuffer.front();
            getInstance()->notificationBuffer.remove(result);
            //getInstance()->notificationBuffer.pop_front();
            printf("DEBUG: notification buffer size: %d\n", getInstance()->notificationBuffer.size());
            return result;
        }
#if DEBUG
        printf("DEBUG: nothing to pop\n");
#endif
        return nullptr;
    }

    static void removeNotification()
    {
        getInstance()->notificationBuffer.pop_front();
    }

    static NotifyBuffer* getInstance() noexcept
    {
        if (!bufferInstance)
        {
            bufferInstance = new NotifyBuffer();
        }
        return bufferInstance;
    }
};

#endif // SUBSCRIPTION_FEAT
#endif // __LOM2M_NOTIFY_H
