/*
------------------------------------------------------------

SOFTWARE EVALUATION LICENSE

LAAS research laboratory, Toulouse, France.
IRIT computer science research laboratory, Toulouse, France.
------------------------------------------------------------

Contact: Thierry Monteil, thierry.monteil@irit.fr

Definitions
SOFTWARE:  The LightOM2M software, version 1.0, in source code form, written by David Gauchard, Guillaume Garzone and Thierry Monteil
at the LAAS research laboratory.

LICENSOR: LE CENTRE NATIONAL DE LA RECHERCHE SCIENTIFIQUE, a public scientific and technical research establishment, having SIREN No. 180 089 013, APE code 7219 Z, having its registered office at 3, rue Michel-Ange, 75796 Paris cedex 16, France, acting in its own name and on its own behalf, and on behalf of Le laboratoire d'analyse et d'architecture des systèmes (LAAS), UPR n°8001.

1. INTENT/PURPOSE
This agreement determines the conditions in which the LICENSOR, who has the rights to the SOFTWARE, grants to the LICENSEE a license for research and evaluation purposes only, excluding any commercial use.

2. LICENSEE
Any person or organization who receives the SOFTWARE with a copy of this license.

3. RIGHTS GRANTED
The rights to use and copy the SOFTWARE, subject to the restrictions described in this agreement.
The rights to modify and compile the SOFTWARE when it's provided in source code form, subject to the restrictions described in this agreement.
For the SOFTWARE provided in binary form only, LICENSEE undertakes to not decompile, disassemble, decrypt, extract the components or perform reverse engineering except to the extent expressly provided by law.

5. SCOPE OF THE LICENSE
- NON-COMMERCIAL license for research and evaluation purposes ONLY.
- NO right to commercialize the SOFTWARE, or any derivative work, without separate agreement with the LICENSOR.

6. MODIFICATION OF THE SOFTWARE
License permits LICENSEE to modify the SOFTWARE provided in source code form for research and evaluation purposes only.

7. REDISTRIBUTION
- License permits LICENSEE to redistribute verbatim copies of the SOFTWARE, accompanied with a copy of this license.
- License DOES NOT permit LICENSEE to redistribute modified versions of the SOFTWARE provided in source code form.
- License DOES NOT permit LICENSEE to commercialize the SOFTWARE or any derivative work of the SOFTWARE.

8. FEE/ROYALTY
- LICENSEE pays no royalty for this license.
- LICENSEE and any third parties must enter a new agreement for any use beyond scope of license. Please contact the IRIT technology transfer office (email numerique@toulouse-tech-transfer.com) for further information.

9. NO WARRANTY
The SOFTWARE is provided "as is" without warranty of any kind, either expressed or implied, including, but not limited to, the implied warranties of merchantability and fitness for a particular purpose. The entire risk as to the quality and performance of the program is with the LICENSEE.

10. NO LIABILITY
In no event unless required by applicable law or agreed to in writing will any copyright owner be liable to LICENSEE for damages, including any general, special, incidental or consequential damages arising out of the use or inability to use the program (including but not limited to loss of data or data being rendered inaccurate or losses sustained by LICENSEE or third parties or a failure of the program to operate with other programs), even if such holder has been advised of the possibility of such damages.
*/


#ifndef __LOM2M_H
#define __LOM2M_H

#include "configuration.h"

#include "bsp.h"
#include <list>
#define TY_NONE             -1
#define TY1_ACP              1
#define TY2_APPL_ENTITY      2
#define TY3_CONTAINER        3
#define TY4_CONTENT_INSTANCE 4
#define TY5_CSEBASE          5
#define TY9_GROUP            9
#define TY16_REMOTE_CSE     16
#define TY23_SUBSCRIPTION   23
#define TY_NOTIFICATION     42

#define TY_URIL            990

/*

    Table 6.3.2.2.2.13
    -
    Interpretation of memberType

    Value Interpretation
    1 accessControlPolicy
    2 AE  (application entity)      m2m:ae
    3 container                     m2m:cnt
    4 contentInstance               m2m:cin
    5 CSEBase            m2m:cb
    6 delivery
    7 eventConfig
    8 execInstance
    9 Group
    10 localPolicy
    11 m2mServiceSubscription
    12 mgmtCmd
    13 mgmtObj
    14 Node
    15 polling channel
    16 nodeInfo
    17 pollingChannel
    18 remoteCSE
    19 Request
    20 Schedule
    21 statsCollect
    22 statsConfig
    23 Subscription                 m2m:sub
    24 Mixed
    See Clause 7.3.12 “Resource Type group”

*/

/*

    Short Names
    https://github.com/hsuyh/om2m_ver1/blob/master/org.eclipse.om2m.commons/src/main/java/org/eclipse/om2m/commons/constants/ShortName.java
    http://www.onem2m.org/images/files/deliverables/TS-0004-CoreProtocol-V-2014-08.pdf

    ae: Application Entity
    cnt: Container
    cin: Content Instance
    sub: Subscription
    sgn: Agregated Notification
    rn: Resource Name
    ty: Type
    ri: Resource ID
    pi: Parent Id
    Acpi: Access Control Policies IDs
    uril: URI List
    ct: Creation Time
    et: Expiration Time
    lt: Last Modified Time
    lbl: Label
    cnf: Content Format
    con: Content
    mni: Maximum Number of Instance
    st: State Tag
    cs: Content Size
    aei: Application Entity Id
    api: Application Id
    poa: Point of Access
    rr: Request Reachability
    nev: Notification Event
    nep: Representation
    sur: Subscription URI
    Enc: Event Notification Criteria
    nu: notification URI
    cst: CSE type - 1:IN-CSE 2:MN-CSE 3:AEN-CSE
    mid: (M)2M (ID)entifiers (used in groups)
    mtv: Member Type Validated (groups)

    http://www.onem2m.org/images/files/deliverables/TS-0009-HTTP_Protocol_Binding-V1_5_1.pdf
    Table 6.2.2-1: oneM2M request parameters mapped as query-string field
    Request Parameter  Field NameNote
    Response Type rt responseTypeelement of the response type parameter
    Result Persistence rp
    Result Content rc
    Delivery Aggregation da
    createdBefore crb filterCriteria condition
    createdAfter cra filterCriteria condition
    modifiedSince ms filterCriteria condition
    unmodifiedSince us filterCriteria condition
    stateTagSmaller sts filterCriteria condition
    stateTagBigger stb filterCriteria condition
    expireBefore exb filterCriteria condition
    expireAfter exa filterCriteria condition
    labels lbl filterCriteria condition
    resourceType rty filterCriteria condition
    sizeAbove sza filterCriteria condition
    sizeBelow szb filterCriteria condition
    contentType cty filterCriteria condition
    limit lim filterCriteria condition
    attribute atr filterCriteria condition
    filterUsage fu filterCriteria condition  indicate that it is a discovery request.
    Discovery Result Type drt

*/

/*
    http://www.onem2m.org/images/files/deliverables/TS-0009-HTTP_Protocol_Binding-V1_0_1.pdf
    oneM2M Operation  HTTPMethod
    Create            POST
    Retrieve          GET
    Update            PUT(full update)
    or                POST(partial update)
    Delete            DELETE
    Notify            POST
*/


/////////////////////////////////////////////

extern const char S_slash [];
extern const char S_date [];
extern const char S_value [];
extern const char S_creation [];
extern const char S_modification [];
extern const char S_expiration [];
extern const char S_xml [];
extern const char S_json [];
extern const char S_tyeq [4];
extern const char S_rn [];
extern const char S_INCSE [];

extern const char ENCODED_TRUE [];
extern const char ENCODED_FALSE [];

extern String CSE_ID;
extern String CSE_NAME;
extern String ACP_ADMIN_NAME;
extern int CSE_TYPE;
extern const String M2M_SP_ID;
extern String ADMIN_ORIGINATOR;
extern String DEFAULT_PROTOCOL;
#if HTTP_BINDING
extern int HTTP_BINDING_ENABLED;
#endif // HTTP_BINDING
#if PERSISTENCE_FEAT
extern int BACKUP_PERIOD;
extern int BACKUP_ENABLED;
#endif
extern int MAX_NUMBER_OF_INSTANCES_DEFAULT;
extern int globalResourcesThreshold;

//extern std::list<String> SUPPORTED_RESTYPE;
extern std::list<int> SUPPORTED_RESTYPE;
extern std::list<String> SUPPORTED_RELEASE_VERSIONS;
extern std::list<String> SERIALISATION_TYPES;

extern String PORT;     // constant defining the port used by the CSE POA
extern String IP;
/**
 * Get the ressource type based on the int value
 * @param ty - int value of the resource type
 * @return actual char* namespace (m2m:xxx)
 */
const char* rqType(int ty);
const char* contentTypeEncoding(Encoding enc);
const char* contentType(const String& filename);


extern String REMOTE_CSE_POA;
extern String REMOTE_CSE_ID;
extern String REMOTE_CSE_NAME;
extern int REMOTE_CSE_TYPE;

extern bool dataUpdated;
extern bool descendantCsesToUpdate;

/**
 * Usefull to handle RCN 9
 */
#if FEAT_RCN_9
extern std::list<String> modifiedAttributes;
#endif // FEAT_RCN_9

/** Used to propagate originator to check access rights recursively if necessary */
extern String FROM_ORIGINATOR;

#endif // __LOM2M_H
