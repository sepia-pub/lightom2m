/*
------------------------------------------------------------

SOFTWARE EVALUATION LICENSE

LAAS research laboratory, Toulouse, France.
IRIT computer science research laboratory, Toulouse, France.
------------------------------------------------------------

Contact: Thierry Monteil, thierry.monteil@irit.fr

Definitions
SOFTWARE:  The LightOM2M software, version 1.0, in source code form, written by David Gauchard, Guillaume Garzone and Thierry Monteil
at the LAAS research laboratory.

LICENSOR: LE CENTRE NATIONAL DE LA RECHERCHE SCIENTIFIQUE, a public scientific and technical research establishment, having SIREN No. 180 089 013, APE code 7219 Z, having its registered office at 3, rue Michel-Ange, 75796 Paris cedex 16, France, acting in its own name and on its own behalf, and on behalf of Le laboratoire d'analyse et d'architecture des systèmes (LAAS), UPR n°8001.

1. INTENT/PURPOSE
This agreement determines the conditions in which the LICENSOR, who has the rights to the SOFTWARE, grants to the LICENSEE a license for research and evaluation purposes only, excluding any commercial use.

2. LICENSEE
Any person or organization who receives the SOFTWARE with a copy of this license.

3. RIGHTS GRANTED
The rights to use and copy the SOFTWARE, subject to the restrictions described in this agreement.
The rights to modify and compile the SOFTWARE when it's provided in source code form, subject to the restrictions described in this agreement.
For the SOFTWARE provided in binary form only, LICENSEE undertakes to not decompile, disassemble, decrypt, extract the components or perform reverse engineering except to the extent expressly provided by law.

5. SCOPE OF THE LICENSE
- NON-COMMERCIAL license for research and evaluation purposes ONLY.
- NO right to commercialize the SOFTWARE, or any derivative work, without separate agreement with the LICENSOR.

6. MODIFICATION OF THE SOFTWARE
License permits LICENSEE to modify the SOFTWARE provided in source code form for research and evaluation purposes only.

7. REDISTRIBUTION
- License permits LICENSEE to redistribute verbatim copies of the SOFTWARE, accompanied with a copy of this license.
- License DOES NOT permit LICENSEE to redistribute modified versions of the SOFTWARE provided in source code form.
- License DOES NOT permit LICENSEE to commercialize the SOFTWARE or any derivative work of the SOFTWARE.

8. FEE/ROYALTY
- LICENSEE pays no royalty for this license.
- LICENSEE and any third parties must enter a new agreement for any use beyond scope of license. Please contact the IRIT technology transfer office (email numerique@toulouse-tech-transfer.com) for further information.

9. NO WARRANTY
The SOFTWARE is provided "as is" without warranty of any kind, either expressed or implied, including, but not limited to, the implied warranties of merchantability and fitness for a particular purpose. The entire risk as to the quality and performance of the program is with the LICENSEE.

10. NO LIABILITY
In no event unless required by applicable law or agreed to in writing will any copyright owner be liable to LICENSEE for damages, including any general, special, incidental or consequential damages arising out of the use or inability to use the program (including but not limited to loss of data or data being rendered inaccurate or losses sustained by LICENSEE or third parties or a failure of the program to operate with other programs), even if such holder has been advised of the possibility of such damages.
*/


#ifndef __REQUESTPRIMITIVE_H
#define __REQUESTPRIMITIVE_H
#define DBUG true

#include "configuration.h"

#include <map>
#include <assert.h> // should be removed
#include <stdlib.h>

#include "Entity.h"
#include "Notify.h"
#include "Enum.h"
#include "AccessControlRule.h"
#include "tools.h"



enum UriType
{
    URI_ABSOLUTE,
    URI_SP_RELATIVE,
    URI_CSE_RELATIVE
};
/////////////////////////////////////////////////

// used by gateway to build answers
// used by client to build requests

class RequestPrimitive
{
protected:

    void setURL(const String& resourceName = emptyString,
                const String& resourceDataName = emptyString);

protected:
    UriType m_uriType = URI_CSE_RELATIVE;
    Operation m_operation;
    String m_to;
    String m_from;
    String m_ri;
    int m_resourceType;    
    String m_content;
    // TODO Content 
    // TODO Role IDs
    String m_originatingTimestamp;
    // TODO request Expiration Timestamp
    // TODO result expiration timestamp
    // TODO response type
    // TODO result persistence
    ResultContentType m_rcn;
    // TODO event category
    int m_deliveryAggregation;
    String m_groupRequestIdentifier;
    FilterCriteria fc;
    // TODO desired identifier result type
    // TODO TOKENS
    // TODO TOKEN IDS
    // TODO local Token IDs
    // TODO Token Request Indicator
    // TODO Group request target Members
    int m_authorizationSignatureIndicator;
    // TODO authorization signatures (list)
    int m_authorizationRelationshipIndicator;
    int m_semanticQueryIndicator;

    // TODO RELEASE VERSION INDICATOR
    String m_rvi;

    String m_vendorInformation;

    String m_url;
    String m_contentType;
    String m_wantedContentType;
    String m_location;

    int qs_ty;
    bool qs_fu;
    std::list<String> qs_lbl;
    int qs_lvl;

public:

    void init();

    UriType getUriType()
    {
        return this->m_uriType;
    }
    void setUriType(UriType uriType)
    {
        this->m_uriType = uriType;
    }
    FilterCriteria& getFilterCriteria() {
        return this->fc;
    }
    void setFilterCriteria(FilterCriteria& fc) {
        this->fc = fc;
    }
    const String& getGroupRequestIdentifier() const
    {
        return this->m_groupRequestIdentifier;
    }
    void setGroupRequestIdentifier(const String& groupRequestIdentifier)
    {
        this->m_groupRequestIdentifier = groupRequestIdentifier;
    }
    int getDeliveryAggregationInt()
    {
        return this->m_deliveryAggregation;
    }
    bool getDeliveryAggregation()
    {
        return (this->m_deliveryAggregation == 1);
    }
    void setDeliveryAggregation(bool deliveryAggregation)
    {
        if (deliveryAggregation)
        {
            this->m_deliveryAggregation = 1;
        }
        else
        {
            this->m_deliveryAggregation = 0;
        }
    }
    const String& getOriginatingTimestamp() const
    {
        return this->m_originatingTimestamp;
    }
    void setOriginatingTimestamp(const String& originatingTimestamp)
    {
        this->m_originatingTimestamp = originatingTimestamp;
    }
    int getSemanticQueryIndicatorInt(){
        return this->m_semanticQueryIndicator;
    }
    bool getSemanticQueryIndicator() {
        return (this->m_semanticQueryIndicator == 1);
    }
    void setSemanticQueryIndicator(bool semanticQueryIndicator) {
        if (semanticQueryIndicator)
        {
            m_semanticQueryIndicator = 1;
        }
        else
        {
            m_semanticQueryIndicator = 0;
        }
    }
    bool getAuthorizationRelationshipIndicator()
    {
        return (this->m_authorizationRelationshipIndicator == 1);
    }
    int getAuthorizationRelationshipIndicatorInt()
    {
        return this->m_authorizationRelationshipIndicator;
    }
    void setAuthorizationRelationshipIndicator(bool authorizationRelationshipIndicator)
    {
        if (authorizationRelationshipIndicator)
        {
            this->m_authorizationRelationshipIndicator = 1;
        }
        else
        {
            this->m_authorizationRelationshipIndicator = 0;
        }
    }
    int getAuthorizationSignatureIndicatorInt()
    {
        return this->m_authorizationSignatureIndicator;
    }
    bool getAuthorizationSignatureIndicator()
    {
        return (this->m_authorizationSignatureIndicator == 1);
    }
    void setAuthorizationSignatureIndicator(bool authorizationSignatureIndicator)
    {
        if (authorizationSignatureIndicator)
        {
            this->m_authorizationSignatureIndicator = 1;
        }
        else
        {
            this->m_authorizationSignatureIndicator = 0;
        }
    }
    const int getQSlvl() const
    {
        return qs_lvl;
    }

    void setQSlvl(int lvl)
    {
        qs_lvl = lvl;
    }

    const int getQSty() const
    {
        return qs_ty;
    }

    void setQSty(int ty)
    {
        qs_ty = ty;
    }

    const bool getQSfu()
    {
        return qs_fu;
    }

    void setQSfu(bool fu)
    {
        qs_fu = fu;
    }

    const String& getURL() const
    {
        return m_url;
    }

    void setContent(String content)
    {
        m_content = content;
    }

    const String& getContent() const
    {
        return m_content;
    }

    void setLocation(String location)
    {
        m_location = location;
    }

    const String& getLocation() const
    {
        return m_location;
    }

    void setResultContentType(ResultContentType rcn)
    {
        m_rcn = rcn;
    }

    void setResultContentType(const char* rcn);

    ResultContentType getResultContentType() const
    {
        return m_rcn;
    }

    void setWantedContentType(String contentType)
    {
        m_wantedContentType = contentType;
    }

    const String& getWantedContentType() const
    {
        return m_wantedContentType;
    }

    void setOperation(Operation operation)
    {
        m_operation = operation;
    }

    void setOperation(int operation)
    {
        m_operation = Operation(operation);
    }

    int getOperation() const
    {
        return m_operation;
    }

    Operation getOperationEnum() const
    {
        return m_operation;
    }

    void setTo(String to)
    {
        m_to = to;
    }

    const String& getTo() const
    {
        return m_to;
    }

    void setFrom(String from)
    {
        m_from = from;
    }

    String getFrom() const
    {
        return m_from;
    }

    void setRequestId(String ri)
    {
        m_ri = ri;
    }

    const String& getRequestId() const
    {
        return m_ri;
    }

    void setResourceType(int ty)
    {
        m_resourceType = ty;
    }

    void setReleaseVersionIndicator(const String& rvi)
    {
        m_rvi = rvi;
    }

    const String& getReleaseVersionIndicator() const
    {
        return m_rvi;
    }

    int getResourceType() const
    {
        return m_resourceType;
    }

    const String& getRequestContentType()
    {
        return m_contentType;
    }

    const String& getVendorInformation()
    {
        return this->m_vendorInformation;
    }
    void setVendorInformation(const String& vendorInformation)
    {
        this->m_vendorInformation = vendorInformation;
    }

    void setRequestContentType();
    void setRequestContentTypeRaw(const String& contentType);

public:

    RequestPrimitive();

    void buildRequest(Encoding enc, String& req);
    Encoding m_encoding;
    String credLogin;
    String credPassw;
    String gateway;

    //XXX these constants must be taken from lom2m.cpp
    //XXX or moved to examples
    String rootCSE = "~/mn-cse/";        // XXX rename var
    String rootName = rootCSE + "mn-name/";    // XXX rename var
    String api = "app-sensor";        // Free string ?
    String type = "sensor";            // Free string ?
    String category = "temperature2";    // Free string ?
    String location = "home2";
    String resourceName = "sensors2";
    String descriptor = "esp-outside2";
    String resourceDataName = "temperature2";

    ///////////////////////////////////////////////////////////

    //XXX rename functions
    //XXX move relevant ones to protected section
    void createEntity();
    void createCSEBase();
    void createContainer(const String& name);
    void initReport();
    [[DEPRECATED]]
    void createReport(Entity* e, ResultContentType rcn = RCN_ATTR_CHREF, Operation op = OP_NULL)
    {
        createReport(e, rcn, 1, op);
    }
    void createReport(Entity* e, ResultContentType rcn = RCN_ATTR_CHREF, int lvl = 1, Operation op = OP_NULL, bool notify = false);
    [[DEPRECATED]]
    void createReport(JsonObject rootElement, Entity* e, ResultContentType rcn = RCN_ATTR_CHREF, Operation op = OP_NULL)
    {
        createReportLvl(rootElement, e, rcn, 1, op);
    }
    void createReportLvl(JsonObject rootElement, Entity* e, ResultContentType rcn = RCN_ATTR_CHREF, int lvl = 1, Operation op = OP_NULL);
    #if SUBSCRIPTION_FEAT
    void createNotifyReport(Notification* n, NotificationContentType nct);
    #endif // SUBSCRIPTION_FEAT
    void addDescriptionInstance();
    void addDataInstance(const String& value);
    void createDescriptorContainer()
    {
        return createContainer(descriptor);
    }
    void createDataContainer()
    {
        return createContainer(resourceDataName);
    }
    void addDataInstance(int value)
    {
        return addDataInstance(String(value, DEC));
    }
    void buildRequest(String& req)
    {
        buildRequest(m_encoding, req);
    }
    ResponseStatusCode createURIList(int ty, String& lbl);

    #if SUBSCRIPTION_FEAT
    String createNotifyBody(Notification* notif, NotificationContentType nct);
    #endif // SUBSCRIPTION_FEAT


}; // class om2m

#endif // __REQUESTPRIMITIVE_H
