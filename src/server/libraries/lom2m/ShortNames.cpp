/*
------------------------------------------------------------

SOFTWARE EVALUATION LICENSE

LAAS research laboratory, Toulouse, France.
IRIT computer science research laboratory, Toulouse, France.
------------------------------------------------------------

Contact: Thierry Monteil, thierry.monteil@irit.fr

Definitions
SOFTWARE:  The LightOM2M software, version 1.0, in source code form, written by David Gauchard, Guillaume Garzone and Thierry Monteil
at the LAAS research laboratory.

LICENSOR: LE CENTRE NATIONAL DE LA RECHERCHE SCIENTIFIQUE, a public scientific and technical research establishment, having SIREN No. 180 089 013, APE code 7219 Z, having its registered office at 3, rue Michel-Ange, 75796 Paris cedex 16, France, acting in its own name and on its own behalf, and on behalf of Le laboratoire d'analyse et d'architecture des systèmes (LAAS), UPR n°8001.

1. INTENT/PURPOSE
This agreement determines the conditions in which the LICENSOR, who has the rights to the SOFTWARE, grants to the LICENSEE a license for research and evaluation purposes only, excluding any commercial use.

2. LICENSEE
Any person or organization who receives the SOFTWARE with a copy of this license.

3. RIGHTS GRANTED
The rights to use and copy the SOFTWARE, subject to the restrictions described in this agreement.
The rights to modify and compile the SOFTWARE when it's provided in source code form, subject to the restrictions described in this agreement.
For the SOFTWARE provided in binary form only, LICENSEE undertakes to not decompile, disassemble, decrypt, extract the components or perform reverse engineering except to the extent expressly provided by law.

5. SCOPE OF THE LICENSE
- NON-COMMERCIAL license for research and evaluation purposes ONLY.
- NO right to commercialize the SOFTWARE, or any derivative work, without separate agreement with the LICENSOR.

6. MODIFICATION OF THE SOFTWARE
License permits LICENSEE to modify the SOFTWARE provided in source code form for research and evaluation purposes only.

7. REDISTRIBUTION
- License permits LICENSEE to redistribute verbatim copies of the SOFTWARE, accompanied with a copy of this license.
- License DOES NOT permit LICENSEE to redistribute modified versions of the SOFTWARE provided in source code form.
- License DOES NOT permit LICENSEE to commercialize the SOFTWARE or any derivative work of the SOFTWARE.

8. FEE/ROYALTY
- LICENSEE pays no royalty for this license.
- LICENSEE and any third parties must enter a new agreement for any use beyond scope of license. Please contact the IRIT technology transfer office (email numerique@toulouse-tech-transfer.com) for further information.

9. NO WARRANTY
The SOFTWARE is provided "as is" without warranty of any kind, either expressed or implied, including, but not limited to, the implied warranties of merchantability and fitness for a particular purpose. The entire risk as to the quality and performance of the program is with the LICENSEE.

10. NO LIABILITY
In no event unless required by applicable law or agreed to in writing will any copyright owner be liable to LICENSEE for damages, including any general, special, incidental or consequential damages arising out of the use or inability to use the program (including but not limited to loss of data or data being rendered inaccurate or losses sustained by LICENSEE or third parties or a failure of the program to operate with other programs), even if such holder has been advised of the possibility of such damages.
*/


#include "ShortNames.h"

const String SUPPORTED_REL_VERSIONS = "srv";
const String CONTENT_SERIALIZATION = "csz";

/** Short name for AccessControl Policy resource */
const String ACP = "acp";
/** Short name for AccessControlPolicyAnnc resource */
const String ACPA = "acpA";
/** Short name for ApplicationEntity resource */
const String AE = "ae";
/** Short name for ApplicationEntityAnnc resource */
const String AEA = "aeA";
/** Short name for Container resource */
const String CNT = "cnt";
/** Short name for ContainerAnnc resource */
const String CNTA = "cntA";
/** Short name for DynamicAuthorizationConsultation resource */
const String DAC = "dac";
/** Short name for FlexContainer resource*/
const String FCNT = "fcnt";
/** Short name for FlexContainerAnnc resource */
const String FCNTA = "fcnta";
/** Short name for Content Instance resource */
const String CIN = "cin";
/** Short name for ContentInstanceAnnc resource */
const String CINA = "cinA";
/** Short name for CseBase resource */
const String CSE_BASE = "cb";
/** Short name for Delivery resource */
const String DELIVERY = "dlv";
/** Short name for EventConfig resource */
const String EVENTCONFIG = "evcg";
/** Short name for ExecInstance resource */
const String EXECINSTANCE = "exin";
/** Short name for FanOutPoint resource */
const String FANOUTPOINT = "fopt";
/** Short name for Group resource */
const String GROUP = "grp";
/** Short name for GroupAnnc resource */
const String GROUPA = "grpA";
/** Short name for LocationPolicy resource */
const String LOCATIONPOLICY = "lcp";
/** Short name for LocationPolicyAnnc resource */
const String LOCATIONPOLICYA = "lcpA";
/** Short name for M2mServiceSubscriptionProfile resource */
const String MSSP = "mssp";
/** Short name for MgmtCmd resource */
const String MGC = "mgc";
/** Short name for MgmtObj resource */
const String MGO = "mgo";
/** Short name for MgmtObjAnnc resource */
const String MGOA = "mgoA";
/** Short name for Node resource */
const String NODE = "nod";
/** Short name for NodeAnnc resource */
const String NODE_ANNC = "nodA";
/** Short name for PollingChannel resource */
const String PCH = "pch";
/** Short name for PollingChannelUri resource */
const String POLLING_CHANNEL_URI = "pcu";
/** Short name for RemoteCse resource */
const String REMOTE_CSE = "csr";
/** Short name for RemoteCseAnnc resource */
const String CSRA = "csrA";
/** Short name for Request resource */
const String REQ = "req";
/** Short name for Schedule resource */
const String SCHEDULE = "sch";
/** Short name for ScheduleAnnc resource */
const String SCHA = "schA";
/** Short name for Service subscribedAppRule resource */
const String ASAR = "asar";
/** Short name for ServiceSubscribedNode resource */
const String SVSN = "svsn";
/** Short name for StatsCollect resource */
const String STCL = "stcl";
/** Short name for StatsConfig resource */
const String STCG = "stcg";
/** Short name for Subscription resource */
const String SUB = "sub";

// Resource attributes short names
/** Short name for Resource ID attribute */
const String RESOURCE_ID = "ri";
/** Short name for CreationTime attribute */
const String RESOURCE_TYPE = "ty";
/** Short name for CreationTime attribute */
const String CREATION_TIME = "ct";
/** Short name for LastModifiedTime attribute */
const String LAST_MODIFIED_TIME = "lt";
/** Short name for ParentID attribute */
const String PARENT_ID = "pi";
/** Short name for ResourceName attribute */
const String RESOURCE_NAME = "rn";
/** Short name for Labels attribute */
const String LABELS = "lbl";
/** Short name for ExpirationTime attribute */
const String EXPIRATION_TIME = "et";
/** Short name for AnnounceTo attribute */
const String ANNOUNCE_TO = "at";
/** Short name for AnnouncedAttribute attribute */
const String ANNOUNCED_ATTRIBUTE = "aa";
/** Short name for PointOfAccess attribute */
const String POA = "poa";
/** Short name for NodeLink attribute */
const String NODE_LINK = "nl";
/** Short name for NodeLink attribute */
const String ACP_IDS = "acpi";
/** Short name for Child Resource */
const String CHILD_RESOURCE = "ch";
/** Short name for DynamicAuthorizationConsultationIDs attribute */
const String DAC_IDS = "daci";

// Attributes for Child Resources
/** Short name for name attribute of a ChildResourceRef */
const String CHILD_RESOURCE_NAME = "nm";
/** Short name for type attribute of a ChildResourceRef */
const String CHILD_RESOURCE_TYPE = "typ";
/** Short name for spid attribute of a ChildResourceRef */
const String CHILD_RESOURCE_SPID = "spid";
/** Short name for val attribute of a ChildResourceRef */
const String CHILD_RESOURCE_VALUE = "val";

// Attributes for CSEBase Entity
/** Short name for SupportedResourceTypes attribute */
const String SRT = "srt";
/** Short name for CSE-ID attribute */
const String SN_CSE_ID = "csi";
/** Short name for CseType attribute */
const String SN_CSE_TYPE = "cst";

// Specific attributes for AccessControlPolicy Entity
/** Short name for privileges attribute */
const String PRIVILEGES = "pv";
/** Short name for privileges attribute */
const String SELF_PRIVILEGES = "pvs";
/** Short name for acr attribute */
const String ACR = "acr";
/** Short name for access control originators attribute */
const String ACOR = "acor";
/** Short name for access control operations attribute */
const String ACOP = "acop";
/** Short name for access control contexts attribute */
const String ACCO = "acco";
/** Short name for access control window attribute */
const String ACTW = "actw";
/** Short name for access control ip adresses attribute */
const String ACIP = "acip";
/** Short name for ipv4 address attribute */
const String IPV4 = "ipv4";
/** Short name for ipv6 address attribute */
const String IPV6 = "ipv6";
/** Short name for access control location region attribute */
const String ACLR = "aclr";
/** Short name for country code attribute */
const String ACCC = "accc";
/** Short name for circ region attribute */
const String ACCR = "accr";
/** Short name for access control authentication flag */
const String ACAF = "acaf";

// Attributes for Application Entity
/** Short name for App Name Attribute */
const String APP_NAME = "apn";
/** Short name for App-ID Attribute */
const String APP_ID = "api";
/** Short name for AE-ID Attribute */
const String AE_ID = "aei";
/** Short name for Ontology Reference Attribute */
const String ONTOLOGY_REF = "or";

// Attributes for Container Entity
/** Short name for Creator attribute */
const String CREATOR = "cr";
/** Short name for StateTag attribute */
const String STATETAG = "st";
/** Short name for Max Number Of Instances attribute */
const String MAX_NR_OF_INSTANCES = "mni";
/** Short name for Max Byte Size attribute */
const String MAX_BYTE_SIZE = "mbs";
/** Short name for Max Instance Age attribute */
const String MAX_INSTANCE_AGE = "mia";
/** Short name for Current Byte Size attribute */
const String CURRENT_BYTE_SIZE = "cbs";
/** Short name for location ID attribute */
const String LOCATION_ID = "li";
/** Short name for Latest attribute */
const String LATEST = "la";
/** Short name for Oldest attribute */
const String OLDEST = "ol";
/** Short name for current number of instances attribute */
const String CURRENT_NUMBER_OF_INSTANCES = "cni";
/** Short name for disable retrieval attribute */
const String DISABLE_RETRIEVAL = "disr";

// Attributes for FlexContainerEntity
/** Short name for ContainerDefinition attribute */
const String CONTAINER_DEFINITION = "cnd";

// Attributes for Content Instance
/** Short name for ContentSize attribute */
const String CONTENT_SIZE = "cs";
/** Short name for ContentInfo attribute */
const String CONTENT_INFO = "cnf";
/** Short name for Content attribute */
const String CONTENT = "con";
/** Short Name fot expiration counter*/
const String EXPIRATION_COUNTER = "exc";

const String NOTIFICATION_URI = "nu";
const String NOTIFICATION_FORWARDING_URI = "nfu";
const String TYPE = "ty";

// specific attributes for Group resource
/** Short Name for the member type attribute */
const String MEMBER_TYPE = "mt";
/** Short Name for the current number of members attribute */
const String CURRENT_NUM_MEMBERS = "cnm";
/** Short Name for the member type attribute */
const String MAX_NUM_MEMBERS = "mnm";
/** Short Name for the memberID type attribute */
const String MEMBER_ID = "mid";
/** Short Name for the member acp id attribute */
const String MEMBER_ACP_ID = "macp";
/** Short Name for the member type validated attribute */
const String MEMBER_TYPE_VALIDATED = "mtv";
/** Short Name for the consistency strategy attribute */
const String CONSISTENCY_STRATEGY = "csy";
/** Short Name for the consistency strategy attribute */
const String GROUP_NAME = "gn";

// Specific attributes for remoteCSE resource
/** Short Name for the M2M-EXT-ID attribute */
const String M2M_EXT_ID = "mei";
const String TRIGGER_RECIPIENT_ID = "tri";
const String REQUEST_REACHABILITY = "rr";
const String REMOTE_CSE_CSEBASE = "cb";
const String SN_DESCENDANT_CSE = "dcse";

// Specific attributes for Subscription resource
/** Short Name for the Event Notification Criteria attribute */
const String EVENT_NOTIFICATION_CRITERIA = "enc";
/** Short name for group id */
const String GROUP_ID = "gpi";
/** Short name for the batch notify attribute */
const String BATCH_NOTIFY = "bn";
/** Short name for rate limit */
const String RATE_LIMIT = "rl";
/** Short name for pre subscription notify */
const String PRE_SUBSCRIPTION_NOTIFY = "psn";
/** Short name for pending notification */
const String PENDING_NOTIFICATION = "pn";
/** Short name for notification storage priority */
const String NOTIFICATION_STORAGE_PRIORITY = "nsp";
/** Short name for latest notify */
const String LATEST_NOTIFY = "ln";
/** Short name for notification content type */
const String NOTIFICATION_CONTENT_TYPE = "nct";
/** Short name for Notification event cat */
const String NOTIFICATION_EVENT_CAT = "nec";
const String NOTIFICATION_EVENT_TYPE = "net";
/** Short name for subscriber uri */
const String SUBSCRIBER_URI = "su";

// attributes for rate limit
/** short name for max number of notify */
const String MAX_NR_OF_NOTIFY = "mnn";
/** short name for time window */
const String TIME_WINDOW = "tww";

// attributes for Schedule
/** short name for schedule element */
const String SCHEDULE_ELEMENT = "se";
/** short name for schedule entry */
const String SCHEDULE_ENTRY = "sce";

// attributes for announced resource
const String LINK = "lnk";
const String AE_ANNC = AE + "A";
const String CNT_ANNC = CNT + "A";

// attributes for request resource
const String OPERATION = "op";
const String TO = "to";
const String FROM = "fr";
const String NAME = "nm";
const String PRIMITIVE_CONTENT = "pc";
const String ORIGINATING_TIMESTAMP = "ot";
const String REQUEST_EXPIRATION_TIMESTAMP = "rqet";
const String RESULT_EXPIRATION_TIMESTAMP = "rset";
const String OPERATION_EXECUTION_TIME = "oet";
const String RESPONSE_TYPE = "rt";
const String RESULT_PERSISTENCE = "rp";
const String RESULT_CONTENT = "rcn";
const String EVENT_CATEGORY = "ec";
const String DELIVERY_AGGREGATION = "da";
const String GROUP_REQUEST_IDENTIFIER = "gid";
const String FILTER_CRITERIA = "fc";
const String CREATED_BEFORE = "crb";
const String CREATED_AFTER = "cra";
const String MODIFIED_SINCE = "ms";
const String UNMODIFIED_SINCE = "us";
const String STATETAG_SMALLER = "sts";
const String STATETAG_BIGGER = "stb";
const String EXPIRE_BEFORE = "exb";
const String EXPIRE_AFTER = "exa";
const String FILTER_RESOURCETYPE = "rty";
const String CONTENT_TYPE = "cty";
const String LIMIT = "lim";
const String LEVEL = "lvl";
const String OFFSET = "ofst";
const String ATTRIBUTE = "atr";
const String FILTER_USAGE = "fu";
const String DISCOVERY_RESULT_TYPE = "drt";
const String RESPONSE_STATUS_CODE = "rsc";
const String ORIGINATOR = "og";
const String META_INFORMATION = "mi";
const String REQUEST_STATUS = "rs";
const String OPERATION_RESULT = "ol";
const String REQUEST_OPERATION = "opn";
const String REQUEST_ID = "rid";
const String TARGET = "tg";
const String EVENT_CAT_TYPE = "ect";
const String EVENT_CAT_NO = "ecn";
const String AUTHORIZATION_SIGNATURE_REQUEST_INFO = "asri";
const String VENDOR_INFO = "vsi";
const String CONTENT_OFFSET = "cnot";
const String CONTENT_STATUS = "cnst";
const String RELEASE_VERSION_INDICATOR = "rvi";
const String AUTHORIZATION_SIGNATURE_INDICATOR = "asi";
const String AUTHORIZATION_RELATIONSHIP_INDICATOR = "auri";
const String SEMANTIC_QUERY_INDICATOR = "sqi";
const String SEMANTICS_FILTER = "smf";
const String FILTER_OPERATION = "fo";
const String CONTENT_FILTER_SYNTAX = "cfs";
const String CONTENT_FILTER_QUERY = "cfq";


// attributes for filter criteria
const String SIZE_ABOVE = "sza";
const String SIZE_BELOW = "szb";

// Attributes for Event Notification Criteria
const String OPERATION_MONITOR = "om";
const String RESOURCE_STATUS = "rss";

// Attributes for Batch Notify
const String NUMBER = "num";
const String DURATION = "dur";

// Attributes for Notification
const String NOTIFICATION = "sgn";
const String NOTIFICATION_EVENT = "nev";
const String VERIFICATION_REQUEST = "vrq";
const String SUBSCRIPTION_DELETION = "sud";
const String SUBSCRIPTION_REFERENCE = "sur";

// Attribute for Notification Event
const String REPRESENTATION = "rep";

// Attributes for OperationMonitor
const String OM_OPERATION = "opr";
const String OM_ORIGINATOR = "org";

const String AGGREGATED_RESPONSE = "agr";
const String RESPONSE_PRIMITIVE = "rsp";
const String REQUEST_PRIMITIVE = "rqp";
const String REQUEST_IDENTIFIER = "rqi";

// Attributes for Node
const String NODE_ID = "ni";
const String HOSTED_CSE_LINK = "hcl";
const String HOSTED_SRV_LINK = "hsl";

// Short names for mgmt objects generic attributes
const String DESCRIPTION = "dc";
const String MGMT_DEF = "mgd";
const String OBJ_IDS = "obis";
const String OBJ_PATHS = "obps";

// Short names for mgmt objects specialization
const String AREA_NWK_DEVICE_INFO = "andi";
const String AREA_NWK_INFO = "ani";
const String BATTERY = "bat";
const String DEVICE_CAPABILITY = "dvc";
const String DEVICE_INFO = "dvi";
const String EVENT_LOG = "evl";
const String FIRMWARE = "fwr";
const String MEMORY = "mem";
const String REBOOT = "rbo";
const String SOFTWARE = "swr";

const String AREA_NWK_DEVICE_INFO_ANNC = "andiA";
const String AREA_NWK_INFO_ANNC = "aniA";
const String BATTERY_ANNC = "batA";
const String DEVICE_CAPABILITY_ANNC = "dvcA";
const String DEVICE_INFO_ANNC = "dviA";
const String EVENT_LOG_ANNC = "evlA";
const String FIRMWARE_ANNC = "fwrA";
const String MEMORY_ANNC = "memA";
const String REBOOT_ANNC = "rboA";
const String SOFTWARE_ANNC = "swrA";

const String ACTIVE_CMDH_POLICY = "acmp";
const String CMDH_POLICY = "cmp";
const String CMDH_DEFAULTS = "cmdf";
const String CMDH_DEF_EC_VALUE = "cmdv";
const String CMDH_EC_DEF_PARAM_VALUES = "cmpv";
const String CMDH_LIMITS = "cml";
const String CMDH_NETWORK_ACCESS_RULES = "cmnr";
const String CMDH_NW_ACCESS_RULE = "cmwr";
const String CMDH_BUFFER = "cmbf";

// Short names for Custom Attributes
const String CUSTOM_ATTRIBUTE_NAME = "can";
const String CUSTOM_ATTRIBUTE_TYPE = "cat";
const String CUSTOM_ATTRIBUTE_VALUE = "cav";

// short names for area nwk info
const String AREA_NWK_TYPE = "ant";
const String LIST_DEVICES = "ldv";

// short names for area nwk device info
const String SN_STATUS = "ss";
const String DEV_ID = "dvd";
const String DEV_TYPE = "dvt";
const String AREA_NWK_ID = "awi";
const String SLEEP_INTERVAL = "sli";
const String SLEEP_DURATION = "sld";
const String LIST_OF_NEIGHBORS = "lnh";

// short names for device info
const String DEVICE_LABEL = "dlb";
const String DEVICE_TYPE = "dty";
const String DEVICE_MODEL = "mod";
const String MANUFACTURER = "man";
const String FW_VERSION = "fwv";
const String SW_VERSION = "swv";
const String HW_VERSION = "hwv";
const String OS_VERSION = "osv";
const String MANUF_DET_LINKS = "mfdl";
const String MANUF_DATE = "mfd";
const String DEVICE_SUB_MODEL = "smod";
const String DEVICE_NAME = "dvnm";
const String COUNTRY = "cnty";
const String LOCATION = "loc";
const String SYS_TIME = "syst";
const String SUPPORT_URL = "spur";
const String PRES_URL = "purl";
const String PROTOCOL = "ptl";

// short names for battery
const String BATTERY_LEVEL = "btl";
const String BATTERY_STATUS = "bts";

// short name for URI List
const String URI_LIST = "uril";

// short names for DynamicAuthorizationConsultation
const String DYNAMIC_AUTHORIZATION_ENABLED = "dae";
const String DYNAMIC_AUTHORIZATION_PoA = "dap";
const String DYNAMIC_AUTHORIZATION_LIFETIME = "dal";

// short names for DynAuthDasRequest
const String TARGETED_RESOURCE_TYPE = "tirt";
const String TARGETED_RESOURCE_ID = "trid";
const String ORIGINATOR_IP = "oip";
const String ORIGINATOR_LOCATION = "olo";
const String ORIGINATOR_ROLE_IDS = "orid";
const String REQUEST_TIMESTAMP = "rts";
const String PROPOSED_PRIVILEDGES_LIFETIME = "ppl";
const String ROLE_IDS_FROM_ACPS = "rfa";
const String TOKEN_IDS = "tids";

// short names for DynamicAuthTokenReqInfo
const String DYNAMIC_AUTHORIZATION_TOKEN_REQ_INFO = "tqf";
const String TOKEN_REQ_INFO = "tqf";
const String DAS_INFO = "dasi";
const String URI = "uri";
const String DAS_REQUEST = "daq";
