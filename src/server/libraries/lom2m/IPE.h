/*
------------------------------------------------------------

SOFTWARE EVALUATION LICENSE

LAAS research laboratory, Toulouse, France.
IRIT computer science research laboratory, Toulouse, France.
------------------------------------------------------------

Contact: Thierry Monteil, thierry.monteil@irit.fr

Definitions
SOFTWARE:  The LightOM2M software, version 1.0, in source code form, written by David Gauchard, Guillaume Garzone and Thierry Monteil
at the LAAS research laboratory.

LICENSOR: LE CENTRE NATIONAL DE LA RECHERCHE SCIENTIFIQUE, a public scientific and technical research establishment, having SIREN No. 180 089 013, APE code 7219 Z, having its registered office at 3, rue Michel-Ange, 75796 Paris cedex 16, France, acting in its own name and on its own behalf, and on behalf of Le laboratoire d'analyse et d'architecture des systèmes (LAAS), UPR n°8001.

1. INTENT/PURPOSE
This agreement determines the conditions in which the LICENSOR, who has the rights to the SOFTWARE, grants to the LICENSEE a license for research and evaluation purposes only, excluding any commercial use.

2. LICENSEE
Any person or organization who receives the SOFTWARE with a copy of this license.

3. RIGHTS GRANTED
The rights to use and copy the SOFTWARE, subject to the restrictions described in this agreement.
The rights to modify and compile the SOFTWARE when it's provided in source code form, subject to the restrictions described in this agreement.
For the SOFTWARE provided in binary form only, LICENSEE undertakes to not decompile, disassemble, decrypt, extract the components or perform reverse engineering except to the extent expressly provided by law.

5. SCOPE OF THE LICENSE
- NON-COMMERCIAL license for research and evaluation purposes ONLY.
- NO right to commercialize the SOFTWARE, or any derivative work, without separate agreement with the LICENSOR.

6. MODIFICATION OF THE SOFTWARE
License permits LICENSEE to modify the SOFTWARE provided in source code form for research and evaluation purposes only.

7. REDISTRIBUTION
- License permits LICENSEE to redistribute verbatim copies of the SOFTWARE, accompanied with a copy of this license.
- License DOES NOT permit LICENSEE to redistribute modified versions of the SOFTWARE provided in source code form.
- License DOES NOT permit LICENSEE to commercialize the SOFTWARE or any derivative work of the SOFTWARE.

8. FEE/ROYALTY
- LICENSEE pays no royalty for this license.
- LICENSEE and any third parties must enter a new agreement for any use beyond scope of license. Please contact the IRIT technology transfer office (email numerique@toulouse-tech-transfer.com) for further information.

9. NO WARRANTY
The SOFTWARE is provided "as is" without warranty of any kind, either expressed or implied, including, but not limited to, the implied warranties of merchantability and fitness for a particular purpose. The entire risk as to the quality and performance of the program is with the LICENSEE.

10. NO LIABILITY
In no event unless required by applicable law or agreed to in writing will any copyright owner be liable to LICENSEE for damages, including any general, special, incidental or consequential damages arising out of the use or inability to use the program (including but not limited to loss of data or data being rendered inaccurate or losses sustained by LICENSEE or third parties or a failure of the program to operate with other programs), even if such holder has been advised of the possibility of such damages.
*/


#ifndef __LOM2M_IPE_H
#define __LOM2M_IPE_H


#include "configuration.h"
#include "Entity.h"
#include "Enum.h"
#include "Observer.h"

extern String DATA_UP;
extern String DATA_DOWN;
extern String MSG_CNT;

class IPEUtil
{
public:
    static Application* createAELocal(const String& name);
    static Container* createCntLocal(Entity* parent, const String& name, int mni = MAX_CIN_PER_CNT);
    static ContentInstance* createCinLocal(Container* parent, const String& name, const String& contentFormat, const String& content);
};

struct Sensor
{
protected:
    String id;
    Container* sensorCnt;
    Container* dataUpCnt;
    Container* messagesCnt;

public:
    Sensor(const String& name);
    Sensor(Entity* parent, const String& name);
    virtual ~Sensor();

    String valueToContent(const String& value);
    void updateValue(const String& newValue);
};

class Actuator : public Sensor, public Observer
{
protected:
    Container* powerOn;
    Container* powerOff;
    bool m_on;

public:
    Actuator(Entity* parent, const String& name, bool initialOn = false) : Sensor(parent, name), m_on(initialOn)
    {
        Container* op = IPEUtil::createCntLocal(this->sensorCnt, "operations");
        op->m_acps = parent->m_acps;
        this->powerOn = IPEUtil::createCntLocal(op, "powerON");
        this->powerOn->register_observer(*this);
        this->powerOn->m_acps = parent->m_acps;
        this->powerOn->m_maxNumberOfInstances = 1;
        this->powerOff = IPEUtil::createCntLocal(op, "powerOFF");
        this->powerOff->register_observer(*this);
        this->powerOff->m_maxNumberOfInstances = 1;
        this->powerOff->m_acps = parent->m_acps;
    };
    virtual ~Actuator() { };
    virtual bool powerSet(bool on)
    {
        m_on = on;
        return false;
    }
    void powerToggle()
    {
        if (m_on && powerSet(false))
            m_on = false;
        else if (!m_on && powerSet(true))
            m_on = true;
    }
    bool powerState()
    {
        return m_on;
    }
    virtual void notify(const String& id) override
    {
#if DEBUG
        printf("DEBUG: received notification: %s\n", id.c_str());
#endif
        if (id.equalsIgnoreCase(this->powerOn->m_name))
        {
            this->powerSet(true);
        }
        else if (id.equalsIgnoreCase(this->powerOff->m_name))
        {
            this->powerSet(false);
        }
    }
};

class BinaryActuator: public Actuator
{
    // use "on" member as state

protected:
    int m_gpio;

    virtual bool powerSet(bool on) override
    {
        digitalWrite(m_gpio, !on);
        m_on = on;
        String boolVal = String(on);
        this->updateValue(valueToContent(boolVal));
        return true;
    }

public:

    BinaryActuator(Entity* parent, int gpio, const String& name, bool initialOn = false): Actuator(parent, name), m_gpio(gpio)
    {
        pinMode(gpio, OUTPUT);
        BinaryActuator::powerSet(initialOn);
    }
    virtual ~BinaryActuator()
    {
        pinMode(m_gpio, INPUT);
    }

    void on()
    {
        powerSet(true);
    }
    void off()
    {
        powerSet(false);
    }
    void set(bool on)
    {
        powerSet(on);
    }
    void toggle()
    {
        powerSet(!m_on);
    }
    bool get()
    {
        return powerState();
    }
};

/// XXX FIXME an interface for sensor / actuator is needed
// (like Java interface, possible with C++ at least as a doc ref for
//  templates or as a less efficient abstract class)

class BinaryDigitalSensor: public Sensor
{
    // use "on" member as state

protected:
    int m_gpio;

public:

    // mode can be INPUT, INPUT_PULLUP, INPUT_PULLDOWN with constraints on gpio (Arduino API)
    BinaryDigitalSensor(Entity* parent, int gpio, const String& name, int mode = INPUT): Sensor(parent, name), m_gpio(gpio)
    {
        pinMode(gpio, mode);
    }

    virtual ~BinaryDigitalSensor()
    {
        // leave gpio floating
        pinMode(m_gpio, INPUT);
    }

    int getGPIO() const
    {
        return m_gpio;
    }

    float get()
    {
        return digitalRead(m_gpio);
    }
    String toString()
    {
        return String(get());
    }
    void publish(const String& val)
    {
        updateValue(valueToContent(val));
    }
    void publish()
    {
        publish(toString());
    }
};
class AnalogSensor: public Sensor
{
protected:
    int m_pin = 0;
    String newValue = "0";
    int a;
    float temperature;
    int B = 3975; //B value of the thermistor
    float resistance;

public:

    // mode can be INPUT, INPUT_PULLUP, INPUT_PULLDOWN with constraints on gpio (Arduino API)
    AnalogSensor(Entity* parent, const String& name, int pin = A0, int mode = INPUT): Sensor(parent, name), m_pin(pin)
    {
        pinMode(m_pin, mode);
    }

    virtual ~AnalogSensor()
    {
        // leave gpio floating
        pinMode(m_pin, INPUT);
    }

    float get()
    {
        a = analogRead(m_pin);
        //get the resistance of the sensor;
        resistance=(float)(1023-a)*10000/a; 
        //convert to temperature via datasheet&nbsp
        temperature=1/(log(resistance/10000)/B+1/298.15)-273.15;
        return temperature;
    }
    String toString()
    {
        return String(get());
    }
    void publish(const String& val)
    {
        updateValue(valueToContent(val));
    }
    void publish()
    {
        newValue = toString();
        Serial.printf("TRACE: UPDATE ANALOG: %s\n", newValue.c_str());
        publish(newValue);
    }
};

class IPE
{
protected:
    String IPEId;
    Application* ipeAE;
    std::list<Sensor*> devices;

public:
    IPE(const String& name);
    ~IPE();

    Application* getIpeAe()
    {
        return this->ipeAE;
    }

    Sensor* addSensor(String& name)
    {
        Sensor* result = new Sensor(this->ipeAE, name);
        devices.push_back(result);
        return result;
    }

    void addSensor(Sensor* sensor)
    {
        devices.push_back(sensor);
    }

    void addActuator(Actuator* actuator)
    {
        devices.push_back(static_cast<Sensor*>(actuator));
    }

    Actuator* addActuator(String& name)
    {
        Actuator* result = new Actuator(this->ipeAE, name);
        devices.push_back(result);
        return result;
    }
};

#endif
