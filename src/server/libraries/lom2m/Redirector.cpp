/*
------------------------------------------------------------

SOFTWARE EVALUATION LICENSE

LAAS research laboratory, Toulouse, France.
IRIT computer science research laboratory, Toulouse, France.
------------------------------------------------------------

Contact: Thierry Monteil, thierry.monteil@irit.fr

Definitions
SOFTWARE:  The LightOM2M software, version 1.0, in source code form, written by David Gauchard, Guillaume Garzone and Thierry Monteil
at the LAAS research laboratory.

LICENSOR: LE CENTRE NATIONAL DE LA RECHERCHE SCIENTIFIQUE, a public scientific and technical research establishment, having SIREN No. 180 089 013, APE code 7219 Z, having its registered office at 3, rue Michel-Ange, 75796 Paris cedex 16, France, acting in its own name and on its own behalf, and on behalf of Le laboratoire d'analyse et d'architecture des systèmes (LAAS), UPR n°8001.

1. INTENT/PURPOSE
This agreement determines the conditions in which the LICENSOR, who has the rights to the SOFTWARE, grants to the LICENSEE a license for research and evaluation purposes only, excluding any commercial use.

2. LICENSEE
Any person or organization who receives the SOFTWARE with a copy of this license.

3. RIGHTS GRANTED
The rights to use and copy the SOFTWARE, subject to the restrictions described in this agreement.
The rights to modify and compile the SOFTWARE when it's provided in source code form, subject to the restrictions described in this agreement.
For the SOFTWARE provided in binary form only, LICENSEE undertakes to not decompile, disassemble, decrypt, extract the components or perform reverse engineering except to the extent expressly provided by law.

5. SCOPE OF THE LICENSE
- NON-COMMERCIAL license for research and evaluation purposes ONLY.
- NO right to commercialize the SOFTWARE, or any derivative work, without separate agreement with the LICENSOR.

6. MODIFICATION OF THE SOFTWARE
License permits LICENSEE to modify the SOFTWARE provided in source code form for research and evaluation purposes only.

7. REDISTRIBUTION
- License permits LICENSEE to redistribute verbatim copies of the SOFTWARE, accompanied with a copy of this license.
- License DOES NOT permit LICENSEE to redistribute modified versions of the SOFTWARE provided in source code form.
- License DOES NOT permit LICENSEE to commercialize the SOFTWARE or any derivative work of the SOFTWARE.

8. FEE/ROYALTY
- LICENSEE pays no royalty for this license.
- LICENSEE and any third parties must enter a new agreement for any use beyond scope of license. Please contact the IRIT technology transfer office (email numerique@toulouse-tech-transfer.com) for further information.

9. NO WARRANTY
The SOFTWARE is provided "as is" without warranty of any kind, either expressed or implied, including, but not limited to, the implied warranties of merchantability and fitness for a particular purpose. The entire risk as to the quality and performance of the program is with the LICENSEE.

10. NO LIABILITY
In no event unless required by applicable law or agreed to in writing will any copyright owner be liable to LICENSEE for damages, including any general, special, incidental or consequential damages arising out of the use or inability to use the program (including but not limited to loss of data or data being rendered inaccurate or losses sustained by LICENSEE or third parties or a failure of the program to operate with other programs), even if such holder has been advised of the possibility of such damages.
*/

#include "Redirector.h"
#include "httpBinding.h"

void Redirector::retarget(RequestPrimitive* request, ResponsePrimitive* response)
{
    Redirector::initResponse(*request, response); 
    response->setResponseStatusCode(R4004_NOT_FOUND);
    #if DEBUG
    printf("DEBUG: request to: %s\n", request->getTo().c_str());
    #endif
    if (request->getTo() == nullptr || request->getTo().isEmpty())
    {
        response->setResponseStatusCode(R4000_BAD_REQUEST);
        #if DEBUG
        printf("No To/TargetId parameter provided\n");
        #endif
        return ;
    }
    String remoteCseId = "";
    if (request->getTo().startsWith("/~/"))
    {
        remoteCseId = request->getTo().substring(3, request->getTo().length());
    }
    else
    {
        remoteCseId = request->getTo().substring(1, request->getTo().length());
    }
    int index = remoteCseId.indexOf("/");
    if (index < 0)
    {
        index = request->getTo().length() - 1;
    }
    remoteCseId = "/" + remoteCseId.substring(0, index);

    RemoteCse* csr = nullptr;
    #if DEBUG
    printf("DEBUG: looking for CSR / descendant CSIs\n");
    #endif

    bool csrFound = false;
    bool dcseFound = false;
    for (Entity* e : Entity::entities)
    {
        if (e->getRemoteCse())
        {
            csr = e->getRemoteCse();
            #if DEBUG
            printf("DEBUG: examining: %s\n", csr->getCseID().c_str());
            #endif
            if (csr->getCseID().equals(remoteCseId))
            {
                csrFound = true;
                #if INFO
                printf("RemoteCSE found: %s\n", csr->getCseID().c_str());
                #endif
            }
            else
            {
                dcseFound = false;
                for (String dcseId : csr->getDescendantCses())
                {
                    if (dcseId.equals(remoteCseId))
                    {
                        dcseFound = true;
                        break;
                    }
                }
            }
            if (dcseFound || csrFound)
            {
                break;
            }
            else
            {
                csr = nullptr;
            }
        }
    }

    if (csr != nullptr)
    {
        // transfer the request and get the response
        Redirector::sendRequest(request, csr, response);
    }
    else
    {
        // GET REMOTE IN / MN
        #if INFO
        printf("INFO: Unknow CSE, sending request to registrar CSE: %s\n", REMOTE_CSE_ID.c_str());
        #endif
        // TODO to improve
        csr = (RemoteCse*) Entity::getByHierUri("/"+CSE_ID+"/"+CSE_NAME+"/"+REMOTE_CSE_NAME);
        if (csr)
        {
            Redirector::sendRequest(request, csr, response);
        }
        else
        {
            response->setFrom("/"+CSE_ID);
        }
    }

    if (response == nullptr)
    {
        // case nothing found
        Redirector::initResponse(*request, response); 
        response->setFrom("/"+CSE_ID);
        response->setResponseStatusCode(R4004_NOT_FOUND);
        #if VERBOSE
        response->setContent("RemoteCse with cseId " + remoteCseId + " has not been found");
        response->setContentType("text/plain");
        #endif // VERBOSE
    }

    return ;
}

void Redirector::sendRequest(RequestPrimitive* request, RemoteCse* csr, ResponsePrimitive* response)
{

    // test if the remoteCse is reachable
    if (!csr->getRequestReachability())
    {
        response->setResponseStatusCode(R5103_TARGET_NOT_REACHABLE);
        return ;
    }
    // get Point of Access
    String url = "";
    if (csr->getPoas().size() > 0)
    {
        // iterating on points of access while target are not reachable
        for (String poa : csr->getPoas())
        {
            #if INFO
            printf("INFO: REDIRECTOR - sending request to CSR: %s\n", csr->getCseID().c_str());
            Serial.print("      - POA: ");
            Serial.println(poa.c_str());
            #endif
            url = poa;
            if (url.endsWith("/"))
            {
                #if DEBUG
                printf("DEBUG: Removing / at the end of poa: %s\n", url.c_str());
                #endif
                url = url.substring(0, url.length()-1);
            }
            if (url.startsWith("http"))
            {
                if (request->getTo().startsWith("//"))
                {
                    url = url + "/_" + request->getTo().substring(1, url.length());
                    //url = ("/_") + url.substring(1, url.length());
                }
                else if (request->getTo().startsWith("/") && (!request->getTo().startsWith("/~/")))
                {
                    url = "/~" + request->getTo();
                }
                else if(request->getTo().startsWith("/~/"))
                {
                    url += request->getTo();
                }
                else
                {
                    url += "/" + request->getTo();
                }

                request->setTo(url);
                if (poa.startsWith(F("http")))
                {
                    HTTPBinding::sendRequest(request, response);
                }
            }
            
            switch (response->getResponseStatusCode())
            {
            case R5103_TARGET_NOT_REACHABLE:
                break;
            default:
                //response->setFrom("/"+CSE_ID);
                csr->poas.remove(poa);
                csr->poas.push_front(poa);
                return ;
            }
        }
        // if we reach this point, there is no poa working
        Serial.printf("INFO: REDIRECTOR - no poa working, target not reachable\n");
        Redirector::initResponse(*request, response);
        response->setResponseStatusCode(R5103_TARGET_NOT_REACHABLE);
        response->setContent("Target is not reachable");
        response->setContentType("text/plain");
        response->setFrom("/"+CSE_ID);
        return ;
    }
    // NB: to improve w/ polling channel policy when implemented
}
