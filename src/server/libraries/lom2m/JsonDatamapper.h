/*
------------------------------------------------------------

SOFTWARE EVALUATION LICENSE

LAAS research laboratory, Toulouse, France.
IRIT computer science research laboratory, Toulouse, France.
------------------------------------------------------------

Contact: Thierry Monteil, thierry.monteil@irit.fr

Definitions
SOFTWARE:  The LightOM2M software, version 1.0, in source code form, written by David Gauchard, Guillaume Garzone and Thierry Monteil
at the LAAS research laboratory.

LICENSOR: LE CENTRE NATIONAL DE LA RECHERCHE SCIENTIFIQUE, a public scientific and technical research establishment, having SIREN No. 180 089 013, APE code 7219 Z, having its registered office at 3, rue Michel-Ange, 75796 Paris cedex 16, France, acting in its own name and on its own behalf, and on behalf of Le laboratoire d'analyse et d'architecture des systèmes (LAAS), UPR n°8001.

1. INTENT/PURPOSE
This agreement determines the conditions in which the LICENSOR, who has the rights to the SOFTWARE, grants to the LICENSEE a license for research and evaluation purposes only, excluding any commercial use.

2. LICENSEE
Any person or organization who receives the SOFTWARE with a copy of this license.

3. RIGHTS GRANTED
The rights to use and copy the SOFTWARE, subject to the restrictions described in this agreement.
The rights to modify and compile the SOFTWARE when it's provided in source code form, subject to the restrictions described in this agreement.
For the SOFTWARE provided in binary form only, LICENSEE undertakes to not decompile, disassemble, decrypt, extract the components or perform reverse engineering except to the extent expressly provided by law.

5. SCOPE OF THE LICENSE
- NON-COMMERCIAL license for research and evaluation purposes ONLY.
- NO right to commercialize the SOFTWARE, or any derivative work, without separate agreement with the LICENSOR.

6. MODIFICATION OF THE SOFTWARE
License permits LICENSEE to modify the SOFTWARE provided in source code form for research and evaluation purposes only.

7. REDISTRIBUTION
- License permits LICENSEE to redistribute verbatim copies of the SOFTWARE, accompanied with a copy of this license.
- License DOES NOT permit LICENSEE to redistribute modified versions of the SOFTWARE provided in source code form.
- License DOES NOT permit LICENSEE to commercialize the SOFTWARE or any derivative work of the SOFTWARE.

8. FEE/ROYALTY
- LICENSEE pays no royalty for this license.
- LICENSEE and any third parties must enter a new agreement for any use beyond scope of license. Please contact the IRIT technology transfer office (email numerique@toulouse-tech-transfer.com) for further information.

9. NO WARRANTY
The SOFTWARE is provided "as is" without warranty of any kind, either expressed or implied, including, but not limited to, the implied warranties of merchantability and fitness for a particular purpose. The entire risk as to the quality and performance of the program is with the LICENSEE.

10. NO LIABILITY
In no event unless required by applicable law or agreed to in writing will any copyright owner be liable to LICENSEE for damages, including any general, special, incidental or consequential damages arising out of the use or inability to use the program (including but not limited to loss of data or data being rendered inaccurate or losses sustained by LICENSEE or third parties or a failure of the program to operate with other programs), even if such holder has been advised of the possibility of such damages.
*/


#ifndef __JSONDATAMAPPER_H
#define __JSONDATAMAPPER_H

#include "configuration.h"

#include "Entity.h"
#include "RequestPrimitive.h"
#include "ResponsePrimitive.h"
#include "ShortNames.h"
#include "tools.h"
#include "lom2m.h"

/**
 * Provide generic mappers to map attributes from resources in memory (entity)
 * Works on provided json Objects
 */
class Mapper
{
public:
    /**
    * Map all atributes of any resource (will use specific mappers to do so)
    * @param jsonObj root of the json object used to map the resource
    * @param e entity to map to a json object
    * @param level level of depth of mapping (in case of mapping with children resources)
    * @param op Operation used to avoid serializing non permitted attributes (useful for REST creation/update of resource)
    * @param mapAll internal parameter used for full serialization of all entities
    * @return int different from 0 if an error occurs
    */
    static int mapResourceAttributes(JsonObject jsonObj, Entity* e, int level = 0, Operation op = OP_NULL, bool mapAll = false);
    /**
    * Map generic atributes of resources (such as creation time, labels, etc.)
    * @param jsonObj root of the json object used to map the resource
    * @param e entity to map to a json object
    * @param level level of depth of mapping (in case of mapping with children resources)
    * @param op Operation used to avoid serializing non permitted attributes (useful for REST creation/update of resource)
    * @param mapAll internal parameter used for full serialization of all entities
    * @return int different from 0 if an error occurs
    */
    static int mapGenericAttributes(JsonObject jsonObj, Entity* e, int level = 0, Operation op = OP_NULL, bool mapAll = false);
    #if FEAT_RCN_9
    /**
    * Map only modified attributes based on global list MODIFIEDATTRIBUTES
    * @param jsonObject root of the json object used to map the resource
    * @param e the entity to be mapped
    * @param level level of depth of the mapping
    * @return int different from 0 if an error occurs
    */
    static int mapGenericModifiedAttributes(JsonObject jsonObj, Entity* e);
    #endif
    /**
    * Map child resources references (name, type, uri)
    * @param jsonObj root of the json object used to map the resource
    * @param e entity to map to a json object
    * @param level level of depth of mapping (in case of mapping with children resources)
    * @param op Operation used to avoid serializing non permitted attributes (useful for REST creation/update of resource)
    * @param mapAll internal parameter used for full serialization of all entities
    * @return int different from 0 if an error occurs
    */
    static int mapChildResourcesRef(JsonObject jsonObj, Entity* e, int level = 0, Operation op = OP_NULL, bool mapAll = false);
    /**
    * Map child resources (will map attributes of child resources of target entity)
    * Will go recursively if level is > 1
    * @param jsonObj root of the json object used to map the resource
    * @param e entity to map to a json object
    * @param level level of depth of mapping (in case of mapping with children resources)
    * @param op Operation used to avoid serializing non permitted attributes (useful for REST creation/update of resource)
    * @param mapAll internal parameter used for full serialization of all entities
    * @return int different from 0 if an error occurs
    */
    static int mapChildResources(JsonObject jsonObj, Entity* e, int level = 0, Operation op = OP_NULL, bool mapAll = false);

    /**
    * Parse generic attributes of entity
    * Used to load attributes (for persistence only)
    * The loaded attributes are mostly NP attributes at creation and / or update
    * @param jsonObj root of the json object to load in memory
    * @param entity the entity with parameter to be loaded 
    */
    static int parseGenericAttributes(JsonObject jsonObj, Entity* e);
};

/**
 * Provide specific mapping and parsing methods dedicated to ACP resource
 */
class ACPMapper
{
public:
    /**
 * Map specific attributes of ACP resource
 * @param jsonObj root of the json object used to map the resource
 * @param e entity to map to a json object
 * @param level of depth of mapping (in case of mapping with children resources)
 * @param restCreation boolean used to avoid serializing non permitted attributes
 * (useful for REST creation of resource)
 * @param mapAll internal parameter used for full serialization of all entities
 * @return int different from 0 if an error occurs
 */
    static int mapAttributes(JsonObject jsonObj, AccessControlPolicy* acp, int level = 0, Operation op = OP_NULL, bool mapAll = false);
    /**
 * Parse ACP resource (will map json object to an ACP entity)
 * @param jsonObj root of the json object used to map the resource
 * @param name of the entity (to be handled externaly)
 * @return the created entity
 */
    static AccessControlPolicy* parseResource(JsonObject o, const String& name, Entity* entityToFill = nullptr);
};

/**
 * Provide specific mapping and parsing methods dedicated to AE resource
 */
class AEMapper
{
public:
    /**
 * Map specific attributes of AE resource
 * @param jsonObj root of the json object used to map the resource
 * @param ae entity to map to a json object
 * @param level of depth of mapping (in case of mapping with children resources)
 * @param restCreation boolean used to avoid serializing non permitted attributes
 * (useful for REST creation of resource)
 * @param mapAll internal parameter used for full serialization of all entities
 * @return int different from 0 if an error occurs
 */
    static int mapAttributes(JsonObject jsonObj, Application* ae, int level = 0, Operation op = OP_NULL, bool mapAll = false);
    /**
 * Parse AE resource (will map json object to an ACP entity)
 * @param jsonObj root of the json object used to map the resource
 * @param name of the entity (to be handled externaly)
 * @return the created entity
 */
    static Application* parseResource(JsonObject o, const String& name, Entity* toUpdate = nullptr);
};

/**
 * Provide specific mapping and parsing methods dedicated to CNT resource
 */
class ContainerMapper
{
public:
    /**
 * Map specific attributes of CNT resource
 * @param jsonObj root of the json object used to map the resource
 * @param cnt entity to map to a json object
 * @param level of depth of mapping (in case of mapping with children resources)
 * @param restCreation boolean used to avoid serializing non permitted attributes
 * (useful for REST creation of resource)
 * @param mapAll internal parameter used for full serialization of all entities
 * @return int different from 0 if an error occurs
 */
    static int mapAttributes(JsonObject jsonObj, Container* cnt, int level = 0, Operation op = OP_NULL, bool mapAll = false);
    /**
 * Parse CNT resource (will map json object to a CNT entity)
 * @param jsonObj root of the json object used to map the resource
 * @param name of the entity (to be handled externaly)
 * @param targetEntity parent of the resource to create
 * @return the created entity
 */
    static Container* parseResource(JsonObject o, const String& name, Entity* targetEntity, Entity* toUpdate = nullptr);
};

/**
 * Provide specific mapping and parsing methods dedicated to CIN resource
 */
class ContentInstanceMapper
{
public:
    /**
 * Map specific attributes of CIN resource
 * @param jsonObj root of the json object used to map the resource
 * @param cnt entity to map to a json object
 * @param level of depth of mapping (in case of mapping with children resources)
 * @param restCreation boolean used to avoid serializing non permitted attributes
 * (useful for REST creation of resource)
 * @param mapAll internal parameter used for full serialization of all entities
 * @return int different from 0 if an error occurs
 */
    static int mapAttributes(JsonObject jsonObj, ContentInstance* cin, int level = 0, Operation op = OP_NULL, bool mapAll = false);
    /**
 * Parse CIN resource (will map json object to a CIN entity)
 * @param jsonObj root of the json object used to map the resource
 * @param name of the entity (to be handled externaly)
 * @param targetEntity parent of the resource to create
 * @return the created entity
 */
    static ContentInstance* parseResource(JsonObject o, const String& name, Entity* targetEntity);
};

/**
 * Provide specific mapping method dedicated to CSE BASE resource
 */
class CseBaseMapper
{
public:
    /**
 * Map specific attributes of CIN resource
 * @param jsonObj root of the json object used to map the resource
 * @param csb entity to map to a json object
 * @return int different from 0 if an error occurs
 */
    static int mapAttributes(JsonObject jsonObj, CseBase* csb);
};

#if GROUP_FEAT
/**
 * Provide specific mapping and parsing methods dedicated to GROUP resource
 */
class GroupMapper
{
public:
    /**
 * Map specific attributes of GROUP resource
 * @param jsonObj root of the json object used to map the resource
 * @param entity to map to a json object
 * @param level of depth of mapping (in case of mapping with children resources)
 * @param restCreation boolean used to avoid serializing non permitted attributes
 * (useful for REST creation of resource)
 * @param mapAll internal parameter used for full serialization of all entities
 * @return int different from 0 if an error occurs
 */
    static int mapAttributes(JsonObject jsonObj, Group* group, int level = 0, Operation op = OP_NULL, bool mapAll = false);
    /**
 * Parse resource (will map json object to a GROUP entity)
 * @param jsonObj root of the json object used to map the resource
 * @param name of the entity (to be handled externaly)
 * @param targetEntity parent of the resource to create
 * @return the created entity
 */
    static Group* parseResource(JsonObject o, const String& name, Entity* ae);
};
#endif // GROUP_FEAT

/**
 * Provide specific mapping and parsing methods dedicated to CSR resource
 */
class RemoteCseMapper
{
public:
    /**
 * Map specific attributes of CSR resource
 * @param jsonObj root of the json object used to map the resource
 * @param csr entity to map to a json object
 * @param level of depth of mapping (in case of mapping with children resources)
 * @param restCreation boolean used to avoid serializing non permitted attributes
 * (useful for REST creation of resource)
 * @param mapAll internal parameter used for full serialization of all entities
 * @return int different from 0 if an error occurs
 */
    static int mapAttributes(JsonObject jsonObj, RemoteCse* csr, int level = 0, Operation op = OP_NULL, bool mapAll = false);
    /**
 * Parse CSR resource (will map json object to a CSR entity)
 * @param jsonObj root of the json object used to map the resource
 * @param name of the entity (to be handled externaly)
 * @param resp response primitive used to return response status code
 * @param targetEntity parent of the resource to create
 * @return the created entity
 */
    static RemoteCse* parseResource(JsonObject o, const String& name, Entity* targetEntity, Entity* toUpdate = nullptr);
};

#if SUBSCRIPTION_FEAT
/**
 * Provide specific mapping and parsing methods dedicated to SUB resource
 */
class SubscriptionMapper
{
public:
   /**
    * Map specific attributes of SUB resource
    * @param jsonObj root of the json object used to map the resource
    * @param sub entity to map to a json object
    * @param level of depth of mapping (in case of mapping with children resources)
    * @param op Operation used to avoid serializing non permitted attributes (useful for REST creation/update of resource)
    * @param mapAll internal parameter used for full serialization of all entities
    * @return int different from 0 if an error occurs
    */
   static int mapAttributes(JsonObject jsonObj, Subscription* sub, int level = 0, Operation op = OP_NULL, bool mapAll = false);
   /**
    * Parse SUB resource (will map json object to a SUB entity)
    * @param jsonObj root of the json object used to map the resource
    * @param name of the entity (to be handled externaly)
    * @param resp response primitive used to return response status code
    * @param targetEntity parent of the resource to create
    * @return the created entity
    */
   static Subscription* parseResource(JsonObject o, const String& name, Entity* targetEntity, Entity* toUpdate = nullptr);
};
#endif // SUBSCRIPTION_FEAT

/**
 * Provide specifi mapping and parsing methods dedicated to request and response primitives
 * Useful for protocol binding such as MQTT
 */
class PrimitiveMapper
{
public:
    /**
     * Map Request primitive to JsonObject
     * @param req the request to map
     * @param o json object (ArduinoJson) to fill
     * @return int different from 0 in case of error
     */
    static int mapRequestPrimitive(RequestPrimitive& req, JsonObject o);
    /**
     * Parse json to fill a request primitive object
     * @param o json object (ArduinoJson)
     * @param req the request primitive to fill
     * @return int different from 0 in case of error
     */
    static int parseRequestPrimitive(JsonObject o, RequestPrimitive* req);
    /**
     * Map Response primitive to JsonObject
     * @param resp the response to map
     * @param o json object (ArduinoJson) to fill
     * @return int different from 0 in case of error
     */
    static int mapResponsePrimitive(ResponsePrimitive& resp, JsonObject o);
    /**
     * Parse json to fill a response primitive object
     * @param o json object (ArduinoJson)
     * @param resp the response primitive to fill
     * @return int different from 0 in case of error
     */
    static int parseResponsePrimitive(JsonObject o, ResponsePrimitive* resp);
};

#endif // __JSONDATAMAPPER_H
