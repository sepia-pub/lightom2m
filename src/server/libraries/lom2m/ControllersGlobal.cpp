/*
------------------------------------------------------------

SOFTWARE EVALUATION LICENSE

LAAS research laboratory, Toulouse, France.
IRIT computer science research laboratory, Toulouse, France.
------------------------------------------------------------

Contact: Thierry Monteil, thierry.monteil@irit.fr

Definitions
SOFTWARE:  The LightOM2M software, version 1.0, in source code form, written by David Gauchard, Guillaume Garzone and Thierry Monteil
at the LAAS research laboratory.

LICENSOR: LE CENTRE NATIONAL DE LA RECHERCHE SCIENTIFIQUE, a public scientific and technical research establishment, having SIREN No. 180 089 013, APE code 7219 Z, having its registered office at 3, rue Michel-Ange, 75796 Paris cedex 16, France, acting in its own name and on its own behalf, and on behalf of Le laboratoire d'analyse et d'architecture des systèmes (LAAS), UPR n°8001.

1. INTENT/PURPOSE
This agreement determines the conditions in which the LICENSOR, who has the rights to the SOFTWARE, grants to the LICENSEE a license for research and evaluation purposes only, excluding any commercial use.

2. LICENSEE
Any person or organization who receives the SOFTWARE with a copy of this license.

3. RIGHTS GRANTED
The rights to use and copy the SOFTWARE, subject to the restrictions described in this agreement.
The rights to modify and compile the SOFTWARE when it's provided in source code form, subject to the restrictions described in this agreement.
For the SOFTWARE provided in binary form only, LICENSEE undertakes to not decompile, disassemble, decrypt, extract the components or perform reverse engineering except to the extent expressly provided by law.

5. SCOPE OF THE LICENSE
- NON-COMMERCIAL license for research and evaluation purposes ONLY.
- NO right to commercialize the SOFTWARE, or any derivative work, without separate agreement with the LICENSOR.

6. MODIFICATION OF THE SOFTWARE
License permits LICENSEE to modify the SOFTWARE provided in source code form for research and evaluation purposes only.

7. REDISTRIBUTION
- License permits LICENSEE to redistribute verbatim copies of the SOFTWARE, accompanied with a copy of this license.
- License DOES NOT permit LICENSEE to redistribute modified versions of the SOFTWARE provided in source code form.
- License DOES NOT permit LICENSEE to commercialize the SOFTWARE or any derivative work of the SOFTWARE.

8. FEE/ROYALTY
- LICENSEE pays no royalty for this license.
- LICENSEE and any third parties must enter a new agreement for any use beyond scope of license. Please contact the IRIT technology transfer office (email numerique@toulouse-tech-transfer.com) for further information.

9. NO WARRANTY
The SOFTWARE is provided "as is" without warranty of any kind, either expressed or implied, including, but not limited to, the implied warranties of merchantability and fitness for a particular purpose. The entire risk as to the quality and performance of the program is with the LICENSEE.

10. NO LIABILITY
In no event unless required by applicable law or agreed to in writing will any copyright owner be liable to LICENSEE for damages, including any general, special, incidental or consequential damages arising out of the use or inability to use the program (including but not limited to loss of data or data being rendered inaccurate or losses sustained by LICENSEE or third parties or a failure of the program to operate with other programs), even if such holder has been advised of the possibility of such damages.
*/


#include "Controller.h"
#include "JsonDatamapper.h"
#include "ShortNames.h"

Entity*
AbstractController::createResource(RequestPrimitive& request, ResponsePrimitive* resp, Entity* targetEntity, JsonObject o)
{
    int ty = request.getResourceType();
    const String& name = o[RESOURCE_NAME];

    Entity* ret = nullptr;

    // checking generic NP parameters
    if (o.containsKey(RESOURCE_TYPE)
    || o.containsKey(RESOURCE_ID)
    || o.containsKey(PARENT_ID)
    || o.containsKey(CREATION_TIME)
    || o.containsKey(LAST_MODIFIED_TIME)
    )
    {
        resp->setResponseStatusCode(R4000_BAD_REQUEST);
#if VERBOSE
        resp->setContent(F("presence of a NP parameter"));
        resp->setContentType(F("plain/text"));
#endif
        return nullptr;
    }

    // checking resource specific parameters
    switch (ty)
    {
    case TY1_ACP:
    {
        // checking NP and M parameters
        // self Privileges not mandatry to have any rule, but shall be present
        if (!o.containsKey(SELF_PRIVILEGES) || !((o[SELF_PRIVILEGES]).containsKey(ACR)) || !(o.containsKey(PRIVILEGES)))
        {
            resp->setResponseStatusCode(R4000_BAD_REQUEST);
#if VERBOSE
            resp->setContent(F("acr is empty or malformed, or PVS or PV is missing"));
            resp->setContentType(F("plain/text"));
#endif
            return nullptr;
        }
        JsonObject t = o[SELF_PRIVILEGES];
        if (((JsonArray)t[ACR]).size() <= 0)
        {
            resp->setResponseStatusCode(R4000_BAD_REQUEST);
#if VERBOSE
            resp->setContent(F("ACR is empty or malformed, in PVS"));
            resp->setContentType(F("plain/text"));
#endif
            return nullptr;
        }
        else
        {
            for (JsonObject acr : (JsonArray)t[ACR])
            {
                if (!acr.containsKey(ACOR) || (((JsonArray)acr[ACOR]).size() <= 0))
                {
                    resp->setResponseStatusCode(R4000_BAD_REQUEST);
#if VERBOSE
                    resp->setContent(F("acor is empty or malformed, in PVS"));
                    resp->setContentType(F("plain/text"));
#endif
                    return nullptr;
                }
            }
        }

        // all is fine, parsing resource
        ret = ACPMapper::parseResource(o, name);
        break;
    }
    case TY2_APPL_ENTITY:
    {
        if (o.containsKey(AE_ID))
        {
            resp->setResponseStatusCode(R4000_BAD_REQUEST);
#if VERBOSE
            resp->setContent(F("AE ID is NP"));
            resp->setContentType(F("plain/text"));
#endif
            return nullptr;
        }
        if (!o.containsKey(REQUEST_REACHABILITY))
        {
            resp->setResponseStatusCode(R4000_BAD_REQUEST);
#if VERBOSE
            resp->setContent(F("rr is M"));
            resp->setContentType(F("plain/text"));
#endif
            return nullptr;
        }
        else
        {
            if (((const String&)o[REQUEST_REACHABILITY]).equalsIgnoreCase(F("true")) && !o.containsKey(POA))
            {
                resp->setResponseStatusCode(R4000_BAD_REQUEST);
#if VERBOSE
                resp->setContent(F("poa is M if rr true"));
                resp->setContentType(F("plain/text"));
#endif
                return nullptr;
            }
        }
        if (!o.containsKey(APP_ID))
        {
            resp->setResponseStatusCode(R4000_BAD_REQUEST);
#if VERBOSE
            resp->setContent(F("api is M"));
            resp->setContentType(F("plain/text"));
#endif
            return nullptr;
        }

        if (!o.containsKey(SUPPORTED_REL_VERSIONS))
        {
            resp->setResponseStatusCode(R4000_BAD_REQUEST);
#if VERBOSE
            resp->setContent(F("supported rel versions is M"));
            resp->setContentType(F("plain/text"));
#endif
            return nullptr;
        }

        ret = AEMapper::parseResource(o, name);
        break;
    }
    case TY3_CONTAINER:
    {
        if (o.containsKey(STATETAG)
        || o.containsKey(CURRENT_NUMBER_OF_INSTANCES)
        || o.containsKey(CURRENT_BYTE_SIZE)
        || o.containsKey(LOCATION_ID)
        )
        {
            resp->setResponseStatusCode(R4000_BAD_REQUEST);
            return nullptr;
        }
        if (!targetEntity->getApplication() && !targetEntity->getCseBase() && !targetEntity->getContainer())
        {
            resp->setResponseStatusCode(R4000_BAD_REQUEST);
#if VERBOSE
            resp->setContent(F("parent of CNT should be CSE, AE, CNT"));
            resp->setContentType(F("plain/text"));
#endif
            return nullptr;
        }
        if ((o.containsKey(MAX_NR_OF_INSTANCES) && ((int)o[MAX_NR_OF_INSTANCES] < 0)) ||
        (o.containsKey(MAX_BYTE_SIZE) && ((int)o[MAX_BYTE_SIZE] < 0)))
        {
            resp->setResponseStatusCode(R4000_BAD_REQUEST);
#if VERBOSE
            resp->setContent(F("mni must be positive int"));
            resp->setContentType(F("plain/text"));
#endif
            return nullptr;
        }
        ret = ContainerMapper::parseResource(o, name, targetEntity);
        if (o.containsKey(CREATOR))
        {
            Serial.printf("CONTAINS CREATOR");
            if (o[CREATOR] == nullptr)
            {
                ret->getContainer()->setCreator(request.getFrom());
            }
        }
        break;
    }
    case TY4_CONTENT_INSTANCE:
    {
        if (!targetEntity->getContainer()
        || !o.containsKey(CONTENT)
        || o.containsKey(CONTENT_SIZE))
        {
            resp->setResponseStatusCode(R4000_BAD_REQUEST);
#if VERBOSE
            resp->setContent(F("Parent of CIN must be CNT, CON is M, CNS is NP"));
            resp->setContentType(F("plain/text"));
#endif
            return nullptr;
        }
        
        if(targetEntity->getContainer()->getMaxByteSize() != -1)
        {
            if (targetEntity->getContainer()->getMaxByteSize() < ((const String&)o[CONTENT]).length())
            {
                resp->setResponseStatusCode(R5207_NOT_ACCEPTABLE);
#if VERBOSE
            resp->setContent(F("new byte size would be too big\n"));
            resp->setContentType(F("plain/text"));
#endif
            return nullptr;
            }
        }
        ret = ContentInstanceMapper::parseResource(o, name, targetEntity);
        break;
    }
    #if GROUP_FEAT
    case TY9_GROUP:
    {
        if (!targetEntity->getApplication() && !targetEntity->getCseBase() && !targetEntity->getRemoteCse())
        {
            resp->setResponseStatusCode(R4000_BAD_REQUEST);
        }
        else
        {
            ret = GroupMapper::parseResource(o, name, targetEntity);
        }
        break;
    }
    #endif
    case TY16_REMOTE_CSE:
    {
        if (!targetEntity->getCseBase() || !o.containsKey(REMOTE_CSE_CSEBASE) || !o.containsKey(REQUEST_REACHABILITY) || !o.containsKey(SN_CSE_ID) || !o.containsKey(SUPPORTED_REL_VERSIONS))
        {
            resp->setResponseStatusCode(R4000_BAD_REQUEST);
            #if VERBOSE
            resp->setContent("Missing one mandatory attribute");
            #endif
            return nullptr;
        }
        if (((const String&)o[REQUEST_REACHABILITY]).equalsIgnoreCase("true") && !o.containsKey(POA))
        {
            resp->setResponseStatusCode(R4000_BAD_REQUEST);
            #if VERBOSE
            resp->setContent("Missing POA");
            #endif
            return nullptr;
        }
        ret = RemoteCseMapper::parseResource(o, name, targetEntity);
        descendantCsesToUpdate = true;
        break;
    }
    #if SUBSCRIPTION_FEAT
    case TY23_SUBSCRIPTION:
    {
        if (!o.containsKey(NOTIFICATION_URI))
        {
            resp->setResponseStatusCode(R4000_BAD_REQUEST);
            return nullptr;
        }
        Subscription* sub = SubscriptionMapper::parseResource(o, name, targetEntity);
        if (!sub)
        {
            break;
        }
#if SKIP_VRQ
        ret = sub;
        break;
#endif
        // SENDING VERIFICATION REQUEST
        ResponseStatusCode rsc = SubscriptionController::performVerificationRequest(request, *sub);
        if (rsc != R2000_OK)
        {
            Entity::deleteEntity(sub);
            resp->setResponseStatusCode(rsc);
#if VERBOSE
            resp->setContent(String(rqType(ty)) + F("Error during VRQ"));
            resp->setContentType("plain/text");
#endif
            return nullptr;
        }
        else
        {
            ret = sub;
            break;
        }
    }
    #endif // SUBSCRIPTION_FEAT
    default:
        resp->setResponseStatusCode(R5001_NOT_IMPLEMENTED);
        #if VERBOSE
        resp->setContentType("plain/text");
        resp->setContent(String(rqType(ty)) + F(" not managed in this implementation"));
        #endif //VERBOSE
        return nullptr;
    }
    if (!ret)
    {
        return nullptr;
    }
    
    Mapper::parseGenericAttributes(o, ret);

    ret->m_parent = targetEntity;
    if (targetEntity->getContainer())
    {
        targetEntity->getContainer()->incrementStateTag();
    }
    targetEntity->addChild(ret);

#if DEBUG
    for (const auto& i : Entity::entities)
        if (i->getType() == TY2_APPL_ENTITY)
        {
            i->printTo(printHumanSerial, 0);
        }
#endif

    resp->setResponseStatusCode(R2001_CREATED);
    return ret;
}


ResponseStatusCode AbstractController::checkValidityRequestPri(const RequestPrimitive& req)
{
    if (req.getOperationEnum() == OP_NULL || !req.getTo() || req.getTo().isEmpty())
    {
        return R4000_BAD_REQUEST;
    }

    #if !DISABLE_RI_CHECK
    if (!req.getRequestId() || req.getRequestId().isEmpty())
    {
        return R4000_BAD_REQUEST;
    }    
    #endif

    if (req.getOperationEnum() == OP_CREATE && req.getResourceType() == -1)
    {
        return R4000_BAD_REQUEST;
    }
    if (req.getOperationEnum() != OP_CREATE && req.getResourceType() != -1)
    {
        return R4000_BAD_REQUEST;
    }
    if (req.getOperationEnum() == OP_RETRIEVE 
    && (req.getResultContentType() == RCN_NOTHING
    || req.getResultContentType() == RCN_HIER_ADDR
    || req.getResultContentType() == RCN_HIER_ADDR_ATTR
    || req.getResultContentType() == RCN_MODIF_ATTR)
    )
    {
        return R4000_BAD_REQUEST;
    }
    if (req.getOperationEnum() == OP_UPDATE 
    && (req.getResultContentType() == RCN_HIER_ADDR
    || req.getResultContentType() == RCN_HIER_ADDR_ATTR)
    )
    {
        return R4000_BAD_REQUEST;
    }

    // TO REMOVE WHEN RCN_MODIF_ATTR IS IMPLEMENTED
    #if !FEAT_RCN_9
    if (req.getResultContentType() == RCN_MODIF_ATTR)
    {
        return R5001_NOT_IMPLEMENTED;
    }
    #endif

    return R2000_OK;
}

Entity*
AbstractController::findEntityFromUri(String uri, UriType uriType)
{
    Entity* result = nullptr;
    switch (uriType)
    {
    case URI_CSE_RELATIVE:
        if (uri.startsWith("/" + CSE_NAME) && !uri.startsWith("/" + CSE_ID))
        { // STRUCTURED / HIERARCHICAL CASE
            uri = "/" + CSE_ID + uri;
            result = Entity::getByHierUri(uri);
        }
        else
        { // UNSTRUCTURED / non HIERARCHICAL CASE
            // if (!uri.startsWith("/"))
            // {
            //     uri = S_slash + uri;
            // }
            result = Entity::getByIdentifier(uri);
        }
        break;
    case URI_SP_RELATIVE:
    default:
        if (uri.startsWith("/" + CSE_ID + "/" + CSE_NAME))
        { // STRUCTURED / HIERARCHICAL CASE
            result = Entity::getByHierUri(uri);
        }
        else if (uri.startsWith("/" + CSE_ID))
        {
            result = Entity::getByIdentifier(uri);
        }
        break;
    case URI_ABSOLUTE:
        if (uri.startsWith("/" + M2M_SP_ID))
        {
            uri = uri.substring(M2M_SP_ID.length() + 1, uri.length());
        }
        if (uri.startsWith("/" + CSE_ID + "/" + CSE_NAME))
        { // HIERARCHICAL
            result = Entity::getByHierUri(uri);
        }
        else if (uri.startsWith("/" + CSE_ID))
        { // NON HIERARCHICAL
            result = Entity::getByIdentifier(uri);
        }
        break;
    }
    return result;
}
