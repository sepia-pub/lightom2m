/*
------------------------------------------------------------

SOFTWARE EVALUATION LICENSE

LAAS research laboratory, Toulouse, France.
IRIT computer science research laboratory, Toulouse, France.
------------------------------------------------------------

Contact: Thierry Monteil, thierry.monteil@irit.fr

Definitions
SOFTWARE:  The LightOM2M software, version 1.0, in source code form, written by David Gauchard, Guillaume Garzone and Thierry Monteil
at the LAAS research laboratory.

LICENSOR: LE CENTRE NATIONAL DE LA RECHERCHE SCIENTIFIQUE, a public scientific and technical research establishment, having SIREN No. 180 089 013, APE code 7219 Z, having its registered office at 3, rue Michel-Ange, 75796 Paris cedex 16, France, acting in its own name and on its own behalf, and on behalf of Le laboratoire d'analyse et d'architecture des systèmes (LAAS), UPR n°8001.

1. INTENT/PURPOSE
This agreement determines the conditions in which the LICENSOR, who has the rights to the SOFTWARE, grants to the LICENSEE a license for research and evaluation purposes only, excluding any commercial use.

2. LICENSEE
Any person or organization who receives the SOFTWARE with a copy of this license.

3. RIGHTS GRANTED
The rights to use and copy the SOFTWARE, subject to the restrictions described in this agreement.
The rights to modify and compile the SOFTWARE when it's provided in source code form, subject to the restrictions described in this agreement.
For the SOFTWARE provided in binary form only, LICENSEE undertakes to not decompile, disassemble, decrypt, extract the components or perform reverse engineering except to the extent expressly provided by law.

5. SCOPE OF THE LICENSE
- NON-COMMERCIAL license for research and evaluation purposes ONLY.
- NO right to commercialize the SOFTWARE, or any derivative work, without separate agreement with the LICENSOR.

6. MODIFICATION OF THE SOFTWARE
License permits LICENSEE to modify the SOFTWARE provided in source code form for research and evaluation purposes only.

7. REDISTRIBUTION
- License permits LICENSEE to redistribute verbatim copies of the SOFTWARE, accompanied with a copy of this license.
- License DOES NOT permit LICENSEE to redistribute modified versions of the SOFTWARE provided in source code form.
- License DOES NOT permit LICENSEE to commercialize the SOFTWARE or any derivative work of the SOFTWARE.

8. FEE/ROYALTY
- LICENSEE pays no royalty for this license.
- LICENSEE and any third parties must enter a new agreement for any use beyond scope of license. Please contact the IRIT technology transfer office (email numerique@toulouse-tech-transfer.com) for further information.

9. NO WARRANTY
The SOFTWARE is provided "as is" without warranty of any kind, either expressed or implied, including, but not limited to, the implied warranties of merchantability and fitness for a particular purpose. The entire risk as to the quality and performance of the program is with the LICENSEE.

10. NO LIABILITY
In no event unless required by applicable law or agreed to in writing will any copyright owner be liable to LICENSEE for damages, including any general, special, incidental or consequential damages arising out of the use or inability to use the program (including but not limited to loss of data or data being rendered inaccurate or losses sustained by LICENSEE or third parties or a failure of the program to operate with other programs), even if such holder has been advised of the possibility of such damages.
*/


#ifndef __LOM2M_RESPONSEPRIMITIVE_H
#define __LOM2M_RESPONSEPRIMITIVE_H

#include "configuration.h"

#include <WString.h>

enum ResponseStatusCode
{
    R000_VOID = 0,
    R2000_OK = 2000,
    R204_NO_CONTENT = 204,
    R2002_DELETED = 2002,
    R2004_UPDATED = 2004,
    R2001_CREATED = 2001,
    R4000_BAD_REQUEST = 4000,
    R4103_ORIGINATOR_HAS_NO_PRIVILEGE = 4103,
    R4004_NOT_FOUND = 4004,
    R4005_OPERATION_NOT_ALLOWED = 4005,
    R4008_REQUEST_TIMEOUT = 4008,
    R4105_CONFLICT = 4105,
    R5103_TARGET_NOT_REACHABLE = 5103,
    R5106_ALREADY_EXISTS = 5106,
    R4015_UNSUPPORTED_MEDIA_TYPE = 4015,
    R5000_INTERNAL_SERVER_ERROR = 5000,
    R5001_NOT_IMPLEMENTED = 5001,
    R5206_NON_BLOCKING_SYNCH_REQUEST_NOT_SUPPORTED = 5206,
    R5207_NOT_ACCEPTABLE = 5207,
};

enum ContentStatus
{
    CS_NULL = -1,
    CS_STATIONARY = 1,
    CS_MOBILE = 2
};

class ResponsePrimitive
{
protected:
    String m_protocol;
    ResponseStatusCode m_rsc;
    String m_ri;
    String m_content;
    String m_to;
    String m_from;
    String m_originatingTimestamp;
    String m_resultExpirationTimestamp;
    // event category
    ContentStatus m_contentStatus;
    int m_contentOffset;

    // TODO assigned token identifiers
    // TODO token request information
    // TODO authorization signature request information
    int m_authorizationSignatureRequestInformation;
    // TODO release version indicator
    String m_vendorInformation;



    String m_location;
    String m_contentType;
    String m_resourceId;


    // rel. >= 2 parameters
    String m_rvi = "3";

    // TODO Missing implem of optional parameters

public:
    static ResponseStatusCode getRSCFromString(String rsc);

    /**
     * Constructor
     */
    ResponsePrimitive()
    {
        this->init();
    }

    ResponsePrimitive(const String& ri, const String& from, const String& to, ResponseStatusCode rsc)
    {
        this->init();
        m_from = from;
        m_to = to;
        m_ri = ri;
        m_rsc = rsc;
    }
    String getProtocol() const
    {
        return this->m_protocol;
    }
    void setProtocol(const String& m_protocol)
    {
        this->m_protocol = m_protocol;
    }
    ContentStatus getContentStatus()
    {
        return this->m_contentStatus;
    }
    void setContentStatus(ContentStatus contentStatus)
    {
        this->m_contentStatus = contentStatus;
    }
    const String& getVendorInformation() {
    	return this->m_vendorInformation;
    }
    void setVendorInformation(const String& vendorInformation) {
    	this->m_vendorInformation = vendorInformation;
    }
    int getAuthorizationSignatureRequestInformationInt()
    {
        return this->m_authorizationSignatureRequestInformation;
    }
    bool getAuthorizationSignatureRequestInformation() {
    	return (this->m_authorizationSignatureRequestInformation == 1);
    }
    void setAuthorizationSignatureRequestInformation(bool authorizationSignatureRequestInformation) {
        if (authorizationSignatureRequestInformation)
        {
            this->m_authorizationSignatureRequestInformation = 1;
        }
        else
        {
            this->m_authorizationSignatureRequestInformation = 0;
        }
    }
    int getContentOffset()
    {
        return this->m_contentOffset;
    }
    void setContentOffset(int contentOffset)
    {
        this->m_contentOffset = contentOffset;
    }
    const String& getResultExpirationTimestamp()
    {
        return this->m_resultExpirationTimestamp;
    }
    void setResultExpirationTimestamp(const String& resultExpirationTimestamp)
    {
        this->m_resultExpirationTimestamp = resultExpirationTimestamp;
    }

    const String& getOriginatingTimetsamp()
    {
        return this->m_originatingTimestamp;
    }

    void setOriginatingTimestamp(const String& originatingTimestamp)
    {
        this->m_originatingTimestamp = originatingTimestamp;
    }
    String getLocation() const
    {
        return this->m_location;
    }
    void setLocation(const String& location)
    {
        this->m_location = location;
    }
    String getContent() const
    {
        return this->m_content;
    }
    void setContent(const String& content)
    {
        this->m_content = content;
    }
    String getContentType() const
    {
        return this->m_contentType;
    }
    void setContentType(const String& contentType)
    {
        this->m_contentType = contentType;
    }
    void init();

    void setTo(const String& to)
    {
        m_to = to;
    }

    String getTo() const
    {
        return m_to;
    }

    void setFrom(const String& from)
    {
        m_from = from;
    }

    String getFrom() const
    {
        return m_from;
    }

    void setRequestId(const String& ri)
    {
        m_ri = ri;
    }

    String getRequestId() const
    {
        return m_ri;
    }
    void setResourceId(const String& ri)
    {
        m_resourceId = ri;
    }

    String getResourceId() const
    {
        return m_resourceId;
    }

    void setResponseStatusCode(ResponseStatusCode rsc)
    {
        m_rsc = rsc;
    }
    void setResponseStatusCodeString(const String& rsc)
    {
        m_rsc = ResponsePrimitive::getRSCFromString(rsc);
    }

    ResponseStatusCode getResponseStatusCode() const
    {
        return m_rsc;
    }

    String getReleaseVersionIndicator() const
    {
        return m_rvi;
    }
    void setReleaseVersionIndicator(const String& rvi)
    {
        m_rvi = rvi;
    }

};

#endif // __LOM2M_RESPONSEPRIMITIVE_H
